//
//  SearchForTagViewController.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/29/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "SearchForTagViewController.h"
#import "SDImageCache.h"
#import "Flurry.h"

@interface SearchForTagViewController ()

@end

@implementation SearchForTagViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];

    [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    search_bar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    search_bar.showsCancelButton = YES;
    search_bar.placeholder = @"Search for a clothing item";
    [search_bar becomeFirstResponder];
    search_bar.delegate = self;
    search_bar.barTintColor = [self colorWithHexString:@"25292b"];
    [self.view addSubview: search_bar];   
    
    search_results = [[NSMutableArray alloc] init];
    
    results_table = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, 320, 300) style:UITableViewStylePlain];
    results_table.backgroundColor = [UIColor clearColor];
    results_table.dataSource = self;
    results_table.delegate = self;
    results_table.separatorStyle = UITableViewCellSeparatorStyleNone;
    results_table.scrollEnabled = YES;
    results_table.hidden = NO;
    results_table.rowHeight = 100;
    results_table.showsVerticalScrollIndicator = NO;
    [self.view addSubview:results_table];
}
-(void) didTapOnTableView:(UIGestureRecognizer*) recognizer
{
    CGPoint tapLocation = [recognizer locationInView:results_table];
    NSIndexPath *indexPath = [results_table indexPathForRowAtPoint:tapLocation];
    if (indexPath)
    {   //we are in a tableview cell, let the gesture be handled by the view
        recognizer.cancelsTouchesInView = NO;
        [search_bar resignFirstResponder];
    }
    else
    { // anywhere else, do what is needed for your case
        //[self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseSearch" object:nil];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    PSSProductQuery *productQuery = [[PSSProductQuery alloc] init];
    productQuery.searchTerm = searchText;
    [[PSSClient sharedClient] searchProductsWithQuery:productQuery offset:nil limit:nil success:^(NSUInteger totalCount, NSArray *availableHistogramTypes, NSArray *products)
    {
        search_results = (NSMutableArray*)products;
        [results_table reloadData];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
         NSLog(@"Request failed with error: %@", error);
    }];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    PSSProduct *thisProduct = search_results[(NSUInteger)indexPath.row];
    cell.textLabel.text = thisProduct.name;
    cell.textLabel.font = [UIFont fontWithName:@"Neutraface Text" size:17];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Neutraface Text" size:14];
    cell.textLabel.textColor = [UIColor lightGrayColor];
    cell.textLabel.numberOfLines = 0;
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
    //cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if (thisProduct.salePriceLabel)
    {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Now %@! (was %@)", thisProduct.salePriceLabel, thisProduct.regularPriceLabel];
    }
    else
    {
        cell.detailTextLabel.text = thisProduct.regularPriceLabel;
    }
    
    if (thisProduct.image != nil)
    {
        [cell.imageView setImageWithURL:[thisProduct.image imageURLWithSize:PSSProductImageSizeIPhoneSmall] placeholderImage:nil];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"Placeholder"];
    }
    
    return cell;

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return search_results.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     PSSProduct *thisProduct = search_results[(NSUInteger)indexPath.row];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"TagChosen" object:thisProduct];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
@end

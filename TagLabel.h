//
//  TagLabel.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/1/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POPSUGARShopSense.h"

@interface TagLabel : UILabel
@property PSSProduct * product;
@end

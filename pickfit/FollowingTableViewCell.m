//
//  FollowingTableViewCell.m
//  pickfit
//
//  Created by treasure on 3/20/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "FollowingTableViewCell.h"

@implementation FollowingTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.userNameLabel.font = [UIFont fontWithName:fontPrimaryName size:20];
    
    [self.followButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.followButton.backgroundColor = [UIColor whiteColor];
    self.followButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
    
    return self;
}

-(void)setFollow:(BOOL)follow
{
}

- (void)followButtonClick
{
    if (self.followingCellDelegate && [self.followingCellDelegate respondsToSelector:@selector(didButtonUnfollowPress:)])
    {
        [self.followingCellDelegate didButtonUnfollowPress:self];
        
        self.follow = !self.follow;
    }
}

@end

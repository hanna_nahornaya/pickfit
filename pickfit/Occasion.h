//
//  Occassion.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/8/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Occasion : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSNumber * occasion_id;
@end

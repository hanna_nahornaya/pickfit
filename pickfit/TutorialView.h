//
//  TutorialView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 11/6/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MaryPopin.h"

@interface TutorialView : UIViewController
{
    
    UILabel * top_label;
    UIImageView * background;
    
    UIImageView * the_frame_for_photo_a;
    UIImageView * the_frame_for_photo_b;

    UIButton * photo_stack_a;
    UIButton * photo_stack_b;
    
    UIView * check_mark_layer_a;
    UIView * check_mark_layer_b;
    
    UILabel * this_outfit_label_a;
    UILabel * this_outfit_label_b;
    
    UIImageView * this_outfit_check_a;
    UIImageView * this_outfit_check_b;
    
    UIImageView * prompt;
    
    UIImageView * dots_moving;
    UIImageView * dots_moving2;
    
    NSMutableArray * dots_array;
    
    UIImageView * arrows;
    NSMutableArray * arrows_array;  
    
}
@end

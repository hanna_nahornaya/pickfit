//
//  User.h
//  pickfit
//
//  Created by ann on 3/19/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (readwrite) NSInteger userId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) UIImage *image;
@property (readwrite) NSInteger followersCount;
@property (readwrite) NSInteger followingsCount;

@property (nonatomic, strong) NSMutableDictionary *attributesDictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (id)initWithUser:(User *)user;

@end


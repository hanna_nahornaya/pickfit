//
//  MyOutfitsViewController.m
//  pickfit
//
//  Created by ann on 3/17/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "MyOutfitsViewController.h"
#import "SlideNavigationController.h"
#import "Post.h"
#import "Outfit.h"
#import "OutfitDetailsView.h"

@interface MyOutfitsViewController () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    UIButton *addOutfitButton;
    Outfit *to_share;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tableArray;

@end

@implementation MyOutfitsViewController

static NSString * const mainFont = @"Avenir";

static CGFloat const kTableHeaderViewHeight = 52;

static CGFloat const kAddOutfitButtonHeight = 40;
static CGFloat const kAddOutfitButtonWidth = 255;
static CGFloat const kAddOutfitButtonFontSize = 16;

static CGFloat const kTableCellHeight = 229;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"MyOutfitsViewController:viewDidLoad");
    
    self.tableArray = [NSMutableArray new];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = kTableCellHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];

    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, kTableHeaderViewHeight)];
    tableHeaderView.backgroundColor = [UIColor clearColor];

    addOutfitButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kAddOutfitButtonWidth, kAddOutfitButtonHeight)];
    [addOutfitButton addTarget:self action:@selector(onAddOutfitButton) forControlEvents:UIControlEventTouchUpInside];
    addOutfitButton.center = CGPointMake(tableHeaderView.frame.size.width / 2, tableHeaderView.frame.size.height / 2);
    [addOutfitButton setTitle:NSLocalizedString(@"+    Add Outfit", @"") forState:UIControlStateNormal];
    [addOutfitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addOutfitButton.titleLabel.font = [UIFont fontWithName:mainFont size:kAddOutfitButtonFontSize];
    addOutfitButton.layer.cornerRadius = 3;
    addOutfitButton.layer.masksToBounds = YES;
    addOutfitButton.backgroundColor = colorPrimaryGreen;
    [tableHeaderView addSubview:addOutfitButton];

    self.tableView.tableHeaderView = tableHeaderView;
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self api_call:@"get_user_posts"];
}

- (void)addRefreshControl
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)refreshTable:(id)sender
{
    [(UIRefreshControl *)sender endRefreshing];
    [self api_call:@"get_user_posts"];
}

- (void)onAddOutfitButton
{
    [[SlideNavigationController sharedInstance] present_outfit_creator];
}

-(void)api_call:(NSString*)to_call
{
    if ([to_call isEqualToString:@"get_user_posts"])
    {
        [self.tableArray removeAllObjects];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserPosts/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"MyOutfitsVC:/user/getUserPosts/: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (response_array.count > 0)
                     {
                         for (NSDictionary * a_post in response_array)
                         {
                             Post * to_create = [[Post alloc] init];
                             to_create.photo_a_url = [a_post objectForKey:@"outfit_a__image"];
                             to_create.photo_b_url = [a_post objectForKey:@"outfit_b__image"];
                             to_create.photo_a_votes = [a_post objectForKey:@"outfit_a_votes"];
                             to_create.photo_b_votes = [a_post objectForKey:@"outfit_b_votes"];
                             to_create.outfit_a_id = [a_post objectForKey:@"outfit_a__id"];
                             to_create.outfit_b_id = [a_post objectForKey:@"outfit_b__id"];
                             to_create.outfit_a_flagged = [a_post objectForKey:@"outfit_a__flagged"];
                             to_create.outfit_b_flagged = [a_post objectForKey:@"outfit_b__flagged"];
                             to_create.total_votes = [a_post objectForKey:@"total_votes"];
                             to_create.post_id = [a_post objectForKey:@"id"];
                             to_create.occasion = [a_post objectForKey:@"occasion__label"];
                             to_create.userName = [a_post objectForKey:@"username"];
                             to_create.user_id = [[a_post objectForKey:@"user_id"] integerValue];
                             Outfit * an_outfit = [[Outfit alloc] initWithData:to_create];
                             [an_outfit load];
                             [self.tableArray addObject:an_outfit];
                         }
                         
                         [self.tableView reloadData];
                     }
                     else
                     {
                         //NSLog(@"MyOutfitsViewController: there are no more posts to show");
                         [self.tableView reloadData];
                     }
                 });
             }
         }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"MyOutfitsViewController:cellForRowAtIndexPath:tableArray.count = %ld", self.tableArray.count);
    //NSLog(@"MyOutfitsViewController:cellForRowAtIndexPath:row = %ld", indexPath.row);
    
    if (indexPath.row >= self.tableArray.count)
    {
        return nil;
    }
    
    Outfit *cell = [self.tableArray objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    //if (!outfits_table.decelerating)
    //{
    [cell load_images];
    //}
    return cell;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //    NSArray *visibleCells = [outfits_table visibleCells];
    //    for (UITableViewCell * cell in visibleCells)
    //    {
    //        Outfit * to_load = [posts objectAtIndex:[outfits_table indexPathForCell:cell].row];
    //        [to_load load_images];
    //    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"MyOutfitsViewController:tableArray.count = %ld", self.tableArray.count);
    return self.tableArray.count;
}
- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!aTableView.isDecelerating)
    {
        OutfitDetailsView * to_present = [[OutfitDetailsView alloc] init];
        Outfit * the =[self.tableArray objectAtIndex:indexPath.row];
        to_present.current_post = the.this_post;

        [[SlideNavigationController sharedInstance] presentViewController:to_present animated:YES completion:^{
            
            /*Outfit * the =[self.tableArray objectAtIndex:indexPath.row];
            to_present.current_post = the.this_post;
            [to_present load];*/
            
        }];
        [aTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [self colorWithHexString:@"9ad426"]  title:@"Share"];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor redColor]   title:@"Delete"];
    
    return rightUtilityButtons;
}
#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
        {
            NSLog(@"More button was pressed");
            
            
            to_share = (Outfit*)cell;
            
            //[self share_pressed];
            
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            
            NSLog(@"Delete button was pressed");
            
            Outfit * pressed = (Outfit*)cell;
            
            NSIndexPath *indexPath = [self.tableView indexPathForCell:pressed];
            
            NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/post/deletePost/", SERVICE_URL]];
            
            NSData *postData= [[NSString stringWithFormat:@"post_id=%@", pressed.this_post.post_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if(error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                         alert.tag = 1;
                         //[alert show];
                         
                     });
                 }
                 else if (!data)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSLog(@"MyOutfitsVC:/post/deletePost/: response data is nil");
                     });
                 }
                 else
                 {
                     
                     //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                     //NSLog(@"data back from /post/delete/ = %@" ,test);
                     
                     NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSString * status = [response_dict objectForKey:@"status"];
                         if ([status isEqualToString:@"success"])
                         {
                             [self.tableArray removeObject:pressed];
                             
                             [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                             [self.tableView reloadData];
                             
                         }
                         
                     });
                 }
                 
             }];
            
            
            break;
        }
        default:
            break;
    }
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

-(void)share_pressed
{
    UIViewController * popin = [[UIViewController alloc] init];
    popin.view.backgroundColor = [self colorWithHexString:@"9ad325"];
    [popin setPreferedPopinContentSize:CGSizeMake(280.0, 170.0)];

    UILabel * share_prompt = [[UILabel alloc] initWithFrame:CGRectMake(-20, 10, popin.view.frame.size.width, 22)];
    share_prompt.text = @"Share this with your friends:";
    share_prompt.font =[UIFont fontWithName:@"Neutraface Text" size:20];
    share_prompt.textColor = [UIColor whiteColor];
    share_prompt.textAlignment = NSTextAlignmentCenter;
    [popin.view addSubview:share_prompt];

    UIButton * text_button  = [[UIButton alloc] initWithFrame:CGRectMake(20, 50, 134/2.1, 76/2.1)];
    [text_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [text_button setTitle:@"TEXT" forState:UIControlStateNormal];
    [[text_button layer] setBorderWidth:1.0f];
    [[text_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    text_button.layer.cornerRadius = 2;
    text_button.clipsToBounds = YES;
    [text_button addTarget:self action:@selector(open_text_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: text_button];

    UIButton * email_button  = [[UIButton alloc] initWithFrame:CGRectMake(88, 50, 143/2.1, 76/2.1)];
    [email_button setTitle:@"EMAIL" forState:UIControlStateNormal];
    [[email_button layer] setBorderWidth:1.0f];
    [[email_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    email_button.layer.cornerRadius = 2;
    email_button.clipsToBounds = YES;
    [email_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [email_button addTarget:self action:@selector(open_email_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: email_button];

    UIButton * facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(160, 50, 200/2.0, 76/2.1)];
    [facebook_button setTitle:@"FACEBOOK" forState:UIControlStateNormal];
    [[facebook_button layer] setBorderWidth:1.0f];
    [[facebook_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    facebook_button.layer.cornerRadius = 2;
    facebook_button.clipsToBounds = YES;
    [facebook_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [facebook_button addTarget:self action:@selector(share_facebook) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: facebook_button];

    UIButton * pintrest_button  = [[UIButton alloc] initWithFrame:CGRectMake(35, 100, 200/2.0, 76/2.1)];
    [pintrest_button setTitle:@"PINTREST" forState:UIControlStateNormal];
    [[pintrest_button layer] setBorderWidth:1.0f];
    [[pintrest_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    pintrest_button.layer.cornerRadius = 2;
    pintrest_button.clipsToBounds = YES;
    [pintrest_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [pintrest_button addTarget:self action:@selector(share_pintrest) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: pintrest_button];

    UIButton * twitter_button  = [[UIButton alloc] initWithFrame:CGRectMake(145, 100, 200/2.0, 76/2.1)];
    [twitter_button setTitle:@"TWITTER" forState:UIControlStateNormal];
    [[twitter_button layer] setBorderWidth:1.0f];
    [[twitter_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    twitter_button.layer.cornerRadius = 2;
    twitter_button.clipsToBounds = YES;
    [twitter_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [twitter_button addTarget:self action:@selector(share_twitter) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: twitter_button];

    [popin setPopinTransitionStyle:2];

    BKTBlurParameters *blurParameters = [BKTBlurParameters new];
    blurParameters.alpha = 1.0f;
    blurParameters.radius = 8.0f;
    blurParameters.saturationDeltaFactor = 1.8f;
    blurParameters.tintColor = [UIColor colorWithRed:0.966 green:0.851 blue:0.038 alpha:0.2];
    [popin setBlurParameters:blurParameters];

    //Add option for a blurry background
    [popin setPopinOptions:[popin popinOptions] |BKTPopinBlurryDimmingView];


    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];

//    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^{
//
//        [[SlideNavigationController sharedInstance] presentPopinController:popin animated:YES completion:^{
//            //NSLog(@"Popin presented !");
//        }];
//
//    }];
    
    [self presentPopinController:popin animated:YES completion:^{
        NSLog(@"Popin presented !");
    }];
}
-(void)share_pintrest
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    UIImageView * photo_a_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 261/2, 720/2)];
    photo_a_image.image = to_share.outfit_a.image;

    UIImageView * photo_b_image = [[UIImageView alloc] initWithFrame:CGRectMake(261/2, 0, 261/2, 720/2)];
    photo_b_image.image = to_share.outfit_b.image;

    /*UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];

    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    NSData * image_data = UIImageJPEGRepresentation(image, .5);*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];

    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];

    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);

                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];

             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"MyOutfitsVC:share_pintrest:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);

             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

             //NSLog(@"upload = %@", response_dict);

             dispatch_async(dispatch_get_main_queue(), ^{

                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];

                 if ([status isEqualToString:@"success"])
                 {
                     //[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES];
                     Pinterest *  pinterest = [[Pinterest alloc] initWithClientId:@"1440608" urlSchemeSuffix:@"prod"];
                     [pinterest createPinWithImageURL:[NSURL URLWithString:[response_dict objectForKey:@"link"]]
                                            sourceURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", to_share.this_post.post_id]]
                                          description:@"Which outfit do you prefer? Click on the photo to vote."];

                 }
                 else if([status isEqualToString:@"fail"])
                 {


                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];

                     alert.tag = 1;
                     [alert show];

                 }

             });
         }

     }];

}
-(void)share_twitter
{
    
    //[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES];
    
    UIImageView * photo_a_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 261/2, 720/2)];
    photo_a_image.image = to_share.outfit_a.image;

    UIImageView * photo_b_image = [[UIImageView alloc] initWithFrame:CGRectMake(261/2, 0, 261/2, 720/2)];
    photo_b_image.image = to_share.outfit_b.image;


    /*UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];

    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);

    NSString * message = [NSString stringWithFormat:@"Which outfit do you prefer? Vote at %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",to_share.this_post.post_id.stringValue]];

    [self TWPostImage:[UIImage imageWithData:image_data] withStatus:message];

}
- (void)TWPostImage:(UIImage *)image withStatus:(NSString *)status
{
    [Flurry logEvent:@"share twitter closet"];
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];

    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];

    //[accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
     {
         ACAccount *ac;
         if(granted) {
             NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
             int i=0;
             for (ACAccount *account in accountsArray ) {
                 i++;
                 //NSLog(@"Account name: %@", account.username);
                 ac=account;
             }
             if (i==0)
             {
                 [self dismissCurrentPopinControllerAnimated:YES];
                 
                 [[[UIAlertView alloc] initWithTitle:@"Wait"
                                             message:@"Please setup Twitter Account Settigns > Twitter > Sign In "
                                            delegate:nil
                                   cancelButtonTitle:@"Thanks!"
                                   otherButtonTitles:nil]
                  show];
                 return ;

             }

             ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];;

             //SLRequestHandler requestHandler;

             SLRequestHandler requestHandler =
             ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseData) {
                         NSInteger statusCode = urlResponse.statusCode;
                         if (statusCode >= 200 && statusCode < 300)
                         {
                             NSDictionary *postResponseData =
                             [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:NSJSONReadingMutableContainers
                                                               error:NULL];
                             NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
                             //AppDelegate * d =[[UIApplication sharedApplication] delegate];
                             //NSString* link = [NSString stringWithFormat:@"Your Message %@", @"testing post"];
                             //[d linkSelected:link PointerToSelf:self];
                             /*[[[UIAlertView alloc] initWithTitle:@"Result"
                              message:[NSString stringWithFormat:@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]]
                              delegate:nil
                              cancelButtonTitle:@"Thanks!"
                              otherButtonTitles:nil]
                              show];*/

                         }
                         else
                         {
                             NSLog(@"[ERROR] Server responded: status code %ld %@", statusCode,
                                   [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
                         }
                     }
                     else {
                         NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
                     }
                 });
             };
             //});
             ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
             ^(BOOL granted, NSError *error) {
                 if (granted) {
                     NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
                     NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                                   @"/1.1/statuses/update_with_media.json"];
                     NSDictionary *params = @{@"status" : status};
                     SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                             requestMethod:SLRequestMethodPOST
                                                                       URL:url
                                                                parameters:params];
                     NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
                     [request addMultipartData:imageData
                                      withName:@"media[]"
                                          type:@"image/jpeg"
                                      filename:@"image.jpg"];
                     [request setAccount:[accounts lastObject]];

                     [request performRequestWithHandler:requestHandler];
                     //});
                 }
                 else {
                     NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                           [error localizedDescription]);
                 }
             };

             [accountStore requestAccessToAccountsWithType:twitterType
                                                   options:NULL
                                                completion:accountStoreHandler];
         }else
         {
             [self dismissCurrentPopinControllerAnimated:YES];
             
             [[[UIAlertView alloc] initWithTitle:@"Wait"
                                         message:@"Please Settigns > Twitter > In bottom Enable DealsHype to post"
                                        delegate:nil
                               cancelButtonTitle:@"Thanks!"
                               otherButtonTitles:nil]
              show];
         }
     }];
}
-(void)share_facebook
{
    NSNumber *post_id = to_share.this_post.post_id;
    
    UIImageView * photo_a_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 261/2, 720/2)];
    photo_a_image.image = to_share.outfit_a.image;

    UIImageView * photo_b_image = [[UIImageView alloc] initWithFrame:CGRectMake(261/2, 0, 261/2, 720/2)];
    photo_b_image.image = to_share.outfit_b.image;

    /*UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];

    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    NSData * image_data = UIImageJPEGRepresentation(image, .5);*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];

    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];

    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);

                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"MyOutfitsVC:share_facebook:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);

             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

             //NSLog(@"upload = %@", response_dict);

             dispatch_async(dispatch_get_main_queue(), ^{

                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];

                 if ([status isEqualToString:@"success"])
                 {
                     [self share_to_facebook:post_id :[response_dict objectForKey:@"link"]];
                 }
                 else if([status isEqualToString:@"fail"])
                 {


                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];

                     alert.tag = 1;
                     [alert show];

                 }

             });
         }

     }];

}
-(void)share_to_facebook:(NSNumber*)post_id :(NSString*)link
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        // permission does not exist
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];

        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)

         {
             if (state == FBSessionStateOpen)
             {
                 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                  {
                      if (error)
                      {
                          NSLog(@"error:%@",error);
                      }
                      else
                      {

                          NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                         @"Help me pick an outfit!", @"name",
                                                         @"Which outfit do you prefer?", @"description",
                                                         [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",post_id], @"link",
                                                         link, @"picture",
                                                         nil];

                          // Make the request
                          [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                       parameters:params
                                                       HTTPMethod:@"POST"
                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                    if (!error)
                                                    {
                                                        // Link posted successfully to Facebook
                                                        NSLog(@"result: %@", result);
                                                        //[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES];
                                                        
                                                        [Flurry logEvent:@"share facebook outfit details"];

                                                    }
                                                    else
                                                    {
                                                        // An error occurred, we need to handle the error
                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                        NSLog(@"%@", error.description);
                                                    }


                                                }];

                      }

                  }];
             }

         } ];


    }
    else
    {
        // permission exists


        // If the Facebook app is installed and we can present the share dialog

        // Present the share dialog
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];

        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)

         {
             if (state == FBSessionStateOpen)
             {

                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @"Help me pick an outfit!", @"name",
                                                @"Which outfit do you prefer?", @"description",
                                                [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",post_id], @"link",
                                                link, @"picture",
                                                nil];

                 // Make the request
                 [FBRequestConnection startWithGraphPath:@"/me/feed"
                                              parameters:params
                                              HTTPMethod:@"POST"
                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          // Link posted successfully to Facebook
                          NSLog(@"result: %@", result);
                          //[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES];
                          [Flurry logEvent:@"share facebook outfit details"];
                      }
                      else
                      {
                          // An error occurred, we need to handle the error
                          // See: https://developers.facebook.com/docs/ios/errors
                          NSLog(@"%@", error.description);
                      }


                  }];

             }

         }];

    }
}
-(void)open_text_message
{
    /*[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES completion:^{

        if([MFMessageComposeViewController canSendText])
        {
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
            controller.body =  [NSString stringWithFormat:@"Help me to choose what to wear tonight, visit %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", to_share.this_post.post_id]];
            controller.messageComposeDelegate = self;
            [[SlideNavigationController sharedInstance] presentViewController:controller animated:YES completion:nil];
        }

    }];*/
    
    [self dismissCurrentPopinControllerAnimated:YES];

    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.body =  [NSString stringWithFormat:@"Help me to choose what to wear tonight, visit %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", to_share.this_post.post_id]];
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        
        [Flurry logEvent:@"share text outfit details"];
    }
}
-(void)open_email_message
{
    //[[SlideNavigationController sharedInstance] dismissCurrentPopinControllerAnimated:YES];
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;

        [mailCont setSubject:@"Help me choose an outfit!"];
        [mailCont setMessageBody: [NSString stringWithFormat:@"Help me to choose what to wear tonight, visit %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", to_share.this_post.post_id]] isHTML:NO];

        //[[SlideNavigationController sharedInstance] presentViewController:mailCont animated:YES completion:nil];
        [self presentViewController:mailCont animated:YES completion:nil];
        
        [Flurry logEvent:@"share email outfit details"];
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{

    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }

}

- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


@end

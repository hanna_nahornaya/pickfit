//
//  ChangePasswordView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 12/8/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface ChangePasswordView : UIViewController<UITextFieldDelegate, UIAlertViewDelegate>
{
    UIImageView * background;
    UILabel * top_label;
    UILabel * info_label;
    UIButton * close_button;
    UITextField * old_password;
    UITextField * new_password;
    UIButton * submit;
}
@end

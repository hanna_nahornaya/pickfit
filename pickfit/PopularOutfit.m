//
//  PopularOutfit.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/7/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "PopularOutfit.h"

@implementation PopularOutfit
- (id) initWithFrame: (CGRect) frame reuseIdentifier : (NSString *) aReuseIdentifier : (NSNumber *) outfit_id_incoming : (NSString *) pic_url
{
    
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self == nil )
        return ( nil );
    
    self.img_url = pic_url;
    self.outfit_id = outfit_id_incoming;
    
    return self;
}
-(void)load
{
    
    self.frame_outfit = [[UIImageView alloc] initWithFrame:CGRectMake(3, 0, 192/2, 566/2)];
   // self.frame_outfit.image = [UIImage imageNamed:@"box_single.png"];
    self.frame_outfit.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.frame_outfit];
    
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    //NSLog(@"pop url - %@", self.img_url);
//    NSRange range = [self.img_url rangeOfString:@".jpg"];
//    NSString *substring = [[self.img_url substringToIndex:range.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//    NSString * thumbnail_url = [NSString stringWithFormat:@"%@_thumb.jpg", substring];
    
    UIImageView * image2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 6, 192/2.2, 588/2.2)];
    [image2 sd_setImageWithURL:[NSURL URLWithString:self.img_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
     {
         self.photo_img = image;
     }];
    image2.tag = 100;
    [self.frame_outfit addSubview:image2];
    
}
@end

//
//  PopularView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "PopularView.h"
#import "Flurry.h"

@implementation PopularView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"f7faf6"];
    
    outfits = [[NSMutableArray alloc] init];
    
    popular_table = [[AQGridView alloc] initWithFrame:self.view.frame];
    popular_table.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    popular_table.backgroundColor = [UIColor clearColor];
	popular_table.delegate = self;
	popular_table.dataSource = self;
    popular_table.hidden = NO;
    popular_table.showsVerticalScrollIndicator = NO;
    [self.view addSubview:popular_table];
}
-(void)api_call:(NSString*)to_call
{
    if([to_call isEqualToString:@"popular"])
    {
        [outfits removeAllObjects];
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/getPopularOutfits/", SERVICE_URL]];
        
        NSData *postData= [@"offset=0&limit=9" dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PopularView:/outfit/getPopularOutfits/: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     for (NSDictionary * to_create in response_array)
                     {
                         PopularOutfit * plainCell = [[PopularOutfit alloc] initWithFrame: CGRectMake(0, 0, 192/2, 566/2) reuseIdentifier: @"FavoriteCellIdentifier" : [to_create objectForKey:@"outfit_id"] : [to_create objectForKey:@"image"]];
                         plainCell.is_flagged = [to_create objectForKey:@"flagged"];                         
                         [plainCell load];
                         [outfits addObject:plainCell];
                     }
                     [popular_table reloadData];
                 });
             }
             
         }];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    if (outfits.count == 0)
    {
        [self api_call:@"popular"];
    }
}
-(void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    [Flurry logEvent:@"outfit pressed from popular section"];
    PopularOutfit * selected = [outfits objectAtIndex:index];
    PhotoDetailsView * to_push = [[PhotoDetailsView alloc] init];
    [[SlideNavigationController sharedInstance] presentViewController:to_push animated:YES completion:^{
        
        to_push.photo.image = selected.photo_img;        
        to_push.outfit_id = selected.outfit_id;
        to_push.url_for_image = selected.img_url;
        //to_push.is_flagged = selected.is_flagged;
        to_push.username_label.text = selected.userName;
        
        [to_push check_favorites];
        [to_push check_following];
        [to_push check_flag];
        [to_push check_tags];
    }];
    
}
-(CGSize) portraitGridCellSizeForGridView: (AQGridView *) gridView
{
    CGSize to_return = CGSizeMake(192/2, 566/2);
    return  to_return;
}
- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    static NSString * PlainCellIdentifier = @"FavoriteCellIdentifier";
    
    PopularOutfit * cell = nil;
    
    cell = (PopularOutfit*)[aGridView dequeueReusableCellWithIdentifier: PlainCellIdentifier];
   
    
    if (cell == nil)
    {
        PopularOutfit * plainCell = [outfits objectAtIndex:index];        
        
        plainCell.selectionGlowColor = [UIColor clearColor];
        
        cell = plainCell;

    }
    else
    {
        cell = [outfits objectAtIndex:index];
        
        cell.selectionGlowColor = [UIColor clearColor];     
    }
    
    return ( cell );
    
}
- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return outfits.count;
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}


@end

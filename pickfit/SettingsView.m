//
//  SettingsView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "SettingsView.h"
#import "ProfileViewController.h"

@interface SettingsView ()

@property (nonatomic, strong) UIButton *profileSettingsButton;

@end

@implementation SettingsView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"SettingsView:viewDidLoad");
    self.view.backgroundColor = [self colorWithHexString:@"f5f5f4"];
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];
   
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-120/2, 30, 120, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Settings";
    top_label.textColor = [UIColor blackColor];
    [self.view addSubview:top_label];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
	[closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
	[closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    menu_button  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 66/2, 53/2)];
	[menu_button setImage:[UIImage imageNamed:@"button_green_bars.png"] forState:UIControlStateNormal];
	[menu_button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: menu_button];
    
    CGFloat borderWidth = 2.0f;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 70.0, self.view.frame.size.width, self.view.frame.size.height - 75.0)];
    [self.view addSubview:scrollView];
    scrollView.backgroundColor = [UIColor clearColor];
    
    show_me = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 100, 20)];
    show_me.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    show_me.textAlignment = NSTextAlignmentLeft;
    show_me.text = @"Show me:";
    show_me.textColor = [UIColor blackColor];
    [scrollView addSubview:show_me];
    
    CGFloat verticalOffset = 15;
    CGFloat buttonHeight = 45;
    
    both_show = [[UIButton alloc] initWithFrame:CGRectMake(25, 20, 90, buttonHeight)];
    both_show.layer.borderWidth = borderWidth;
    both_show.layer.borderColor = [UIColor whiteColor].CGColor;
    [both_show addTarget:self action:@selector(both_show_pressed:) forControlEvents:UIControlEventTouchUpInside];
    [both_show setTitle:[NSString stringWithFormat:@"%@", @"Both"] forState:UIControlStateNormal];
    both_show.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    both_show.titleLabel.textColor = [UIColor blackColor];
    [both_show setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [both_show setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    both_show.titleLabel.textAlignment = NSTextAlignmentLeft;
    [scrollView addSubview:both_show];
    
    both_show_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, both_show.frame.size.width, both_show.frame.size.height)];
    both_show_background.backgroundColor = [UIColor whiteColor];
    both_show_background.alpha = 1.0;
    [both_show addSubview:both_show_background];
    [both_show bringSubviewToFront:both_show.titleLabel];
    
    male_show = [[UIButton alloc] initWithFrame:CGRectMake(115, 20, 90, buttonHeight)];
    male_show.layer.borderWidth = borderWidth;
    male_show.layer.borderColor = [UIColor whiteColor].CGColor;
    [male_show addTarget:self action:@selector(male_show_pressed:) forControlEvents:UIControlEventTouchUpInside];
    [male_show setTitle:[NSString stringWithFormat:@"%@", @"Male"] forState:UIControlStateNormal];
    male_show.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    male_show.titleLabel.textColor = [UIColor blackColor];
    [male_show setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [male_show setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    male_show.titleLabel.textAlignment = NSTextAlignmentLeft;
    [scrollView addSubview:male_show];
    
    male_show_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, male_show.frame.size.width, male_show.frame.size.height)];
    male_show_background.backgroundColor = [UIColor whiteColor];
    male_show_background.alpha = 0.0;
    [male_show addSubview:male_show_background];
    [male_show bringSubviewToFront:male_show.titleLabel];
    
    female_show = [[UIButton alloc] initWithFrame:CGRectMake(205, 20, 90, buttonHeight)];
    female_show.layer.borderWidth = borderWidth;
    female_show.layer.borderColor = [UIColor whiteColor].CGColor;
    [female_show addTarget:self action:@selector(female_show_pressed:) forControlEvents:UIControlEventTouchUpInside];
    [female_show setTitle:[NSString stringWithFormat:@"%@", @"Female"] forState:UIControlStateNormal];
    female_show.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    female_show.titleLabel.textColor = [UIColor blackColor];
    [female_show setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [female_show setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    male_show.titleLabel.textAlignment = NSTextAlignmentLeft;
    [scrollView addSubview:female_show];
    
    female_show_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, female_show.frame.size.width, female_show.frame.size.height)];
    female_show_background.backgroundColor = [UIColor whiteColor];
    female_show_background.alpha = 0.0;
    [female_show addSubview:female_show_background];
    [female_show bringSubviewToFront:female_show.titleLabel];
    
    white_background_profile_settings = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(both_show.frame) + verticalOffset, 320, buttonHeight)];
    white_background_profile_settings.backgroundColor = [UIColor whiteColor];
    white_background_profile_settings.userInteractionEnabled = YES;
    [white_background_profile_settings addTarget:self action:@selector(profile_settings_pressed) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:white_background_profile_settings];

    profile_settings = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 140, 20)];
    profile_settings.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    profile_settings.textAlignment = NSTextAlignmentLeft;
    profile_settings.text = @"Profile Settings";
    profile_settings.textColor = [UIColor blackColor];
    [white_background_profile_settings addSubview:profile_settings];
    
    arrow_profile_settings = [[UIImageView alloc] initWithFrame:CGRectMake(270, 12, 50/2, 61/2)];
    arrow_profile_settings.image = [UIImage imageNamed:@"button_arrow_forward.png"];
    [white_background_profile_settings addSubview:arrow_profile_settings];
    
    white_background_notifications = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(white_background_profile_settings.frame) + verticalOffset, 320, buttonHeight)];
    white_background_notifications.backgroundColor = [UIColor whiteColor];
    white_background_notifications.userInteractionEnabled = YES;
    [scrollView addSubview:white_background_notifications];
    
    notifications = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 120, 20)];
    notifications.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    notifications.textAlignment = NSTextAlignmentLeft;
    notifications.text = @"Notifications";
    notifications.textColor = [UIColor blackColor];
    [white_background_notifications addSubview:notifications];
    
    notifications_switch = [[UISwitch alloc] initWithFrame:CGRectMake(250, 10, 50, 50)];
    [notifications_switch addTarget:self action:@selector(toggle:) forControlEvents:UIControlEventValueChanged];
    notifications_switch.onTintColor = [self colorWithHexString:@"76ac0a"];
    notifications_switch.on = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pickfitEnableNotifications"] boolValue];
    [white_background_notifications addSubview:notifications_switch];
    
    white_background_contact_us = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(white_background_notifications.frame) + verticalOffset, 320, buttonHeight)];
    white_background_contact_us.backgroundColor = [UIColor whiteColor];
    white_background_contact_us.userInteractionEnabled = YES;
    [white_background_contact_us addTarget:self action:@selector(contact_us_pressed) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:white_background_contact_us];
    
    contact_us = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 120, 20)];
    contact_us.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    contact_us.textAlignment = NSTextAlignmentLeft;
    contact_us.text = @"Contact Us";
    contact_us.textColor = [UIColor blackColor];
    [white_background_contact_us addSubview:contact_us];
    
    arrow_contact_us = [[UIImageView alloc] initWithFrame:CGRectMake(270, 12, 50/2, 61/2)];
    arrow_contact_us.image = [UIImage imageNamed:@"button_arrow_forward.png"];
    [white_background_contact_us addSubview:arrow_contact_us];
    
    white_background_information = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(white_background_contact_us.frame) + verticalOffset, 320, buttonHeight)];
    white_background_information.backgroundColor = [UIColor whiteColor];
    white_background_information.userInteractionEnabled = YES;
    [white_background_information addTarget:self action:@selector(information_pressed) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:white_background_information];
    
    information = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 120, 20)];
    information.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    information.textAlignment = NSTextAlignmentLeft;
    information.text = @"Information";
    information.textColor = [UIColor blackColor];
    [white_background_information addSubview:information];
    
    arrow_information = [[UIImageView alloc] initWithFrame:CGRectMake(270, 12, 50/2, 61/2)];
    arrow_information.image = [UIImage imageNamed:@"button_arrow_forward.png"];
    [white_background_information addSubview:arrow_information];
    
    white_background_change_password = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(white_background_information.frame) + verticalOffset, 320, buttonHeight)];
    white_background_change_password.backgroundColor = [UIColor whiteColor];
    white_background_change_password.userInteractionEnabled = YES;
    [white_background_change_password addTarget:self action:@selector(change_password_pressed) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:white_background_change_password];
    
    change_password = [[UILabel alloc] initWithFrame:CGRectMake(30, 15, 200, 20)];
    change_password.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    change_password.textAlignment = NSTextAlignmentLeft;
    change_password.text = @"Change Password";
    change_password.textColor = [UIColor blackColor];
    [white_background_change_password addSubview:change_password];
    
    arrow_change_password = [[UIImageView alloc] initWithFrame:CGRectMake(270, 12, 50/2, 61/2)];
    arrow_change_password.image = [UIImage imageNamed:@"button_arrow_forward.png"];
    [white_background_change_password addSubview:arrow_change_password];
    
    version_label = [[UILabel alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(white_background_change_password.frame) + verticalOffset * 2, 120, 20)];
    version_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    version_label.textAlignment = NSTextAlignmentLeft;
    version_label.text = [self appNameAndVersionNumberDisplayString];
    version_label.textColor = [UIColor blackColor];
    [scrollView addSubview:version_label];
    
    logout = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-264/2, CGRectGetMaxY(version_label.frame) + 5, 264, 35)];

    [logout addTarget:self action:@selector(logout_pressed) forControlEvents:UIControlEventTouchUpInside];
    
    [logout setTitle:@"Log Off" forState:UIControlStateNormal];
    [logout.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
    [logout setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
    logout.layer.cornerRadius = 3; // this value vary as per your desire
    logout.clipsToBounds = YES;
    logout.backgroundColor = [self colorWithHexString:@"c43553"];
    [scrollView addSubview:logout];

    delete_account = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, CGRectGetMaxY(logout.frame) + verticalOffset, 240, 34/2)];
    [delete_account addTarget:self action:@selector(delete_account_pressed) forControlEvents:UIControlEventTouchUpInside];
    [delete_account setTitle:@"Delete Account" forState:UIControlStateNormal];
    delete_account.backgroundColor = [UIColor clearColor];
    delete_account.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    delete_account.titleLabel.textColor = [UIColor blackColor];
    [delete_account setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [delete_account setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [scrollView addSubview:delete_account];
    
    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(delete_account.frame));
    
    [self api_call:@""];
    
    
    
}

-(void)openProfileSettings
{
    
}

-(void)toggle:(id)sender
{
    [self api_notification_call:((UISwitch *)sender).on];
}

-(void)both_show_pressed:(id)sender
{
    if (!show_both)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.7];
        
        [male_show_background setAlpha:0.0];
        [female_show_background setAlpha:0.0];
        [both_show_background setAlpha:1.0];
        
        [UIView commitAnimations];
        show_male = NO;
        show_female = NO;
        show_both = YES;
        
        if (sender)
        {
            [self api_gender_call:@"both"];
        }
    }
    else
    {
        show_male = NO;
    }
    
}
-(void)male_show_pressed:(id)sender
{
    if (!show_male)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.7];
        
        [male_show_background setAlpha:1.0];
        [female_show_background setAlpha:0.0];
        [both_show_background setAlpha:0.0];
        
        [UIView commitAnimations];
        show_male = YES;
        show_female = NO;
        show_both = NO;
        
        if (sender)
        {
            [self api_gender_call:@"male"];
        }
    }
    else
    {
        show_male = NO;
    }
}
-(void)female_show_pressed:(id)sender
{
    if (!show_female)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.7];
        
        [male_show_background setAlpha:0.0];
        [female_show_background setAlpha:1.0];
        [both_show_background setAlpha:0.0];
        
        [UIView commitAnimations];
        show_male = NO;
        show_female = YES;
        show_both = NO;
        
        if (sender)
        {
            [self api_gender_call:@"female"];
        }
    }
    else
    {
        show_male = NO;
    }
}
-(void)profile_settings_pressed
{
    [[SlideNavigationController sharedInstance] presentViewController:[[ProfileViewController alloc] init] animated:YES completion:nil];
}
-(void)contact_us_pressed
{
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        [mailCont setToRecipients:[NSArray arrayWithObject:@"info@pickfitapp.com"]];
        [mailCont setSubject:@"Pickfit Contact"];
        [self presentViewController:mailCont animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"We do not have access to your mail app. To contact us, please email info@pickfitapp.com" delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
    
}
-(void)information_pressed
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:@"https://dl.dropboxusercontent.com/u/79503615/Information%20Pickfit.pdf"];
    
    [self presentViewController:webViewController animated:YES completion:^{
        
    }];
}
-(void)change_password_pressed
{
    ChangePasswordView * to_push = [[ChangePasswordView alloc] init];
    [[SlideNavigationController sharedInstance] presentViewController:to_push animated:YES completion:nil];
}
-(void)logout_pressed
{
    UIAlertView * pop_up = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    pop_up.tag = 100;
    [pop_up show];
    
}
-(void)delete_account_pressed
{
    UIAlertView * pop_up = [[UIAlertView alloc] initWithTitle:@"Delete Account" message:@"Are you sure you want to delete your account?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    pop_up.tag = 101;
    [pop_up show];
    
}
-(void)api_call:(NSString *)to_call
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //make the activity indicator start spinning
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUser/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SettingsView:/user/getUser/: response data is nil");
             });
         }
         else
         {
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"get user = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"gender_preference"];
                 if ([status isEqualToString:@"both"])
                 {
                     [self both_show_pressed:nil];
                 }
                 else if ([status isEqualToString:@"female"])
                 {
                     [self female_show_pressed:nil];
                 }
                 else if ([status isEqualToString:@"male"])
                 {
                     [self male_show_pressed:nil];
                 }
             });
         }
         
     }];
    
}
-(void)api_gender_call:(NSString *)gender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //make the activity indicator start spinning
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/editUser/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%@&username=%@&password=%@&email=%@&get_notified=%@&ios_token=%@&full_name=%@&facebook_token=%@&twitter_token=%@&gender=%@&gender_preference=%@", [defaults objectForKey:@"user_id"], @"", @"", @"", @"", @"", @"", @"", @"", @"", gender] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SettingsView:/user/editUser/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             
             //NSLog(@"data back from /user/edit/ = %@" ,test);
             
             //NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"update user = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"gender_changed" object:nil];
                 
                 
             });
         }
         
     }];
    
}

-(void)api_notification_call:(BOOL)enableNotification
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    //NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/editUser/", SERVICE_URL]];
    
    //NSData *postData= [[NSString stringWithFormat:@"user_id=%@&username=%@&password=%@&email=%@&gender_preference=%@&ios_token=%@&full_name=%@&facebook_token=%@&twitter_token=%@&gender=%@&get_notified=%ld", [defaults objectForKey:@"user_id"], @"", @"", @"", @"", @"", @"", @"", @"", @"", (NSInteger)enableNotification] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/editGetNotified/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%@&get_notified=%ld", [defaults objectForKey:@"user_id"], (NSInteger)enableNotification] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SettingsView:/user/editUser/: response data is nil");
             });
         }
         else
         {
             NSLog(@"data back from /user/edit/ = %@", [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy]);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             if (response_dict)
             {
                 if ([[response_dict objectForKey:@"status"] isEqualToString:@"success"])
                 {
                     [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:enableNotification] forKey:@"pickfitEnableNotifications"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                 }
             }
             
             //NSLog(@"update user = %@", response_dict);
             
             /*dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"gender_changed" object:nil];
                 
                 
             });*/
         }
     }];
}

- (NSString *)appNameAndVersionNumberDisplayString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return [NSString stringWithFormat:@"Version %@", majorVersion];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100)
    {
        //this is the save alertview.
        if (buttonIndex == 1)
        {
            OpeningView * to_push = [[OpeningView alloc] init];
            [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:to_push withCompletion:nil];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults removeObjectForKey:@"username"];
            [defaults removeObjectForKey:@"password"];
            [defaults removeObjectForKey:@"user_id"];
            [defaults removeObjectForKey:@"fb_token"];
            [defaults removeObjectForKey:@"fb_id"];
            [defaults removeObjectForKey:@"email"];
            [defaults removeObjectForKey:@"gender"];
            [defaults synchronize];
            
        }
    }
    else if (alertView.tag == 101)
    {
        //this is the save alertview.
        if (buttonIndex == 1)
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/deleteUser/", SERVICE_URL]];
            
            NSData *postData = [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if(error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     });
                 }
                 else if (!data)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSLog(@"SettingsView:/user/deleteUser/: response data is nil");
                     });
                 }
                 else
                 {
                     NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         
                         NSString * status = [response_dict objectForKey:@"status"];
                         NSString * message = [response_dict objectForKey:@"msg"];
                         
                         
                         if ([status isEqualToString:@"success"])
                         {
                             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                             [defaults removeObjectForKey:@"username"];
                             [defaults removeObjectForKey:@"password"];
                             [defaults removeObjectForKey:@"user_id"];
                             [defaults removeObjectForKey:@"fb_token"];
                             [defaults removeObjectForKey:@"fb_id"];
                             [defaults removeObjectForKey:@"email"];
                             [defaults removeObjectForKey:@"gender"];
                             [defaults synchronize];

                             OpeningView * to_push = [[OpeningView alloc] init];
                             [[SlideNavigationController sharedInstance] popAllAndSwitchToViewController:to_push withCompletion:nil];
                         }
                         else if([status isEqualToString:@"fail"])
                         {
                             
                             UIAlertView *alert = [[UIAlertView alloc]
                                                   initWithTitle:@"Error"
                                                   message:message
                                                   delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
                             
                             alert.tag = 1;
                             [alert show];
                         }
                         
                     });
                 }
                 
             }];
            
           
          
            
            
            
            
        }
    }
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end

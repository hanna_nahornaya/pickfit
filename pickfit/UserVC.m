//
//  UserVC.m
//  pickfit
//
//  Created by D.Gonchenko on 18.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "UserVC.h"
#import "SlideNavigationController.h"

@interface UserVC () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate, UIGestureRecognizerDelegate>
{
    UIButton * closet_button;
    UIButton *backButton;
    UILabel * top_label;
    UIImageView * top_background;
    NSMutableArray * outfits;
    Outfit *to_share;
    
    UITapGestureRecognizer *singleTap;
}

@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *descriptionLabel;

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tableArray;

@property (nonatomic, strong) UIButton *numberOfFollowersButton;
@property (nonatomic, strong) UIButton *numberOfFollowingButton;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic) BOOL follow;

@end

static CGFloat const kTableCellHeight = 229;

@implementation UserVC

static CGFloat const kBackButtonWidth = 50;
static CGFloat const kBackButtonHeight = 30;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableArray = [NSMutableArray new];
    self.follow = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
    [closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
    [closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kBackButtonWidth, kBackButtonHeight)];
    imageView.image = [UIImage imageNamed:@"button_green_arrow_back"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 26, kBackButtonWidth, kBackButtonHeight)];
    [backButton addTarget:self action:@selector(onBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton addSubview:imageView];
    backButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backButton];
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backButton.frame), 30, CGRectGetMinX(closet_button.frame) - CGRectGetMaxX(backButton.frame), 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"";
    top_label.textColor = [UIColor blackColor];
    top_label.backgroundColor = [UIColor clearColor];
    [self.view addSubview:top_label];

    self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 73.0, 73.0)];
    self.avatarImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
    self.avatarImageView.center = CGPointMake(self.view.frame.size.width / 2.0, 40.0 + self.avatarImageView.frame.size.height);
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2.0;
    self.avatarImageView.layer.borderColor = [UIColor colorWithRed:197/255.0 green:197/255.0 blue:197/255.0 alpha:1.0].CGColor;
    self.avatarImageView.layer.borderWidth = 1.5;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
    self.avatarImageView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:self.avatarImageView];
    
    self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.avatarImageView.frame) + 5, self.view.frame.size.width, 45.0)];
    self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
    self.descriptionLabel.textColor = [UIColor darkGrayColor];
    [self.view addSubview:self.descriptionLabel];
    
    UIView *separatorViewFirst = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.descriptionLabel.frame) + 10.0, self.view.frame.size.width, 0.5)];
    separatorViewFirst.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:separatorViewFirst];
    
    //Buttons
    CGFloat buttonsHeight = 35.0;
    CGFloat followButtonsWidth = 70.0;
    CGFloat buttonsCenterY = CGRectGetMaxY(separatorViewFirst.frame) + buttonsHeight / 2.0 + 7.5;
    
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 110.0, buttonsHeight)];
    [self.followButton setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
    [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    self.followButton.titleLabel.numberOfLines = 0;
    self.followButton.layer.borderColor = colorPrimaryGreen.CGColor;
    self.followButton.layer.borderWidth = 2.0;
    self.followButton.layer.cornerRadius = 3.0;
    self.followButton.layer.masksToBounds = YES;
    self.followButton.center = CGPointMake(self.view.frame.size.width - self.followButton.frame.size.width / 2.0 - 20.0, buttonsCenterY);
    [self.followButton addTarget:self action:@selector(followButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.followButton];
    
    self.numberOfFollowersButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, followButtonsWidth, buttonsHeight)];
    self.numberOfFollowersButton.titleLabel.numberOfLines = 0;
    self.numberOfFollowersButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.numberOfFollowersButton setAttributedTitle:[self attributedStringForNumber:@"0" andText:@"Followers"] forState:UIControlStateNormal];
    [self.numberOfFollowersButton addTarget:self action:@selector(showFollowers) forControlEvents:UIControlEventTouchUpInside];
    self.numberOfFollowersButton.center = CGPointMake(15.0 + self.numberOfFollowersButton.frame.size.width / 2.0, buttonsCenterY);
    [self.view addSubview:self.numberOfFollowersButton];
    
    self.numberOfFollowingButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, followButtonsWidth, buttonsHeight)];
    self.numberOfFollowingButton.titleLabel.numberOfLines = 0;
    self.numberOfFollowingButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.numberOfFollowingButton setAttributedTitle:[self attributedStringForNumber:@"0" andText:@"Following"] forState:UIControlStateNormal];
    [self.numberOfFollowingButton addTarget:self action:@selector(showFollowers) forControlEvents:UIControlEventTouchUpInside];
    self.numberOfFollowingButton.center = CGPointMake(CGRectGetMaxX(self.numberOfFollowersButton.frame) + 15.0 + self.numberOfFollowingButton.frame.size.width / 2.0, buttonsCenterY);
    [self.view addSubview:self.numberOfFollowingButton];
    
    self.tableArray = [NSMutableArray new];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.numberOfFollowersButton.frame) + 7.5, self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.numberOfFollowersButton.frame) - 7.5) style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = kTableCellHeight;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    UIView *separatorViewLasr = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.tableView.frame) - 0.5, self.view.frame.size.width, 0.5)];
    separatorViewLasr.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:separatorViewLasr];
    
    
    singleTap = [[UITapGestureRecognizer alloc] init];
    singleTap.delegate = self;
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.tableView addGestureRecognizer:singleTap];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.userID > 0)
    {
        [self api_call:@"get_user_posts"];
        [self api_call:@"get_user_profile"];
        [self isFollowedUserID:self.userID];
    }
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [self colorWithHexString:@"9ad426"]  title:@"Share"];
    [rightUtilityButtons sw_addUtilityButtonWithColor: [UIColor redColor]   title:@"Delete"];
    
    return rightUtilityButtons;
}

-(NSAttributedString*)attributedStringForNumber:(NSString*)number andText:(NSString*)text
{
    NSString *titleString = [NSString stringWithFormat:@"%@\n%@", number, text];
    
    UIFont *boldFont = [UIFont boldSystemFontOfSize:20];
    UIFont *regularFont = [UIFont systemFontOfSize:14];
    
    NSDictionary *attrsBold = [NSDictionary dictionaryWithObjectsAndKeys: boldFont, NSFontAttributeName, nil];
    NSDictionary *subAttrs = [NSDictionary dictionaryWithObjectsAndKeys: regularFont, NSFontAttributeName, nil];
    
    NSMutableAttributedString *resultAttrString = [[NSMutableAttributedString alloc] initWithString:titleString];
    [resultAttrString setAttributes:attrsBold range:NSMakeRange(0, number.length)];
    [resultAttrString setAttributes:subAttrs range:NSMakeRange(number.length + 1, text.length)];
    
    return resultAttrString;
}

-(void)setFollowButtonToFollow:(BOOL)follow
{
    if (follow) {
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.followButton.backgroundColor = colorPrimaryGreen;
        [self.followButton setTitle:@"Following" forState:UIControlStateNormal];
    }else
    {
        [self.followButton setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
        self.followButton.backgroundColor = [UIColor whiteColor];
        [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    }
}

#pragma mark - API Calls
-(void)api_call:(NSString*)to_call
{
    if ([to_call isEqualToString:@"get_user_posts"])
    {
        [self.tableArray removeAllObjects];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserPosts/", SERVICE_URL]];
        
        NSString *postDataString = [NSString stringWithFormat:@"user_id=%ld", self.userID];
        NSData *postData= [postDataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"UserVC:/user/getUserPosts/:: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (response_array.count != 0)
                     {
                         for (NSDictionary * a_post in response_array)
                         {
                             Post * to_create = [[Post alloc] init];
                             to_create.photo_a_url = [a_post objectForKey:@"outfit_a__image"];
                             to_create.photo_b_url = [a_post objectForKey:@"outfit_b__image"];
                             to_create.photo_a_votes = [a_post objectForKey:@"outfit_a_votes"];
                             to_create.photo_b_votes = [a_post objectForKey:@"outfit_b_votes"];
                             to_create.outfit_a_id = [a_post objectForKey:@"outfit_a__id"];
                             to_create.outfit_b_id = [a_post objectForKey:@"outfit_b__id"];
                             to_create.outfit_a_flagged = [a_post objectForKey:@"outfit_a__flagged"];
                             to_create.outfit_b_flagged = [a_post objectForKey:@"outfit_b__flagged"];
                             to_create.total_votes = [a_post objectForKey:@"total_votes"];
                             to_create.post_id = [a_post objectForKey:@"id"];
                             to_create.occasion = [a_post objectForKey:@"occasion__label"];
                             to_create.userName = [a_post objectForKey:@"username"];
                             to_create.user_id = [[a_post objectForKey:@"user_id"] integerValue];
                             Outfit * an_outfit = [[Outfit alloc] initWithData:to_create];
                             [an_outfit load];
                             [self.tableArray addObject:an_outfit];
                         }
                         
                         [self.tableView reloadData];
                     }
                     else
                     {
                         //NSLog(@"UserVC: there are no more posts to show");
                         [self.tableView reloadData];
                     }
                 });
             }
         }];

    }
    else if ([to_call isEqualToString:@"get_user_profile"])
    {
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserProfile/", SERVICE_URL]];
        
        NSString *postDataString = [NSString stringWithFormat:@"user_id=%ld", self.userID];
        NSData *postData= [postDataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"UserVC:/user/getUserProfile/:: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 if (response_array.count > 0) {
                     NSDictionary *userDict = response_array[0];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSString *avatarURL = [[userDict objectForKey:@"image"] isKindOfClass:[NSNull class]] ? @"" : [userDict objectForKey:@"image"];
                         [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatarURL] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
                          {
                              if (error || !image)
                              {
                                  self.avatarImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
                              }
                          }];
                         
                         NSString *userNameString = [userDict objectForKey:@"username"];
                         top_label.text = userNameString;
                         
                         NSString *descriptionString = [userDict objectForKey:@"description"];
                         if (![descriptionString isKindOfClass:[NSNull class]]) {
                             self.descriptionLabel.text = descriptionString;
                         }
                         
                         NSString *countFollowings = [[userDict objectForKey:@"count_followings"] stringValue];
                         NSString *countFollowers = [[userDict objectForKey:@"count_followers"] stringValue];
                         [self.numberOfFollowersButton setAttributedTitle:[self attributedStringForNumber:countFollowers andText:@"Followers"] forState:UIControlStateNormal];
                         [self.numberOfFollowingButton setAttributedTitle:[self attributedStringForNumber:countFollowings andText:@"Following"] forState:UIControlStateNormal];
                     });
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                 });
             }
         }];
    }
}

-(void)isFollowedUserID:(NSInteger)followedID
{
    [self.tableArray removeAllObjects];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/follower/isFollowed/", SERVICE_URL]];
    
    NSString *postDataString = [NSString stringWithFormat:@"user_id=%@&following_user_id=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], self.userID];
    NSData *postData= [postDataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(),^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"UserVC:/follower/isFollowed/: response data is nil");
             });
         }
         else
         {
             NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             if ([[responseDict objectForKey:@"status"] isEqualToString:@"success"]) {
                 __block BOOL result = [[responseDict objectForKey:@"result"] boolValue];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.follow = result;
                     [self setFollowButtonToFollow:result];
                 });
             }
         }
     }];
}

-(void)needFollowing:(BOOL)needFollowing toUserID:(NSInteger)userID
{
    NSString *URL = needFollowing ? @"/follower/createFollower/" : @"/follower/deleteFollower/";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_URL, URL]];
    NSString *reqestDataString = [NSString stringWithFormat:@"user_id=%@&following_user_id=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], (long)userID];
    NSData *requestData = [reqestDataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    __block NSError *error;
    if (!error) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        request.HTTPBody = requestData;
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag = 1;
                    //[alert show];
                    
                });
            }
            else if (!data)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"UserVC:/follower/deleteFollower/: response data is nil");
                });
            }
            else
            {
                NSDictionary * response_Dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if (!error) {
                    if ([[response_Dict objectForKey:@"status"] isEqualToString:@"success"])
                    {
                        [self api_call:@"get_user_profile"];
                    }
                }
            }
        }];
    }
}


#pragma mark - Table view
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Outfit *cell = [self.tableArray objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    //if (!outfits_table.decelerating)
    //{
    [cell load_images];
    //}
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableArray.count;
}
- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [aTableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //NSLog(@"didSelectRowAtIndexPath");
    
    CGPoint singleTapPoint = [singleTap locationInView:aTableView];
    //NSLog(@"tapPoint: x = %lf y = %lf", singleTapPoint.x, singleTapPoint.y);
    
    
    Outfit *cell = [self.tableArray objectAtIndex:indexPath.row];
    
    CGRect outfit_a_rect = [cell.outfit_a convertRect:cell.outfit_a.frame toView:aTableView];
    CGRect outfit_b_rect = [cell.outfit_b convertRect:cell.outfit_b.frame toView:aTableView];
    
    BOOL outfit_a_selected = CGRectContainsPoint(outfit_a_rect, singleTapPoint);
    BOOL outfit_b_selected = CGRectContainsPoint(outfit_b_rect, singleTapPoint);
    
    if (outfit_a_selected || outfit_b_selected)
    {
        PhotoDetailsView * photoDetailsView = [[PhotoDetailsView alloc] init];
        
        [Flurry logEvent:@"outfit pressed from user's profile section"];
        
        [[SlideNavigationController sharedInstance] presentViewController:photoDetailsView animated:YES completion:^{

            Post *post = cell.this_post;
            
            if (outfit_a_selected)
            {
                photoDetailsView.photo.image = post.outfit_a_image;
                photoDetailsView.outfit_id = post.outfit_a_id;
                photoDetailsView.url_for_image = post.photo_a_url;
                //photoDetailsView.is_flagged = post.outfit_a_flagged;
            }
            else if (outfit_b_selected)
            {
                photoDetailsView.photo.image = post.outfit_b_image;
                photoDetailsView.outfit_id = post.outfit_b_id;
                photoDetailsView.url_for_image = post.photo_b_url;
                //photoDetailsView.is_flagged = post.outfit_b_flagged;
            }
            
            photoDetailsView.username_label.text = post.userName;
            
            [photoDetailsView load_photo];
            [photoDetailsView check_favorites];
            [photoDetailsView check_following];
            [photoDetailsView check_flag];
            [photoDetailsView check_tags];
        }];
    }
}

#pragma mark - Buttons actions
-(void)followButtonClick
{
    self.follow = !self.follow;
    [self setFollowButtonToFollow:self.follow];
    [self needFollowing:self.follow toUserID:self.userID];
}

-(void)showFollowers
{
}

-(void)showFollowings
{
}

- (void)onBackButton
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
        {
            //NSLog(@"More button was pressed");
            
            
            to_share = (Outfit*)cell;
            
            //[self share_pressed];
            
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            
            //NSLog(@"Delete button was pressed");
            
            Outfit * pressed = (Outfit*)cell;
            
            NSIndexPath *indexPath = [self.tableView indexPathForCell:pressed];
            
            NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/post/deletePost/", SERVICE_URL]];
            
            NSData *postData= [[NSString stringWithFormat:@"post_id=%@", pressed.this_post.post_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if(error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                         alert.tag = 1;
                         //[alert show];
                         
                     });
                 }
                 else if (!data)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         //NSLog(@"UserVC:/post/deletePost/: response data is nil");
                     });
                 }
                 else
                 {
                     
                     NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                     
                     //NSLog(@"data back from /post/delete/ = %@" ,test);
                     
                     NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         NSString * status = [response_dict objectForKey:@"status"];
                         if ([status isEqualToString:@"success"])
                         {
                             [self.tableArray removeObject:pressed];
                             
                             [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                             [self.tableView reloadData];
                             
                         }
                         
                     });
                 }
                 
             }];
            
            
            break;
        }
        default:
            break;
    }
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    return NO;
    
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

#pragma mark - Colors

- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}


@end

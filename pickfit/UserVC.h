//
//  UserVC.h
//  pickfit
//
//  Created by D.Gonchenko on 18.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserVC : UIViewController

@property (nonatomic) NSInteger userID;

@end

//
//  ClosetView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "Outfit.h"
#import "CreateOutfitView.h"
#import "SlideNavigationController.h"
#import "Post.h"
#import <MessageUI/MessageUI.h>
#import <FacebookSDK/FacebookSDK.h>

@interface ClosetView : UIViewController

-(void)api_call:(NSString*)to_call;

@end

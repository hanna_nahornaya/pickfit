//
//  HomeView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "HomeView.h"
#import "AppDelegate.h"
#import "Flurry.h"

@implementation HomeView

@synthesize posts;
@synthesize sponsored_posts;

static volatile BOOL post_presented = NO;

static const NSInteger postsLimit = 6;
static const NSInteger minPostsLimit = 4;
static const NSInteger sponsorPostsLimit = 1;
static const NSInteger minSponsorPostsLimit = 1;

float firstX;
float firstY;
float originalCenter_a;
float originalCenter_b;

NSInteger attemptsCount = 0;

BOOL sponsored_post_shown = NO;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"HomeView:viewDidLoad");
    
    self.view.backgroundColor = [self colorWithHexString:@"f7faf6"];
    
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    
    post_presented = NO;
    
    total_post_shown = 0;
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];
    
    logo = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-183/4, 17, 183/2, 80/2)];
    logo.image = [UIImage imageNamed:@"text_pickfit_logo.png"];
    [self.view addSubview:logo];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
    [closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
    [closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    menu_button  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 66/2, 53/2)];
    [menu_button setImage:[UIImage imageNamed:@"button_green_bars.png"] forState:UIControlStateNormal];
    [menu_button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: menu_button];
    
    the_frame_for_photo_a = [[UIImageView alloc] initWithFrame:CGRectMake(-605, 4, 150, 460)];
    the_frame_for_photo_b = [[UIImageView alloc] initWithFrame:CGRectMake(605, 4, 150, 460)];
    
    photo_stack_a  = [[UIButton alloc] initWithFrame:CGRectMake(0, 65, 320/2, 933/2)];
    [photo_stack_a setImage:[UIImage imageNamed:@"box_left.png"] forState:UIControlStateNormal];
    [photo_stack_a addTarget:self action:@selector(view_a) forControlEvents:UIControlEventTouchUpInside];
    [photo_stack_a setImage:[photo_stack_a imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
    [self.view addSubview: photo_stack_a];
    [self.view sendSubviewToBack:photo_stack_a];
    
    photo_stack_b  = [[UIButton alloc] initWithFrame:CGRectMake(160, 65, 320/2, 933/2)];
    [photo_stack_b setImage:[UIImage imageNamed:@"box_right.png"] forState:UIControlStateNormal];
    [photo_stack_b setImage:[photo_stack_b imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
    [photo_stack_b addTarget:self action:@selector(view_b) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: photo_stack_b];
    [self.view sendSubviewToBack:photo_stack_b];
    
    check_mark_layer_a = [[UIView alloc] initWithFrame:CGRectMake(5, 4, photo_stack_a.frame.size.width-12, photo_stack_a.frame.size.height-16)];
    check_mark_layer_a.backgroundColor = [self colorWithHexString:@"9ad325"];
    check_mark_layer_a.alpha = 0;
    [photo_stack_a addSubview:check_mark_layer_a];
    
    this_outfit_check_a = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_a.frame.size.width/2 - 35, check_mark_layer_a.frame.size.height*.7, 71, 70)];
    this_outfit_check_a.image = [UIImage imageNamed:@"check_mark.png"];
    [check_mark_layer_a addSubview:this_outfit_check_a];
    
    check_mark_layer_b = [[UIView alloc] initWithFrame:CGRectMake(7, 4, photo_stack_b.frame.size.width-12, photo_stack_b.frame.size.height-16)];
    check_mark_layer_b.backgroundColor = [self colorWithHexString:@"9ad325"];
    check_mark_layer_b.alpha = 0;
    [photo_stack_b addSubview:check_mark_layer_b];
    
    this_outfit_check_b = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_b.frame.size.width/2 - 35, check_mark_layer_b.frame.size.height*.7, 71, 70)];
    this_outfit_check_b.image = [UIImage imageNamed:@"check_mark.png"];
    [check_mark_layer_b addSubview:this_outfit_check_b];
    
    occasion_background = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 460/4, self.view.frame.size.height - 80/2, 460/2, 80/2)];
    occasion_background.image = [UIImage imageNamed:@"field.png"];
    [self.view addSubview:occasion_background];
    
    occasion = [[UILabel alloc] initWithFrame:CGRectMake(occasion_background.frame.size.width/2-100, 200, 200, 20)];
    occasion.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    occasion.textAlignment = NSTextAlignmentCenter;
    occasion.text = @"";
    [occasion_background addSubview:occasion];
    
    occasion_hanger = [[UIImageView alloc] initWithFrame:CGRectMake(25, 15, 37/2, 31/2)];
    occasion_hanger.image = [UIImage imageNamed:@"coat_hanger_small.png"];
    [occasion_background addSubview:occasion_hanger];
    
    id <SlideNavigationContorllerAnimator> revealAnimator;
    revealAnimator = [[SlideNavigationContorllerAnimatorScaleAndFade alloc] initWithMaximumFadeAlpha:.6 fadeColor:[UIColor blackColor] andMinimumScale:.8];
    
    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^
     {
         [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
     }];
    
    self.posts = [[NSMutableArray alloc] init];
    self.sponsored_posts = [[NSMutableArray alloc] init];
    
    CGRect screenBounds_a = photo_stack_a.bounds;
    CGRect screenBounds_b = photo_stack_b.bounds;
    
    activity_a = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
    activity_a.center = CGPointMake(CGRectGetMidX(screenBounds_a), CGRectGetMidY(screenBounds_a));
    [activity_a startAnimating];
    [photo_stack_a addSubview: activity_a];
    
    activity_b = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
    activity_b.center = CGPointMake(CGRectGetMidX(screenBounds_b), CGRectGetMidY(screenBounds_b));
    [activity_b startAnimating];
    [photo_stack_b addSubview: activity_b];
 
    emptyClosetImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    emptyClosetImageView.image = [UIImage imageNamed:@"background_empty_closet.png"];
    emptyClosetImageView.hidden = YES;
    [self.view addSubview:emptyClosetImageView];
    [self.view sendSubviewToBack:emptyClosetImageView];
    
    self.isShowRandomPost = NO;
    
    [self showPropositionForReviewIfNeed];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"HomeView:viewWillAppear:isShowRandomPost = %i", self.isShowRandomPost);
    
    if (self.posts.count == 0)
    {
        [self api_call:@"get_random_posts"];
    }
    else if (self.isShowRandomPost)
    {
        [self showRandomPost];
    }
    
    if (self.sponsored_posts.count == 0)
    {
        [self api_call:@"get_random_sponsored"];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    //[self present_tutorial];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"seen_tutorial"])
    {
        
    }
    else
    {
        [self present_tutorial];
        [defaults setObject:@"seen" forKey:@"seen_tutorial"];
        [defaults synchronize];
    }
}
-(void)present_tutorial
{
    //Add option for a blurry background
    TutorialView * tutorial = [[TutorialView alloc] init];
    [tutorial setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    
    [self.navigationController presentPopinController:tutorial animated:YES completion:^{
        NSLog(@"Popin presented !");
    }];
}

- (void)showPropositionForReviewIfNeed
{
    NSInteger numberOfLaunches = [[[NSUserDefaults standardUserDefaults] objectForKey:@"pickfitNumberOfLaunches"] integerValue];
    
    NSLog(@"showPropositionForReviewIfNeed:numberOfLaunches = %ld", numberOfLaunches);
    
    //[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:0] forKey:@"pickfitNumberOfLaunches"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    if (numberOfLaunches < 4)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:numberOfLaunches + 1] forKey:@"pickfitNumberOfLaunches"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if (numberOfLaunches == 4)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you love Pickfit? If so, please give us a review. Thank you!" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alert.tag = 99;
        [alert show];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:numberOfLaunches + 1] forKey:@"pickfitNumberOfLaunches"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99 && buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/pickfit-pick-your-outfit/id910957691?mt=8"]];
    }
}

- (void)emptyClosetHidden:(BOOL)hidden
{
    //NSLog(@"emptyClosetHidden = %ld", hidden);
    
    top_prompt.hidden = !hidden;
    occasion_background.hidden = !hidden;
    if (!hidden)
    {
        occasion.text = @"";
    }
    choose_a.hidden = !hidden;
    choose_b.hidden = !hidden;
    photo_stack_a.hidden = !hidden;
    photo_stack_b.hidden = !hidden;
    
    emptyClosetImageView.hidden = hidden;
}

- (void)api_call:(NSString *)to_call
{
    if ([to_call isEqualToString:@"get_random_posts"])
    {
        NSLog(@"api_call:get_random_posts");
        //NSLog(@"api_call:post_presented = %i", post_presented);
        
        [self emptyClosetHidden:YES];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableArray * to_exclude = [[NSMutableArray alloc] init];
        
        for (Post * exclude in self.posts)
        {
            //NSLog(@"if ([to_call isEqualToString:@""get_random_posts""]): exclude_id = %@", exclude.post_id);
            [to_exclude addObject:exclude.post_id];
        }
        
        NSData *json = [NSJSONSerialization dataWithJSONObject:to_exclude options:0 error:nil];
        NSString * json_string = [[NSString alloc] initWithData:json
                                                       encoding:NSUTF8StringEncoding];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/post/getRandomPost/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&limit=%ld&exclude=%@", [defaults objectForKey:@"user_id"], postsLimit, json_string] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     if (!self.posts.count && !post_presented)
                     {
                         [self emptyClosetHidden:NO];
                     }
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"HomeView:/post/getRandomPost/: response data is nil");
                     
                     if (!self.posts.count && !post_presented)
                     {
                         [self emptyClosetHidden:NO];
                     }
                 });
             }
             else
             {
                 //NSLog(@"api_call:post_presented = %i", post_presented);
                 
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"/post/getRandomPost/posts count = %ld", (long)self.posts.count);
                 //NSLog(@"/post/getRandomPost/response_array.count = %ld", (long)response_array.count);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                 //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     if (response_array.count > 0)
                     {
                         NSMutableArray * postsToAddArray = [NSMutableArray array];
                         
                         for (NSDictionary * a_post in response_array)
                         {
                             Post * to_create = [[Post alloc] init];
                             to_create.photo_a_url = [a_post objectForKey:@"outfit_a__image"];
                             to_create.photo_b_url = [a_post objectForKey:@"outfit_b__image"];
                             to_create.photo_a_votes = [a_post objectForKey:@"outfit_a__votes"];
                             to_create.photo_b_votes = [a_post objectForKey:@"outfit_b__votes"];
                             to_create.outfit_a_id = [a_post objectForKey:@"outfit_a__id"];
                             to_create.outfit_b_id = [a_post objectForKey:@"outfit_b__id"];
                             to_create.outfit_a_flagged = [a_post objectForKey:@"outfit_a__flagged"];
                             to_create.outfit_b_flagged = [a_post objectForKey:@"outfit_b__flagged"];
                             to_create.total_votes = [a_post objectForKey:@"total_votes"];
                             to_create.post_id = [a_post objectForKey:@"id"];
                             to_create.occasion = [a_post objectForKey:@"occasion__label"];
                             to_create.userName = [a_post objectForKey:@"username"];
                             to_create.user_id = [[a_post objectForKey:@"user_id"] integerValue];
                             to_create.is_sponsored_post = NO;
                             to_create.photo_a_loaded = NO;
                             to_create.photo_b_loaded = NO;
                             
                             //dispatch_async(dispatch_get_main_queue(), ^{
                             //NSLog(@"/post/getRandomPost/ add object to posts:postId = %@", [a_post objectForKey:@"id"]);
                             //NSLog(@"/post/getRandomPost/ add object to posts:occasion.text = %@", occasion.text);
                             [postsToAddArray addObject:to_create];
                             //[postsToAddArray addObject:to_create];
                             //});
                         }
                         
                         [self shuffleObjectsInArray:postsToAddArray];
                         
                         [self.posts addObjectsFromArray:postsToAddArray];
                         
                         /*dispatch_async(dispatch_get_main_queue(), ^{
                             
                             [self.posts addObjectsFromArray:postsToAddArray];
                             
                         });*/
                         [self start_loading_images];
                         
                     }
                     else if (self.posts.count > 0)
                     {
                         //check
                         //[self start_loading_images];
                     }
                     else
                     {
                         //NSLog(@"HomeView: there are no more posts to show");
                         if (!post_presented)
                         {
                             [self emptyClosetHidden:NO];
                         }
                     }
                 });
             }
         }];
    }
    else if([to_call isEqualToString:@"get_random_sponsored"])
    {
         //NSLog(@"if ([to_call isEqualToString:@""get_random_sponsored""])");
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/sponsor/getRandomSponsor/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&limit=%ld", [defaults objectForKey:@"user_id"], sponsorPostsLimit] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if (error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"HomeView:/sponsor/getRandomSponsor/: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"/sponsor/getRandomSponsor/response_array.count = %ld", response_array.count);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     if (response_array.count != 0)
                     {
                         for (NSDictionary * a_post in response_array)
                         {
                             Post * to_create = [[Post alloc] init];
                             to_create.photo_a_url = [a_post objectForKey:@"outfit_a__image"];
                             to_create.photo_b_url = [a_post objectForKey:@"outfit_b__image"];
                             to_create.is_sponsored_post = YES;
                             to_create.photo_a_buy_url = [a_post objectForKey:@"purchase_url_a"];
                             to_create.photo_b_buy_url = [a_post objectForKey:@"purchase_url_b"];
                             to_create.outfit_a_id = [a_post objectForKey:@"outfit_a__id"];
                             to_create.outfit_b_id = [a_post objectForKey:@"outfit_b__id"];
                             to_create.outfit_a_flagged = [a_post objectForKey:@"outfit_a__flagged"];
                             to_create.outfit_b_flagged = [a_post objectForKey:@"outfit_b__flagged"];
                             to_create.total_votes = [a_post objectForKey:@"total_votes"];
                             to_create.post_id = [a_post objectForKey:@"id"];
                             to_create.occasion = [a_post objectForKey:@"occasion__label"];
                             to_create.userName = [a_post objectForKey:@"username"];
                             to_create.user_id = [[a_post objectForKey:@"user_id"] integerValue];
                             [sponsored_posts addObject:to_create];
                             
                         }
                         [self start_loading_images_sponsored];
                     }
                 });
             }
         }];
    }
    else if ([to_call isEqualToString:@"choose_a"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/vote/", SERVICE_URL]];
        
        Post * to_show = [self.posts firstObject];
        //NSLog(@"post to delete = %@", to_show.post_id);
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&post_id=%@&outfit_id=%@", [defaults objectForKey:@"user_id"], to_show.post_id, to_show.outfit_a_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"HomeView:A:/user/vote/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         [UIView animateWithDuration:.5
                                          animations:^
                          {
                              the_frame_for_photo_b.center = CGPointMake(600, the_frame_for_photo_b.center.y);
                              occasion.center = CGPointMake(occasion.center.x, occasion.center.y+200);
                          }
                                          completion:^(BOOL finished)
                          {
                              [self.posts removeObject:to_show];
                              [self present_post];
                          }];
                     }
                 });
             }
         }];
    }
    else if([to_call isEqualToString:@"choose_b"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/vote/", SERVICE_URL]];
        
        Post * to_show  = [self.posts firstObject];
        //NSLog(@"post to delete = %@", to_show.post_id);
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&post_id=%@&outfit_id=%@", [defaults objectForKey:@"user_id"], to_show.post_id, to_show.outfit_b_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"HomeView:B:/user/vote/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         [UIView animateWithDuration:.5
                                          animations:^
                          {
                              the_frame_for_photo_a.center = CGPointMake(-600, the_frame_for_photo_a.center.y);
                              occasion.center = CGPointMake(occasion.center.x, occasion.center.y+200);
                          }
                                          completion:^(BOOL finished)
                          {
                              [self.posts removeObject:to_show];
                              [self present_post];
                          }];
                     }
                 });
             }
         }];
    }
    else if([to_call isEqualToString:@"choose_a_sponsored"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/sponsor/sponsorVote/", SERVICE_URL]];
        
        Post * to_show = [sponsored_posts firstObject];
        //NSLog(@"sponsored_posts to delete = %@", to_show.post_id);
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&sponsor_id=%@&outfit_id=%@", [defaults objectForKey:@"user_id"], to_show.post_id, to_show.outfit_a_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"HomeView:A:/sponsor/sponsorVote/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         [UIView animateWithDuration:.5
                                          animations:^
                          {
                              the_frame_for_photo_b.center = CGPointMake(600, the_frame_for_photo_b.center.y);
                              occasion.center = CGPointMake(occasion.center.x, occasion.center.y+200);
                          }
                                          completion:^(BOOL finished)
                          {
                              [sponsored_posts removeObject:to_show];
                              sponsored_post_shown = NO;
                              [self present_post];
                          }];
                     }
                 });
             }
         }];
    }
    else if([to_call isEqualToString:@"choose_b_sponsored"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/sponsor/sponsorVote/", SERVICE_URL]];
        
        Post * to_show  = [sponsored_posts firstObject];
        //NSLog(@"sponsored_posts to delete = %@", to_show.post_id);
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&sponsor_id=%@&outfit_id=%@", [defaults objectForKey:@"user_id"], to_show.post_id, to_show.outfit_b_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"HomeView:B:/sponsor/sponsorVote/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         [UIView animateWithDuration:.5
                                          animations:^
                          {
                              the_frame_for_photo_a.center = CGPointMake(-600, the_frame_for_photo_a.center.y);
                              occasion.center = CGPointMake(occasion.center.x, occasion.center.y+200);
                          }
                                          completion:^(BOOL finished)
                          {
                              [sponsored_posts removeObject:to_show];
                              sponsored_post_shown = NO;
                              [self present_post];
                          }];
                     }
                 });
             }
         }];
    }
}

-(void)present_post
{
    //this means we have posts to show.
    
    dispatch_async(dispatch_get_main_queue(), ^{
        post_presented = YES;
    });
    
    //NSLog(@"present_post");
    
    [activity_a removeFromSuperview];
    [activity_b removeFromSuperview];
    
    //SDWebImageManager *webImageManager = [SDWebImageManager sharedManager];
    
    Post *post = [self.posts firstObject];
    
    NSLog(@"postId = %@", post.post_id);
    //NSLog(@"post.occasion = %@", post.occasion);
    
    if (post)
    {
        NSInteger frequency_of_sponosors = [((AppDelegate *)[UIApplication sharedApplication].delegate).frequency_of_sponosors integerValue];
        
        if (frequency_of_sponosors > 0)
        {
            int to_show_sponsored = total_post_shown % frequency_of_sponosors;
            
            if (to_show_sponsored == 0 && sponsored_posts.count > 0 && total_post_shown != 0)
            {
                [self showSponsoredPost];
            }
            else
            {
                [self showPost:post];
            }
        }
        else
        {
            [self showPost:post];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            post_presented = NO;
            [self emptyClosetHidden:NO];
        });
    }
}

- (void)showPost:(Post *)post
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        //NSLog(@"showPost: post.occasion = %@", post.occasion);
        occasion.text = [NSString stringWithString:post.occasion];
        occasion_hanger.image = [UIImage imageNamed:@"coat_hanger_small.png"];
    });
    
    //NSLog(@"showPost:after := occasion text");
    
    if (post.photo_a_loaded && post.photo_b_loaded)
    {
        //NSLog(@"showPost:post.photo_a_loaded && post.photo_b_loaded");
        
        the_frame_for_photo_a.frame = CGRectMake(-605, 6, 150, 445);
        the_frame_for_photo_a.tag = 100;
        the_frame_for_photo_a.userInteractionEnabled = YES;
        check_mark_layer_a.alpha = 0.0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            the_frame_for_photo_a.image = post.outfit_a_image;
            [[SDImageCache sharedImageCache] removeImageForKey:post.photo_a_url fromDisk:YES];
        });
        
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        [the_frame_for_photo_a addGestureRecognizer:panRecognizer];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_a)];
        tapRecognizer.numberOfTapsRequired = 1;
        [the_frame_for_photo_a addGestureRecognizer:tapRecognizer];
        
        originalCenter_a = the_frame_for_photo_a.center.y;
        
        [photo_stack_a addSubview:the_frame_for_photo_a];
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        the_frame_for_photo_b.frame = CGRectMake(605, 6, 150, 445);
        the_frame_for_photo_b.tag = 100;
        the_frame_for_photo_b.userInteractionEnabled = YES;
        check_mark_layer_b.alpha = 0.0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            the_frame_for_photo_b.image = post.outfit_b_image;
            [[SDImageCache sharedImageCache] removeImageForKey:post.photo_b_url fromDisk:YES];
        });

        UIPanGestureRecognizer *panRecognizer23 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move2:)];
        [the_frame_for_photo_b addGestureRecognizer:panRecognizer23];
        
        UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_b)];
        tapRecognizer3.numberOfTapsRequired = 1;
        [the_frame_for_photo_b addGestureRecognizer:tapRecognizer3];
        
        originalCenter_b = the_frame_for_photo_b.center.y;
        [photo_stack_b addSubview:the_frame_for_photo_b];
        
        //NSLog(@"showPost:isShowRandomPost = %i", self.isShowRandomPost);
        //NSLog(@"showPost:total_post_shown = %ld", (long)total_post_shown);

        if (!self.isShowRandomPost)
        {
            total_post_shown++;
            //NSLog(@"showPost:total_post_shown = %ld", (long)total_post_shown);
        }
        
        sponsored_post_shown = NO;
        
        [UIView animateWithDuration:0.5
                         animations:^
         {
             the_frame_for_photo_a.frame = CGRectMake(5, 6, 150, 445);
             the_frame_for_photo_b.frame = CGRectMake(5, 6, 150, 445);
             //occasion.text = [NSString stringWithString:post.occasion];
             occasion.frame = CGRectMake(occasion_background.frame.size.width/2-100, 13, 200, 20);
             //NSLog(@"animateWithDuration: occasion = %@", occasion.text);
         }
                         completion:^(BOOL finished)
         {
             if (self.posts.count <= minPostsLimit)
             {
                 NSLog(@"asking for more posts!");
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     [self api_call:@"get_random_posts"];
                 });
             }
         }];
    }
    else if (self.posts.count > 0)
    {
        //NSLog(@"showPost:!post.photo_a_loaded && !post.photo_b_loaded");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            post_presented = NO;
        });

        CGRect screenBounds_a = photo_stack_a.bounds;
        CGRect screenBounds_b = photo_stack_b.bounds;
        
        activity_a = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
        activity_a.center = CGPointMake(CGRectGetMidX(screenBounds_a), CGRectGetMidY(screenBounds_a));
        [activity_a startAnimating];
        [photo_stack_a addSubview: activity_a];
        
        activity_b = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
        activity_b.center = CGPointMake(CGRectGetMidX(screenBounds_b), CGRectGetMidY(screenBounds_b));
        [activity_b startAnimating];
        [photo_stack_b addSubview: activity_b];
        
        //dispatch_async(dispatch_get_main_queue(), ^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self start_loading_images];
        });
    }
}

- (void)showSponsoredPost
{
    //show a sponsored ad
    
    Post * sponsored_post = [sponsored_posts firstObject];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        occasion.text = @"Sponsored";
        occasion_hanger.image = [UIImage imageNamed:@"sponsored_hanger.png"];
    });
    
    if (sponsored_post.photo_a_loaded && sponsored_post.photo_b_loaded)
    {
        the_frame_for_photo_a.frame = CGRectMake(-605, 6, 150, 445);
        the_frame_for_photo_a.tag = 100;
        the_frame_for_photo_a.userInteractionEnabled = YES;
        check_mark_layer_a.alpha = 0.0;
        
        [the_frame_for_photo_a sd_setImageWithURL:[NSURL URLWithString:sponsored_post.photo_a_url]];
        [[SDImageCache sharedImageCache] removeImageForKey:sponsored_post.photo_a_url fromDisk:YES];
        
        UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        [the_frame_for_photo_a addGestureRecognizer:panRecognizer];
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_a)];
        tapRecognizer.numberOfTapsRequired = 1;
        [the_frame_for_photo_a addGestureRecognizer:tapRecognizer];
        
        originalCenter_a = the_frame_for_photo_a.center.y;
        
        [photo_stack_a addSubview:the_frame_for_photo_a];
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        the_frame_for_photo_b.frame = CGRectMake(605, 6, 150, 445);
        the_frame_for_photo_b.tag = 100;
        the_frame_for_photo_b.userInteractionEnabled = YES;
        check_mark_layer_b.alpha = 0.0;
        
        [the_frame_for_photo_b sd_setImageWithURL:[NSURL URLWithString:sponsored_post.photo_b_url]];
        [[SDImageCache sharedImageCache] removeImageForKey:sponsored_post.photo_b_url fromDisk:YES];
        
        UIPanGestureRecognizer *panRecognizer23 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move2:)];
        [the_frame_for_photo_b addGestureRecognizer:panRecognizer23];
        
        UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_b)];
        tapRecognizer3.numberOfTapsRequired = 1;
        [the_frame_for_photo_b addGestureRecognizer:tapRecognizer3];
        
        originalCenter_b = the_frame_for_photo_b.center.y;
        [photo_stack_b addSubview:the_frame_for_photo_b];
        
        total_post_shown++;
        sponsored_post_shown = YES;
        
        [UIView animateWithDuration:0.5
                         animations:^
         {
             the_frame_for_photo_a.frame = CGRectMake(5, 6, 150, 445);
             the_frame_for_photo_b.frame = CGRectMake(5, 6, 150, 445);
             occasion.frame = CGRectMake(occasion_background.frame.size.width/2-100, 13, 200, 20);
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"present_post:sponsored posts count = %ld ", (long)sponsored_posts.count);
             if (sponsored_posts.count <= minSponsorPostsLimit)
             {
                 NSLog(@"asking for more sponsored posts!");
                 //[self performSelectorInBackground:@selector(api_call:) withObject:@"get_random_sponsored"];
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     [self api_call:@"get_random_sponsored"];
                 });
             }
         }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            total_post_shown ++;
            post_presented = NO;
            [self present_post];
        });
    }
}

-(void)start_loading_images
{
    //NSLog(@"start_loading_images");
    
    Post * to_start = nil;
    
    for (Post* to_test in self.posts)
    {
        //NSLog(@"start_loading_images:post_id = %@", to_test.post_id);
        
        if (!to_test.photo_a_loaded || !to_test.photo_b_loaded)
        {
            //NSLog(@"start_loading_images: !photos_loaded");
            //this means that one the photos are nil this is the first non loaded image in our array.
            to_start = to_test;
            break;
        }
    }
    
    if (to_start)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self load_images_for_post:to_start];
        });
    }
    else if (!post_presented)
    {
        //NSLog(@"start_loading_images: !post_presented");
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self present_post];
        });
    }
}

-(void)start_loading_images_sponsored
{
    Post * to_start;
    for (Post* to_test in self.sponsored_posts)
    {
        if (!to_test.photo_a_loaded || !to_test.photo_b_loaded)
        {
            //this means that one the photos are nil this is the first non loaded image in our array.
            to_start = to_test;
            break;
        }
    }
    
    //NSLog(@"start_loading_images_sponsored");
    [self load_images_for_post_sponsored:to_start];
}

-(void)load_images_for_post:(Post*)incoming
{
    //NSLog(@"load_images_for_post: post_id = %@", incoming.post_id);
    
    SDWebImageManager *webImageManager = [SDWebImageManager sharedManager];

    if (!incoming.photo_a_loaded)
    {
        [webImageManager downloadImageWithURL:[NSURL URLWithString:incoming.photo_a_url] options:0 progress:nil
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
        {
            if (error || !image)
            {
                //NSLog(@"load_images_for_post:error loading image %@ or image is nil; attemptsCount = %ld", error, (long)attemptsCount);
                
                if (attemptsCount < 3)
                {
                    //dispatch_async(dispatch_get_main_queue(), ^{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        NSLog(@"load_images_for_post:attemptsCount++");

                        attemptsCount++;

                        NSLog(@"load_images_for_post A:if (attemptsCount < 3)");
                        [self load_images_for_post:incoming];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        
                        NSLog(@"load_images_for_post:show error");
                        
                        attemptsCount = 0;
                        
                        //NSLog(@"load_images_for_post 1posts.count = %ld", (long)self.posts.count);
                        [self.posts removeObject:incoming];

                        
                        //NSLog(@"load_images_for_post 2posts.count = %ld", (long)self.posts.count);
                        
                        [self start_loading_images];
                    });
                }
            }
            else
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     incoming.photo_a_loaded = YES;
                     incoming.outfit_a_image = image;
                 });

                 if (!incoming.photo_b_loaded)
                 {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         //NSLog(@"load_images_for_post A:image downloaded");
                         attemptsCount = 0;
                         
                         incoming.photo_a_loaded = YES;
                         incoming.outfit_a_image = image;

                         [self load_images_for_post:incoming];
                     });
                 }
            }
        }];
    }
    else if(!incoming.photo_b_loaded)
    {
        [webImageManager downloadImageWithURL:[NSURL URLWithString:incoming.photo_b_url] options:0 progress:nil
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
         {
             if (error || !image)
             {
                 //NSLog(@"load_images_for_post:error loading image %@ or image is nil; attemptsCount = %ld", error, (long)attemptsCount);
                 
                 if (attemptsCount < 3)
                 {
                     //dispatch_async(dispatch_get_main_queue(), ^{
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         //NSLog(@"load_images_for_post:attemptsCount++");
                         
                         attemptsCount++;
                         
                         //NSLog(@"load_images_for_post B:if (attemptsCount < 3)");
                         [self load_images_for_post:incoming];
                     });
                 }
                 else
                 {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         
                         //NSLog(@"load_images_for_post:show error");
                         
                         attemptsCount = 0;
                         
                         //NSLog(@"load_images_for_post 1posts.count = %ld", (long)self.posts.count);
                         [self.posts removeObject:incoming];

                         
                         //NSLog(@"load_images_for_post 2posts.count = %ld", (long)self.posts.count);
                         
                         [self start_loading_images];
                         
                     });
                 }
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     attemptsCount = 0;
                     
                     incoming.photo_b_loaded = YES;
                     incoming.outfit_b_image = image;
                 });
                 
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     //NSLog(@"load_images_for_post B:image downloaded");
                     [self load_images_for_post:incoming];
                 });
             }
         }];
    }
    else
    {
        NSInteger postIndex = [self.posts indexOfObject:incoming];
        
        //NSLog(@"load_images_for_post:postIndex = %ld", postIndex);
        
        if (postIndex < (self.posts.count - 1))
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
                //NSLog(@"load_images_for_post A&B loaded");
                [self load_images_for_post:[self.posts objectAtIndex:postIndex + 1]];
            });
        }
        else if (!post_presented)
        {
            //NSLog(@"load_images_for_post:post_presented = %ld", post_presented);
            //NSLog(@"load_images_for_postId = %@", incoming.post_id);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self present_post];
            });
        }
    }
}

-(void)load_images_for_post_sponsored:(Post*)incoming
{
    SDWebImageManager *webImageManager = [SDWebImageManager sharedManager];
    if (!incoming.photo_a_loaded)
    {
        [webImageManager downloadImageWithURL:[NSURL URLWithString:incoming.photo_a_url] options:0 progress:nil
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
         {
             if (error)
             {
                 NSLog(@"error loading image %@", error);
             }
             
             incoming.photo_a_loaded = YES;
             if (!incoming.outfit_b_image)
             {
                 //NSLog(@"load_images_for_post_sponsored A");
                 [self load_images_for_post_sponsored:incoming];
             }
         }];
    }
    else if(!incoming.photo_b_loaded)
    {
        [webImageManager downloadImageWithURL: [NSURL URLWithString:incoming.photo_b_url] options:0 progress:nil
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
         {
             if (error)
             {
                 NSLog(@"error loading image %@", error);
             }
             incoming.photo_b_loaded = YES;
             //NSLog(@"load_images_for_post_sponsored B");
             [self load_images_for_post_sponsored:incoming];
         }];
    }
    else
    {
        //this means that all the photos are loaded for this particular post.
        int spot = (int)[self.sponsored_posts indexOfObject:incoming];
        int whos_next = spot+1;
        if (whos_next >= self.sponsored_posts.count)
        {
            //we have finished
            
        }
        else
        {
            //NSLog(@"load_images_for_post_sponsored A&B loaded");
            [self load_images_for_post_sponsored:[self.sponsored_posts objectAtIndex:whos_next]];
        }
    }
}

-(void)view_a
{
    if (sponsored_post_shown)
    {
        Post * the_post = [self.sponsored_posts firstObject];
        
        SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:the_post.photo_a_buy_url];
        
        [self presentViewController:webViewController animated:YES completion:nil];
        
        [Flurry logEvent:@"view sponsored post"];
    }
    else
    {
        Post * the_post = [self.posts firstObject];
        
        PhotoDetailsView * to_push = [[PhotoDetailsView alloc] init];
        [[SlideNavigationController sharedInstance] presentViewController:to_push animated:YES completion:^{
            
            to_push.photo.image = the_frame_for_photo_a.image;
            to_push.outfit_id = the_post.outfit_a_id;
            to_push.post_id = the_post.post_id;
            //to_push.is_flagged = the_post.outfit_a_flagged;
            to_push.url_for_image = the_post.photo_a_url;
            to_push.username_label.text = the_post.userName;
            
            float outfit_percent;
            
            if (the_post.photo_a_votes.floatValue != 0)
                outfit_percent = the_post.photo_a_votes.floatValue/the_post.total_votes.floatValue;
            else
                outfit_percent = 0;
            
            int percent = outfit_percent *100;
            
            to_push.username_label.text = the_post.userName;
            to_push.percent.text = [NSString stringWithFormat:@"%d%%" , percent];
            to_push.occasion_label.text = the_post.occasion;
            [to_push check_favorites];
            [to_push check_following];
            [to_push check_flag];
            [to_push check_tags];
            
            [Flurry logEvent:@"view post"];
        }];
    }
}
-(void)view_b
{
    if (sponsored_post_shown)
    {
        Post * the_post = [self.sponsored_posts firstObject];
        
        SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:the_post.photo_b_buy_url];
        
        [self presentViewController:webViewController animated:YES completion:nil];
        
        [Flurry logEvent:@"view sponosored post"];
    }
    else
    {
        Post * the_post = [self.posts firstObject];
        
        PhotoDetailsView * to_push = [[PhotoDetailsView alloc] init];
        [[SlideNavigationController sharedInstance] presentViewController:to_push animated:YES completion:^{
            
            to_push.photo.image = the_frame_for_photo_b.image;
            to_push.outfit_id = the_post.outfit_b_id;
            to_push.post_id = the_post.post_id;
            //to_push.is_flagged = the_post.outfit_b_flagged;
            to_push.url_for_image = the_post.photo_b_url;
            to_push.username_label.text = the_post.userName;
            
            float outfit_percent;
            
            if (the_post.photo_b_votes.floatValue != 0)
                outfit_percent = the_post.photo_b_votes.floatValue/the_post.total_votes.floatValue;
            else
                outfit_percent = 0;
            
            int percent = outfit_percent *100;
            
            to_push.percent.text = [NSString stringWithFormat:@"%d%%" , percent];
            to_push.occasion_label.text = the_post.occasion;
            [to_push check_favorites];
            [to_push check_following];
            [to_push check_flag];
            [to_push check_tags];
            
            [Flurry logEvent:@"view post"];
        }];
    }
}
-(void)choose_a
{
    [Flurry logEvent:@"voted a"];
    if (sponsored_post_shown)
    {
        [self api_call:@"choose_a_sponsored"];
    }
    else
    {
        [self api_call:@"choose_a"];
    }
}
-(void)choose_b
{
    [Flurry logEvent:@"voted b"];
    if (sponsored_post_shown)
    {
        [self api_call:@"choose_b_sponsored"];
    }
    else
    {
        [self api_call:@"choose_b"];
    }
}
-(void)move:(id)sender
{
    //NSLog(@"move");
    
    //[self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:photo_stack_a];
    //    NSLog(@"this is the original point  %f", originalCenter);
    //    NSLog(@"this is the translated point  %f", translatedPoint.y);
    check_mark_layer_a.alpha = 1.0;
    
    if ([(UIPanGestureRecognizer *)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    else if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
    {
        if (translatedPoint.y >= 0)
        {
        }
        else
        {
            translatedPoint = CGPointMake(firstX, firstY+translatedPoint.y);
            [[sender view] setCenter:translatedPoint];
        }
    }
    else if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded)
    {
        CGFloat velocityY = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
        CGFloat finalY = translatedPoint.y + velocityY;
        
        if (finalY >= originalCenter_a)
        {
            CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_a);
            check_mark_layer_a.alpha = 0.0;
            [UIView commitAnimations];
            
        }
        else if (finalY >= -233)
        {
            CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_a);
            check_mark_layer_a.alpha = 0.0;
            [UIView commitAnimations];
            
        }
        else
        {
            [self choose_a];
            [UIView animateWithDuration:0.5
                             animations:^
             {
                 [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_a-600);
                 check_mark_layer_a.alpha = 0.0;
                 
             }
                             completion:nil];
        }
    }
}
-(void)move2:(id)sender
{
    //NSLog(@"move2");
    
    check_mark_layer_b.alpha = 1.0;
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:photo_stack_b];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX = [[sender view] center].x;
        firstY = [[sender view] center].y;
    }
    else if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
    {
        if (translatedPoint.y >= 0)
        {
            
        }
        else
        {
            translatedPoint = CGPointMake(firstX, firstY+translatedPoint.y);
            [[sender view] setCenter:translatedPoint];
        }
        
    }
    else
    {
        
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded)
        {
            
            CGFloat velocityY = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            
            CGFloat finalY = translatedPoint.y + velocityY;
            if (finalY >= originalCenter_b)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_b);
                check_mark_layer_b.alpha = 0.0;
                [UIView commitAnimations];
                
            }
            else if (finalY >= -233)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_b);
                check_mark_layer_b.alpha = 0.0;
                [UIView commitAnimations];
            }
            else
            {
                [self choose_b];
                [UIView animateWithDuration:0.5
                                 animations:^
                 {
                     [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX, originalCenter_b-600);
                     check_mark_layer_b.alpha = 0.0;
                     
                 }
                                 completion:^(BOOL finished)
                 {
                     
                 }];
            }
            
        }
    }
}

- (void)shuffleObjectsInArray:(NSMutableArray *)array
{
    NSUInteger count = array.count;
    for (NSUInteger i = 0; i < count; ++i)
    {
        NSInteger remainingCount = count - i;
        NSInteger exchangeIndex = i + arc4random_uniform((u_int32_t )remainingCount);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchangeIndex];
    }
}

- (void)showRandomPost
{
    NSInteger postsCount = self.posts.count;
    
    if (postsCount > 1)
    {
        NSInteger exchangeIndex = 1 + arc4random_uniform((u_int32_t )(postsCount - 1));
        [self.posts exchangeObjectAtIndex:0 withObjectAtIndex:exchangeIndex];
        //NSLog(@"showRandomPost = %ld", (long)exchangeIndex);
        [self present_post];
        self.isShowRandomPost = NO;
        NSLog(@"showRandomPost = %i", self.isShowRandomPost);
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer
{
    CGPoint velocity = [panGestureRecognizer velocityInView:panGestureRecognizer.view.superview];
    return fabs(velocity.y) > fabs(velocity.x);
}

- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

@end

//
//  Outfit.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "Post.h"
#import "UIImageView+WebCache.h"

@interface Outfit : SWTableViewCell

@property (strong, atomic) UIView * cellBackgroudView;
@property (strong, atomic) UIImageView * outfit_a;
@property (strong, atomic) UIImageView * outfit_b;
@property (strong, atomic) UIImageView * frame_a;
@property (strong, atomic) UIImageView * frame_b;
@property (strong, atomic) UIImageView * middle_sep;
@property (strong, atomic) UILabel * outfit_a_label;
@property (strong, atomic) UILabel * outfit_b_label;
@property (strong, atomic) Post * this_post;
@property (strong, atomic) UIImage * placeholder;
@property BOOL loaded;
- (id)initWithData:(Post*)incoming;
- (void)load;
- (void)load_images;
@end

//
//  AppDelegate.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuView.h"
#import "HomeView.h"
#import "ClosetView.h"
#import "SlideNavigationController.h"
#import "Occasion.h"
#import "LogoView.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeView * home_view;
@property (strong, nonatomic) LogoView * logo_view;
@property (strong, nonatomic) MenuView * left_view;
@property (strong, nonatomic) ClosetView * right_view;
@property (strong, nonatomic) SlideNavigationController * slideNavigationController;
@property (strong, nonatomic) FBSession *session;
@property (strong, nonatomic) NSMutableArray * occasions;
@property (strong, nonatomic) NSNumber * frequency_of_sponosors;
@end

//
//  FollowingViewController.m
//  pickfit
//
//  Created by treasure on 3/18/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "FollowingViewController.h"
#import "SlideNavigationController.h"
#import "FollowingTableViewCell.h"
#import "User.h"
#import "UserVC.h"

@interface FollowingViewController () <UITableViewDelegate, UITableViewDataSource, FollowingTableViewCellDelegate>
{
    UIImageView *topBackgroundImageView;
    UILabel *titleLabel;
    UIButton *editButton;
    UIButton *backButton;
    
    UILabel *noFollowingsLabel;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tableArray;

@end

@implementation FollowingViewController

static NSString * const mainFont = @"Avenir";

static CGFloat const kTopOffset = 30;

static CGFloat const kTitleLabelHeight = 22;
static CGFloat const kTitleLabelFontSize = 20;

static CGFloat const kBackButtonWidth = 50;
static CGFloat const kBackButtonHeight = 30;

static NSString *followingTableViewCellID = @"followingTableViewCellID";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect mainViewRect = self.view.frame;
    
    topBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    topBackgroundImageView.image = [UIImage imageNamed:@"header_white_bar.png"];
    //[self.view addSubview:topBackgroundImageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kBackButtonWidth, kTopOffset, (mainViewRect.size.width - kBackButtonWidth * 2), kTitleLabelHeight)];
    titleLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"People I Follow", @"");
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kBackButtonWidth, kBackButtonHeight)];
    imageView.image = [UIImage imageNamed:@"button_green_arrow_back"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kTopOffset, kBackButtonWidth, kBackButtonHeight)];
    [backButton addTarget:self action:@selector(onBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton addSubview:imageView];
    backButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backButton];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(topBackgroundImageView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(topBackgroundImageView.frame)) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[FollowingTableViewCell class] forCellReuseIdentifier:followingTableViewCellID];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    noFollowingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame) * 2, mainViewRect.size.width, kTitleLabelHeight)];
    noFollowingsLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    noFollowingsLabel.textAlignment = NSTextAlignmentCenter;
    noFollowingsLabel.text = NSLocalizedString(@"You are not following anyone", @"");
    noFollowingsLabel.textColor = [UIColor blackColor];
    noFollowingsLabel.backgroundColor = [UIColor clearColor];
    noFollowingsLabel.hidden = YES;
    [self.view addSubview:noFollowingsLabel];
    
    self.tableArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self performRequest:@"getUserFollowings"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

- (void)onBackButton
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)performRequest:(NSString *)requestString
{
    if ([requestString isEqualToString:@"getUserFollowings"])
    {
        [self.tableArray removeAllObjects];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserFollowings/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&limit=0&offset=0", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 dispatch_async(dispatch_get_main_queue(),^{
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"FollowingVC:/user/getUserFollowings/: response data is nil");
                 });
             }
             else
             {
                 NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseArray.count > 0)
                     {
                         for (NSDictionary *dictionary in responseArray)
                         {
                             User *user = [[User alloc] initWithDictionary:dictionary];
                             [self.tableArray addObject:user];
                         }
                         
                         noFollowingsLabel.hidden = YES;
                         
                         [self.tableView reloadData];
                     }
                     else
                     {
                         noFollowingsLabel.hidden = NO;
                         
                         [self.tableView reloadData];
                     }
                 });
             }
         }];
    }
}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowingTableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:followingTableViewCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.followingCellDelegate = self;

    return cell;
}

-(void)tableView:(UITableView *)aTableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (aTableView.isDecelerating)
    {
        return;
    }

    FollowingTableViewCell *followingTableViewCell = (FollowingTableViewCell *)cell;
    
    User *user = self.tableArray[indexPath.row];
    
    followingTableViewCell.userNameLabel.text = user.name;
    followingTableViewCell.follow = NO;
    [followingTableViewCell.userImageView sd_setImageWithURL:[NSURL URLWithString:user.imageUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
     {
         if (error || !image)
         {
             followingTableViewCell.userImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
         }
     }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserVC *userVC = [[UserVC alloc] init];
    userVC.userID = ((User *)[self.tableArray objectAtIndex:indexPath.row]).userId;
    [[SlideNavigationController sharedInstance] pushViewController:userVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableArray.count;
}

#pragma mark - FollowingTableViewCellDelegate

- (void)didButtonUnfollowPress:(FollowingTableViewCell *)cell
{
    NSInteger indexToDelete = [self.tableView indexPathForCell:cell].row;
    User *user = [self.tableArray objectAtIndex:indexToDelete];
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/follower/deleteFollower/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%@&following_user_id=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], (long)user.userId] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
             
             dispatch_async(dispatch_get_main_queue(),^{
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"FollowingVC:/follower/deleteFollower/: response data is nil");
             });
         }
         else
         {
             NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSString *status = [responseDictionary objectForKey:@"status"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [self.tableArray removeObjectAtIndex:indexToDelete];
                 }
                 else if ([status isEqualToString:@"fail"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     [alert show];
                 }
                 
                 [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationRight];
                 [self.tableView reloadData];
             });
         }
     }];
}

@end
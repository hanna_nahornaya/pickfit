//
//  CreateOutfitView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "CreateOutfitView.h"
#import "AppDelegate.h"
#import "Flurry.h"
#import <Pinterest/Pinterest.h>


@implementation CreateOutfitView
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"f7faf6"];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
        [self.view addSubview:top_background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"Add My Outfits";
        top_label.textColor = [UIColor blackColor];
        [self.view addSubview:top_label];
        
        a_tags = [[NSMutableArray alloc] init];
        b_tags = [[NSMutableArray alloc] init];
        
        x  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: x];
        
        photo_a  = [[UIButton alloc] initWithFrame:CGRectMake(30, 75, 261/2, 720/2)];
        [photo_a setImage:[UIImage imageNamed:@"create_outfit_box_new.png"] forState:UIControlStateNormal];
        [photo_a addTarget:self action:@selector(a_pressed) forControlEvents:UIControlEventTouchUpInside];
        photo_a.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview: photo_a];
        
        photo_b  = [[UIButton alloc] initWithFrame:CGRectMake(160, 75, 261/2, 720/2)];
        [photo_b setImage:[UIImage imageNamed:@"create_outfit_box_new.png"] forState:UIControlStateNormal];
        [photo_b addTarget:self action:@selector(b_pressed) forControlEvents:UIControlEventTouchUpInside];
        photo_b.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview: photo_b];
        
        choose_occasion = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 604/4, 435, 604/2, 103/2)];
        [choose_occasion addTarget:self action:@selector(choose_pressed) forControlEvents:UIControlEventTouchUpInside];
        [choose_occasion setTitle:@"What's the occasion?" forState:UIControlStateNormal];
        [choose_occasion setBackgroundImage:[UIImage imageNamed:@"create_outfit_button-1.png"] forState:UIControlStateNormal];
        choose_occasion.backgroundColor = [UIColor clearColor];
        choose_occasion.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:19];
        [choose_occasion setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [choose_occasion setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
        [self.view addSubview:choose_occasion];
        
        occasions = [(AppDelegate *)[[UIApplication sharedApplication] delegate] occasions];
        
        share_to_label = [[UILabel alloc] initWithFrame:CGRectMake(25, 490, 250, 20)];
        share_to_label.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        share_to_label.textAlignment = NSTextAlignmentLeft;
        share_to_label.text = @"Share to:";
        share_to_label.textColor = [UIColor blackColor];
        [self.view addSubview:share_to_label];
        
        facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(120, 488, 71/2, 68/2)];
        [facebook_button setImage:[UIImage imageNamed:@"social_fb.png"] forState:UIControlStateNormal];
        [facebook_button addTarget:self action:@selector(facebook_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: facebook_button];
        
        twitter_button  = [[UIButton alloc] initWithFrame:CGRectMake(175, 488, 71/2, 68/2)];
        [twitter_button setImage:[UIImage imageNamed:@"social_twitter.png"] forState:UIControlStateNormal];
        [twitter_button addTarget:self action:@selector(twitter_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: twitter_button];
        
        pintrest_button  = [[UIButton alloc] initWithFrame:CGRectMake(230, 488, 71/2, 68/2)];
        [pintrest_button setImage:[UIImage imageNamed:@"social_pinterest.png"] forState:UIControlStateNormal];
        [pintrest_button addTarget:self action:@selector(pintrest_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: pintrest_button];
        
        facebook_bool = NO;
        twitter_bool = NO;
        pintrest_bool = NO;
        uploading_a = NO;
        uploading_b = NO;
        
        submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 528, 320, 45)];
        [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
        [submit setTitle:@"Submit" forState:UIControlStateNormal];
        [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:18]];
        [submit setTitleEdgeInsets:UIEdgeInsetsMake(-3.f, 0.f, 0.f, 0.f)];
        submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [self.view addSubview:submit];

    }
    else
    {
        
        content_scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
        content_scroll.contentSize = CGSizeMake(320, 568);
        content_scroll.backgroundColor = [UIColor clearColor];
        content_scroll.showsVerticalScrollIndicator = NO;
        [self.view addSubview:content_scroll];

        
        top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
        [content_scroll addSubview:top_background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"Add My Outfits";
        top_label.textColor = [UIColor blackColor];
        [content_scroll addSubview:top_label];
        
        
        a_tags = [[NSMutableArray alloc] init];
        b_tags = [[NSMutableArray alloc] init];
        
        x  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [content_scroll addSubview: x];
        
        photo_a  = [[UIButton alloc] initWithFrame:CGRectMake(30, 75, 261/2, 720/2)];
        [photo_a setImage:[UIImage imageNamed:@"create_outfit_box_new.png"] forState:UIControlStateNormal];
        [photo_a addTarget:self action:@selector(a_pressed) forControlEvents:UIControlEventTouchUpInside];
        photo_a.contentMode = UIViewContentModeScaleAspectFit;
        [content_scroll addSubview: photo_a];
        
        photo_b  = [[UIButton alloc] initWithFrame:CGRectMake(160, 75, 261/2, 720/2)];
        [photo_b setImage:[UIImage imageNamed:@"create_outfit_box_new.png"] forState:UIControlStateNormal];
        [photo_b addTarget:self action:@selector(b_pressed) forControlEvents:UIControlEventTouchUpInside];
        photo_b.contentMode = UIViewContentModeScaleAspectFit;
        [content_scroll addSubview: photo_b];
        
        choose_occasion = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 604/4, 435, 604/2, 103/2)];
        [choose_occasion addTarget:self action:@selector(choose_pressed) forControlEvents:UIControlEventTouchUpInside];
        [choose_occasion setTitle:@"What's the occasion?" forState:UIControlStateNormal];
        [choose_occasion setBackgroundImage:[UIImage imageNamed:@"create_outfit_button-1.png"] forState:UIControlStateNormal];
        choose_occasion.backgroundColor = [UIColor clearColor];
        choose_occasion.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:19];
        [choose_occasion setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [choose_occasion setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
        [content_scroll addSubview:choose_occasion];
        
        occasions = [(AppDelegate *)[[UIApplication sharedApplication] delegate] occasions];
        
        share_to_label = [[UILabel alloc] initWithFrame:CGRectMake(25, 490, 250, 20)];
        share_to_label.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        share_to_label.textAlignment = NSTextAlignmentLeft;
        share_to_label.text = @"Share to:";
        share_to_label.textColor = [UIColor blackColor];
        [content_scroll addSubview:share_to_label];
        
        facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(120, 488, 71/2, 68/2)];
        [facebook_button setImage:[UIImage imageNamed:@"social_fb.png"] forState:UIControlStateNormal];
        [facebook_button addTarget:self action:@selector(facebook_pressed) forControlEvents:UIControlEventTouchUpInside];
        [content_scroll addSubview: facebook_button];
        
        twitter_button  = [[UIButton alloc] initWithFrame:CGRectMake(175, 488, 71/2, 68/2)];
        [twitter_button setImage:[UIImage imageNamed:@"social_twitter.png"] forState:UIControlStateNormal];
        [twitter_button addTarget:self action:@selector(twitter_pressed) forControlEvents:UIControlEventTouchUpInside];
        [content_scroll addSubview: twitter_button];
        
        pintrest_button  = [[UIButton alloc] initWithFrame:CGRectMake(230, 488, 71/2, 68/2)];
        [pintrest_button setImage:[UIImage imageNamed:@"social_pinterest.png"] forState:UIControlStateNormal];
        [pintrest_button addTarget:self action:@selector(pintrest_pressed) forControlEvents:UIControlEventTouchUpInside];
        [content_scroll addSubview: pintrest_button];
        
        facebook_bool = NO;
        twitter_bool = NO;
        pintrest_bool = NO;
        uploading_a = NO;
        uploading_b = NO;
        
        submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 528, 320, 45)];
        [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
        [submit setTitle:@"Submit" forState:UIControlStateNormal];
        [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:18]];
        [submit setTitleEdgeInsets:UIEdgeInsetsMake(-3.f, 0.f, 0.f, 0.f)];
        submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [content_scroll addSubview:submit];
        
    }    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assign_tags:) name:@"TagsForPhoto" object:nil];
    
}
-(void)choose_pressed
{
    if (photo_a.enabled)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 568)
            occasion_picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 370, 320, 228)];
        else
            occasion_picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 370-84, 320, 228)];
        occasion_picker.delegate = self;
        occasion_picker.showsSelectionIndicator = YES;
       
        occasion_picker.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:occasion_picker];
        
        UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
        gestureRecognizer.delegate = self;
        [occasion_picker addGestureRecognizer:gestureRecognizer];
        
        tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
        [self.view addGestureRecognizer:tapRecognizer];
        
        photo_a.enabled = NO;
        photo_b.enabled = NO;
        
        Occasion * choosen = [occasions objectAtIndex:0];
        selected_occasion = choosen;
        [choose_occasion setTitle:choosen.name forState:UIControlStateNormal];
    }
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    
    // return
    return true;
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    
    float oldWidth = sourceImage.size.width;
    if (oldWidth > i_width)
    {
        float scaleFactor = i_width / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
   
    return sourceImage;
}
-(void)upload_photo_a:(UIImage*)incoming
{
    uploading_a = YES;
    incoming = [self imageWithImage:incoming scaledToWidth:320];
    NSData * image_data = UIImageJPEGRepresentation(incoming , .65);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"CreateOutfit:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             
             
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     uploading_a = NO;
                     link_a = [response_dict objectForKey:@"link"];
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];
}
-(void)upload_photo_b:(UIImage*)incoming
{
    uploading_b = YES;
    incoming = [self imageWithImage:incoming scaledToWidth:320];
    NSData * image_data = UIImageJPEGRepresentation(incoming , .65);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];

    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"CreateOutfit:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     uploading_b = NO;
                     link_b = [response_dict objectForKey:@"link"];
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];
}
-(void)assign_tags:(NSNotification*)message
{
    NSMutableArray * tags = message.object;
    if (user_pressed_a)
    {
        a_tags = tags;
        
    }
    else if(user_pressed_b)
    {
        b_tags = tags;
    }    
}
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:NO completion:NULL];
    if (user_pressed_a)
    {
        
        for (UIView * to_remove in photo_a.subviews)
        {
            if (to_remove.tag == 100)
            {
                [to_remove removeFromSuperview];
            }
        }
        
        UIImageView * a_photo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, photo_a.frame.size.width-20, photo_a.frame.size.height-20)];
        a_photo.image = croppedImage;
        a_photo.tag = 100;
        //a_photo.contentMode = UIViewContentModeScaleAspectFit;
        [photo_a addSubview:a_photo];
        
        [self upload_photo_a:croppedImage];
        
    
    }
    else
    {
        for (UIView * to_remove in photo_b.subviews)
        {
            if (to_remove.tag == 100)
            {
                [to_remove removeFromSuperview];
            }
        }
        
        UIImageView * b_photo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, photo_b.frame.size.width-20, photo_b.frame.size.height-20)];
        b_photo.image = croppedImage;
        b_photo.tag = 100;
        //b_photo.contentMode = UIViewContentModeScaleAspectFit;
        [photo_b addSubview:b_photo];
        
        [self upload_photo_b:croppedImage];
    }
    
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
- (void)showCamera
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];

}
- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:controller animated:YES completion:NULL];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Library", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)])
    {
        [self showCamera];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * originalImage  = [UIImage imageWithData:UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], .6)];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        
        // Resize the image
        UIImage * image = [self imageWithImage:originalImage scaledToWidth:850];
        
        // Optionally save the image here...
        dispatch_async(dispatch_get_main_queue(), ^
        {
            // ... Set / use the image here...
            [picker dismissViewControllerAnimated:NO completion:^
             {
                 
                 PECropViewController *controller_crop = [[PECropViewController alloc] init];
                 controller_crop.delegate = self;
                 controller_crop.image = image;
                 controller_crop.keepingCropAspectRatio = YES;
                 
                 UIImage *image = controller_crop.image;
                 
                 CGFloat width = image.size.height/2.75;
                 
                 if (width >= image.size.width)
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Error" message:@"This photo is not in the correct format to ensure an optimal viewing experience. Please try another photo." delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     [alert show];
                     
                     NSLog(@"can not use this image. Your image will end up being stretched by our system.");
                 }
                 else
                 {
                     CGFloat inset = image.size.width/2-image.size.width/5.5;
                     CGFloat height = image.size.height;
                     controller_crop.imageCropRect = CGRectMake(inset, 0, width, height);
                     
                     //NSLog(@"image width %f", image.size.width);
                     //NSLog(@"image height %f", image.size.height);
                     
                     //NSLog(@"width %f", width);
                     //NSLog(@"height %f", height);
                     
                     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller_crop];
                     
                     [self presentViewController:navigationController animated:YES completion:NULL];
                 }
                 
             }];
            
        });           
    });
    
//
    
}
- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize
{
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)a_pressed
{
    user_pressed_a = YES;
    user_pressed_b = NO;
    
#if TARGET_IPHONE_SIMULATOR
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
#else
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Camera", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    [actionSheet showInView:self.view];
#endif
}
-(void)b_pressed
{
    user_pressed_a = NO;
    user_pressed_b = YES;
    
#if TARGET_IPHONE_SIMULATOR
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
#else
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Camera", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    [actionSheet showInView:self.view];
#endif
}
-(void)submit_pressed
{
    if (!selected_occasion)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please choose an occasion." delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
    else
    {
        
        if (!uploading_a && !uploading_b && link_a && link_b && photo_a && photo_b)
        {
            UIImageView * photo_a_image;
            for (UIView * to_find in photo_a.subviews)
            {
                if (to_find.tag == 100)
                {
                    photo_a_image = (UIImageView*)to_find;
                }
            }
            
            UIImageView * photo_b_image;
            for (UIView * to_find in photo_b.subviews)
            {
                if (to_find.tag == 100)
                {
                    photo_b_image = (UIImageView*)to_find;
                }
            }
            
            photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
            photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
            
            to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
            [to_render addSubview:photo_a_image];
            [to_render addSubview:photo_b_image];
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/post/createPost/", SERVICE_URL]];
            
            NSData * json_a = [NSJSONSerialization dataWithJSONObject:a_tags options:0 error:nil];
            NSData * json_b = [NSJSONSerialization dataWithJSONObject:b_tags options:0 error:nil];
            
            NSString * jsonString_a = [[NSString alloc] initWithData:json_a
                                                            encoding:NSUTF8StringEncoding];
            
            NSString * jsonString_b = [[NSString alloc] initWithData:json_b
                                                            encoding:NSUTF8StringEncoding];
            
            NSData *postData= [[NSString stringWithFormat:@"user_id=%@&occasion_id=%@&outfit_a_image=%@&outfit_b_image=%@&outfit_a_tags=%@&outfit_b_tags=%@", [defaults objectForKey:@"user_id"], selected_occasion.occasion_id, link_a, link_b, jsonString_a, jsonString_b] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if(error)
                 {
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                         alert.tag = 1;
                         //[alert show];
                         
                     });
                 }
                 else if (!data)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSLog(@"CreateOutfit:/post/createPost/: response data is nil");
                     });
                 }
                 else
                 {
                     //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                     //NSLog(@"data back from /user/create/ = %@" ,test);
                     
                     NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         
                         NSString * status = [response_dict objectForKey:@"status"];
                         if ([status isEqualToString:@"success"])
                         {
                             NSNumber * number = [response_dict objectForKey:@"post_id"];
                             if (facebook_bool)
                             {
                                 [self create_facebook_image:number];
                             }
                             if (twitter_bool)
                             {
                                 NSString * message = [NSString stringWithFormat:@"Which outfit do you prefer? Vote at %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",number.stringValue]];
                                 [self TWPostImage:nil withStatus:message];
                             }
                             if (pintrest_bool)
                             {
                                 [self create_pintrest_image:number];
                             }
                             
                             [self dismissViewControllerAnimated:YES completion:^
                              {
                                  
                                  NSMutableDictionary * post_info = [[NSMutableDictionary alloc] init];
                                  [post_info setObject: [response_dict objectForKey:@"post_id"] forKey:@"post_id"];
                                  if (photo_a_image)
                                  {
                                      [post_info setObject: photo_a_image forKey:@"photo_a"];
                                  }
                                  if (photo_b_image)
                                  {
                                      [post_info setObject: photo_b_image forKey:@"photo_b"];
                                  }
                                  [[SlideNavigationController sharedInstance] post_submitted:post_info];
                                  
                                  //[self post_submitted:post_info];
                                  
                              }];
                         }
                         
                     });
                 }
                 
             }];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@" Your outfits were not submitted. Please try again." delegate:nil cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            alert.tag = 1;
            [alert show];
            
        }       
        
    }
}

-(void)facebook_pressed
{
    if (!facebook_bool)
    {
        UIImageView * button_bg = [[UIImageView alloc] initWithFrame:CGRectMake(-6, -5, 117/2.6, 113/2.6)];
        button_bg.image = [UIImage imageNamed:@"button_pressed_gray_behind_social_buttons.png"];
        button_bg.tag = 100;
        [facebook_button addSubview:button_bg];
        [facebook_button bringSubviewToFront:facebook_button.imageView];
    }
    else
    {
        for (UIView * to_remove in facebook_button.subviews)
        {
            if (to_remove.tag == 100)
            {
                [to_remove removeFromSuperview];
            }
        }
    }
    facebook_bool = !facebook_bool;
    
    
}
-(void)create_facebook_image:(NSNumber*)post_id
{
    if (!share_link)
    {
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData * image_data = UIImageJPEGRepresentation(image, .5);
        NSString * base64 = [image_data base64EncodedStringWithOptions:0];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"CreateOutfit:/utility/uploadImage64/: response data is nil");
                 });
             }
             else
             {
                 //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                 //NSLog(@"data back from pic upload = %@" ,test);
                 
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"upload = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         [self share_to_facebook:post_id :[response_dict objectForKey:@"link"]];
                         if (!share_link)
                         {
                             share_link = [response_dict objectForKey:@"link"];
                         }
                         
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Error"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                         
                     }
                     
                 });
             }
             
         }];

    }
}
-(void)share_to_facebook:(NSNumber*)post_id :(NSString*)link
{
    [Flurry logEvent:@"share facebook create post"];
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        // permission does not exist
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                 {
                     if (error)
                     {
                         NSLog(@"error:%@",error);
                     }
                     else
                     {
                         
                         NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                        @"Help me pick an outfit!", @"name",
                                                        @"Which outfit do you prefer?", @"description",
                                                        [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",post_id], @"link",
                                                        link, @"picture",
                                                        nil];
                         
                         // Make the request
                         [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                      parameters:params
                                                      HTTPMethod:@"POST"
                                               completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                   if (!error)
                                                   {
                                                       // Link posted successfully to Facebook
                                                       NSLog(@"result: %@", result);
                                                       if (pintrest_bool)
                                                       {
                                                           [self create_pintrest_image:post_id];
                                                       }

                                                   }
                                                   else
                                                   {
                                                       // An error occurred, we need to handle the error
                                                       // See: https://developers.facebook.com/docs/ios/errors
                                                       NSLog(@"%@", error.description);
                                                   }
                                                   
                                                   
                                               }];
                         
                     }
                     
                 }];
             }
             
         } ];
        
        
    }
    else
    {
        // permission exists
        
        
        // If the Facebook app is installed and we can present the share dialog
        
        // Present the share dialog
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 
                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @"Help me pick an outfit!", @"name",
                                                @"Which outfit do you prefer?", @"description",
                                               [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",post_id], @"link",
                                                link, @"picture",
                                                nil];
                 
                 // Make the request
                 [FBRequestConnection startWithGraphPath:@"/me/feed"
                                              parameters:params
                                              HTTPMethod:@"POST"
                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                                        {
                                           if (!error)
                                           {
                                               // Link posted successfully to Facebook
                                               NSLog(@"result: %@", result);
                                               if (pintrest_bool)
                                               {
                                                   [self create_pintrest_image:post_id];
                                               }

                                           }
                                           else
                                           {
                                               // An error occurred, we need to handle the error
                                               // See: https://developers.facebook.com/docs/ios/errors
                                               NSLog(@"%@", error.description);
                                           }
                                           
                                           
                                       }];
                 
             }
             
         }];
        
        
        
        
    }   
}
-(void)pintrest_pressed
{
    if (!pintrest_bool)
    {
        UIImageView * button_bg = [[UIImageView alloc] initWithFrame:CGRectMake(-6, -5, 117/2.6, 113/2.6)];
        button_bg.image = [UIImage imageNamed:@"button_pressed_gray_behind_social_buttons.png"];
        button_bg.tag = 100;
        [pintrest_button addSubview:button_bg];
        [pintrest_button bringSubviewToFront:pintrest_button.imageView];
    }
    else
    {
        for (UIView * to_remove in pintrest_button.subviews)
        {
            if (to_remove.tag == 100)
            {
                [to_remove removeFromSuperview];
            }
        }
    }
    pintrest_bool = !pintrest_bool;
    
    
}
-(void)create_pintrest_image:(NSNumber*)post_id
{
    [Flurry logEvent:@"share pinterest create post"];
    if (!share_link)
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData * image_data = UIImageJPEGRepresentation(image , .5);
        NSString * base64 = [image_data base64EncodedStringWithOptions:0];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"CreateOutfit:/utility/uploadImage64/: response data is nil");
                 });
             }
             else
             {
                 //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                 //NSLog(@"data back from pic upload = %@" ,test);
                 
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"upload = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         [self share_to_pintrest:post_id :[response_dict objectForKey:@"link"]];
                         if (!share_link)
                         {
                             share_link = [response_dict objectForKey:@"link"];
                         }
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Error"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                     }
                 });
             }
             
         }];
    }
    else
    {
        [self share_to_pintrest:post_id :share_link];
    }
}
-(void)share_to_pintrest:(NSNumber*)post_id :(NSString*)link
{
    Pinterest *  pinterest = [[Pinterest alloc] initWithClientId:@"1440608" urlSchemeSuffix:@"prod"];
    [pinterest createPinWithImageURL:[NSURL URLWithString:link]
                            sourceURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",post_id]]
                          description:@"Which outfit do you prefer? Click on the photo to vote."];
    
}
-(void)twitter_pressed
{
    if (!twitter_bool)
    {
        UIImageView * button_bg = [[UIImageView alloc] initWithFrame:CGRectMake(-6, -4, 117/2.6, 113/2.6)];
        button_bg.image = [UIImage imageNamed:@"button_pressed_gray_behind_social_buttons.png"];
        button_bg.tag = 100;
        [twitter_button addSubview:button_bg];
        [twitter_button bringSubviewToFront:twitter_button.imageView];
    }
    else
    {
        for (UIView * to_remove in twitter_button.subviews)
        {
            if (to_remove.tag == 100)
            {
                [to_remove removeFromSuperview];
            }
        }
    }
    twitter_bool = !twitter_bool;
}
- (void)TWPostImage:(UIImage *)image withStatus:(NSString *)status
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    [Flurry logEvent:@"share twitter create post"];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //[accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
    {
        ACAccount *ac;
        if(granted) {
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            int i=0;
            for (ACAccount *account in accountsArray ) {
                i++;
                //NSLog(@"Account name: %@", account.username);
                ac=account;
            }
            if (i==0) {
                [[[UIAlertView alloc] initWithTitle:@"Wait"
                                            message:@"Please setup Twitter Account Settigns > Twitter > Sign In "
                                           delegate:nil
                                  cancelButtonTitle:@"Thanks!"
                                  otherButtonTitles:nil]
                 show];
                return ;
                
            }
            
            ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];;
            
            //SLRequestHandler requestHandler;
            
            SLRequestHandler requestHandler =
            ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (responseData) {
                        NSInteger statusCode = urlResponse.statusCode;
                        if (statusCode >= 200 && statusCode < 300)
                        {
                            NSDictionary *postResponseData =
                            [NSJSONSerialization JSONObjectWithData:responseData
                                                            options:NSJSONReadingMutableContainers
                                                              error:NULL];
                            NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
                            //AppDelegate * d =[[UIApplication sharedApplication] delegate];
                            //NSString* link = [NSString stringWithFormat:@"Your Message %@", @"testing post"];
                            //[d linkSelected:link PointerToSelf:self];
                            /*[[[UIAlertView alloc] initWithTitle:@"Result"
                             message:[NSString stringWithFormat:@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]]
                             delegate:nil
                             cancelButtonTitle:@"Thanks!"
                             otherButtonTitles:nil]
                             show];*/
                            
                        }
                        else
                        {
                            NSLog(@"[ERROR] Server responded: status code %ld %@", statusCode,
                                  [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
                        }
                    }
                    else {
                        NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
                    }
                });
            };
            //});
            ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
            ^(BOOL granted, NSError *error) {
                if (granted) {
                    NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
                    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                                  @"/1.1/statuses/update_with_media.json"];
                    NSDictionary *params = @{@"status" : status};
                    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                            requestMethod:SLRequestMethodPOST
                                                                      URL:url
                                                               parameters:params];
                    NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
                    [request addMultipartData:imageData
                                     withName:@"media[]"
                                         type:@"image/jpeg"
                                     filename:@"image.jpg"];
                    [request setAccount:[accounts lastObject]];
                    
                    [request performRequestWithHandler:requestHandler];
                    //});
                }
                else {
                    NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                          [error localizedDescription]);
                }
            };
            
            [accountStore requestAccessToAccountsWithType:twitterType
                                                  options:NULL
                                               completion:accountStoreHandler];
        }else
        {
            [[[UIAlertView alloc] initWithTitle:@"Wait"
                                        message:@"Please Settigns > Twitter > In bottom Enable DealsHype to post"
                                       delegate:nil
                              cancelButtonTitle:@"Thanks!"
                              otherButtonTitles:nil]
             show];
        }
    }];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    
    CGRect frame = occasion_picker.frame;
    CGRect selectorFrame = CGRectInset( frame, 0.0, occasion_picker.bounds.size.height * 0.85 / 2.0 );
    
    if( CGRectContainsPoint( selectorFrame, touchPoint))
    {
        //NSLog( @"Selected Row: %@", [occasions objectAtIndex:[occasion_picker selectedRowInComponent:0]] );
        [occasion_picker removeFromSuperview];
        [self.view removeGestureRecognizer:tapRecognizer];
        photo_a.enabled = YES;
        photo_b.enabled = YES;
    }
}
// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return occasions.count;
}
// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Occasion * to_return = [occasions objectAtIndex:row];
    return to_return.name;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        tView.font = [UIFont fontWithName:@"Neutraface Text" size:18];
        Occasion * to_return = [occasions objectAtIndex:row];
        tView.textAlignment = NSTextAlignmentCenter;
        tView.text = to_return.name;
    }
    return tView;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    Occasion * choosen = [occasions objectAtIndex:row];
    selected_occasion = choosen;
    [choose_occasion setTitle:choosen.name forState:UIControlStateNormal];
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    
    return 35;
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer
{
    [occasion_picker removeFromSuperview];
    [self.view removeGestureRecognizer:tapRecognizer];
    photo_a.enabled = YES;
    photo_b.enabled = YES;
    
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}
@end

//
//  PopularView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "AQGridView.h"
#import "PopularOutfit.h"

@interface PopularView : UIViewController <AQGridViewDataSource, AQGridViewDelegate>
{
    UIButton * closet_button;
    UIButton * menu_button;
    UILabel * top_label;
    UIImageView * top_background;
    AQGridView * popular_table;
    NSMutableArray * outfits;    
}
-(void)api_call:(NSString*)to_call;
@end

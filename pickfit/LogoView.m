//
//  LogoView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/10/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "LogoView.h"
#import "AppDelegate.h"

@implementation LogoView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    background.image = [UIImage imageNamed:@"screen_splash.png"];
    [self.view addSubview:background];
    
    logo_pngs = (NSMutableArray*)[self animationWith:@"PICKFIT FINAL LOGO_animation_" andimageCount:28];
    logo_animation = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-540/4, 45, 540/2, 540/2)];
    logo_animation.animationImages = logo_pngs;
    logo_animation.image = [UIImage imageNamed:@"PICKFIT FINAL LOGO_animation_29.png"];
    logo_animation.animationDuration = 1.5;
    logo_animation.animationRepeatCount = 1;
    logo_animation.backgroundColor = [UIColor clearColor];
    [self.view addSubview:logo_animation];
    [logo_animation startAnimating];
    
    [self performSelector:@selector(done) withObject:nil afterDelay:2.5];

    
    
}
- (NSArray *)animationWith:(NSString *) name andimageCount: (int) imageCount
{
    @autoreleasepool
    {
    
        NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:imageCount];
        
        for(int i=0; i <=imageCount; i++)
        {
            NSString *imgLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%d",name,i] ofType:@"png"];
            UIImage *frameImage = [UIImage imageWithContentsOfFile:imgLocation];
            [tempArray addObject:frameImage];
        }
        return (tempArray);
    }
}
-(void)done
{
    [self initLocalVariableForNotificationsIfNeed];
    
    if(self.go_home)
    {
        ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view = [[HomeView alloc] init];
        [[SlideNavigationController sharedInstance] pushHomeViewController];
        [logo_pngs removeAllObjects];
        logo_pngs = nil;
    }
    else
    {
        OpeningView * to_push = [[OpeningView alloc] init];
        [[SlideNavigationController sharedInstance] pushViewController:to_push animated:YES];
        [logo_pngs removeAllObjects];
        logo_pngs = nil;
    }
}

- (void)initLocalVariableForNotificationsIfNeed
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[[userDefaults dictionaryRepresentation] allKeys] containsObject:@"pickfitEnableNotifications"])
    {
        /*BOOL enableNotifications = [userDefaults objectForKey:@"pickfitEnableNotifications"];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications])
            {
                enableNotifications = NO;
            }
        }
        else
        {
            if ([[UIApplication sharedApplication] enabledRemoteNotificationTypes] == UIRemoteNotificationTypeNone)
            {
                enableNotifications = NO;
            }
        }
        
        [userDefaults setObject:[NSNumber numberWithBool:enableNotifications] forKey:@"pickfitEnableNotifications"];
        [userDefaults synchronize];*/
    }
    else
    {
        BOOL enableNotifications = NO;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            enableNotifications = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
        }
        else
        {
            enableNotifications = [[UIApplication sharedApplication] enabledRemoteNotificationTypes] != UIRemoteNotificationTypeNone;
        }
        
        [userDefaults setObject:[NSNumber numberWithBool:enableNotifications] forKey:@"pickfitEnableNotifications"];
        [userDefaults synchronize];
    }
}

@end

//
//  ForgotPasswordView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/15/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface ForgotPasswordView : UIViewController<UITextFieldDelegate>
{
    UIImageView * background;
    UIButton * back_button;
    UILabel * forgot_password;
    UITextField * username;
    UITextField * email;
    UILabel * top_label;
    UILabel * info_label;
    UIButton * submit;
}
@end

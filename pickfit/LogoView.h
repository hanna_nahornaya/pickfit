//
//  LogoView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/10/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeView.h"
#import "OpeningView.h"
#import "SlideNavigationController.h"

@interface LogoView : UIViewController
{
    UIImageView * background;
    UIImageView * logo_animation;
    NSMutableArray * logo_pngs;

}
@property BOOL go_home;
@end

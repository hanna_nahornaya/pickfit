//
//  CreateOutfitView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "FDTakeController.h"
#import "PECropViewController.h"
#import "Occasion.h"
#import <Twitter/Twitter.h>


@interface CreateOutfitView : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, PECropViewControllerDelegate, UIGestureRecognizerDelegate>
{
    UIButton * x;
    UILabel * top_label;
    UIImageView * top_background;
    UIButton * photo_a;
    UIButton * photo_b;
    UILabel * occasion_label;
    UIImageView * line;
    UIButton * choose_occasion;
    UIPickerView * occasion_picker;
    NSMutableArray * occasions;
    UILabel * share_to_label;
    UIButton * facebook_button;
    UIButton * pintrest_button;
    UIButton * twitter_button;
    UIButton * submit;
    UIGestureRecognizer * tapRecognizer;
    UIScrollView * content_scroll;
    
    BOOL facebook_bool;
    BOOL pintrest_bool;
    BOOL twitter_bool;
    BOOL user_pressed_a;
    BOOL user_pressed_b;
    BOOL uploading_a;
    BOOL uploading_b;
    
    NSString * link_a;
    NSString * link_b;
    NSString * share_link;
    
    Occasion * selected_occasion;
    
    NSMutableArray * a_tags;
    NSMutableArray * b_tags;
    
    UIView * to_render;
    
}
@property FDTakeController *takeController;
@property (nonatomic) UIPopoverController *popover;

@end

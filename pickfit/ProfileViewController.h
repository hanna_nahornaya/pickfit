//
//  ProfileViewController.h
//  pickfit
//
//  Created by ann on 3/16/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface ProfileViewController : UIViewController

- (id)initWithUser:(User *)user;

@end

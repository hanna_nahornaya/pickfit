//
//  HomeView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
//#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
//#import "PhotoDetailsView.h"
//#import "Post.h"
#import "RTSpinKitView.h"
//#import "UIViewController+MaryPopin.h"
//#import <FacebookSDK/FacebookSDK.h>
//#import "SVModalWebViewController.h"
#import "TutorialView.h"

@interface HomeView : UIViewController
{
    UIButton * closet_button;
    UIButton * menu_button;
    
    UIImageView * logo;
    UIImageView * top_background;
    UIImageView * top_prompt;
    UILabel * top_prompt_label;
    UIImageView * bottom_background;
    UIImageView * occasion_background;
    UIImageView * occasion_hanger;
    
    UIView * check_mark_layer_a;
    UIView * check_mark_layer_b;
    
    UILabel * this_outfit_label_a;
    UILabel * this_outfit_label_b;
    
    UIImageView * this_outfit_check_a;
    UIImageView * this_outfit_check_b;
    
    UIImageView * the_frame_for_photo_a;
    UIImageView * the_frame_for_photo_b;
    
    UIButton * photo_stack_a;
    UIButton * photo_stack_b;
    
    UIButton * choose_a;
    UIButton * choose_b;
    
    UILabel * occasion;
    
    UIImageView * temp;
    UIImageView * temp2;
    
    NSString * url_a;
    NSString * url_b;   
    
    int index_of_photo_shown;
    
    RTSpinKitView * activity_a;
    RTSpinKitView * activity_b;

    BOOL waiting_for_images_to_load;
    
    UIImageView * just_posted_photo_a;
    UIImageView * just_posted_photo_b;
    NSNumber * just_posted_post_id;
    
    int total_post_shown;
    
    UIImageView *emptyClosetImageView;
}

@property (strong, nonatomic) NSMutableArray * posts;
@property (strong, nonatomic) NSMutableArray * sponsored_posts;

@property (readwrite) BOOL isShowRandomPost;

-(void)api_call:(NSString*)to_call;

@end

//
//  PhotoDetailsView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/7/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "PhotoDetailsView.h"
#import "Flurry.h"

@implementation PhotoDetailsView
@synthesize photo;
@synthesize percent;
@synthesize occasion_label;
//@synthesize is_flagged;
@synthesize url_for_image;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(open_product:) name:@"open_product" object:nil];
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        x  = [[UIButton alloc] initWithFrame:CGRectMake(10, 22, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button-x_white.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: x];
        
        flag  = [[UIButton alloc] initWithFrame:CGRectMake(280, 22, 60/2, 64/2)];
        [flag setImage:[UIImage imageNamed:@"icon_flag.png"] forState:UIControlStateNormal];
        [flag setImage:[UIImage imageNamed:@"icon_flag_red.png"] forState:UIControlStateSelected];
        [flag addTarget:self action:@selector(flag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: flag];
                
        [self addUserNameLabel];
        
        photo_scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 74, self.view.frame.size.width, 400)];
        photo_scroll.backgroundColor = [UIColor clearColor];
        photo_scroll.showsHorizontalScrollIndicator = NO;
        photo_scroll.showsVerticalScrollIndicator = NO;
        photo_scroll.maximumZoomScale = 3.0;
        photo_scroll.minimumZoomScale = 1.0;
        photo_scroll.delegate = self;
        [self.view addSubview:photo_scroll];
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(photo_scroll.frame.size.width/2-261/3.6, photo_scroll.frame.size.height/2-720/3.6, 261/1.8, 720/1.8)];
        photo.contentMode = UIViewContentModeScaleAspectFit;
        photo_scroll.contentMode = UIViewContentModeCenter;
        [photo_scroll addSubview:photo];
        
        occasion_label = [[UILabel alloc] initWithFrame:CGRectMake(30, 490, 250, 20)];
        occasion_label.font = [UIFont fontWithName:@"Neutraface Text" size:14];
        occasion_label.textAlignment = NSTextAlignmentLeft;
        occasion_label.text = @"";
        occasion_label.textColor = [UIColor whiteColor];
        [self.view addSubview:occasion_label];
        
        line = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-565/4, 515, 565/2, 15/2)];
        line.image = [UIImage imageNamed:@"dots_gray_line.png"];
        [self.view addSubview:line];
        
        favorite  = [[UIButton alloc] initWithFrame:CGRectMake(20, 522, 75/2, 71/2)];
        [favorite setImage:[UIImage imageNamed:@"icon_star.png"] forState:UIControlStateNormal];
        [favorite setImage:[UIImage imageNamed:@"icon_star_green2.png"] forState:UIControlStateSelected];
        [favorite addTarget:self action:@selector(favorite_pressed) forControlEvents:UIControlEventTouchUpInside];
        favorite.adjustsImageWhenHighlighted = NO;
        [self.view addSubview: favorite];
        
        share  = [[UIButton alloc] initWithFrame:CGRectMake(65, 522, 68/2, 71/2)];
        [share setImage:[UIImage imageNamed:@"icon_send_white.png"] forState:UIControlStateNormal];
        [share addTarget:self action:@selector(share_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: share];
        
        tags_buttons  = [[UIButton alloc] initWithFrame:CGRectMake(110, 522, 74/2, 68/2)];
        [tags_buttons setImage:[UIImage imageNamed:@"icon_tag.png"] forState:UIControlStateNormal];
        [tags_buttons addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tags_buttons];
        
        [self addFollowButton];
        
        percent = [[UILabel alloc] initWithFrame:CGRectMake(280, 530, 40, 20)];
        percent.font = [UIFont fontWithName:@"Neutraface Text" size:12];
        percent.textAlignment = NSTextAlignmentLeft;
        percent.text = @"";
        percent.textColor = [UIColor whiteColor];
        //[self.view addSubview:percent];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        
        [doubleTap setNumberOfTapsRequired:2];
        
        [photo_scroll addGestureRecognizer:doubleTap];
        
        is_favorited = NO;
        
        showing_tags = NO;

        
    }
    else
    {
        x  = [[UIButton alloc] initWithFrame:CGRectMake(10, 22, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button-x_white.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: x];
        
        flag  = [[UIButton alloc] initWithFrame:CGRectMake(280, 22, 60/2, 64/2)];
        [flag setImage:[UIImage imageNamed:@"icon_flag.png"] forState:UIControlStateNormal];
        [flag setImage:[UIImage imageNamed:@"icon_flag_red.png"] forState:UIControlStateSelected];
        [flag addTarget:self action:@selector(flag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: flag];
        
        [self addUserNameLabel];
        
        photo_scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 74, self.view.frame.size.width, 400-84)];
        photo_scroll.backgroundColor = [UIColor clearColor];
        photo_scroll.showsHorizontalScrollIndicator = NO;
        photo_scroll.showsVerticalScrollIndicator = NO;
        photo_scroll.maximumZoomScale = 3.0;
        photo_scroll.minimumZoomScale = 1.0;
        photo_scroll.delegate = self;
        [self.view addSubview:photo_scroll];
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(photo_scroll.frame.size.width/2-261/4.0, photo_scroll.frame.size.height/2-720/4.0, 261/2, 720/2)];
        photo.contentMode = UIViewContentModeScaleAspectFit;
        photo_scroll.contentMode = UIViewContentModeCenter;
        [photo_scroll addSubview:photo];
        
        occasion_label = [[UILabel alloc] initWithFrame:CGRectMake(30, 490-84, 250, 20)];
        occasion_label.font = [UIFont fontWithName:@"Neutraface Text" size:14];
        occasion_label.textAlignment = NSTextAlignmentLeft;
        occasion_label.text = @"";
        occasion_label.textColor = [UIColor whiteColor];
        [self.view addSubview:occasion_label];
        
        line = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-565/4, 515-84, 565/2, 15/2)];
        line.image = [UIImage imageNamed:@"dots_gray_line.png"];
        [self.view addSubview:line];
        
        favorite  = [[UIButton alloc] initWithFrame:CGRectMake(20, 522-84, 75/2, 71/2)];
        [favorite setImage:[UIImage imageNamed:@"icon_star.png"] forState:UIControlStateNormal];
        [favorite setImage:[UIImage imageNamed:@"icon_star_green2.png"] forState:UIControlStateSelected];
        [favorite addTarget:self action:@selector(favorite_pressed) forControlEvents:UIControlEventTouchUpInside];
        favorite.adjustsImageWhenHighlighted = NO;
        [self.view addSubview: favorite];
        
        share  = [[UIButton alloc] initWithFrame:CGRectMake(65, 522-84, 68/2, 71/2)];
        [share setImage:[UIImage imageNamed:@"icon_send_white.png"] forState:UIControlStateNormal];
        [share addTarget:self action:@selector(share_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: share];
        
        tags_buttons  = [[UIButton alloc] initWithFrame:CGRectMake(110, 522-84, 74/2, 68/2)];
        [tags_buttons setImage:[UIImage imageNamed:@"icon_tag.png"] forState:UIControlStateNormal];
        [tags_buttons addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tags_buttons];
        
        [self addFollowButton];
        
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        
        [doubleTap setNumberOfTapsRequired:2];
        
        [photo_scroll addGestureRecognizer:doubleTap];
        
        is_favorited = NO;
        
        showing_tags = NO;

    }
}

- (void)addUserNameLabel
{
    self.username_label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(x.frame) + 5, CGRectGetMinY(x.frame), CGRectGetMinX(flag.frame) - CGRectGetMaxX(x.frame) - 5 * 2, x.frame.size.height)];
    self.username_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    self.username_label.textAlignment = NSTextAlignmentCenter;
    self.username_label.text = @"";
    self.username_label.textColor = [UIColor whiteColor];
    self.username_label.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.username_label];
}

- (void)addFollowButton
{
    follow_button = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 200/2 - CGRectGetMinX(favorite.frame), CGRectGetMinY(favorite.frame), 200/2, 70/2)];
    
    [follow_button addTarget:self action:@selector(follow_button_pressed) forControlEvents:UIControlEventTouchUpInside];
    [follow_button setTitle:NSLocalizedString(@"Follow", @"") forState:UIControlStateNormal];
    [follow_button setTitle:NSLocalizedString(@"Following", @"") forState:UIControlStateSelected];
    [follow_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[follow_button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    follow_button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:13];
    follow_button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    follow_button.adjustsImageWhenHighlighted = NO;
//    follow_button.layer.borderColor = [UIColor whiteColor].CGColor;
//    follow_button.layer.borderWidth = 2.0;
    follow_button.layer.cornerRadius = 5.0;
    follow_button.layer.masksToBounds = YES;
    [follow_button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
    follow_button.titleEdgeInsets = UIEdgeInsetsZero;
    follow_button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 7);
    follow_button.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:follow_button];
}

//-(void)setSelectedFollowButton:(BOOL)selected
//{
//    [follow_button setSelected:selected];
////    follow_button.backgroundColor = selected ? [UIColor whiteColor] : [UIColor clearColor];
//    
//}

-(void)load_photo
{
    [self.photo sd_setImageWithURL:[NSURL URLWithString:self.url_for_image] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
     {
         self.photo.alpha = 0.0;
         [UIView animateWithDuration:0.3
                          animations:^
          {
              self.photo.alpha = 1.0;
          }];
         
     }];
}

-(void)open_product:(NSNotification*)message
{
    PSSProduct * to_open = message.object;
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:to_open.buyURL];
    
    [self presentViewController:webViewController animated:YES completion:^{
        
    }];
    
}
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer
{
    if(photo_scroll.zoomScale > photo_scroll.minimumZoomScale)
    {
        [photo_scroll setZoomScale:photo_scroll.minimumZoomScale animated:YES];
    }
    else
    {
        CGPoint touch = [gestureRecognizer locationInView:gestureRecognizer.view];
        
        CGSize scrollViewSize = photo_scroll.bounds.size;
        
        CGFloat w = scrollViewSize.width / photo_scroll.maximumZoomScale;
        CGFloat h = scrollViewSize.height / photo_scroll.maximumZoomScale;
        CGFloat x2 = touch.x-(w/2.0);
        CGFloat y = touch.y-(h/2.0);
        
        CGRect rectTozoom=CGRectMake(x2, y, w, h);
        [photo_scroll zoomToRect:rectTozoom animated:YES];
    
    }
}

- (void)favorite_pressed
{
    if (favorite.selected)
    {
        [self api_call:@"un_favorite"];
    }
    else
    {
        [self api_call:@"favorite"];
        [Flurry logEvent:@"favorite pressed"];
    }
}

-(void)tag_pressed
{
    [Flurry logEvent:@"tag pressed"];
    if (showing_tags)
    {
        for (TagLabel * to_use in photo.subviews)
        {
            if ([to_use isKindOfClass:[TagLabel class]])
            {
                to_use.hidden = YES;
            }
        }
    }
    else
    {
        int count = 0;
        for (TagLabel * to_use in photo.subviews)
        {
            if ([to_use isKindOfClass:[TagLabel class]])
            {
                to_use.hidden = NO;
                count++;
            }
        }
        if (count == 0)
        {
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"There are no clothing items tagged for this outfit."
                                       delegate:nil
                              cancelButtonTitle:@"Thanks!"
                              otherButtonTitles:nil] show];
        }
    }
    showing_tags = !showing_tags;
}
-(void)share_pressed
{
    UIViewController * popin = [[UIViewController alloc] init];
    popin.view.backgroundColor = [self colorWithHexString:@"9ad325"];
    [popin setPreferedPopinContentSize:CGSizeMake(280.0, 160.0)];
    
    UILabel * share_prompt = [[UILabel alloc] initWithFrame:CGRectMake(-20, 10, popin.view.frame.size.width, 22)];
    share_prompt.text = @"Share this with your friends:";
    share_prompt.font =[UIFont fontWithName:@"Neutraface Text" size:20];
    share_prompt.textColor = [UIColor whiteColor];
    share_prompt.textAlignment = NSTextAlignmentCenter;
    [popin.view addSubview:share_prompt];
    
    UIButton * text_button  = [[UIButton alloc] initWithFrame:CGRectMake(20, 50, 134/2.1, 76/2.1)];
    [text_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [text_button setTitle:@"Text" forState:UIControlStateNormal];
    [[text_button layer] setBorderWidth:1.0f];
    [[text_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    text_button.layer.cornerRadius = 2;
    text_button.clipsToBounds = YES;
    [text_button addTarget:self action:@selector(open_text_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: text_button];
    
    UIButton * email_button  = [[UIButton alloc] initWithFrame:CGRectMake(88, 50, 143/2.1, 76/2.1)];
    [email_button setTitle:@"Email" forState:UIControlStateNormal];
    [[email_button layer] setBorderWidth:1.0f];
    [[email_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    email_button.layer.cornerRadius = 2;
    email_button.clipsToBounds = YES;
    [email_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [email_button addTarget:self action:@selector(open_email_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: email_button];
    
    UIButton * facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(160, 50, 200/2.0, 76/2.1)];
    [facebook_button setTitle:@"Facebook" forState:UIControlStateNormal];
    [[facebook_button layer] setBorderWidth:1.0f];
    [[facebook_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    facebook_button.layer.cornerRadius = 2;
    facebook_button.clipsToBounds = YES;
    [facebook_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [facebook_button addTarget:self action:@selector(share_facebook) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: facebook_button];
    
    UIButton * pintrest_button  = [[UIButton alloc] initWithFrame:CGRectMake(35, 100, 200/2.0, 76/2.1)];
    [pintrest_button setTitle:@"Pinterest" forState:UIControlStateNormal];
    [[pintrest_button layer] setBorderWidth:1.0f];
    [[pintrest_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    pintrest_button.layer.cornerRadius = 2;
    pintrest_button.clipsToBounds = YES;
    [pintrest_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [pintrest_button addTarget:self action:@selector(share_pintrest) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: pintrest_button];
    
    UIButton * twitter_button  = [[UIButton alloc] initWithFrame:CGRectMake(145, 100, 200/2.0, 76/2.1)];
    [twitter_button setTitle:@"Twitter" forState:UIControlStateNormal];
    [[twitter_button layer] setBorderWidth:1.0f];
    [[twitter_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    twitter_button.layer.cornerRadius = 2;
    twitter_button.clipsToBounds = YES;
    [twitter_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [twitter_button addTarget:self action:@selector(share_twitter) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: twitter_button];
    
    [popin setPopinTransitionStyle:2];
    
    BKTBlurParameters *blurParameters = [BKTBlurParameters new];
    blurParameters.alpha = 1.0f;
    blurParameters.radius = 8.0f;
    blurParameters.saturationDeltaFactor = 1.8f;
    blurParameters.tintColor = [UIColor colorWithRed:0.966 green:0.851 blue:0.038 alpha:0.2];
    [popin setBlurParameters:blurParameters];
    
    //Add option for a blurry background
    [popin setPopinOptions:[popin popinOptions] |BKTPopinBlurryDimmingView];
    
    [Flurry logEvent:@"share outfit details"];
    
    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    [self presentPopinController:popin animated:YES completion:^{
        NSLog(@"Popin presented !");
    }];
}

- (void)follow_button_pressed
{
    if (follow_button.isSelected)
    {
        [self api_call:@"deleteFollowerByOutfit"];
    }
    else
    {
        [self api_call:@"createFollowerByOutfit"];
    }
}

-(void)open_text_message
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.body =  [NSString stringWithFormat:@"Check out this outfit I found on Pickfit! Visit http://www.pickfitapp.com to find more outfits."];
        controller.messageComposeDelegate = self;
        NSData * imgData = UIImagePNGRepresentation(photo.image);
        [controller addAttachmentData:imgData typeIdentifier:@"public.content" filename:@"image.png"];
        [self presentViewController:controller animated:YES completion:nil];
        
         [Flurry logEvent:@"share text outfit details"];
    }
}
-(void)open_email_message
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        NSData * image_data = UIImagePNGRepresentation(photo.image);
        [mailCont addAttachmentData:image_data mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"a.jpg"]];
        
        [mailCont setSubject:@"Check out this outfit I found on Pickfit"];
        [mailCont setMessageBody: @"Visit http://pickfitapp.com to download the Pickfit app" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
        
        [Flurry logEvent:@"share email outfit details"];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"We do not have access to your mail app. Please set up your email app in order to send a post via email." delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
-(void)share_facebook
{
    
    UIImage *image = [self imageWithImage:photo.image scaledToWidth:120];

    
    NSData * image_data = UIImageJPEGRepresentation(image, .5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"PhotoDetailsView:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [self share_to_facebook:[response_dict objectForKey:@"link"]];
                     
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];
    
}
-(void)share_to_facebook:(NSString*)url_pic
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        // permission does not exist
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                  {
                      if (error)
                      {
                          NSLog(@"error:%@",error);
                      }
                      else
                      {
                          
                          NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                         @"Check out this outfit I found on Pickfit!", @"name",
                                                         @"Download the Pickfit app to see more.", @"description",
                                                         @"http://pickfitapp.com", @"link",
                                                         url_pic, @"picture",
                                                         nil];
                          
                          // Make the request
                          [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                       parameters:params
                                                       HTTPMethod:@"POST"
                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                    if (!error)
                                                    {
                                                        // Link posted successfully to Facebook
                                                        NSLog(@"result: %@", result);
                                                        [Flurry logEvent:@"share facebook outfit details"];
                                                    }
                                                    else
                                                    {
                                                        // An error occurred, we need to handle the error
                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                        NSLog(@"%@", error.description);
                                                    }
                                                    
                                                    
                                                }];
                          
                      }
                      
                  }];
             }
             
         } ];
        
        
    }
    else
    {
        // permission exists
        
        
        // If the Facebook app is installed and we can present the share dialog
        
        // Present the share dialog
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 
                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @"Check out this outfit I found on Pickfit!", @"name",
                                                @"Download the Pickfit app to see more.", @"description",
                                                @"http://pickfitapp.com", @"link",
                                                url_pic, @"picture",
                                                nil];
                 
                 // Make the request
                 [FBRequestConnection startWithGraphPath:@"/me/feed"
                                              parameters:params
                                              HTTPMethod:@"POST"
                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          // Link posted successfully to Facebook
                          NSLog(@"result: %@", result);
                          [Flurry logEvent:@"share facebook outfit details"];
                      }
                      else
                      {
                          // An error occurred, we need to handle the error
                          // See: https://developers.facebook.com/docs/ios/errors
                          NSLog(@"%@", error.description);
                      }
                      
                      
                  }];
                 
             }
             
         }];
        
    }    
}
-(void)share_twitter
{
    NSString * message = [NSString stringWithFormat:@"Check out this outfit I found on Pickfit, download the Pickfit app at %@", @"http://pickfitapp.com"];
    [self TWPostImage:self.photo.image withStatus:message];
    
    [Flurry logEvent:@"share twitter outfit details"];
}
- (void)TWPostImage:(UIImage *)image withStatus:(NSString *)status
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //[accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
     {
         ACAccount *ac;
         if(granted)
         {
             NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
             int i=0;
             for (ACAccount *account in accountsArray )
             {
                 i++;
                 //NSLog(@"Account name: %@", account.username);
                 ac=account;
             }
             if (i==0)
             {
                 [self dismissCurrentPopinControllerAnimated:YES];
                 
                 [[[UIAlertView alloc] initWithTitle:@"Wait"
                                             message:@"Please setup Twitter Account Settings > Twitter > Sign In "
                                            delegate:nil
                                   cancelButtonTitle:@"Thanks!"
                                   otherButtonTitles:nil]
                  show];
                 return ;
                 
             }
             
             ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
             
             //SLRequestHandler requestHandler;
             
             SLRequestHandler requestHandler =
             ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseData)
                     {
                         NSInteger statusCode = urlResponse.statusCode;
                         if (statusCode >= 200 && statusCode < 300)
                         {
                             NSDictionary *postResponseData =
                             [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:NSJSONReadingMutableContainers
                                                               error:NULL];
                             NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
                             
                         }
                         else
                         {
                             NSLog(@"[ERROR] Server responded: status code %ld %@", statusCode, [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
                         }
                     }
                     else {
                         NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
                     }
                 });
             };
             //});
             ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
             ^(BOOL granted, NSError *error)
             {
                 if (granted)
                 {
                     NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
                     NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                                   @"/1.1/statuses/update_with_media.json"];
                     NSDictionary *params = @{@"status" : status};
                     SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                             requestMethod:SLRequestMethodPOST
                                                                       URL:url
                                                                parameters:params];
                     NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
                     [request addMultipartData:imageData
                                      withName:@"media[]"
                                          type:@"image/jpeg"
                                      filename:@"image.jpg"];
                     [request setAccount:[accounts lastObject]];
                     
                     [request performRequestWithHandler:requestHandler];
                     //});
                 }
                 else
                 {
                     NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                           [error localizedDescription]);
                 }
             };
             
             [accountStore requestAccessToAccountsWithType:twitterType
                                                   options:NULL
                                                completion:accountStoreHandler];
         }
         else
         {
             [self dismissCurrentPopinControllerAnimated:YES];
             
             [[[UIAlertView alloc] initWithTitle:@"Wait"
                                         message:@"Please Settings > Twitter > In bottom Enable "
                                        delegate:nil
                               cancelButtonTitle:@"Thanks!"
                               otherButtonTitles:nil]
              show];
         }
     }];
}
-(void)share_pintrest
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    Pinterest *  pinterest = [[Pinterest alloc] initWithClientId:@"1440608" urlSchemeSuffix:@"prod"];
    [pinterest createPinWithImageURL:[NSURL URLWithString:self.url_for_image]
                           sourceURL: [NSURL URLWithString:@"http://pickfitapp.com"]
                         description:@"Check out this outfit I found on Pickfit, visit the link below to download the app"];
    
    [Flurry logEvent:@"share pinterest outfit details"];
}
-(void)check_favorites
{
    [self api_call:@"is_favorited"];
}
-(void)check_flag
{
    [self api_call:@"isFlagged"];
    
//    if (is_flagged.intValue == 0)
//    {
//        [flag setImage:[UIImage imageNamed:@"icon_flag.png"] forState:UIControlStateNormal];
//    }
//    else
//    {
//        [flag setImage:[UIImage imageNamed:@"icon_flag_red.png"] forState:UIControlStateNormal];
//    }
}
-(void)flag_pressed
{
    if (flag.isSelected)
    {
        [self api_call:@"un_flag"];
    }
    else
    {
        [Flurry logEvent:@"flag outfit"];
        [self api_call:@"flag"];
    }

//    if (is_flagged.intValue == 0)
//    {
//        [Flurry logEvent:@"flag outfit"];
//        [self api_call:@"flag"];
//    }
//    else
//    {
//        [self api_call:@"un_flag"];
//    }
}

-(void)check_following
{
    [self api_call:@"isFollowed"];
}

-(void)check_tags
{
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/getTags/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@", self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"PhotoDetailsView:/outfit/getTags/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from /user/getTags/ = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 if ([status isEqualToString:@"success"])
                 {
                     NSString * tags_array_string = [response_dict objectForKey:@"tags"];
                     //if (![tags_array_string  isEqual:[NSNull null]])
                     if ([tags_array_string isKindOfClass:[NSString class]])
                     {
                         NSData* data_string = [tags_array_string dataUsingEncoding:NSUTF8StringEncoding];
                         NSMutableArray * tags_array = [NSJSONSerialization JSONObjectWithData:data_string options:kNilOptions error:nil];
                         if (tags_array)
                         {
                             //create tags
                             for (NSDictionary * a_tag in tags_array)
                             {
                                 NSNumber * x_p = [a_tag objectForKey:@"x"];
                                 NSNumber * y = [a_tag objectForKey:@"y"];
                                 NSNumber * p_id = [a_tag objectForKey:@"product_id"];
                                 CGPoint point = CGPointMake(x_p.floatValue, y.floatValue);
                                 
                                 
                                 [[PSSClient sharedClient] getProductByID:p_id success:^(PSSProduct *product)
                                  {
                                      PSSProduct * tagged_product = product;
                                      
                                      TagLabel * created_label = [[TagLabel alloc] initWithFrame:CGRectMake(point.x, point.y, 90, 30)];
                                      created_label.font = [UIFont fontWithName:@"Neutraface Text" size:13];
                                      created_label.textAlignment = NSTextAlignmentCenter;
                                      created_label.textColor = [UIColor whiteColor];
                                      created_label.backgroundColor = [self colorWithHexString:@"9ad325"];
                                      created_label.text = tagged_product.name;
                                      created_label.product = tagged_product;
                                      created_label.hidden = YES;
                                      created_label.userInteractionEnabled = YES;
                                      photo.userInteractionEnabled = YES;
                                      [photo addSubview:created_label];
                                  }
                                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                      
                                      
                                      
                                  }];
                                 
                                 
                                 
                             }
                             
                         }

                     }
                     
                 }
                 
             });
         }
         
     }];
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
    
}
-(void)api_call:(NSString*)to_call
{
    if([to_call isEqualToString:@"flag"])
    {
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/flagOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@", self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/flagOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Flagged" message:@"This photo has been flagged for inappropriate content and will be reviewed by Pickfit." delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                         alert.tag = 1;
                         [alert show];
                         
                         //[flag setImage:[UIImage imageNamed:@"icon_flag_red.png"] forState:UIControlStateNormal];
                         //is_flagged = [NSNumber numberWithInt:1];
                         
                         flag.selected = YES;
                     }
                     
                 });
             }
             
         }];
    }
    if([to_call isEqualToString:@"favorite"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/favoriteOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@&user_id=%@", self.outfit_id, [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/favoriteOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 if (response_dict)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSString * status = [response_dict objectForKey:@"status"];
                         
                         if ([status isEqualToString:@"success"])
                         {
                             //is_favorited = YES;
                             //[favorite setImage:[UIImage imageNamed:@"icon_star_green2.png"] forState:UIControlStateNormal];
                             
                             favorite.selected = YES;
                             
                             if ([defaults objectForKey:@"seen_favorites"])
                             {
                                 
                             }
                             else
                             {
                                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Favorited" message:@"You just selected an image to be added to your Favorites." delegate:nil cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil];
                                 alert.tag = 1;
                                 [alert show];
                                 [defaults setObject:@"seen" forKey:@"seen_favorites"];
                             }
                         }
                        else if ([status isEqualToString:@"fail"])
                        {
                            favorite.selected = NO;
                        }
                     });
                 }
                 else
                 {
                     favorite.selected = NO;
                 }
             }
             
         }];
    }
    if([to_call isEqualToString:@"un_favorite"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/unfavoriteOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@&user_id=%@", self.outfit_id, [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/unfavoriteOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         //is_favorited = NO;
                         //[favorite setImage:[UIImage imageNamed:@"icon_star.png"] forState:UIControlStateNormal];
                         
                         favorite.selected = NO;
                         
                         if ([defaults objectForKey:@"seen_unfavorites"])
                         {
                             
                         }
                         else
                         {
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unfavorited" message:@"This outfit has been removed from your favorites section!" delegate:nil cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
                             alert.tag = 1;
                             [alert show];
                             [defaults setObject:@"seen" forKey:@"seen_unfavorites"];
                         }
                     }
                     else if ([status isEqualToString:@"fail"])
                     {
                         favorite.selected =YES;
                     }
                 });
             }
             
         }];
    }
    if([to_call isEqualToString:@"un_flag"])
    {
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/unflagOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@", self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/unflagOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     if ([status isEqualToString:@"success"])
                     {
                         //is_flagged = [NSNumber numberWithBool:NO];
                         //[flag setImage:[UIImage imageNamed:@"icon_flag.png"] forState:UIControlStateNormal];
                         
                         /*if ([[response_dict objectForKey:@"is_favorited"] intValue] == 1)
                         {
                             favorite.selected = YES;
                         }*/

                         
                         flag.selected = NO;
                     }
                     
                 });
             }
             
         }];
    }
    if([to_call isEqualToString:@"is_favorited"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/isFavorited/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@&user_id=%@", self.outfit_id, [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/isFavorited/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     /*NSNumber * is_favorite = [response_dict objectForKey:@"is_favorited"];
                     if (is_favorite.intValue == 1)
                     {
                         [favorite setImage:[UIImage imageNamed:@"icon_star_green2.png"] forState:UIControlStateNormal];
                         is_favorited = YES;
                     }*/
                     
                     if ([[response_dict objectForKey:@"is_favorited"] intValue] == 1)
                     {
                         favorite.selected = YES;
                     }
                 });
             }
             
         }];
    }
    else if ([to_call isEqualToString:@"isFlagged"])
    {
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/isFlagged/", SERVICE_URL]];
        NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@", self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/isFlagged/: response data is nil");
                 });
             }
             else
             {
                 //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                 //NSLog(@"data back from /outfit/isFlagged/ = %@", test);
                 
                 NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 if (responseDictionary)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSString * status = [responseDictionary objectForKey:@"status"];
                         
                         if ([status isEqualToString:@"success"])
                         {
                             flag.selected = [[responseDictionary objectForKey:@"result"] boolValue];;
                         }
                     });
                 }
                 else
                 {
                 }
             }
         }];
    }
    else if ([to_call isEqualToString:@"isFollowed"])
    {
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/isFollowed/", SERVICE_URL]];
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&outfit_id=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/outfit/isFollowed/: response data is nil");
                 });
             }
             else
             {
                 //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                 //NSLog(@"data back from /outfit/isFollowed/ = %@", test);
                 
                 NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 if (responseDictionary)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSString * status = [responseDictionary objectForKey:@"status"];
                         
                         if ([status isEqualToString:@"success"])
                         {
                             follow_button.selected = [[responseDictionary objectForKey:@"result"] boolValue];
                             //[self setSelectedFollowButton:[[responseDictionary objectForKey:@"result"] boolValue]];
                         }
                     });
                 }
                 else
                 {
                 }
             }
         }];
    }
    else if ([to_call isEqualToString:@"createFollowerByOutfit"])
    {
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/follower/createFollowerByOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&outfit_id=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/follower/createFollowerByOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                 
                 if (responseDictionary)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSString * status = [responseDictionary objectForKey:@"status"];
                         
                         if ([status isEqualToString:@"success"])
                         {
                             //[self setSelectedFollowButton:YES];
                             follow_button.selected = YES;
                         }
                         else if ([status isEqualToString:@"fail"])
                         {
                             //[self setSelectedFollowButton:NO];
                             follow_button.selected = NO;
                             
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                             alert.tag = 1;
                             [alert show];
                         }
                     });
                 }
                 else
                 {
                     //[self setSelectedFollowButton:NO];
                     follow_button.selected = NO;
                 }
             }
         }];
    }
    else if ([to_call isEqualToString:@"deleteFollowerByOutfit"])
    {
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/follower/deleteFollowerByOutfit/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&outfit_id=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"PhotoDetailsView:/follower/deleteFollowerByOutfit/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 if (responseDictionary)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         NSString * status = [responseDictionary objectForKey:@"status"];
                         
                         if ([status isEqualToString:@"success"])
                         {
                             //[self setSelectedFollowButton:NO];
                             follow_button.selected = NO;
                         }
                         else if ([status isEqualToString:@"fail"])
                         {
                             //[self setSelectedFollowButton:YES];
                             follow_button.selected = YES;
                             
                             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                             alert.tag = 1;
                             [alert show];
                         }
                     });
                 }
                 else
                 {
                     //[self setSelectedFollowButton:YES];
                     follow_button.selected = YES;
                 }
             }
         }];
    }
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return photo;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
    
}
-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGSize boundsSize = scrollView.bounds.size;
    CGRect frameToCenter = photo.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
    {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else
    {
        frameToCenter.origin.x = 0;
    }
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
    {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else
    {
        frameToCenter.origin.y = 0;
    }
    
    photo.frame = frameToCenter;
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

@end

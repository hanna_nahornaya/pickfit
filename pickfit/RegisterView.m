//
//  RegisterView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "RegisterView.h"
#import "AppDelegate.h"

@implementation RegisterView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"9ad426"];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        background.image = [UIImage imageNamed:@"icons.png"];
        background.alpha = .5;
        [self.view addSubview:background];
        
        back_button = [[UIButton alloc] initWithFrame:CGRectMake(9, 23, 53/2, 70/2)];
        [back_button addTarget:self action:@selector(back_pressed) forControlEvents:UIControlEventTouchUpInside];
        [back_button setImage:[UIImage imageNamed:@"button_back_arrow.png"] forState:UIControlStateNormal];
        [self.view addSubview:back_button];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"Create your account";
        top_label.textColor = [UIColor whiteColor];
        [self.view addSubview:top_label];
        
        username = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 75, 264, 45)];
        username.textAlignment = NSTextAlignmentLeft;
        username.backgroundColor = [UIColor whiteColor];
        username.placeholder = @"  Username";
        UIColor *color = [UIColor lightGrayColor];
        username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:username.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        username.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        username.textColor = [UIColor blackColor];
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        username.delegate = self;
        username.returnKeyType = UIReturnKeyNext;
        username.keyboardType = UIKeyboardTypeASCIICapable;
        username.autocapitalizationType = UITextAutocapitalizationTypeNone;
        username.layer.cornerRadius = 3;
        username.autocorrectionType = UITextAutocorrectionTypeNo;// this value vary as per your desire
        username.clipsToBounds = YES;
        [username setKeyboardAppearance:UIKeyboardAppearanceAlert];
        username.leftView = paddingView;
        username.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview: username];
        
        email = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 135, 264, 45)];
        email.textAlignment = NSTextAlignmentLeft;
        email.backgroundColor = [UIColor whiteColor];
        email.placeholder = @"  E-mail";
        email.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        email.textColor = [UIColor blackColor];
        email.delegate = self;
        email.secureTextEntry = NO;
        email.returnKeyType = UIReturnKeyNext;
        email.keyboardType = UIKeyboardTypeEmailAddress;
        email.autocapitalizationType = UITextAutocapitalizationTypeNone;
        email.autocorrectionType = UITextAutocorrectionTypeNo;
        email.layer.cornerRadius = 3; // this value vary as per your desire
        email.clipsToBounds = YES;
        email.leftView = paddingView2;
        email.leftViewMode = UITextFieldViewModeAlways;
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:email.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [email setKeyboardAppearance:UIKeyboardAppearanceAlert];
        [self.view addSubview: email];
        
        
        UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
        [self.view addGestureRecognizer:tapRecognizer];
        
        password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 195, 264, 45)];
        password.textAlignment = NSTextAlignmentLeft;
        password.backgroundColor = [UIColor whiteColor];
        password.placeholder = @"  Password";
        password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        password.textColor = [UIColor blackColor];
        password.delegate = self;
        password.secureTextEntry = YES;
        password.returnKeyType = UIReturnKeyDone;
        password.keyboardType = UIKeyboardTypeASCIICapable;
        password.autocapitalizationType = UITextAutocapitalizationTypeNone;
        password.autocorrectionType = UITextAutocorrectionTypeNo;
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [password setKeyboardAppearance:UIKeyboardAppearanceAlert];
        password.layer.cornerRadius = 3;
        password.leftView = paddingView3;
        password.leftViewMode = UITextFieldViewModeAlways;// this value vary as per your desire
        password.clipsToBounds = YES;
        [self.view addSubview: password];
        
        i_am = [[UILabel alloc] initWithFrame:CGRectMake(35, 255, 100, 20)];
        i_am.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        i_am.textAlignment = NSTextAlignmentLeft;
        i_am.text = @"I am:";
        i_am.textColor = [UIColor whiteColor];
        [self.view addSubview:i_am];
        
        CGFloat borderWidth = 2.0f;
        
        male_am = [[UIButton alloc] initWithFrame:CGRectMake(30, 285, 130, 50)];
        male_am.layer.borderWidth = borderWidth;
        male_am.layer.borderColor = [UIColor whiteColor].CGColor;
        [male_am addTarget:self action:@selector(male_am_pressed) forControlEvents:UIControlEventTouchUpInside];
        [male_am setTitle:[NSString stringWithFormat:@"%@", @"Male"] forState:UIControlStateNormal];
        male_am.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        male_am.titleLabel.textColor = [UIColor blackColor];
        [male_am setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [male_am setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        male_am.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:male_am];
        
        male_am_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, male_am.frame.size.width, male_am.frame.size.height)];
        male_am_background.backgroundColor = [UIColor whiteColor];
        male_am_background.alpha = 0.0;
        [male_am addSubview:male_am_background];
        [male_am bringSubviewToFront:male_am.titleLabel];
        
        
        female_am = [[UIButton alloc] initWithFrame:CGRectMake(160, 285, 130, 50)];
        female_am.layer.borderWidth = borderWidth;
        female_am.layer.borderColor = [UIColor whiteColor].CGColor;
        [female_am addTarget:self action:@selector(female_am_pressed) forControlEvents:UIControlEventTouchUpInside];
        [female_am setTitle:[NSString stringWithFormat:@"%@", @"Female"] forState:UIControlStateNormal];
        female_am.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        female_am.titleLabel.textColor = [UIColor blackColor];
        [female_am setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [female_am setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        female_am.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:female_am];
        
        female_am_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, female_am.frame.size.width, female_am.frame.size.height)];
        female_am_background.backgroundColor = [UIColor whiteColor];
        female_am_background.alpha = 1.0;
        [female_am addSubview:female_am_background];
        [female_am bringSubviewToFront:female_am.titleLabel];
        
        female_pressed = YES;
        male_pressed = NO;
        
        
        submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 508, 320, 60)];
        [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
        [submit setTitle:@"Submit" forState:UIControlStateNormal];
        [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [submit setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
        submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [self.view addSubview:submit];

    }
    else
    {
        background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        background.image = [UIImage imageNamed:@"icons.png"];
        background.alpha = .5;
        [self.view addSubview:background];
        
        back_button = [[UIButton alloc] initWithFrame:CGRectMake(9, 23, 53/2, 70/2)];
        [back_button addTarget:self action:@selector(back_pressed) forControlEvents:UIControlEventTouchUpInside];
        [back_button setImage:[UIImage imageNamed:@"button_back_arrow.png"] forState:UIControlStateNormal];
        [self.view addSubview:back_button];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"Create your account";
        top_label.textColor = [UIColor whiteColor];
        [self.view addSubview:top_label];
        
        username = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 75, 264, 45)];
        username.textAlignment = NSTextAlignmentLeft;
        username.backgroundColor = [UIColor whiteColor];
        username.placeholder = @"  Username";
        UIColor *color = [UIColor lightGrayColor];
        username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:username.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        username.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        username.textColor = [UIColor blackColor];
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        username.delegate = self;
        username.returnKeyType = UIReturnKeyNext;
        username.keyboardType = UIKeyboardTypeASCIICapable;
        username.autocapitalizationType = UITextAutocapitalizationTypeNone;
        username.layer.cornerRadius = 3;
        username.autocorrectionType = UITextAutocorrectionTypeNo;// this value vary as per your desire
        username.clipsToBounds = YES;
        [username setKeyboardAppearance:UIKeyboardAppearanceAlert];
        username.leftView = paddingView;
        username.leftViewMode = UITextFieldViewModeAlways;
        [self.view addSubview: username];
        
        email = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 135, 264, 45)];
        email.textAlignment = NSTextAlignmentLeft;
        email.backgroundColor = [UIColor whiteColor];
        email.placeholder = @"  E-mail";
        email.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        email.textColor = [UIColor blackColor];
        email.delegate = self;
        email.secureTextEntry = NO;
        email.returnKeyType = UIReturnKeyNext;
        email.keyboardType = UIKeyboardTypeEmailAddress;
        email.autocapitalizationType = UITextAutocapitalizationTypeNone;
        email.autocorrectionType = UITextAutocorrectionTypeNo;
        email.layer.cornerRadius = 3; // this value vary as per your desire
        email.clipsToBounds = YES;
        email.leftView = paddingView2;
        email.leftViewMode = UITextFieldViewModeAlways;
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:email.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [email setKeyboardAppearance:UIKeyboardAppearanceAlert];
        [self.view addSubview: email];
        
        
        UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
        [self.view addGestureRecognizer:tapRecognizer];
        
        password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 195, 264, 45)];
        password.textAlignment = NSTextAlignmentLeft;
        password.backgroundColor = [UIColor whiteColor];
        password.placeholder = @"  Password";
        password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        password.textColor = [UIColor blackColor];
        password.delegate = self;
        password.secureTextEntry = YES;
        password.returnKeyType = UIReturnKeyDone;
        password.keyboardType = UIKeyboardTypeASCIICapable;
        password.autocapitalizationType = UITextAutocapitalizationTypeNone;
        password.autocorrectionType = UITextAutocorrectionTypeNo;
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [password setKeyboardAppearance:UIKeyboardAppearanceAlert];
        password.layer.cornerRadius = 3;
        password.leftView = paddingView3;
        password.leftViewMode = UITextFieldViewModeAlways;// this value vary as per your desire
        password.clipsToBounds = YES;
        [self.view addSubview: password];
        
        i_am = [[UILabel alloc] initWithFrame:CGRectMake(35, 255, 100, 20)];
        i_am.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        i_am.textAlignment = NSTextAlignmentLeft;
        i_am.text = @"I am:";
        i_am.textColor = [UIColor whiteColor];
        [self.view addSubview:i_am];
        
        CGFloat borderWidth = 2.0f;
        
        male_am = [[UIButton alloc] initWithFrame:CGRectMake(30, 285, 130, 50)];
        male_am.layer.borderWidth = borderWidth;
        male_am.layer.borderColor = [UIColor whiteColor].CGColor;
        [male_am addTarget:self action:@selector(male_am_pressed) forControlEvents:UIControlEventTouchUpInside];
        [male_am setTitle:[NSString stringWithFormat:@"%@", @"Male"] forState:UIControlStateNormal];
        male_am.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        male_am.titleLabel.textColor = [UIColor blackColor];
        [male_am setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [male_am setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        male_am.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:male_am];
        
        male_am_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, male_am.frame.size.width, male_am.frame.size.height)];
        male_am_background.backgroundColor = [UIColor whiteColor];
        male_am_background.alpha = 0.0;
        [male_am addSubview:male_am_background];
        [male_am bringSubviewToFront:male_am.titleLabel];        
        
        female_am = [[UIButton alloc] initWithFrame:CGRectMake(160, 285, 130, 50)];
        female_am.layer.borderWidth = borderWidth;
        female_am.layer.borderColor = [UIColor whiteColor].CGColor;
        [female_am addTarget:self action:@selector(female_am_pressed) forControlEvents:UIControlEventTouchUpInside];
        [female_am setTitle:[NSString stringWithFormat:@"%@", @"Female"] forState:UIControlStateNormal];
        female_am.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        female_am.titleLabel.textColor = [UIColor blackColor];
        [female_am setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [female_am setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        female_am.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:female_am];
        
        female_am_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, female_am.frame.size.width, female_am.frame.size.height)];
        female_am_background.backgroundColor = [UIColor whiteColor];
        female_am_background.alpha = 1.0;
        [female_am addSubview:female_am_background];
        [female_am bringSubviewToFront:female_am.titleLabel];
        
        female_pressed = YES;
        male_pressed = NO;
        
        submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 508-84, 320, 60)];
        [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
        [submit setTitle:@"Submit" forState:UIControlStateNormal];
        [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [submit setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
        submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [self.view addSubview:submit];
    }
    
}
-(void)male_am_pressed
{
    if (!male_pressed)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.7];
        
        [male_am_background setAlpha:1.0];
        [female_am_background setAlpha:0.0];
        
        [UIView commitAnimations];
        male_pressed = YES;
        female_pressed = NO;
        
    }
    else
    {
        male_pressed = NO;
    }
}
-(void)female_am_pressed
{
    if (!female_pressed)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.7];
        
        [female_am_background setAlpha:1.0];
        [male_am_background setAlpha:0.0];
        
        [UIView commitAnimations];
        male_pressed = NO;
        female_pressed = YES;
        
    }
    else
    {
        female_pressed = NO;
    }
}
-(void)api_call:(NSString*)to_call
{
    if ([to_call isEqualToString:@"register"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        //make the activity indicator start spinning
        NSString * gender_string;
        if (male_pressed)
        {
            gender_string = @"male";
        }
        else
        {
            gender_string = @"female";
        }
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/register/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"username=%@&password=%@&email=%@&get_notified=1&ios_token=%@&full_name=%@&facebook_token=%@&twitter_token=%@&gender=%@&gender_preference=%@", username.text, password.text, email.text, [defaults objectForKey:@"device_token"], @"", @"", @"", gender_string, @"both"] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"RegisterView:/user/register/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"register = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:username.text forKey:@"username"];
                         [defaults setObject:password.text forKey:@"password"];
                         [defaults setObject:email.text forKey:@"email"];
                         [defaults setObject:[NSNumber numberWithInteger:1] forKey:@"pickfitEnableNotifications"];
                         [defaults synchronize];
                         
                         [self api_call:@"login"];
                         
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         if ([message isEqualToString:@"please check email"])
                         {
                             message = @"Please, enter a valid email address";
                         }
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Oops!"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                         
                     }
                     
                 });
             }
             
         }];
    }
    else if ([to_call isEqualToString:@"login"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/login/", SERVICE_URL]];
        
        NSLog(@"/user/login/postData = %@", [NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", email.text, password.text, [defaults objectForKey:@"device_token"]]);
        
        NSData *postData= [[NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", email.text, password.text, [defaults objectForKey:@"device_token"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"RegisterView:/user/login/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"/user/login/ = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     NSString * user_id = [NSString stringWithFormat:@"%ld", [[response_dict objectForKey:@"user_id"] longValue]];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:user_id forKey:@"user_id"];
                         [defaults synchronize];
                         
                         //HomeView * to_push = [[HomeView alloc] init];
                         //[[SlideNavigationController sharedInstance] pushViewController:to_push animated:YES];
                         
                         ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view = [[HomeView alloc] init];
                         [[SlideNavigationController sharedInstance] pushHomeViewController];
                         
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Error"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                     }
                 });
             }
         }];
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == password || textField == confirm_password)
    {
         //[self animateTextField: textField up: YES];
    }
}
- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == password || textField == confirm_password)
    {
        //[self animateTextField: textField up: NO];
    }
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int movementDistance = 250;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        movementDistance = 215;
    }
    else
    {
        movementDistance = 200;
    }
    
    const float movementDuration = 0.3f;
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
-(void)submit_pressed
{
    NSString *message = @"";
    
    if (username.text.length && email.text.length && password.text.length)
    {
        if (username.text.length < 4)
        {
            message = NSLocalizedString(@"Username should contain at least 4 symbols", nil);
        }
        else if (![self isValidEmail:email.text])
        {
            message = NSLocalizedString(@"Please, enter a valid email address", nil);
        }
        else if (password.text.length < 8)
        {
            message = NSLocalizedString(@"Password should contain at least 8 symbols", nil);
        }
        else
        {
            [self api_call:@"register"];
        }
    }
    else
    {
        message = NSLocalizedString(@"Please, enter all required fields", nil);
    }
    
    if (message.length)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:message delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil];
        [alert show];
    }
}
-(void)back_pressed
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer
{
    [password resignFirstResponder];
    [email resignFirstResponder];
    [confirm_password resignFirstResponder];
    [username resignFirstResponder];
}
- (BOOL) textFieldShouldReturn: (UITextField *) txtField
{
    if (txtField == username)
    {
        [email becomeFirstResponder];
    }
    else if (txtField == email)
    {
        [password becomeFirstResponder];
    }
    else if(txtField == password)
    {
        //[self submit_pressed];
    }
	[txtField resignFirstResponder];
	return YES;
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (BOOL)isValidEmail:(NSString *)emailString
{
    NSString *emailRegEx =
    @"(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:emailString];
}

@end

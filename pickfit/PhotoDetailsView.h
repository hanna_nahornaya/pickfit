//
//  PhotoDetailsView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/7/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "TagLabel.h"
#import "POPSUGARShopSense.h"
#import "SVModalWebViewController.h"
#import <MessageUI/MessageUI.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Pinterest/Pinterest.h>


@interface PhotoDetailsView : UIViewController<UIScrollViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate>
{
    UIButton * x;
    UIButton * flag;
    UILabel * user;
    UIButton * favorite;
    UIButton * share;
    UIButton * tags_buttons;
    UIButton * follow_button;
    UIImageView * line;
    UILabel * percent;
    UILabel * occasion_label;
    UIScrollView * photo_scroll;
    UIImageView * photo;
    NSMutableArray * tags;
    
    BOOL is_favorited;
    BOOL showing_tags;
    
}

@property (strong, atomic) UILabel * username_label;
@property (strong, atomic) UIImageView * photo;
@property (strong, atomic) NSNumber * post_id;
@property (strong, atomic) NSNumber * outfit_id;
//@property (strong, atomic) NSNumber * is_flagged;
@property (strong, atomic) UILabel * percent;
@property (strong, atomic) UILabel * occasion_label;
@property (strong, atomic) UIImage * photo_image;
@property (strong, atomic) NSString * url_for_image;
-(void)check_favorites;
-(void)check_flag;
-(void)check_tags;
-(void)check_following;
-(void)load_photo;
@end

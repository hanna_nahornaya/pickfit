//
//  User.m
//  pickfit
//
//  Created by ann on 3/19/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "User.h"
#import "UIImageView+WebCache.h"

@implementation User

NSString* const kUserId = @"id";
NSString* const kUserName = @"username";
NSString* const kUserDescription = @"description";
NSString* const kUserEmail = @"email";
NSString* const kUserFollowersCount = @"count_followers";
NSString* const kUserFollowingsCount = @"count_followings";
NSString* const kUserImageUrl = @"image";

NSString* const kUserIsFollower = @"is_follower";
NSString* const kUserIsOnline = @"is_online";

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
    {
        self.userId = [[dictionary objectForKey:kUserId] integerValue];
        self.name = [[dictionary objectForKey:kUserName] isKindOfClass:[NSNull class]] ? @"" : [dictionary objectForKey:kUserName];
        self.descriptionText = [[dictionary objectForKey:kUserDescription] isKindOfClass:[NSNull class]] ? @"" : [dictionary objectForKey:kUserDescription];
        self.email = [[dictionary objectForKey:kUserEmail] isKindOfClass:[NSNull class]] ? @"" : [dictionary objectForKey:kUserEmail];
        
        self.imageUrl = [[dictionary objectForKey:kUserImageUrl] isKindOfClass:[NSNull class]] ? @"" : [dictionary objectForKey:kUserImageUrl];

        if (self.imageUrl)
        {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:self.imageUrl] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){} completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (!error && image)
                    {
                        self.image = image;
                    }
                    else
                    {
                        self.image = [UIImage imageNamed:@"user_profile_empty.png"];
                    }
                    
                    //NSLog(@"User:postnotif");
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"didDownloadUserImage" object:nil];
                });
            }];
        }
        else
        {
            self.image = [UIImage imageNamed:@"user_profile_empty.png"];
        }
        
        self.followersCount = [[dictionary objectForKey:kUserFollowersCount] integerValue];
        self.followingsCount = [[dictionary objectForKey:kUserFollowingsCount] integerValue];
        
        self.attributesDictionary = [NSMutableDictionary dictionary];
        
        if ([[dictionary allKeys] containsObject:kUserIsFollower])
        {
            [self.attributesDictionary setObject:[dictionary objectForKey:kUserIsFollower] forKey:kUserIsFollower];
        }
        
        if ([[dictionary allKeys] containsObject:kUserIsOnline])
        {
            [self.attributesDictionary setObject:[dictionary objectForKey:kUserIsOnline] forKey:kUserIsOnline];
        }
    }
    return self;
}

- (id)initWithUser:(User *)user
{
    self = [super init];
    if (self)
    {
        self.userId = user.userId;
        self.name = user.name;
        self.descriptionText = user.descriptionText;
        self.email = user.email;
        self.imageUrl = user.imageUrl;
        self.image = user.image;
        self.followersCount = user.followersCount;
        self.followingsCount = user.followingsCount;
    }
    return self;
}

@end

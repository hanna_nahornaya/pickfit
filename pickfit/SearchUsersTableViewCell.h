//
//  SearchUsersTableViewCell.h
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchUsersTableViewCellDelegate <NSObject>

-(void)needFollowing:(BOOL)needFollowing toUserID:(NSInteger)userID;

@end

@interface SearchUsersTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic) BOOL follow;
@property (nonatomic, strong) NSDictionary *userDict;

@property (nonatomic, assign) id <SearchUsersTableViewCellDelegate> cellDelegate;

@end

//
//  Post.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/8/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImage/UIImageView+WebCache.h"

@interface Post : NSObject
@property(strong, nonatomic) NSString * photo_a_url;
@property(strong, nonatomic) NSString * photo_b_url;
@property(strong, nonatomic) NSString * occasion;
@property(strong, nonatomic) NSNumber * photo_a_votes;
@property(strong, nonatomic) NSNumber * photo_b_votes;
@property(strong, nonatomic) NSNumber * total_votes;
@property(strong, nonatomic) NSNumber * outfit_a_id;
@property(strong, nonatomic) NSNumber * outfit_b_id;
@property(strong, nonatomic) NSNumber * outfit_a_flagged;
@property(strong, nonatomic) NSNumber * outfit_b_flagged;
@property(strong, nonatomic) NSNumber * post_id;
@property(strong, nonatomic) UIImage * outfit_a_image;
@property(strong, nonatomic) UIImage * outfit_b_image;
@property (readwrite) BOOL is_sponsored_post;
@property(strong, nonatomic) NSString * photo_a_buy_url;
@property(strong, nonatomic) NSString * photo_b_buy_url;
@property (readwrite) BOOL photo_a_loaded;
@property (readwrite) BOOL photo_b_loaded;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *userImageURL;
@property (readwrite) NSInteger user_id;

@end

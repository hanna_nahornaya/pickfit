//
//  ProfileViewController.m
//  pickfit
//
//  Created by ann on 3/16/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "ProfileViewController.h"
#import "SlideNavigationController.h"

#define kOnePixel  (1 / [[UIScreen mainScreen] scale])

@interface ProfileViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, PECropViewControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) User *initialUser;
@property (nonatomic, strong) User *editedUser;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIButton *updateButton;

@property (nonatomic, strong) UIView *avatarBackgroundView;
@property (nonatomic, strong) UIImageView *avatarImageView;

@property (nonatomic, strong) UIButton *changeAvatarButton;

@property (nonatomic, strong) UITextField *userNameTextField;
@property (nonatomic, strong) UITextField *descriptionTextField;

@end

@implementation ProfileViewController

static NSString * const mainFont = @"Avenir";

static CGFloat const kTopOffset = 30;

static CGFloat const kTitleLabelHeight = 22;
static CGFloat const kTitleLabelFontSize = 20;

static CGFloat const kTopButtonWidth = 65;
static CGFloat const kTopButtonHeight = 22;
static CGFloat const kTopButtonFontSize = 16;

static CGFloat const kAvatarBackgroundViewTopOffset = 20;
static CGFloat const kAvatarBackgroundViewSide = 72;
static CGFloat const kAvatarImageViewSide = 70;

static CGFloat const kChangeAvatarButtonTopOffset = 6;
static CGFloat const kChangeAvatarButtonHeight = 13;
static CGFloat const kChangeAvatarButtonFontSize = 12;

static CGFloat const kLeftOffset = 50;

static CGFloat const kUserNameTextFieldTopOffset = 40;
static CGFloat const kUserNameTextFieldHeight = 23;
static CGFloat const kUserNameTextFieldFontSize = 15;

static CGFloat const kDescriptionTextFieldHeight = 90;
static CGFloat const kDescriptionTextFieldFontSize = 15;
static CGFloat const kDescriptionBottomSeparatorOffset = 28;

- (id)initWithUser:(User *)user
{
    self = [super init];
    if (self)
    {
        self.initialUser = [[User alloc] initWithUser:user];
        
        self.editedUser = [[User alloc] initWithUser:self.initialUser];
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect mainViewRect = self.view.frame;
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kTopButtonWidth, kTopOffset, (mainViewRect.size.width - kTopButtonWidth * 2), kTitleLabelHeight)];
    self.titleLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.text = NSLocalizedString(@"Profile", @"");
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.titleLabel];
    
    self.closeButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 20, 60 / 2, 64 / 2)];
    [self.closeButton setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
    [self.closeButton addTarget:self action:@selector(onCloseButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: self.closeButton];
    
    self.updateButton = [self topButtonWithLeftOffset:CGRectGetMaxX(self.titleLabel.frame) text:NSLocalizedString(@"Update", @"")];
    [self.updateButton addTarget:self action:@selector(onUpdateButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.updateButton];
    
    self.avatarBackgroundView = [[UIView alloc] initWithFrame:CGRectMake((mainViewRect.size.width - kAvatarBackgroundViewSide) / 2, CGRectGetMaxY(self.titleLabel.frame) + kAvatarBackgroundViewTopOffset, kAvatarBackgroundViewSide, kAvatarBackgroundViewSide)];
    self.avatarBackgroundView.layer.cornerRadius = kAvatarBackgroundViewSide / 2;
    self.avatarBackgroundView.layer.masksToBounds = YES;
    self.avatarBackgroundView.backgroundColor = [UIColor colorWithRed:196.0 / 255.0 green:196.0 / 255.0 blue:196.0 / 255.0 alpha:1.0];
    [self.view addSubview:self.avatarBackgroundView];
    
    self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kAvatarImageViewSide, kAvatarImageViewSide)];
    self.avatarImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
    self.avatarImageView.center = CGPointMake(kAvatarBackgroundViewSide / 2, kAvatarBackgroundViewSide / 2);
    self.avatarImageView.layer.cornerRadius = kAvatarImageViewSide / 2;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.avatarBackgroundView addSubview:self.avatarImageView];
    
    self.changeAvatarButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.avatarBackgroundView.frame), CGRectGetMaxY(self.avatarBackgroundView.frame) + kChangeAvatarButtonTopOffset, kAvatarBackgroundViewSide, kChangeAvatarButtonHeight)];
    [self.changeAvatarButton addTarget:self action:@selector(onChangeAvatarButton) forControlEvents:UIControlEventTouchUpInside];
    [self.changeAvatarButton setTitle:NSLocalizedString(@"change", @"") forState:UIControlStateNormal];
    [self.changeAvatarButton setTitleColor:[UIColor colorWithRed:84.0 / 255.0 green:158.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    self.changeAvatarButton.titleLabel.font = [UIFont fontWithName:mainFont size:kChangeAvatarButtonFontSize];
    self.changeAvatarButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.changeAvatarButton];
    
    self.userNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(kLeftOffset, CGRectGetMaxY(self.changeAvatarButton.frame) + kUserNameTextFieldTopOffset, self.view.frame.size.width - kLeftOffset * 2, kUserNameTextFieldHeight)];
    self.userNameTextField.placeholder = @"Username";
    self.userNameTextField.backgroundColor = [UIColor clearColor];
    self.userNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.userNameTextField.textColor = [UIColor blackColor];
    self.userNameTextField.font = [UIFont fontWithName:mainFont size:kUserNameTextFieldFontSize];
    self.userNameTextField.textAlignment = NSTextAlignmentCenter;
    self.userNameTextField.borderStyle = UITextBorderStyleNone;
    self.userNameTextField.keyboardType = UIKeyboardTypeASCIICapable;
    self.userNameTextField.returnKeyType = UIReturnKeyNext;
    self.userNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.userNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.userNameTextField.keyboardAppearance = UIKeyboardAppearanceAlert;
    self.userNameTextField.delegate = self;
    [self.view addSubview:self.userNameTextField];
    
    UIView *userNameTextFieldBottomSeparator = [self horizontalSeparatorViewWithTopOffset:CGRectGetMaxY(self.userNameTextField.frame)];
    [self.view addSubview:userNameTextFieldBottomSeparator];
    
    self.descriptionTextField = [[UITextField alloc] initWithFrame:CGRectMake(kLeftOffset, CGRectGetMaxY(userNameTextFieldBottomSeparator.frame), self.view.frame.size.width - kLeftOffset * 2, kDescriptionTextFieldHeight)];
    self.descriptionTextField.placeholder = @"Tell us about your style";
    self.descriptionTextField.backgroundColor = [UIColor clearColor];
    self.descriptionTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.descriptionTextField.textColor = [UIColor blackColor];
    self.descriptionTextField.font = [UIFont fontWithName:mainFont size:kDescriptionTextFieldFontSize];
    self.descriptionTextField.textAlignment = NSTextAlignmentCenter;
    self.descriptionTextField.borderStyle = UITextBorderStyleNone;
    self.descriptionTextField.keyboardType = UIKeyboardTypeASCIICapable;
    self.descriptionTextField.returnKeyType = UIReturnKeyDone;
    self.descriptionTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.descriptionTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.descriptionTextField.keyboardAppearance = UIKeyboardAppearanceAlert;
    self.descriptionTextField.delegate = self;
    [self.view addSubview:self.descriptionTextField];
    
    [self.view addSubview:[self horizontalSeparatorViewWithTopOffset:CGRectGetMaxY(self.descriptionTextField.frame) + kDescriptionBottomSeparatorOffset]];
    
    UIGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    [self reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.initialUser)
    {
        [self performRequest:@"getUserProfile"];
    }
}

- (UIButton *)topButtonWithLeftOffset:(CGFloat)leftOffset text:(NSString *)text
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(leftOffset, kTopOffset, kTopButtonWidth, kTopButtonHeight)];
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:mainFont size:kTopButtonFontSize];
    button.backgroundColor = [UIColor clearColor];
    return button;
}

- (UIView *)horizontalSeparatorViewWithTopOffset:(CGFloat)topOffset
{
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(kLeftOffset, topOffset, self.view.frame.size.width - kLeftOffset * 2, kOnePixel)];
    separatorView.backgroundColor = [UIColor colorWithRed:193.0 / 255.0 green:193.0 / 255.0 blue:193.0 / 255.0 alpha:1.0];
    return separatorView;
}

- (void)reloadData
{
    self.avatarImageView.image = self.editedUser.image;
    self.userNameTextField.text = self.editedUser.name;
    self.descriptionTextField.text = self.editedUser.descriptionText;
}

- (void)onUpdateButton
{
    self.editedUser.name = self.userNameTextField.text;
    self.editedUser.descriptionText = self.descriptionTextField.text;
    
    /*if ([self.editedUser isEqualToUser:self.initialUser])
    {
    }*/
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/editUserProfile/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%ld&username=%@&description=%@&image=%@", self.editedUser.userId, self.editedUser.name, self.editedUser.descriptionText, self.editedUser.imageUrl] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"ProfileVC:/user/editUserProfile/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             
             //NSLog(@"data back from /user/editUserProfile/ = %@" ,test);
             
             NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSString * status = [responseDictionary objectForKey:@"status"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
                 }
                 else if ([status isEqualToString:@"fail"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     [alert show];
                 }
             });
         }
     }];
}

- (void)onCloseButton
{
    [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
}

- (void)onChangeAvatarButton
{
#if TARGET_IPHONE_SIMULATOR
    
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];

#else
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"Camera", nil), nil];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Photo Library", nil)];
    }
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
    
    [actionSheet showInView:self.view];
#endif
}

- (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToSide:(float)side
{
    float oldWidth = sourceImage.size.width;
    float oldHeight = sourceImage.size.height;
    
    float squareSide = oldWidth;
    UIImage *squareImage = nil;
    
    if (oldWidth != oldHeight)
    {
        if (oldWidth > oldHeight)
        {
            squareSide = oldHeight;
        }
        
        UIGraphicsBeginImageContext(CGSizeMake(squareSide, squareSide));
        [sourceImage drawInRect:CGRectMake(0, 0, squareSide, squareSide)];
        squareImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return squareImage;
    }
    
    if (squareImage.size.width > side)
    {
        UIGraphicsBeginImageContext(CGSizeMake(side, side));
        [sourceImage drawInRect:CGRectMake(0, 0, side, side)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    
    return sourceImage;
}

- (UIImage *)squareImageWithImage:(UIImage *)sourceImage
{
    float originWidth = sourceImage.size.width;
    float originHeight = sourceImage.size.height;
    
    float squareSide = originWidth;
    UIImage *squareImage = nil;
    
    if (originWidth != originHeight)
    {
        if (originWidth > originHeight)
        {
            squareSide = originHeight;
        }
        
        UIGraphicsBeginImageContext(CGSizeMake(squareSide, squareSide));
        [sourceImage drawInRect:CGRectMake(0, 0, squareSide, squareSide)];
        squareImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return squareImage;
    }
    
    return sourceImage;
}

- (UIImage *)testImage:(UIImage *)image
{
    int iconSize = [UIScreen mainScreen].scale == 2 ? 140 : 70;
    
    if (image.size.width != iconSize || image.size.height != iconSize)
    {
        CGRect rect;
        if (image.size.width > image.size.height)
        {
            int height = image.size.height;
            rect = CGRectMake((image.size.width - height) / 2, 0, height, height);
        }
        else
        {
            int width = image.size.width;
            rect = CGRectMake(0, 0, width, width);
        }
        
        CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, rect);
        UIImage *resultImage = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        CGSize itemSize = CGSizeMake(iconSize, iconSize);
        UIGraphicsBeginImageContext(itemSize);
        CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
        [resultImage drawInRect:imageRect];
        UIImage *squareImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return squareImage;
    }
    else
    {
        return image;
    }
}


- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize
{
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height)
    {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    }
    else
    {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    }
    else
    {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)uploadImage:(UIImage *)image
{
//rotate 90 degrees
//    UIImage *squareImage = [self testImage:image];
//    NSData *imageData = UIImageJPEGRepresentation(squareImage, 1.0);//0.65
//    NSString *base64 = [imageData base64EncodedStringWithOptions:0];

//stretch
//    UIImage *squareImage = [self imageWithImage:image scaledToSide:140.0];
//    NSData *imageData = UIImageJPEGRepresentation(squareImage, 0.3);//0.65
//    NSString *base64 = [imageData base64EncodedStringWithOptions:0];
    
//stretch and rotate
//    UIImage *squareImage = [self resizeImage:image newSize:CGSizeMake(140, 140)];
//    NSData *imageData = UIImageJPEGRepresentation(squareImage, .3);//0.65
//    NSString *base64 = [imageData base64EncodedStringWithOptions:0];
    
//    //NSData *imageData = UIImageJPEGRepresentation(image, .3);//0.65
//    UIImage *squareImage = [self squareImageWithImage:image];
//    NSData *imageData = UIImageJPEGRepresentation(squareImage, .3);//0.65
//    NSString *base64 = [imageData base64EncodedStringWithOptions:0];
    
    UIImage *squareImage = [self squareImageFromImage:image scaledToSize:140];
    NSData *imageData = UIImageJPEGRepresentation(squareImage, 0.3);//0.65
    NSString *base64 = [imageData base64EncodedStringWithOptions:0];
    
    //self.avatarImageView.image = squareImage;
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
            });
        }
        else if (!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"ProfileVC:/utility/uploadImage64/: response data is nil");
            });
        }
        else
        {
            //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
            //NSLog(@"data back from pic upload = %@" ,test);
            
            NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            //NSLog(@"upload = %@", responseDictionary);
             
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *status = [responseDictionary objectForKey:@"status"];
                 
                if ([status isEqualToString:@"success"])
                {
                    self.editedUser.imageUrl = [responseDictionary objectForKey:@"link"];
                    self.editedUser.image = image;
                    
                    [self reloadData];
                }
                else if ([status isEqualToString:@"fail"])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    alert.tag = 1;
                    [alert show];
                }
            });
        }
        
        //[self reloadData];
    }];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    [controller dismissViewControllerAnimated:NO completion:NULL];
    
    /*for (UIView * to_remove in photo_a.subviews)
    {
        if (to_remove.tag == 100)
        {
            [to_remove removeFromSuperview];
        }
    }
    
    UIImageView * a_photo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 9, photo_a.frame.size.width-20, photo_a.frame.size.height-20)];
    a_photo.image = croppedImage;
    a_photo.tag = 100;
    [photo_a addSubview:a_photo];
    
    [self upload_photo_a:croppedImage];*/
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
- (void)showCamera
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    controller.delegate = self;
    [self presentViewController:controller animated:YES completion:nil];
    
}

- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:controller animated:YES completion:NULL];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if ([buttonTitle isEqualToString:NSLocalizedString(@"Photo Library", nil)])
    {
        [self openPhotoAlbum];
    }
    else if ([buttonTitle isEqualToString:NSLocalizedString(@"Camera", nil)])
    {
        [self showCamera];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * originalImage  = [UIImage imageWithData:UIImageJPEGRepresentation(info[UIImagePickerControllerOriginalImage], .6)];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [picker dismissViewControllerAnimated:YES completion:^{
            [self uploadImage:originalImage];
        }];
                       
                       // Resize the image
//                       UIImage * image = [self imageWithImage:originalImage scaledToWidth:850];
//
//                       // Optionally save the image here...
//                       dispatch_async(dispatch_get_main_queue(), ^
//                                      {
//                                          // ... Set / use the image here...
//                                          [picker dismissViewControllerAnimated:NO completion:^
//                                           {
//                                               
//                                               PECropViewController *controller_crop = [[PECropViewController alloc] init];
//                                               controller_crop.delegate = self;
//                                               controller_crop.image = image;
//                                               controller_crop.keepingCropAspectRatio = YES;
//                                               
//                                               UIImage *image = controller_crop.image;
//                                               
//                                               CGFloat width = image.size.height/2.75;
//                                               
//                                               if (width >= image.size.width)
//                                               {
//                                                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upload Error" message:@"This photo is not in the correct format to ensure an optimal viewing experience. Please try another photo." delegate:nil cancelButtonTitle:@"OK"
//                                                                                         otherButtonTitles:nil];
//                                                   alert.tag = 1;
//                                                   [alert show];
//                                                   
//                                                   NSLog(@"can not use this image. Your image will end up being stretched by our system.");
//                                               }
//                                               else
//                                               {
//                                                   CGFloat inset = image.size.width/2-image.size.width/5.5;
//                                                   CGFloat height = image.size.height;
//                                                   controller_crop.imageCropRect = CGRectMake(inset, 0, width, height);
//                                                   
//                                                   //NSLog(@"image width %f", image.size.width);
//                                                   //NSLog(@"image height %f", image.size.height);
//                                                   
//                                                   //NSLog(@"width %f", width);
//                                                   //NSLog(@"height %f", height);
//                                                   
//                                                   UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller_crop];
//                                                   
//                                                   [self presentViewController:navigationController animated:YES completion:NULL];
//                                               }
//                                               
//                                           }];
//                                          
//                                      });
    });
}
- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize
{
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    //CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    //CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)performRequest:(NSString *)requestString
{
    if ([requestString isEqualToString:@"getUserProfile"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserProfile/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
#if TARGET_IPHONE_SIMULATOR
        //NSLog(@"/user/getUserProfile/postData = %@", [NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]]);
#endif
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"ProfileVC:/user/getUserProfile/: response data is nil");
                 });
             }
             else
             {
                 NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseArray.count > 0)
                     {
                         NSDictionary *dictionary = [responseArray firstObject];
                         
                         if (dictionary)
                         {
                             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDownloadUserImage) name:@"didDownloadUserImage" object:nil];
                             
                             self.initialUser = [[User alloc] initWithDictionary:dictionary];
                         }
                     }
                     else
                     {
                     }
                 });
             }
         }];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return ((textView.text.length - range.length + text.length) <= 25) ? YES : NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return ((textField.text.length - range.length + string.length) <= 25) ? YES : NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userNameTextField)
    {
        [self.descriptionTextField becomeFirstResponder];
    }
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didTapAnywhere:(UITapGestureRecognizer *)recognizer
{
    [self.userNameTextField resignFirstResponder];
    [self.descriptionTextField resignFirstResponder];
}

- (void)didDownloadUserImage
{
    //NSLog(@"ProfileVC:didDownloadUserImage notif");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didDownloadUserImage" object:nil];
    
    self.editedUser = [[User alloc] initWithUser:self.initialUser];
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}


@end

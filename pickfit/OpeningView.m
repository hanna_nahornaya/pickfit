//
//  OpeningView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "OpeningView.h"
#import "AppDelegate.h"

@implementation OpeningView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"9ad426"];
    
    background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    background.image = [UIImage imageNamed:@"icons.png"];
    background.alpha = .4;
    [self.view addSubview:background];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        logo = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-234/4, 45, 234/2, 303/2)];
        logo.image = [UIImage imageNamed:@"logo.png"];
        [self.view addSubview:logo];
        
        username = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 215, 264, 35)];
        username.textAlignment = NSTextAlignmentCenter;
        username.backgroundColor = [UIColor whiteColor];
        username.placeholder = @"Email";
        UIColor *color = [UIColor blackColor];
        username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:username.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        username.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        username.textColor = [UIColor blackColor];
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        username.delegate = self;
        username.returnKeyType = UIReturnKeyNext;
        username.keyboardType = UIKeyboardTypeEmailAddress;
        username.autocapitalizationType = UITextAutocapitalizationTypeNone;
        [username setKeyboardAppearance:UIKeyboardAppearanceAlert];
        username.layer.cornerRadius = 3; // this value vary as per your desire
        username.clipsToBounds = YES;
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.view addSubview: username];
        
        password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 255, 264, 35)];
        password.textAlignment = NSTextAlignmentCenter;
        password.backgroundColor = [UIColor whiteColor];
        password.placeholder = @"Password";
        password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        password.textColor = [UIColor blackColor];
        password.delegate = self;
        password.secureTextEntry = YES;
        password.returnKeyType = UIReturnKeyDone;
        password.keyboardType = UIKeyboardTypeASCIICapable;
        password.autocapitalizationType = UITextAutocapitalizationTypeNone;
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [password setKeyboardAppearance:UIKeyboardAppearanceAlert];
        password.layer.cornerRadius = 3; // this value vary as per your desire
        password.clipsToBounds = YES;
        password.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.view addSubview: password];
        
        //this is for closing down view.
        UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
        [self.view addGestureRecognizer:tapRecognizer];
        
        login = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-264/2, 300, 264, 35)];
        [login addTarget:self action:@selector(login_pressed) forControlEvents:UIControlEventTouchUpInside];
        [login setTitle:@"Login" forState:UIControlStateNormal];
        [login.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:16]];
        [login setTitleEdgeInsets:UIEdgeInsetsMake(-3.f, 0.f, 0.f, 0.f)];
        login.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [self.view addSubview:login];
        
        //or_label = [UIButton buttonWithType:UIButtonTypeCustom];
        //or_label.frame = CGRectMake(self.view.frame.size.width*.5-240/2, 378, 240, 34/2);
        or_label = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 378, 240, 34/2)];
        [or_label setTitle:@"Or log in with" forState:UIControlStateNormal];
        or_label.backgroundColor = [UIColor clearColor];
        or_label.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:14];
        or_label.titleLabel.textColor = [UIColor blackColor];
        or_label.enabled = YES;
        [self.view addSubview:or_label];
        
        facebook_login = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-528/4, 405, 528/2, 84/2)];
        [facebook_login addTarget:self action:@selector(facebook_pressed) forControlEvents:UIControlEventTouchUpInside];
        [facebook_login setBackgroundImage:[UIImage imageNamed:@"button_fb.png"] forState:UIControlStateNormal];
        facebook_login.backgroundColor = [UIColor clearColor];
        [self.view addSubview:facebook_login];
        
        dont_have_account = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 485, 240, 40)];
        [dont_have_account addTarget:self action:@selector(register_pressed) forControlEvents:UIControlEventTouchUpInside];
        [dont_have_account setTitle:@"Sign Up" forState:UIControlStateNormal];
        dont_have_account.backgroundColor = [UIColor clearColor];
        dont_have_account.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        dont_have_account.titleLabel.textColor = [UIColor whiteColor];
        dont_have_account.adjustsImageWhenHighlighted = YES;
        [self.view addSubview:dont_have_account];
        
        forgot_password = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 525, 240, 40)];
        [forgot_password addTarget:self action:@selector(forgot_password) forControlEvents:UIControlEventTouchUpInside];
        [forgot_password setTitle:@"Forgot Password?" forState:UIControlStateNormal];
        forgot_password.backgroundColor = [UIColor clearColor];
        forgot_password.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        forgot_password.titleLabel.textColor = [UIColor whiteColor];
        forgot_password.adjustsImageWhenHighlighted = YES;
        [self.view addSubview:forgot_password];
    
    }
    else
    {
        logo = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-234/4, 45, 234/2, 303/2)];
        logo.image = [UIImage imageNamed:@"logo.png"];
        [self.view addSubview:logo];
        
        username = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 200, 264, 35)];
        username.textAlignment = NSTextAlignmentCenter;
        username.backgroundColor = [UIColor whiteColor];
        username.placeholder = @"Email";
        UIColor *color = [UIColor blackColor];
        username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:username.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        username.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        username.textColor = [UIColor blackColor];
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        username.delegate = self;
        username.returnKeyType = UIReturnKeyNext;
        username.keyboardType = UIKeyboardTypeEmailAddress;
        username.autocapitalizationType = UITextAutocapitalizationTypeNone;
        [username setKeyboardAppearance:UIKeyboardAppearanceAlert];
        username.layer.cornerRadius = 3; // this value vary as per your desire
        username.clipsToBounds = YES;
        username.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.view addSubview: username];
        
        password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 240, 264, 35)];
        password.textAlignment = NSTextAlignmentCenter;
        password.backgroundColor = [UIColor whiteColor];
        password.placeholder = @"Password";
        password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        password.textColor = [UIColor blackColor];
        password.delegate = self;
        password.secureTextEntry = YES;
        password.returnKeyType = UIReturnKeyDone;
        password.keyboardType = UIKeyboardTypeASCIICapable;
        password.autocapitalizationType = UITextAutocapitalizationTypeNone;
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        [password setKeyboardAppearance:UIKeyboardAppearanceAlert];
        password.layer.cornerRadius = 3; // this value vary as per your desire
        password.clipsToBounds = YES;
        password.autocorrectionType = UITextAutocorrectionTypeNo;
        [self.view addSubview: password];
        
        //this is for closing down view.
        UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
        [self.view addGestureRecognizer:tapRecognizer];
        
        login = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-264/2, 285, 264, 35)];
        [login addTarget:self action:@selector(login_pressed) forControlEvents:UIControlEventTouchUpInside];
        [login setTitle:@"Login" forState:UIControlStateNormal];
        [login.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:16]];
        [login setTitleEdgeInsets:UIEdgeInsetsMake(-3.f, 0.f, 0.f, 0.f)];
        login.backgroundColor = [self colorWithHexString:@"76ac0a"];
        [self.view addSubview:login];
       
        or_label = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 325, 240, 34/2)];
        [or_label setTitle:@"Or log in with" forState:UIControlStateNormal];
        or_label.backgroundColor = [UIColor clearColor];
        or_label.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:14];
        or_label.titleLabel.textColor = [UIColor blackColor];
        or_label.enabled = YES;
        [self.view addSubview:or_label];
        
        facebook_login = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-528/4, 345, 528/2, 84/2)];
        [facebook_login addTarget:self action:@selector(facebook_pressed) forControlEvents:UIControlEventTouchUpInside];
        [facebook_login setBackgroundImage:[UIImage imageNamed:@"button_fb.png"] forState:UIControlStateNormal];
        facebook_login.backgroundColor = [UIColor clearColor];
        [self.view addSubview:facebook_login];
        
        dont_have_account = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 405, 240, 40)];
        [dont_have_account addTarget:self action:@selector(register_pressed) forControlEvents:UIControlEventTouchUpInside];
        [dont_have_account setTitle:@"Sign Up" forState:UIControlStateNormal];
        dont_have_account.backgroundColor = [UIColor clearColor];
        dont_have_account.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        dont_have_account.titleLabel.textColor = [UIColor whiteColor];
        dont_have_account.adjustsImageWhenHighlighted = YES;
        [self.view addSubview:dont_have_account];
        
        forgot_password = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width*.5-240/2, 445, 240, 40)];
        [forgot_password addTarget:self action:@selector(forgot_password) forControlEvents:UIControlEventTouchUpInside];
        [forgot_password setTitle:@"Forgot Password?" forState:UIControlStateNormal];
        forgot_password.backgroundColor = [UIColor clearColor];
        forgot_password.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:16];
        forgot_password.titleLabel.textColor = [UIColor whiteColor];
        forgot_password.adjustsImageWhenHighlighted = YES;
        [self.view addSubview:forgot_password];
        
        
    }
}
-(void)api_call:(NSString*)to_call
{
    if ([to_call isEqualToString:@"login"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/login/", SERVICE_URL]];
        
        NSLog(@"/user/login/postData = %@", [NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", username.text, password.text, [defaults objectForKey:@"device_token"]]);
        
        NSData *postData= [[NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", username.text, password.text, [defaults objectForKey:@"device_token"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"OpeningView:/user/login/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 NSLog(@"/user/login/ = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * user_id = [NSString stringWithFormat:@"%ld", [[response_dict objectForKey:@"user_id"] longValue]];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:user_id forKey:@"user_id"];
                         [defaults setObject:username.text forKey:@"email"];
                         [defaults setObject:password.text forKey:@"password"];
                         [defaults synchronize];
                         
                         //HomeView * to_push = [[HomeView alloc] init];
                         //[[SlideNavigationController sharedInstance] pushViewController:to_push animated:YES];
                         
                         ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view = [[HomeView alloc] init];
                         [[SlideNavigationController sharedInstance] pushHomeViewController];
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Let’s Try That Again"
                                               message:@"Please check your login details."
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                     }
                 });
             }
         }];
    }
}
-(void)login_pressed
{
    [self api_call:@"login"];   
}
-(void)register_pressed
{
    RegisterView * to_push = [[RegisterView alloc] init];
    [[SlideNavigationController sharedInstance] pushViewController:to_push animated:YES];
}
-(void)forgot_password
{
    ForgotPasswordView * forgot_password_view = [[ForgotPasswordView alloc] init];
    [[SlideNavigationController sharedInstance] pushViewController:forgot_password_view animated:YES];
}
-(void)facebook_pressed
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email",@"public_profile",@"publish_actions",
                            nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState state,
                                                      NSError *error)
     
     {
         if (state == FBSessionStateOpen)
         {
             [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
                 if (error)
                 {
                     NSLog(@"error:%@",error);
                 }
                 else
                 {
                     NSString * email = [user objectForKey:@"email"];
                     NSString * fb_id = [user objectForKey:@"id"];
                     NSString * gender = [user objectForKey:@"gender"];
                     NSString * fb_username = [NSString stringWithFormat:@"%@.%@", [user objectForKey:@"first_name"], [user objectForKey:@"last_name"]];
                     NSString * fb_full_name = [user objectForKey:@"name"];
                     
                     NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/fbLogin/", SERVICE_URL]];
                     
                     NSLog(@"/user/fbLogin/ = %@", [NSString stringWithFormat:@"fb_id=%@&email=%@&gender=%@&username=%@&full_name=%@&ios_token=%@", fb_id, email, gender, fb_username, fb_full_name, [[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]]);
                     
                     NSData *postData= [[NSString stringWithFormat:@"fb_id=%@&email=%@&gender=%@&username=%@&full_name=%@&ios_token=%@", fb_id, email, gender, fb_username, fb_full_name, [[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                     
                     NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
                     [theRequest setHTTPMethod:@"POST"];
                     [theRequest setHTTPBody:postData];
                     
                     NSOperationQueue *queue = [[NSOperationQueue alloc] init];
                     [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
                      {
                          if(error)
                          {
                              NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                  alert.tag = 1;
                                  //[alert show];
                              });
                          }
                          else if (!data)
                          {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  NSLog(@"OpeningView:/user/fbLogin/: response data is nil");
                              });
                          }
                          else
                          {
                              NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                              
                              NSLog(@"/user/fbLogin/ = %@", response_dict);
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  
                                  NSString * status = [response_dict objectForKey:@"status"];
                                  NSString * message = [response_dict objectForKey:@"msg"];
                                  NSString * user_id = [response_dict objectForKey:@"user_id"];
                                  
                                  if ([status isEqualToString:@"success"])
                                  {
                                      
                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                      [defaults setObject:user_id forKey:@"user_id"];
                                      
                                      [defaults setObject:email forKey:@"email"];
                                      [defaults setObject:gender forKey:@"gender"];
                                      [defaults setObject:fb_id forKey:@"fb_id"];
                                      
                                      [defaults setObject:fb_username forKey:@"username"];
                                      [defaults setObject:fb_full_name forKey:@"full_name"];
                                      
                                      [defaults synchronize];
                                                                            
                                      ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view = [[HomeView alloc] init];
                                      [[SlideNavigationController sharedInstance] pushHomeViewController];
                                  }
                                  else if([status isEqualToString:@"fail"])
                                  {
                                      
                                      
                                      UIAlertView *alert = [[UIAlertView alloc]
                                                            initWithTitle:@"Error"
                                                            message:message
                                                            delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                                      
                                      alert.tag = 1;
                                      [alert show];
                                      
                                  }
                                  
                              });
                          }
                          
                      }];

                     
                     
                 }
             }];
         }
         
     } ];

    
    
}
- (BOOL) textFieldShouldReturn: (UITextField *) txtField
{
    if (txtField == username)
    {
        [password becomeFirstResponder];
    }
	[txtField resignFirstResponder];
	return YES;
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer
{
    [password resignFirstResponder];
    [username resignFirstResponder];
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
@end

//
//  ClosetView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "ClosetView.h"
#import "ProfileViewController.h"
#import "MyOutfitsViewController.h"
#import "FavoritesViewController.h"
#import "FollowersViewController.h"
#import "FollowingViewController.h"
#import "User.h"

#define kOnePixel  (1 / [[UIScreen mainScreen] scale])

@interface ClosetView ()
{
    UILabel *titleLabel;
    UIButton *editButton;
    UIButton *backButton;
    UIView *avatarBackgroundView;
    UIButton *followersButton;
    UIButton *followingButton;
}

@property (nonatomic, strong) User *currentUser;

@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;

@property (nonatomic, strong) UILabel *followersCountLabel;
@property (nonatomic, strong) UILabel *followingCountLabel;

@property (nonatomic, strong) UIButton *myOutfitsButton;
@property (nonatomic, strong) UIButton *favoritesButton;

@property (nonatomic, strong) MyOutfitsViewController *myOutfitsViewController;
@property (nonatomic, strong) UIView *myOutfitsView;

@property (nonatomic, strong) FavoritesViewController *favoritesViewController;
@property (nonatomic, strong) UIView *favoritesView;

@end

@implementation ClosetView

static NSString * const mainFont = @"Avenir";

static CGFloat const kTopOffset = 30;

static CGFloat const kTitleLabelHeight = 22;
static CGFloat const kTitleLabelFontSize = 20;

static CGFloat const kEditButtonWidth = 50;
static CGFloat const kEditButtonHeight = 22;
static CGFloat const kEditButtonFontSize = 16;

static CGFloat const kBackButtonWidth = 50;
static CGFloat const kBackButtonHeight = 30;

static CGFloat const kAvatarBackgroundViewTopOffset = 20;
static CGFloat const kAvatarBackgroundViewSide = 72;
static CGFloat const kAvatarImageViewSide = 70;

static CGFloat const kNameLabelTopOffset = 6;
static CGFloat const kNameLabelLeftOffset = 5;
static CGFloat const kNameLabelHeight = 18;
static CGFloat const kNameLabelFontSize = 16;

static CGFloat const kDescriptionLabelTopOffset = 4;
static CGFloat const kDescriptionLabelHeight = 15;
static CGFloat const kDescriptionLabelFontSize = 12;

static CGFloat const kFollowButtonTopOffset = 12;
static CGFloat const kFollowButtonWidth = 103;
static CGFloat const kFollowButtonHeight = 53;
static CGFloat const kFollowButtonFontSize = 10;


static CGFloat const kOutfitButtonHeight = 37;
static CGFloat const kOutfitButtonFontSize = 13;
static CGFloat const kOutfitButtonMiddleSeparatorTopOffset = 12;
static CGFloat const kOutfitButtonMiddleSeparatorHeight = 16;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect mainViewRect = self.view.frame;
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kBackButtonWidth, kTopOffset, (mainViewRect.size.width - kEditButtonWidth - kBackButtonWidth), kTitleLabelHeight)];
    titleLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"My Closet", @"");
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
    
    editButton = [[UIButton alloc] initWithFrame:CGRectMake(mainViewRect.size.width - kEditButtonWidth, kTopOffset, kEditButtonWidth, kEditButtonHeight)];
    [editButton setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
    [editButton setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
    [editButton addTarget:self action:@selector(onEditButton) forControlEvents:UIControlEventTouchUpInside];
    editButton.titleEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0);
    editButton.titleLabel.font = [UIFont fontWithName:mainFont size:kEditButtonFontSize];
    editButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:editButton];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kBackButtonWidth, kBackButtonHeight)];
    imageView.image = [UIImage imageNamed:@"button_green_arrow_back"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kTopOffset, kBackButtonWidth, kBackButtonHeight)];
    [backButton addTarget:self action:@selector(onBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton addSubview:imageView];
    backButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backButton];
    
    avatarBackgroundView = [[UIView alloc] initWithFrame:CGRectMake((mainViewRect.size.width - kAvatarBackgroundViewSide) / 2, CGRectGetMaxY(titleLabel.frame) + kAvatarBackgroundViewTopOffset, kAvatarBackgroundViewSide, kAvatarBackgroundViewSide)];
    avatarBackgroundView.layer.cornerRadius = kAvatarBackgroundViewSide / 2;
    avatarBackgroundView.layer.masksToBounds = YES;
    avatarBackgroundView.backgroundColor = [UIColor colorWithRed:196.0 / 255.0 green:196.0 / 255.0 blue:196.0 / 255.0 alpha:1.0];
    [self.view addSubview:avatarBackgroundView];
    
    self.avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kAvatarImageViewSide, kAvatarImageViewSide)];
    self.avatarImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
    self.avatarImageView.center = CGPointMake(kAvatarBackgroundViewSide / 2, kAvatarBackgroundViewSide / 2);
    self.avatarImageView.layer.cornerRadius = kAvatarImageViewSide / 2;
    self.avatarImageView.layer.masksToBounds = YES;
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [avatarBackgroundView addSubview:self.avatarImageView];
    
    self.nameLabel = [self labelWithText:@"" fontSize:kNameLabelFontSize textColor:[UIColor blackColor]];
    self.nameLabel.frame = CGRectMake(kNameLabelLeftOffset, CGRectGetMaxY(avatarBackgroundView.frame) + kNameLabelTopOffset, mainViewRect.size.width - kNameLabelLeftOffset * 2, kNameLabelHeight);
    [self.view addSubview:self.nameLabel];
    
    self.descriptionLabel = [self labelWithText:@"" fontSize:kDescriptionLabelFontSize textColor:[UIColor colorWithRed:119.0 / 255.0 green:119.0 / 255.0 blue:119.0 / 255.0 alpha:1.0]];
    self.descriptionLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kDescriptionLabelTopOffset, self.nameLabel.frame.size.width, kDescriptionLabelHeight);
    [self.view addSubview:self.descriptionLabel];
    
    followersButton = [self followButtonWithText:NSLocalizedString(@"Followers", @"") origin:CGPointMake(self.view.frame.size.width / 2 - kFollowButtonWidth, CGRectGetMaxY(self.descriptionLabel.frame) + kFollowButtonTopOffset) horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [followersButton addTarget:self action:@selector(onFollowersButton) forControlEvents:UIControlEventTouchUpInside];
    followersButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 9, 0);
    [self.view addSubview:followersButton];
    
    self.followersCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, followersButton.frame.size.width, 17)];
    self.followersCountLabel.font = [UIFont fontWithName:fontBoldPrimaryName size:20];
    self.followersCountLabel.textAlignment = NSTextAlignmentCenter;
    self.followersCountLabel.text = @"";
    self.followersCountLabel.textColor = [UIColor blackColor];
    self.followersCountLabel.backgroundColor = [UIColor clearColor];
    [followersButton addSubview:self.followersCountLabel];
    
    followingButton = [self followButtonWithText:NSLocalizedString(@"Following", @"") origin:CGPointMake(CGRectGetMaxX(followersButton.frame), followersButton.frame.origin.y) horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [followingButton addTarget:self action:@selector(onFollowingButton) forControlEvents:UIControlEventTouchUpInside];
    followingButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 9, 0);
    [self.view addSubview:followingButton];
    
    self.followingCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 12, followingButton.frame.size.width, 17)];
    self.followingCountLabel.font = [UIFont fontWithName:fontBoldPrimaryName size:20];
    self.followingCountLabel.textAlignment = NSTextAlignmentCenter;
    self.followingCountLabel.text = @"";
    self.followingCountLabel.textColor = [UIColor blackColor];
    self.followingCountLabel.backgroundColor = [UIColor clearColor];
    [followingButton addSubview:self.followingCountLabel];
    
    UIView *followTopSeparatorView = [self horizontalSeparatorViewWithTopOffset:CGRectGetMinY(followersButton.frame) - kOnePixel];
    [self.view addSubview:followTopSeparatorView];
    
    UIView *followBottomSeparatorView = [self horizontalSeparatorViewWithTopOffset:CGRectGetMaxY(followingButton.frame)];
    [self.view addSubview:followBottomSeparatorView];
    
    self.myOutfitsButton = [self outfitButtonWithText:NSLocalizedString(@"My Outfits", @"") origin:CGPointMake(0, CGRectGetMaxY(followBottomSeparatorView.frame))];
    [self.view addSubview:self.myOutfitsButton];
    
    self.favoritesButton = [self outfitButtonWithText:NSLocalizedString(@"Favorites", @"") origin:CGPointMake(mainViewRect.size.width - self.myOutfitsButton.frame.size.width, self.myOutfitsButton.frame.origin.y)];
    [self.view addSubview:self.favoritesButton];
    
    UIView *outfitBottomSeparator = [self horizontalSeparatorViewWithTopOffset:CGRectGetMaxY(self.myOutfitsButton.frame)];
    [self.view addSubview:outfitBottomSeparator];
    
    UIView *outfitMiddleSeparator = [[UIView alloc] initWithFrame:CGRectMake(mainViewRect.size.width / 2, self.myOutfitsButton.frame.origin.y + kOutfitButtonMiddleSeparatorTopOffset, kOnePixel, kOutfitButtonMiddleSeparatorHeight)];
    outfitMiddleSeparator.backgroundColor = [UIColor colorWithRed:209.0 / 255.0 green:209.0 / 255.0 blue:209.0 / 255.0 alpha:1.0];
    [self.view addSubview:outfitMiddleSeparator];
    
    self.myOutfitsViewController = [[MyOutfitsViewController alloc] init];
    [self addChildViewController:self.myOutfitsViewController];
    self.myOutfitsView = self.myOutfitsViewController.view;
    self.myOutfitsView.frame = CGRectMake(0, CGRectGetMaxY(outfitBottomSeparator.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(outfitBottomSeparator.frame));
    self.myOutfitsView.hidden = NO;
    [self.myOutfitsViewController addRefreshControl];
    [self.view addSubview:self.myOutfitsView];
    
    self.favoritesViewController = [[FavoritesViewController alloc] init];
    [self addChildViewController:self.favoritesViewController];
    self.favoritesView = self.favoritesViewController.view;
    self.favoritesView.frame = CGRectMake(0, CGRectGetMaxY(outfitBottomSeparator.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(outfitBottomSeparator.frame));
    self.favoritesViewController.view.backgroundColor = [UIColor clearColor];
    self.favoritesView.hidden = YES;
    [self.view addSubview:self.favoritesView];
    
    [self onMyOutfitsOrFavoritesButton:self.myOutfitsButton];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self performRequest:@"getUserProfile"];
}

- (UILabel *)labelWithText:(NSString *)text fontSize:(CGFloat)fontSize textColor:(UIColor *)textColor
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = textColor;
    label.font = [UIFont fontWithName:mainFont size:fontSize];
    label.backgroundColor = [UIColor clearColor];
    return label;
}

- (UIView *)horizontalSeparatorViewWithTopOffset:(CGFloat)topOffset
{
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, topOffset, self.view.frame.size.width, kOnePixel)];
    separatorView.backgroundColor = [UIColor colorWithRed:193.0 / 255.0 green:193.0 / 255.0 blue:193.0 / 255.0 alpha:1.0];
    return separatorView;
}

- (UIButton *)followButtonWithText:(NSString *)text origin:(CGPoint)origin horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(origin.x, origin.y, kFollowButtonWidth, kFollowButtonHeight)];
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:mainFont size:kFollowButtonFontSize];
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    button.contentHorizontalAlignment = horizontalAlignment;
    button.backgroundColor = [UIColor clearColor];
    return button;
}

- (UIButton *)outfitButtonWithText:(NSString *)text origin:(CGPoint)origin
{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(origin.x, origin.y, self.view.frame.size.width / 2 - 1, kOutfitButtonHeight)];
    [button addTarget:self action:@selector(onMyOutfitsOrFavoritesButton:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[self colorWithHexString:@"9ad426"] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont fontWithName:mainFont size:kOutfitButtonFontSize];
    button.backgroundColor = [UIColor clearColor];
    return button;
}

- (void)onEditButton
{
    [[SlideNavigationController sharedInstance] presentViewController:[[ProfileViewController alloc] initWithUser:self.currentUser] animated:YES completion:nil];
}

- (void)open_text_message
{
    [self dismissCurrentPopinControllerAnimated:YES];
}

- (void)onBackButton
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

//- (void)onAddOutfitButton
//{
//    [self create_outfit];
//}

- (void)onFollowersButton
{
    [[SlideNavigationController sharedInstance] pushViewController:[[FollowersViewController alloc] init] animated:YES];
}

- (void)onFollowingButton
{
    [[SlideNavigationController sharedInstance] pushViewController:[[FollowingViewController alloc] init] animated:YES];
}

- (void)onMyOutfitsOrFavoritesButton:(UIButton *)button
{
    [self.myOutfitsButton setSelected:(button == self.myOutfitsButton)];
    [self.favoritesButton setSelected:!(button == self.myOutfitsButton)];
    
    self.myOutfitsView.hidden = !(button == self.myOutfitsButton);
    self.favoritesView.hidden = (button == self.myOutfitsButton);
}

#warning add refresh
//-(void)refresh_table
//{
//    [refresh endRefreshing];
//    [self api_call:@"get_user_posts"];
//}

- (void)performRequest:(NSString *)requestString
{
    if ([requestString isEqualToString:@"getUserProfile"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserProfile/", SERVICE_URL]];

        NSData *postData= [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
#if TARGET_IPHONE_SIMULATOR
        //NSLog(@"/user/getUserProfile/postData = %@", [NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]]);
#endif

        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];

        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"ClosetView:/user/getUserProfile/: response data is nil");
                 });
             }
             else
             {
                 NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseArray.count > 0)
                     {
                         NSDictionary *dictionary = [responseArray firstObject];
                         
                         if (dictionary)
                         {
                             [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDownloadUserImage) name:@"didDownloadUserImage" object:nil];
                             self.currentUser = [[User alloc] initWithDictionary:dictionary];
                             
                             self.nameLabel.text = self.currentUser.name;
                             self.descriptionLabel.text = self.currentUser.descriptionText;
                             self.followersCountLabel.text = [NSString stringWithFormat:@"%ld", self.currentUser.followersCount];
                             self.followingCountLabel.text = [NSString stringWithFormat:@"%ld", self.currentUser.followingsCount];
                         }
                     }
                     else
                     {
                     }
                 });
             }
         }];
    }
}

-(void)api_call:(NSString*)to_call
{
}

- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didDownloadUserImage
{
    //NSLog(@"ClosetView:didDownloadUserImage notif");
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"didDownloadUserImage" object:nil];
    
    self.avatarImageView.image = self.currentUser.image;
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

@end

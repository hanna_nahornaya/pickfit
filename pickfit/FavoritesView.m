//
//  FavoritesView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "FavoritesView.h"
#import "Flurry.h"

@implementation FavoritesView

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"f7faf6"];
    
    favorites = [[NSMutableArray alloc] init];
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];    
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-120/2, 30, 120, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Favorites";
    top_label.textColor = [UIColor blackColor];
    [self.view addSubview:top_label];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
	[closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
	[closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    menu_button  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 66/2, 53/2)];
	[menu_button setImage:[UIImage imageNamed:@"button_green_bars.png"] forState:UIControlStateNormal];
	[menu_button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: menu_button];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
        favorites_table = [[AQGridView alloc] initWithFrame:CGRectMake(0, 70, 320, 498)];
    else
        favorites_table = [[AQGridView alloc] initWithFrame:CGRectMake(0, 70, 320, 498-84)];
    favorites_table.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    favorites_table.backgroundColor = [UIColor clearColor];
	favorites_table.delegate = self;
	favorites_table.dataSource = self;
    favorites_table.hidden = NO;
    favorites_table.showsVerticalScrollIndicator = NO;
    [self.view addSubview:favorites_table];   
    
}
-(void)api_call:(NSString*)to_call
{
    if([to_call isEqualToString:@"favorites"])
    {
        [favorites removeAllObjects];
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/getFavoriteOutfits/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"FavoritesView:/outfit/getFavoriteOutfits/: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     for (NSDictionary * to_create in response_array)
                     {
                         FavoriteOutfit * plainCell = [[FavoriteOutfit alloc] initWithFrame: CGRectMake(0, 0, 180, 260) reuseIdentifier: @"FavoriteCellIdentifier" : [to_create objectForKey:@"id"] : [to_create objectForKey:@"image"]];
                         plainCell.is_flagged = [to_create objectForKey:@"flagged"];
                         [plainCell load];
                         [favorites addObject:plainCell];
                     }
                     
                     [favorites_table reloadData];
                 });
                 
             }
             
         }];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [self api_call:@"favorites"];
}
-(void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    PhotoDetailsView * to_push = [[PhotoDetailsView alloc] init];
    FavoriteOutfit * selected = [favorites objectAtIndex:index];
    
    [Flurry logEvent:@"outfit pressed from favorites section"];
    
    [[SlideNavigationController sharedInstance] presentViewController:to_push animated:YES completion:^{
        
        to_push.photo.image = selected.photo_img;
        to_push.outfit_id = selected.outfit_id;
        to_push.url_for_image = selected.img_url;
        //to_push.is_flagged = selected.is_flagged;
        
        [to_push check_favorites];
        [to_push check_following];
        [to_push check_flag];
        [to_push check_tags];
    }];
}
 -(CGSize) portraitGridCellSizeForGridView: (AQGridView *) gridView
{
    
    CGSize to_return = CGSizeMake(192/2, 566/2);
    return  to_return;
    
}
- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    
    static NSString * PlainCellIdentifier = @"FavoriteCellIdentifier";
    
    FavoriteOutfit * cell = nil;
    
    cell = (FavoriteOutfit*)[aGridView dequeueReusableCellWithIdentifier: PlainCellIdentifier];
    
    
    if (cell == nil)
    {
        FavoriteOutfit * plainCell = [favorites objectAtIndex:index];
        
        
        plainCell.selectionGlowColor = [UIColor clearColor];
        
        cell = plainCell;
        
    }
    else
    {
        cell = [favorites objectAtIndex:index];
        
        cell.selectionGlowColor = [UIColor clearColor];
    }
    
    return ( cell );    
    
}
- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return favorites.count;
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}


@end

//
//  SettingsView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "OpeningView.h"
#import "ChangePasswordView.h"

@interface SettingsView : UIViewController<UIAlertViewDelegate, MFMailComposeViewControllerDelegate>
{
    UIButton * closet_button;
    UIButton * menu_button;
    
    UIImageView * logo;
    UILabel * top_label;
    UIImageView * top_background;
    
    UILabel * i_am;
    UIButton * male_am;
    UIImageView * male_am_background;
    UIButton * female_am;
    UIImageView * female_am_background;
    
    UILabel * show_me;
    UIButton * both_show;
    UIImageView * both_show_background;
    UIButton * male_show;
    UIImageView * male_show_background;
    UIButton * female_show;
    UIImageView * female_show_background;
    
    UIButton *white_background_profile_settings;
    UILabel * profile_settings;
    UIImageView * arrow_profile_settings;
    
    UIImageView * white_background_notifications;
    UILabel * notifications;
    UISwitch * notifications_switch;
    
    UIButton * white_background_contact_us;
    UILabel * contact_us;
    UIImageView * arrow_contact_us;
    
    UIButton * white_background_information;
    UILabel * information;
    UIImageView * arrow_information;
    
    UIButton * white_background_change_password;
    UILabel * change_password;
    UIImageView * arrow_change_password;
    
    UIButton * logout;
    UIButton * delete_account;
    
    UILabel * version_label;
    
    
    BOOL show_both;
    BOOL show_female;
    BOOL show_male;
    
    
}
@end

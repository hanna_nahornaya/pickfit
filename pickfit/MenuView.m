//
//  MenuView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "MenuView.h"
#import "Flurry.h"
#import "AppDelegate.h"

@implementation MenuView
@synthesize settings_view;
//@synthesize home_view;
@synthesize trendingVC;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"gender_changed" object:nil];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
//    if([self.navigationController.topViewController isKindOfClass:[HomeView class]])
//    {
//       self.home_view = (HomeView*)self.navigationController.topViewController;
//    }
//    else
//    {
//       self.home_view = [[HomeView alloc] init];
//    }
    
    self.settings_view = [[SettingsView alloc] init];
    self.trendingVC = [TrendingVC new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gender_changed) name:@"gender_changed" object:nil];
    
    home_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 68, self.view.frame.size.width, 90)];
    home_button.backgroundColor = [UIColor clearColor];
    [home_button addTarget:self action:@selector(home_pressed) forControlEvents:UIControlEventTouchUpInside];
    [home_button setTitle:@"Home" forState:UIControlStateNormal];
    [home_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [home_button setTintColor:[UIColor greenColor]];
    home_button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:21];
    [home_button setImage:[[UIImage imageNamed:@"icon_home.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [home_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 75.0, 0.0, 30.0)];
    [home_button setImageEdgeInsets:UIEdgeInsetsMake(0.0, 50.0, 0.0, 25.0)];
    home_button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:home_button];
    
    trending_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 68+90, self.view.frame.size.width, 90)];
    trending_button.backgroundColor = [UIColor clearColor];
    [trending_button addTarget:self action:@selector(trending_pressed) forControlEvents:UIControlEventTouchUpInside];
    [trending_button setTitle:@"Trending" forState:UIControlStateNormal];
    trending_button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:21];
    [trending_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [trending_button setImage:[[UIImage imageNamed:@"icon_tranding.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [trending_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 75.0, 0.0, 30.0)];
    [trending_button setImageEdgeInsets:UIEdgeInsetsMake(0.0, 50.0, 0.0, 25.0)];
    trending_button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:trending_button];
    
    invite_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 68+90*2, self.view.frame.size.width, 90)];
    invite_button.backgroundColor = [UIColor clearColor];
    [invite_button addTarget:self action:@selector(invite_pressed) forControlEvents:UIControlEventTouchUpInside];
    [invite_button setTitle:@"Invite" forState:UIControlStateNormal];
    invite_button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:21];
    [invite_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [invite_button setImage:[[UIImage imageNamed:@"icon_invite.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [invite_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 75.0, 0.0, 30.0)];
    [invite_button setImageEdgeInsets:UIEdgeInsetsMake(0.0, 50.0, 0.0, 25.0)];
    invite_button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:invite_button];
    
    settings_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 68+90*3, self.view.frame.size.width, 90)];
    settings_button.backgroundColor = [UIColor clearColor];
    [settings_button addTarget:self action:@selector(settings_pressed) forControlEvents:UIControlEventTouchUpInside];
    [settings_button setTitle:@"Settings" forState:UIControlStateNormal];
    settings_button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:21];
    [settings_button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [settings_button setImage:[[UIImage imageNamed:@"icon_settings.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [settings_button setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 75.0, 0.0, 30.0)];
    [settings_button setImageEdgeInsets:UIEdgeInsetsMake(0.0, 50.0, 0.0, 25.0)];
    settings_button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self.view addSubview:settings_button];
    

    for (int n = 0; n < 3; n++) {
        [self createLineAtNumber:n];
    }
    
    [self menuButtonSelected:home_button];
}

-(void)menuButtonSelected:(UIButton*)button
{
    [self setSelectedMenuButton:button];
    if (button != home_button) {
        [self setUnselectedMenuButton:home_button];
    }
    if (button != trending_button) {
        [self setUnselectedMenuButton:trending_button];
    }
    if (button != invite_button) {
        [self setUnselectedMenuButton:invite_button];
    }
    if (button != settings_button) {
        [self setUnselectedMenuButton:settings_button];
    }
}

-(void)setSelectedMenuButton:(UIButton*)button
{
    [button setBackgroundColor:colorPrimaryGreen];
    [button setTintColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(void)setUnselectedMenuButton:(UIButton*)button
{
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTintColor:colorPrimaryGreen];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)createLineAtNumber:(NSInteger)lineNumber
{
    CGFloat firstLineY = CGRectGetMaxY(home_button.frame);
    CGFloat lineWidth = self.view.frame.size.width;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, firstLineY + 90 * lineNumber, lineWidth, 0.5)];
    line.backgroundColor = colorPrimaryGreen;
    [self.view addSubview:line];
}

-(void)home_pressed
{
    [self menuButtonSelected:home_button];
    
    HomeView *homeViewController = ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view;
    NSLog(@"home_pressed:equal = %i", [[SlideNavigationController sharedInstance].visibleViewController isEqual:homeViewController]);
    if (![[SlideNavigationController sharedInstance].visibleViewController isEqual:homeViewController])
    {
        homeViewController.isShowRandomPost = YES;
    }
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:homeViewController
															 withSlideOutAnimation:YES
																	 andCompletion:nil];
    [Flurry logEvent:@"home pressed"];
}
-(void)trending_pressed
{
    [self menuButtonSelected:trending_button];
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:trendingVC
															 withSlideOutAnimation:YES
																	 andCompletion:nil];
    [Flurry logEvent:@"favorites pressed"];
}

-(void)settings_pressed
{
    [self menuButtonSelected:settings_button];
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:settings_view
															 withSlideOutAnimation:YES
																	 andCompletion:nil];
    [Flurry logEvent:@"settings pressed"];
}
-(void)invite_pressed
{
    [self menuButtonSelected:invite_button];
    
    NSString * to_post = [NSString stringWithFormat:@"Check out the Pickfit app!  Download it at www.pickfitapp.com"];
    NSArray * social_items =  [NSArray arrayWithObject:to_post];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:social_items applicationActivities:nil];
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:((AppDelegate *)[UIApplication sharedApplication].delegate).home_view
                                                             withSlideOutAnimation:YES
                                                                     andCompletion:^{                                                                         
                                                                         
                                                                         [[SlideNavigationController sharedInstance] presentViewController:activityVC animated:YES completion:nil];
                                                                         [self menuButtonSelected:home_button];
                                                                     }];
    [Flurry logEvent:@"invite pressed"];
}
-(void)gender_changed
{
    HomeView *homeView = ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view;
    [homeView.posts removeAllObjects];
    [homeView api_call:@"get_random_posts"];
}
@end

//
//  SearchOccasionTableViewCell.m
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "SearchOccasionTableViewCell.h"

@interface SearchOccasionTableViewCell ()

@property (nonatomic, strong) UIView *whiteContentView;
@property (nonatomic, strong) UILabel *userNameLabel;

@end

@implementation SearchOccasionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    CGFloat cellHeight = 275.0;
    
    self.whiteContentView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width - 20.0, cellHeight - 20.0)];
    self.whiteContentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.whiteContentView];
    
    self.userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5.0, self.whiteContentView.frame.size.width, 23.0)];
    self.userNameLabel.minimumScaleFactor = 0.5;
    self.userNameLabel.center = CGPointMake(self.frame.size.width / 2.0, CGRectGetMidY(self.userNameLabel.frame));
    [self.whiteContentView addSubview:self.userNameLabel];
    
    self.userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 23, 23)];
    //self.userImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height / 2.0;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.center = CGPointMake(CGRectGetMinX(self.userNameLabel.frame) - 4.0 - self.userImageView.frame.size.width / 2.0, self.userNameLabel.center.y);
    [self.whiteContentView addSubview:self.userImageView];
    
    self.itemAImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70.0, 210.0)];
    self.itemAImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.itemAImageView.center = CGPointMake(25.0 + self.itemAImageView.frame.size.width / 2.0, self.whiteContentView.frame.size.height / 2.0 + 10.0);
    [self.whiteContentView addSubview:self.itemAImageView];
    
    self.itemBImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70.0, 210.0)];
    self.itemBImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.itemBImageView.center = CGPointMake(self.whiteContentView.frame.size.width - 25.0 - self.itemBImageView.frame.size.width / 2.0, self.whiteContentView.frame.size.height / 2.0 + 10.0);
    [self.whiteContentView addSubview:self.itemBImageView];
    
    UIColor *greenTextColor = [UIColor colorWithRed:155/255.0 green:210/255.0 blue:57/255.0 alpha:1.0];
    
    self.itemALabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30.0, 15.0)];
    self.itemALabel.textColor = greenTextColor;
    self.itemALabel.textAlignment = NSTextAlignmentRight;
    self.itemALabel.text = @"40";
    self.itemALabel.center = CGPointMake(self.whiteContentView.frame.size.width / 2 - self.itemALabel.frame.size.width / 2 - 20.0, self.whiteContentView.frame.size.height / 2.0);
    [self.whiteContentView addSubview:self.itemALabel];
    
    self.itemBLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30.0, 15.0)];
    self.itemBLabel.textColor = greenTextColor;
    self.itemBLabel.textAlignment = NSTextAlignmentRight;
    self.itemBLabel.text = @"60";
    self.itemBLabel.center = CGPointMake(self.whiteContentView.frame.size.width / 2 + self.itemBLabel.frame.size.width / 2, self.whiteContentView.frame.size.height / 2.0);
    [self.whiteContentView addSubview:self.itemBLabel];
    
    UILabel *percentALabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.itemALabel.frame), CGRectGetMinY(self.itemALabel.frame) - 3.0, 15.0, 15.0)];
    percentALabel.textColor = greenTextColor;
    percentALabel.font = [UIFont systemFontOfSize:14];
    percentALabel.text = @"%";
    [self.whiteContentView addSubview:percentALabel];
    
    UILabel *percentBLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.itemBLabel.frame), CGRectGetMinY(self.itemBLabel.frame) - 3.0, 15.0, 15.0)];
    percentBLabel.textColor = greenTextColor;
    percentBLabel.font = [UIFont systemFontOfSize:14];
    percentBLabel.text = @"%";
    [self.whiteContentView addSubview:percentBLabel];
    
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, self.itemAImageView.frame.size.height - 20.0)];
    separatorView.center = CGPointMake(self.whiteContentView.frame.size.width / 2.0, self.itemAImageView.center.y);
    separatorView.backgroundColor = [UIColor colorWithRed:209/255.0 green:209/255.0 blue:209/255.0 alpha:1.0];
    [self.whiteContentView addSubview:separatorView];
    
    return self;
}

-(void)setUserNameString:(NSString *)userNameString
{
    _userNameString = userNameString;
    self.userNameLabel.text = userNameString;
    [self.userNameLabel sizeToFit];
    CGRect newFrame = self.userNameLabel.frame;
    if (newFrame.size.width > self.whiteContentView.frame.size.width - self.userImageView.frame.size.width - 4.0) {
        newFrame.size.width = self.whiteContentView.frame.size.width - self.userImageView.frame.size.width - 4.0;
        self.userNameLabel.frame = newFrame;
    }
    
    self.userNameLabel.center = CGPointMake(self.whiteContentView.frame.size.width / 2.0, CGRectGetMidY(self.userNameLabel.frame));
    self.userImageView.center = CGPointMake(CGRectGetMinX(self.userNameLabel.frame) - 4.0 - self.userImageView.frame.size.width / 2.0, self.userNameLabel.center.y);
}

@end

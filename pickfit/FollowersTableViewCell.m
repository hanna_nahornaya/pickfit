//
//  FollowersTableViewCell.m
//  pickfit
//
//  Created by treasure on 3/20/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "FollowersTableViewCell.h"

@implementation FollowersTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.userNameLabel.font = [UIFont fontWithName:fontPrimaryName size:20];
    
    return self;
}

- (void)followButtonClick
{
    if (self.followersCellDelegate && [self.followersCellDelegate respondsToSelector:@selector(didButtonFollowPress:)])
    {
        [self.followersCellDelegate didButtonFollowPress:self];
        
        self.follow = !self.follow;
    }
}

@end

//
//  FeedVC.h
//  pickfit
//
//  Created by D.Gonchenko on 13.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedVCDelegate <NSObject>

-(void)feedVCNoFeeds;
-(void)feedVCGoToSearch;

@end

@interface FeedVC : UIViewController

@property (nonatomic, assign) id <FeedVCDelegate> feedVCDelegate;

@end

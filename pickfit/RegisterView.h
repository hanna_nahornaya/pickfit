//
//  RegisterView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "HomeView.h"

@interface RegisterView : UIViewController<UITextFieldDelegate>
{
 
    UIButton * back_button;
    UILabel * top_label;
    UITextField * username;
    UITextField * password;
    UITextField * confirm_password;
    UITextField * email;
    
    UILabel * i_am;
    UIButton * male_am;
    UIImageView * male_am_background;
    UIButton * female_am;
    UIImageView * female_am_background;
    
    UIButton * submit;
    
    BOOL male_pressed;
    BOOL female_pressed;
    
    UIImageView * background;
    
}
@end

//
//  OutfitDetailsView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/6/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "OutfitDetailsView.h"
#import "Flurry.h"
#import <Pinterest/Pinterest.h>
#import "RTSpinKitView.h"
//#import <MessageUI/MessageUI.h>
//#import <MessageUI/MFMessageComposeViewController.h>
//#import <FacebookSDK/FacebookSDK.h>
//#import <Twitter/Twitter.h>
//#import "PECropViewController.h"

@interface OutfitDetailsView () <MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, PECropViewControllerDelegate>

@end

@implementation OutfitDetailsView

@synthesize current_post;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"f7faf6"];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
        [self.view addSubview:top_background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"My Outfit";
        top_label.textColor = [UIColor blackColor];
        [self.view addSubview:top_label];
        
        share  = [[UIButton alloc] initWithFrame:CGRectMake(285, 20, 55/2, 73/2)];
        [share setImage:[UIImage imageNamed:@"icon_send_black.png"] forState:UIControlStateNormal];
        [share addTarget:self action:@selector(share_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: share];
        
        x = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: x];
        
        photo_stack_a  = [[UIButton alloc] initWithFrame:CGRectMake(20, 70, 320/2.5, 933/2.5)];
        [photo_stack_a setImage:[UIImage imageNamed:@"box_left.png"] forState:UIControlStateNormal];
        [photo_stack_a addTarget:self action:@selector(view_a) forControlEvents:UIControlEventTouchUpInside];
        [photo_stack_a setImage:[photo_stack_a imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [self.view addSubview: photo_stack_a];
        
        photo_stack_b  = [[UIButton alloc] initWithFrame:CGRectMake(170, 70, 320/2.5, 933/2.5)];
        [photo_stack_b setImage:[UIImage imageNamed:@"box_right.png"] forState:UIControlStateNormal];
        [photo_stack_b setImage:[photo_stack_b imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [photo_stack_b addTarget:self action:@selector(view_b) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: photo_stack_b];
        
        occasion_background = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 460/4.2, 440, 460/2.1, 80/2.1)];
        occasion_background.image = [UIImage imageNamed:@"field.png"];
        [self.view addSubview:occasion_background];
        
        occasion_label = [[UILabel alloc] initWithFrame:CGRectMake(occasion_background.frame.size.width/2-100, 11, 200, 20)];
        occasion_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        occasion_label.textAlignment = NSTextAlignmentCenter;
        occasion_label.backgroundColor = [UIColor clearColor];
        occasion_label.textColor = [UIColor blackColor];
        occasion_label.text = @"";
        [occasion_background addSubview:occasion_label];
        
        occasion_hanger = [[UIImageView alloc] initWithFrame:CGRectMake(25, 15, 37/2, 31/2)];
        occasion_hanger.image = [UIImage imageNamed:@"coat_hanger_small.png"];
        [occasion_background addSubview:occasion_hanger];
        
        [occasion_background bringSubviewToFront:occasion_label];
        
        line = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-565/4, 475, 565/2, 15/2)];
        line.image = [UIImage imageNamed:@"dots_gray_line.png"];
        [self.view addSubview:line];
        
        line_vert = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-14/4, 485, 14/2, 184/2.5)];
        line_vert.image = [UIImage imageNamed:@"dots_vertical_line.png"];
        [self.view addSubview:line_vert];
        
        outfit_a_label = [[UILabel alloc] initWithFrame:CGRectMake(30, 500, 120, 45)];
        outfit_a_label.font = [UIFont fontWithName:@"Neutraface Text" size:40];
        outfit_a_label.textAlignment = NSTextAlignmentCenter;
        outfit_a_label.text = @"";
        outfit_a_label.textColor = [self colorWithHexString:@"9ad426"];
        [self.view addSubview:outfit_a_label];
        
        outfit_b_label = [[UILabel alloc] initWithFrame:CGRectMake(180, 500, 120, 45)];
        outfit_b_label.font = [UIFont fontWithName:@"Neutraface Text" size:40];
        outfit_b_label.textAlignment = NSTextAlignmentCenter;
        outfit_b_label.text = @"";
        outfit_b_label.textColor = [self colorWithHexString:@"9ad426"];
        [self.view addSubview:outfit_b_label];

    }
    else
    {
        top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
        top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
        [self.view addSubview:top_background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 200/2, 30, 200, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.text = @"My Outfit";
        top_label.textColor = [UIColor blackColor];
        [self.view addSubview:top_label];
        
        share  = [[UIButton alloc] initWithFrame:CGRectMake(285, 20, 55/2, 73/2)];
        [share setImage:[UIImage imageNamed:@"icon_send_black.png"] forState:UIControlStateNormal];
        [share addTarget:self action:@selector(share_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: share];
        
        x = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60/2, 64/2)];
        [x setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
        [x addTarget:[SlideNavigationController sharedInstance] action:@selector(remove_outfit_creator) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: x];
        
        photo_stack_a  = [[UIButton alloc] initWithFrame:CGRectMake(35, 70, 320/3, 933/3)];
        [photo_stack_a setImage:[UIImage imageNamed:@"box_left.png"] forState:UIControlStateNormal];
        [photo_stack_a addTarget:self action:@selector(view_a) forControlEvents:UIControlEventTouchUpInside];
        [photo_stack_a setImage:[photo_stack_a imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [self.view addSubview: photo_stack_a];
        
        photo_stack_b  = [[UIButton alloc] initWithFrame:CGRectMake(180, 70, 320/3, 933/3)];
        [photo_stack_b setImage:[UIImage imageNamed:@"box_right.png"] forState:UIControlStateNormal];
        [photo_stack_b setImage:[photo_stack_b imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [photo_stack_b addTarget:self action:@selector(view_b) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: photo_stack_b];
        
        occasion_background = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 460/4.2, 380, 460/2.1, 80/2.1)];
        occasion_background.image = [UIImage imageNamed:@"field.png"];
        [self.view addSubview:occasion_background];
        
        occasion_label = [[UILabel alloc] initWithFrame:CGRectMake(occasion_background.frame.size.width/2-100, 11, 200, 20)];
        occasion_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        occasion_label.textAlignment = NSTextAlignmentCenter;
        occasion_label.backgroundColor = [UIColor clearColor];
        occasion_label.textColor = [UIColor blackColor];
        occasion_label.text = @"";
        [occasion_background addSubview:occasion_label];
        
        occasion_hanger = [[UIImageView alloc] initWithFrame:CGRectMake(25, 15, 37/2, 31/2)];
        occasion_hanger.image = [UIImage imageNamed:@"coat_hanger_small.png"];
        [occasion_background addSubview:occasion_hanger];
        
        [occasion_background bringSubviewToFront:occasion_label];
        
        line = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-565/4, 415, 565/2, 15/2)];
        line.image = [UIImage imageNamed:@"dots_gray_line.png"];
        [self.view addSubview:line];
        
        line_vert = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-14/4, 420, 14/2, 184/2.5)];
        line_vert.image = [UIImage imageNamed:@"dots_vertical_line.png"];
        [self.view addSubview:line_vert];
        
        outfit_a_label = [[UILabel alloc] initWithFrame:CGRectMake(30, 500-75, 120, 45)];
        outfit_a_label.font = [UIFont fontWithName:@"Neutraface Text" size:40];
        outfit_a_label.textAlignment = NSTextAlignmentCenter;
        outfit_a_label.text = @"";
        outfit_a_label.textColor = [self colorWithHexString:@"9ad426"];
        [self.view addSubview:outfit_a_label];
        
        outfit_b_label = [[UILabel alloc] initWithFrame:CGRectMake(180, 500-75, 120, 45)];
        outfit_b_label.font = [UIFont fontWithName:@"Neutraface Text" size:40];
        outfit_b_label.textAlignment = NSTextAlignmentCenter;
        outfit_b_label.text = @"";
        outfit_b_label.textColor = [self colorWithHexString:@"9ad426"];
        [self.view addSubview:outfit_b_label];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assign_tags:) name:@"TagsForPhoto" object:nil];
    

    user_pressed_a = NO;
    user_pressed_b = NO;
    
    a_tags = [[NSMutableArray alloc] init];
    b_tags = [[NSMutableArray alloc] init];
    
}
//-(void)load
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    float outfit_a_percent;
    float outfit_b_percent;
    
    if (current_post.photo_a_votes.floatValue != 0)
        outfit_a_percent = current_post.photo_a_votes.floatValue/current_post.total_votes.floatValue;
    else
        outfit_a_percent = 0;
    
    
    if (current_post.photo_b_votes.floatValue != 0)
        outfit_b_percent = current_post.photo_b_votes.floatValue/current_post.total_votes.floatValue;
    else
        outfit_b_percent = 0;
    
    int the_percent_a = outfit_a_percent*100;
    int the_percent_b = outfit_b_percent*100;
    
    
    outfit_a_label.text = [NSString stringWithFormat:@"%d%%", the_percent_a];
    outfit_b_label.text = [NSString stringWithFormat:@"%d%%", the_percent_b];
    
    occasion_label.text = current_post.occasion;
    
    CGFloat scale = [[UIScreen mainScreen] bounds].size.height == 568 ? 1.25 : 1.5;
    
    __block UIImageView *image2 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 6, 150/scale, 445/scale)];
        
    RTSpinKitView *spinView2 = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
    spinView2.center = CGPointMake(CGRectGetMidX(image2.frame), CGRectGetMidY(image2.frame));
    [image2 addSubview: spinView2];
    [spinView2 startAnimating];
    
    [image2 sd_setImageWithURL:[NSURL URLWithString:current_post.photo_a_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
    {
        [spinView2 removeFromSuperview];
        
        image2.alpha = 0.0;
        [UIView animateWithDuration:1.0
                         animations:^
        {
            image2.alpha = 1.0;
        }];
    }];
    
    image2.tag = 100;
    [photo_stack_a addSubview:image2];
    
    __block UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(3, 6, 150/scale, 445/scale)];
    
    RTSpinKitView *spinView1 = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleChasingDots color:[self colorWithHexString:@"9ad325"]];
    spinView1.center = CGPointMake(CGRectGetMidX(image1.frame), CGRectGetMidY(image1.frame));
    [image1 addSubview: spinView1];
    [spinView1 startAnimating];
    
    [image1 sd_setImageWithURL:[NSURL URLWithString:current_post.photo_b_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
    {
        [spinView1 removeFromSuperview];
        
        image1.alpha = 0.0;
        [UIView animateWithDuration:1.0
                         animations:^
        {
            image1.alpha = 1.0;
        }];
    }];
    image1.tag = 100;
    [photo_stack_b addSubview:image1];
}

-(void)assign_tags:(NSNotification*)message
{
    NSMutableArray * tags = message.object;
    if (user_pressed_a)
    {
        a_tags = tags;
    }
    else if(user_pressed_b)
    {
        b_tags = tags;
    }
}
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    if (user_pressed_a)
    {
        [self update_tags_a];
    }
    if (user_pressed_b)
    {
        [self update_tags_b];
    }
    [controller dismissViewControllerAnimated:YES completion:NULL];
    user_pressed_a = NO;
    user_pressed_b = NO;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
   
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
-(void)view_a
{
    user_pressed_a = YES;
    
    Post * the_post = current_post;
    
    UIImage * the_photo;
    for (UIImageView * to_test in photo_stack_a.subviews)
    {
        if (to_test.tag == 100)
        {
            the_photo = to_test.image;
        }
    }
    
    PECropViewController *controller_crop = [[PECropViewController alloc] init];
    controller_crop.delegate = self;
    controller_crop.image = the_photo;
    controller_crop.outfit_id = the_post.outfit_a_id;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller_crop];
    [self presentViewController:navigationController animated:YES completion:^{
        [controller_crop next:nil];
        [controller_crop current_tags];
    }];
    
}
-(void)view_b
{
    user_pressed_b = YES;
    
    Post * the_post = current_post;
    
    UIImage * the_photo;
    for (UIImageView * to_test in photo_stack_b.subviews)
    {
        if (to_test.tag == 100)
        {
            the_photo = to_test.image;
        }
    }
    
    PECropViewController *controller_crop = [[PECropViewController alloc] init];
    controller_crop.delegate = self;
    controller_crop.image = the_photo;
    controller_crop.outfit_id = the_post.outfit_b_id;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller_crop];
    [self presentViewController:navigationController animated:YES completion:^{
        
        [controller_crop next:nil];
        [controller_crop current_tags];
        
    }];
    
}
-(void)update_tags_a
{
    NSData * json_a = [NSJSONSerialization dataWithJSONObject:a_tags options:0 error:nil];   
    NSString * jsonString_a = [[NSString alloc] initWithData:json_a
                                                    encoding:NSUTF8StringEncoding];

    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/editTags/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@&tags=%@", current_post.outfit_a_id, jsonString_a] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
         }
         else if (!data)
         {
             NSLog(@"OutfitDetailsView:/outfit/editTags/: response data is nil");
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from /outfit/editTags/ = %@", test);
         }
     }];
}
-(void)update_tags_b
{
    NSData * json_a = [NSJSONSerialization dataWithJSONObject:b_tags options:0 error:nil];
    NSString * jsonString_a = [[NSString alloc] initWithData:json_a
                                                    encoding:NSUTF8StringEncoding];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/editTags/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@&tags=%@", current_post.outfit_b_id, jsonString_a] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
         }
         else if (!data)
         {
             NSLog(@"OutfitDetailsView:/outfit/editTags/: response data is nil");
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from /outfit/editTags/ = %@", test);
         }
     }];
}
-(void)share_pressed
{
    [Flurry logEvent:@"share pressed closet"];
    
    UIViewController * popin = [[UIViewController alloc] init];
    popin.view.backgroundColor = [self colorWithHexString:@"9ad325"];
    [popin setPreferedPopinContentSize:CGSizeMake(280.0, 160.0)];
    
    UILabel * share_prompt = [[UILabel alloc] initWithFrame:CGRectMake(-20, 10, popin.view.frame.size.width, 22)];
    share_prompt.text = @"Share this with your friends:";
    share_prompt.font =[UIFont fontWithName:@"Neutraface Text" size:20];
    share_prompt.textColor = [UIColor whiteColor];
    share_prompt.textAlignment = NSTextAlignmentCenter;
    [popin.view addSubview:share_prompt];
    
    UIButton * text_button  = [[UIButton alloc] initWithFrame:CGRectMake(20, 50, 134/2.1, 76/2.1)];
    [text_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [text_button setTitle:@"Text" forState:UIControlStateNormal];
    [[text_button layer] setBorderWidth:1.0f];
    [[text_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    text_button.layer.cornerRadius = 2;
    text_button.clipsToBounds = YES;
    [text_button addTarget:self action:@selector(open_text_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: text_button];
    
    UIButton * email_button  = [[UIButton alloc] initWithFrame:CGRectMake(88, 50, 143/2.1, 76/2.1)];
    [email_button setTitle:@"Email" forState:UIControlStateNormal];
    [[email_button layer] setBorderWidth:1.0f];
    [[email_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    email_button.layer.cornerRadius = 2;
    email_button.clipsToBounds = YES;
    [email_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [email_button addTarget:self action:@selector(open_email_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: email_button];
    
    UIButton * facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(160, 50, 200/2.0, 76/2.1)];
    [facebook_button setTitle:@"Facebook" forState:UIControlStateNormal];
    [[facebook_button layer] setBorderWidth:1.0f];
    [[facebook_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    facebook_button.layer.cornerRadius = 2;
    facebook_button.clipsToBounds = YES;
    [facebook_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [facebook_button addTarget:self action:@selector(share_facebook) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: facebook_button];
    
    UIButton * pintrest_button  = [[UIButton alloc] initWithFrame:CGRectMake(35, 100, 200/2.0, 76/2.1)];
    [pintrest_button setTitle:@"Pinterest" forState:UIControlStateNormal];
    [[pintrest_button layer] setBorderWidth:1.0f];
    [[pintrest_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    pintrest_button.layer.cornerRadius = 2;
    pintrest_button.clipsToBounds = YES;
    [pintrest_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [pintrest_button addTarget:self action:@selector(share_pintrest) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: pintrest_button];
    
    UIButton * twitter_button  = [[UIButton alloc] initWithFrame:CGRectMake(145, 100, 200/2.0, 76/2.1)];
    [twitter_button setTitle:@"Twitter" forState:UIControlStateNormal];
    [[twitter_button layer] setBorderWidth:1.0f];
    [[twitter_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    twitter_button.layer.cornerRadius = 2;
    twitter_button.clipsToBounds = YES;
    [twitter_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [twitter_button addTarget:self action:@selector(share_twitter) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: twitter_button];
    
    [popin setPopinTransitionStyle:2];
    
    BKTBlurParameters *blurParameters = [BKTBlurParameters new];
    blurParameters.alpha = 1.0f;
    blurParameters.radius = 8.0f;
    blurParameters.saturationDeltaFactor = 1.8f;
    blurParameters.tintColor = [UIColor colorWithRed:0.966 green:0.851 blue:0.038 alpha:0.2];
    [popin setBlurParameters:blurParameters];
    
    //Add option for a blurry background
    [popin setPopinOptions:[popin popinOptions] |BKTPopinBlurryDimmingView];
    
    
    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    
    [self presentPopinController:popin animated:YES completion:^{
        NSLog(@"Popin presented !");
    }];
}

-(void)share_pintrest
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    [Flurry logEvent:@"share pinterest closet"];
    UIImageView * photo_a_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_a.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_a_image.image = to_find.image;
        }
    }
    
    UIImageView * photo_b_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_b.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_b_image.image = to_find.image;
        }
    }
    
    /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
    photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
    
    UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
     
    NSData * image_data = UIImageJPEGRepresentation(image, .5);*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"OutfitDetailsView:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     //[self dismissCurrentPopinControllerAnimated:YES];
                     Pinterest *  pinterest = [[Pinterest alloc] initWithClientId:@"1440608" urlSchemeSuffix:@"prod"];
                     [pinterest createPinWithImageURL:[NSURL URLWithString:[response_dict objectForKey:@"link"]]
                                            sourceURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",current_post.post_id]]
                                          description:@"Which outfit do you prefer? Click on the photo to vote."];

                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];

    
    
}
-(void)share_twitter
{
    UIImageView * photo_a_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_a.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_a_image.image = to_find.image;
        }
    }
    
    UIImageView * photo_b_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_b.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_b_image.image = to_find.image;
        }
    }
    
    /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
    photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
    
    UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    
    NSString * message = [NSString stringWithFormat:@"Which outfit do you prefer? Vote at %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/",current_post.post_id.stringValue]];
    [self TWPostImage:[UIImage imageWithData:image_data] withStatus:message];
    
}
- (void)TWPostImage:(UIImage *)image withStatus:(NSString *)status
{
    [self dismissCurrentPopinControllerAnimated:YES];
    [Flurry logEvent:@"share twitter closet"];
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //[accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
     {
         ACAccount *ac;
         if(granted) {
             NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
             int i=0;
             for (ACAccount *account in accountsArray ) {
                 i++;
                 //NSLog(@"Account name: %@", account.username);
                 ac=account;
             }
             if (i==0)
             {
                 [self dismissCurrentPopinControllerAnimated:YES];
                 
                 [[[UIAlertView alloc] initWithTitle:@"Wait"
                                             message:@"Please setup Twitter Account Settigns > Twitter > Sign In "
                                            delegate:nil
                                   cancelButtonTitle:@"Thanks!"
                                   otherButtonTitles:nil]
                  show];
                 return ;
                 
             }
             
             ACAccountType *twitterType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];;
             
             //SLRequestHandler requestHandler;
             
             SLRequestHandler requestHandler =
             ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseData) {
                         NSInteger statusCode = urlResponse.statusCode;
                         if (statusCode >= 200 && statusCode < 300)
                         {
                             NSDictionary *postResponseData =
                             [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:NSJSONReadingMutableContainers
                                                               error:NULL];
                             NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
                             //AppDelegate * d =[[UIApplication sharedApplication] delegate];
                             //NSString* link = [NSString stringWithFormat:@"Your Message %@", @"testing post"];
                             //[d linkSelected:link PointerToSelf:self];
                             /*[[[UIAlertView alloc] initWithTitle:@"Result"
                              message:[NSString stringWithFormat:@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]]
                              delegate:nil
                              cancelButtonTitle:@"Thanks!"
                              otherButtonTitles:nil]
                              show];*/
                             
                         }
                         else
                         {
                             NSLog(@"[ERROR] Server responded: status code %ld %@", statusCode,
                                   [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
                         }
                     }
                     else {
                         NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
                     }
                 });
             };
             //});
             ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
             ^(BOOL granted, NSError *error) {
                 if (granted) {
                     NSArray *accounts = [accountStore accountsWithAccountType:twitterType];
                     NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                                   @"/1.1/statuses/update_with_media.json"];
                     NSDictionary *params = @{@"status" : status};
                     SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                             requestMethod:SLRequestMethodPOST
                                                                       URL:url
                                                                parameters:params];
                     NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
                     [request addMultipartData:imageData
                                      withName:@"media[]"
                                          type:@"image/jpeg"
                                      filename:@"image.jpg"];
                     [request setAccount:[accounts lastObject]];
                     
                     [request performRequestWithHandler:requestHandler];
                     //});
                 }
                 else {
                     NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                           [error localizedDescription]);
                 }
             };
             
             [accountStore requestAccessToAccountsWithType:twitterType
                                                   options:NULL
                                                completion:accountStoreHandler];
         }else
         {
             [self dismissCurrentPopinControllerAnimated:YES];
             
             [[[UIAlertView alloc] initWithTitle:@"Wait"
                                         message:@"Please Settigns > Twitter > In bottom Enable DealsHype to post"
                                        delegate:nil
                               cancelButtonTitle:@"Thanks!"
                               otherButtonTitles:nil]
              show];
         }
     }];
}
-(void)share_facebook
{
    NSNumber *post_id = current_post.post_id;
    
    UIImageView * photo_a_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_a.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_a_image.image = to_find.image;
        }
    }
    
    UIImageView * photo_b_image = [[UIImageView alloc] init];
    for (UIImageView * to_find in photo_stack_b.subviews)
    {
        if (to_find.tag == 100)
        {
            photo_b_image.image = to_find.image;
        }
    }
    
    /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
    photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
    
    UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData * image_data = UIImageJPEGRepresentation(image, .5);*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
                 
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"OutfitDetailsView:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [self share_to_facebook:post_id :[response_dict objectForKey:@"link"]];
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];
    
}
-(void)share_to_facebook:(NSNumber*)post_id :(NSString*)link
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        // permission does not exist
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                  {
                      if (error)
                      {
                          NSLog(@"error:%@",error);
                      }
                      else
                      {
                          
                          NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                         @"Help me pick an outfit!", @"name",
                                                         @"Which outfit do you prefer?", @"description",
                                                         [NSString stringWithFormat:@"http://pickfitapp.com/postVote//%@/",post_id], @"link",
                                                         link, @"picture",
                                                         nil];
                          
                          // Make the request
                          [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                       parameters:params
                                                       HTTPMethod:@"POST"
                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                    if (!error)
                                                    {
                                                        // Link posted successfully to Facebook
                                                        NSLog(@"result: %@", result);
                                                        [Flurry logEvent:@"share facebook closet"];
                                                        //[self dismissCurrentPopinControllerAnimated:YES];
                                                        
                                                    }
                                                    else
                                                    {
                                                        // An error occurred, we need to handle the error
                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                        NSLog(@"%@", error.description);
                                                    }
                                                    
                                                    
                                                }];
                          
                      }
                      
                  }];
             }
             
         } ];
        
        
    }
    else
    {
        // permission exists
        
        
        // If the Facebook app is installed and we can present the share dialog
        
        // Present the share dialog
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 
                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @"Help me pick an outfit!", @"name",
                                                @"Which outfit do you prefer?", @"description",
                                                [NSString stringWithFormat:@"http://pickfitapp.com/postVote//%@/",post_id], @"link",
                                                link, @"picture",
                                                nil];
                 
                 // Make the request
                 [FBRequestConnection startWithGraphPath:@"/me/feed"
                                              parameters:params
                                              HTTPMethod:@"POST"
                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          // Link posted successfully to Facebook
                          NSLog(@"result: %@", result);
                          [Flurry logEvent:@"share facebook closet"];
                          //[self dismissCurrentPopinControllerAnimated:YES];
                          
                      }
                      else
                      {
                          // An error occurred, we need to handle the error
                          // See: https://developers.facebook.com/docs/ios/errors
                          NSLog(@"%@", error.description);
                      }
                      
                      
                  }];
                 
             }
             
         }];
        
    }
}
-(void)open_text_message
{
    //[self.navigationController dismissCurrentPopinControllerAnimated:YES];
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.body =  [NSString stringWithFormat:@"Help me pick an outfit!  Which outfit do you prefer? %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", current_post.post_id]];
        controller.messageComposeDelegate = self;
        
        UIImageView * photo_a_image = [[UIImageView alloc] init];
        for (UIImageView * to_find in photo_stack_a.subviews)
        {
            if (to_find.tag == 100)
            {
                photo_a_image.image = to_find.image;
            }
        }
        
        UIImageView * photo_b_image = [[UIImageView alloc] init];
        for (UIImageView * to_find in photo_stack_b.subviews)
        {
            if (to_find.tag == 100)
            {
                photo_b_image.image = to_find.image;
            }
        }
        
        /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
        photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
        
        UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        [to_render addSubview:photo_a_image];
        [to_render addSubview:photo_b_image];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData * image_data = UIImagePNGRepresentation(image);
        [controller addAttachmentData:imgData typeIdentifier:@"public.content" filename:@"image.png"];
        [self presentViewController:controller animated:YES completion:nil];*/
        
        NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
        [controller addAttachmentData:image_data typeIdentifier:@"public.content" filename:@"image.png"];
        
        [self presentViewController:controller animated:YES completion:nil];
        
        [Flurry logEvent:@"share text closet"];
    }
}
-(void)open_email_message
{
    //[self.navigationController dismissCurrentPopinControllerAnimated:YES];
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"Help me choose an outfit!"];
        [mailCont setMessageBody: [NSString stringWithFormat:@"Help me pick an outfit!  Which outfit do you prefer? %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", current_post.post_id]] isHTML:NO];
        
        UIImageView * photo_a_image = [[UIImageView alloc] init];
        for (UIImageView * to_find in photo_stack_a.subviews)
        {
            if (to_find.tag == 100)
            {
                photo_a_image.image = to_find.image;
            }
        }
        
        UIImageView * photo_b_image = [[UIImageView alloc] init];
        for (UIImageView * to_find in photo_stack_b.subviews)
        {
            if (to_find.tag == 100)
            {
                photo_b_image.image = to_find.image;
            }
        }
        
        /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
        photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
        
        UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        [to_render addSubview:photo_a_image];
        [to_render addSubview:photo_b_image];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        NSData * image_data = UIImagePNGRepresentation(image);*/
        
        NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
        [mailCont addAttachmentData:image_data mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"a.jpg"]];
        UIGraphicsEndImageContext();
        
        [self presentViewController:mailCont animated:YES completion:nil];
        
        [Flurry logEvent:@"share email closet"];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"We do not have access to your mail app. Please set up your email app in order to send a post via email." delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
    
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
@end

//
//  FollowersViewController.m
//  pickfit
//
//  Created by treasure on 3/18/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "FollowersViewController.h"
#import "SlideNavigationController.h"
#import "FollowersTableViewCell.h"
#import "User.h"
#import "UserVC.h"

@interface FollowersViewController () <UITableViewDelegate, UITableViewDataSource, FollowersTableViewCellDelegate>
{
    UIImageView *topBackgroundImageView;
    UILabel *titleLabel;
    UIButton *editButton;
    UIButton *backButton;
    UILabel *noFollowersLabel;
}

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tableArray;

@end

@implementation FollowersViewController

static NSString * const mainFont = @"Avenir";

static CGFloat const kTopOffset = 30;

static CGFloat const kTitleLabelHeight = 22;
static CGFloat const kTitleLabelFontSize = 20;

static CGFloat const kBackButtonWidth = 50;
static CGFloat const kBackButtonHeight = 30;

static NSString *followersTableViewCellID = @"followersTableViewCellID";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect mainViewRect = self.view.frame;
    
    topBackgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    topBackgroundImageView.image = [UIImage imageNamed:@"header_white_bar.png"];
    //[self.view addSubview:topBackgroundImageView];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kBackButtonWidth, kTopOffset, (mainViewRect.size.width - kBackButtonWidth * 2), kTitleLabelHeight)];
    titleLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"My Followers", @"");
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleLabel];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kBackButtonWidth, kBackButtonHeight)];
    imageView.image = [UIImage imageNamed:@"button_green_arrow_back"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, kTopOffset, kBackButtonWidth, kBackButtonHeight)];
    [backButton addTarget:self action:@selector(onBackButton) forControlEvents:UIControlEventTouchUpInside];
    [backButton addSubview:imageView];
    backButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:backButton];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(topBackgroundImageView.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(topBackgroundImageView.frame)) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[FollowersTableViewCell class] forCellReuseIdentifier:followersTableViewCellID];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    noFollowersLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame) * 2, mainViewRect.size.width, kTitleLabelHeight)];
    noFollowersLabel.font = [UIFont fontWithName:mainFont size:kTitleLabelFontSize];
    noFollowersLabel.textAlignment = NSTextAlignmentCenter;
    noFollowersLabel.text = NSLocalizedString(@"You have 0 followers", @"");
    noFollowersLabel.textColor = [UIColor blackColor];
    noFollowersLabel.backgroundColor = [UIColor clearColor];
    noFollowersLabel.hidden = YES;
    [self.view addSubview:noFollowersLabel];
    
    self.tableArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self performRequest:@"getUserFollowers"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

- (void)onBackButton
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

- (void)performRequest:(NSString *)requestString
{
    if ([requestString isEqualToString:@"getUserFollowers"])
    {
        [self.tableArray removeAllObjects];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/getUserFollowers/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&limit=0&offset=0", [defaults objectForKey:@"user_id"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 dispatch_async(dispatch_get_main_queue(),^{
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"FollowersVC:/user/getUserFollowers/: response data is nil");
                 });
             }
             else
             {
                 NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (responseArray.count > 0)
                     {
                         for (NSDictionary *dictionary in responseArray)
                         {
                             User *user = [[User alloc] initWithDictionary:dictionary];
                             [self.tableArray addObject:user];
                             
                             //[self.tableArray addObject:[dictionary objectForKey:@"username"]];
                         }
                         
                         noFollowersLabel.hidden = YES;
                         
                         [self.tableView reloadData];
                     }
                     else
                     {
                         noFollowersLabel.hidden = NO;
                         [self.tableView reloadData];
                     }
                 });
             }
         }];
    }
}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    FollowersTableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:followersTableViewCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.followersCellDelegate = self;
    
    return cell;
}

-(void)tableView:(UITableView *)aTableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (aTableView.isDecelerating)
    {
        return;
    }
    
    FollowersTableViewCell *followersTableViewCell = (FollowersTableViewCell *)cell;
    
    User *user = self.tableArray[indexPath.row];
    
    followersTableViewCell.userNameLabel.text = user.name;
    followersTableViewCell.follow = [[user.attributesDictionary objectForKey:@"is_follower"] boolValue];
    [followersTableViewCell.userImageView sd_setImageWithURL:[NSURL URLWithString:user.imageUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
     {
         if (error || !image)
         {
             followersTableViewCell.userImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
         }
     }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserVC *userVC = [[UserVC alloc] init];
    userVC.userID = ((User *)[self.tableArray objectAtIndex:indexPath.row]).userId;
    [[SlideNavigationController sharedInstance] pushViewController:userVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableArray.count;
}

#pragma mark - FollowingTableViewCellDelegate

- (void)didButtonFollowPress:(FollowersTableViewCell *)cell
{
    //NSLog(@"didButtonFollowPress:");
    
    NSInteger indexToDelete = [self.tableView indexPathForCell:cell].row;
    User *user = [self.tableArray objectAtIndex:indexToDelete];
    BOOL isFollowing = [[user.attributesDictionary objectForKey:@"is_follower"] boolValue];
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:(isFollowing ? @"%@/follower/deleteFollower/" : @"%@/follower/createFollower/"), SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"user_id=%@&following_user_id=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], user.userId] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
             
             dispatch_async(dispatch_get_main_queue(),^{
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"FollowersVC:/follower/deleteFollower || createFollower/: response data is nil");
             });
         }
         else
         {
             NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString *status = [responseDictionary objectForKey:@"status"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [user.attributesDictionary setObject:[NSNumber numberWithBool:(isFollowing ? NO : YES)] forKey:@"is_follower"];
                 }
                 else if ([status isEqualToString:@"fail"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     [alert show];
                 }
             });
         }
         
         cell.follow = [[user.attributesDictionary objectForKey:@"is_follower"] boolValue];
     }];
}

@end

//
//  FavoritesView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "AQGridView.h"
#import "FavoriteOutfit.h"

@interface FavoritesView : UIViewController<AQGridViewDelegate, AQGridViewDataSource>
{
    UIButton * closet_button;
    UIButton * menu_button;
    UILabel * top_label;
    UIImageView * top_background;
    AQGridView * favorites_table;
    NSMutableArray * favorites;
}
-(void)api_call:(NSString*)to_call;
@end

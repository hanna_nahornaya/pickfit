//
//  SearchOccasionTableViewCell.h
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchOccasionTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) NSString *userNameString;

@property (nonatomic, strong) UIImageView *itemAImageView;
@property (nonatomic, strong) UIImageView *itemBImageView;

@property (nonatomic, strong) UILabel *itemALabel;
@property (nonatomic, strong) UILabel *itemBLabel;

@end

//
//  AppDelegate.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"
#import <Crashlytics/Crashlytics.h>

#warning add to build phases ./Crashlytics.framework/run 164c6b4ccc5bf1988bcec03012abc67365dea683

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    int cacheSizeMemory = 16*1024*1024; // 16MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    //[Crashlytics startWithAPIKey:@"164c6b4ccc5bf1988bcec03012abc67365dea683"];
    [Flurry startSession:@"Y7KHQ652ZVYMFX74RCVR"];
    
    self.left_view = [[MenuView alloc] init];
    self.right_view = [[ClosetView alloc] init];
    self.logo_view = [[LogoView alloc] init];
    self.home_view = [[HomeView alloc] init];
    
    [[PSSClient sharedClient] setPartnerID:@"uid6096-25671564-94"];
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"user_id"])
    {
        //NSLog(@"user_id = %@", [defaults objectForKey:@"user_id"]);
        
        if ([defaults objectForKey:@"fb_id"])
        {
            //this means they are a faecbook login
            [self api_call:@"fb_login"];
            self.slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController:self.logo_view];
            self.logo_view.go_home = YES;
        }
        else
        {
            //normal login
            [self api_call:@"login"];
            self.slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController:self.logo_view];
            self.logo_view.go_home = YES;
        }
    }
    else
    {
         self.slideNavigationController = [[SlideNavigationController alloc] initWithRootViewController:self.logo_view];
         self.logo_view.go_home = NO;
    }
    
   
    self.slideNavigationController.navigationBarHidden = YES;
    
    self.slideNavigationController.rightMenu = nil;//self.right_view;
	self.slideNavigationController.leftMenu = self.left_view;
    
    self.occasions = [[NSMutableArray alloc] init];
    
    [self api_call:@"occasions"];
    [self api_call:@"sponsor_frequency"];
   
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = self.slideNavigationController;
    [self.window makeKeyAndVisible];
    return YES;
}
-(void)api_call:(NSString*)to_call
{
    if ([to_call isEqualToString:@"login"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/login/", SERVICE_URL]];
        
        NSLog(@"/user/login/postData = %@", [NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", [defaults objectForKey:@"email"], [defaults objectForKey:@"password"], [defaults objectForKey:@"device_token"]]);
        
        NSData *postData= [[NSString stringWithFormat:@"username=%@&password=%@&ios_token=%@", [defaults objectForKey:@"email"], [defaults objectForKey:@"password"], [defaults objectForKey:@"device_token"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             //to check Keep-Alive
             //NSDictionary *dic = [(NSHTTPURLResponse *)response allHeaderFields];
             
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"AppDelegate:/user/login/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 NSLog(@"/user/login/ = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     NSString * user_id = [NSString stringWithFormat:@"%ld", [[response_dict objectForKey:@"user_id"] longValue]];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setObject:user_id forKey:@"user_id"];
                         [defaults synchronize];
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Error"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                         
                     }
                 });
             }
         }];
    }
    else if ([to_call isEqualToString:@"occasions"])
    {
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/occasion/getOccasions/", SERVICE_URL]];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"AppDelegate:/occasion/getOccasions/: response data is nil");
                 });
             }
             else
             {
                 NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"/occasion/getOccasions/ = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     for (NSDictionary * to_add in response_array)
                     {
                         Occasion * to_create = [[Occasion alloc] init];
                         to_create.occasion_id = [to_add objectForKey:@"id"];
                         to_create.name = [to_add objectForKey:@"label"];
                         [self.occasions addObject:to_create];
                     }
                     
                 });
             }
             
         }];
        
    }
    else if([to_call isEqualToString:@"fb_login"])
    {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/fbLogin/", SERVICE_URL]];
        
        NSLog(@"/user/fbLogin/postData = %@", [NSString stringWithFormat:@"fb_id=%@&email=%@&gender=%@&username=%@&full_name=%@&ios_token=%@", [defaults objectForKey:@"fb_id"], [defaults objectForKey:@"email"], [defaults objectForKey:@"gender"], [defaults objectForKey:@"username"], [defaults objectForKey:@"full_name"], [defaults objectForKey:@"device_token"]]);
        
        NSData *postData= [[NSString stringWithFormat:@"fb_id=%@&email=%@&gender=%@&username=%@&full_name=%@&ios_token=%@", [defaults objectForKey:@"fb_id"], [defaults objectForKey:@"email"], [defaults objectForKey:@"gender"], [defaults objectForKey:@"username"], [defaults objectForKey:@"full_name"], [defaults objectForKey:@"device_token"]] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"AppDelegate:/user/fbLogin/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 NSLog(@"/user/fbLogin/ = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     NSString * message = [response_dict objectForKey:@"msg"];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         
                         
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         
                         UIAlertView *alert = [[UIAlertView alloc]
                                               initWithTitle:@"Error"
                                               message:message
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                         
                         alert.tag = 1;
                         [alert show];
                         
                     }
                     
                 });
             }
             
         }];
    }
    else if([to_call isEqualToString:@"sponsor_frequency"])
    {
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/sponsor/getSponsorFrequency/", SERVICE_URL]];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"AppDelegate:/sponsor/getSponsorFrequency/: response data is nil");
                 });
             }
             else
             {
                 //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
                 //NSLog(@"data back from /sponsor/getSponsorFrequency/ = %@" ,test);
                 
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];                
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //self.frequency_of_sponosors = 0;
                     //self.frequency_of_sponosors = [NSNumber numberWithInteger:4];
                     self.frequency_of_sponosors = [response_dict objectForKey:@"frequency"];
                 });
             }
         }];
    }
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *deviceToken_string = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken_string = [deviceToken_string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken: %@", deviceToken_string);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken_string forKey:@"device_token"];
    [defaults synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:1] forKey:@"pickfitEnableNotifications"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", [error localizedDescription]);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:0] forKey:@"pickfitEnableNotifications"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}
@end

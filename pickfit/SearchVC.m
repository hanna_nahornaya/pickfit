//
//  SearchVC.m
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "SearchVC.h"
#import "SlideNavigationController.h"
#import "SearchUsersTableViewCell.h"
#import "SearchOccasionTableViewCell.h"
#import "Occasion.h"
#import "AppDelegate.h"
#import "UserVC.h"
#import "SlideNavigationController.h"
#import "UIImageView+AFNetworking.h"

@interface SearchVC () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIPickerViewDataSource, UIPickerViewDelegate, SearchUsersTableViewCellDelegate, UIGestureRecognizerDelegate>
{
    UIButton * closet_button;
    UIButton * menu_button;
    UILabel * top_label;
    UIImageView * top_background;
    NSMutableArray * outfits;
    
    NSMutableArray * occasions;
    Occasion * selected_occasion;
    
    UIGestureRecognizer * tapRecognizer;
}

@property (nonatomic, strong) UIButton *usersButton;
@property (nonatomic, strong) UIButton *occasionButton;

@property (nonatomic, strong) UITableView *usersTableView;
@property (nonatomic, strong) UITableView *occasionTableView;

@property (nonatomic, strong) NSMutableArray *searchResultUsersArray;
@property (nonatomic, strong) NSMutableArray *searchResultOccasionArray;

@property (nonatomic, strong) UIButton *selectOccasionsButton;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSString *searchString;
@property (nonatomic) BOOL lastSearchUserResult;

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) Occasion *selectedOccation;
@property (nonatomic) NSInteger fidingOccasionsCount;

@property (nonatomic) NSInteger usersOffset;
@property (nonatomic) NSInteger occasionsOffset;

@property (nonatomic, strong) NSMutableDictionary *followersDictionary; //key - UserID : value - YES/NO

@end

static NSString *usersTableViewCellID = @"usersTableViewCellID";
static NSString *occasionTableViewCellID = @"occasionTableViewCellID";

@implementation SearchVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.usersOffset = 0;
    self.occasionsOffset = 0;
    self.searchString = @"";
    self.fidingOccasionsCount = 0;
    self.lastSearchUserResult = NO;
    
    self.followersDictionary = [NSMutableDictionary new];
    
    occasions = [(AppDelegate *)[[UIApplication sharedApplication] delegate] occasions];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-120/2, 30, 120, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Search";
    top_label.textColor = [UIColor blackColor];
    [self.view addSubview:top_label];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
    [closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
    [closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    menu_button = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 60 / 2, 64 / 2)];
    [menu_button setImage:[UIImage imageNamed:@"button_green_x.png"] forState:UIControlStateNormal];
    [menu_button addTarget:self action:@selector(dismissSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:menu_button];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(11, CGRectGetMaxY(top_background.frame), self.view.frame.size.width - 11.0*2.0, 33.0)];
    self.searchBar.delegate = self;
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchBar.backgroundColor = [UIColor whiteColor];
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchBar.keyboardType = UIKeyboardTypeASCIICapable;
    self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [self.searchBar setKeyboardAppearance:UIKeyboardAppearanceAlert];
    [self.view addSubview:self.searchBar];
    
    self.selectOccasionsButton = [[UIButton alloc] initWithFrame:self.searchBar.frame];
    self.selectOccasionsButton.layer.cornerRadius = 3.0;
    self.selectOccasionsButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.selectOccasionsButton.layer.borderWidth = 0.5;
    self.selectOccasionsButton.backgroundColor = [UIColor clearColor];
    self.selectOccasionsButton.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:19];
    [self.selectOccasionsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.selectOccasionsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    [self.selectOccasionsButton setTitle:((Occasion*)[occasions firstObject]).name forState:UIControlStateNormal];
    [self.selectOccasionsButton addTarget:self action:@selector(changeOccasion) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.selectOccasionsButton];
    
    self.selectedOccation = [occasions firstObject];
    
    self.usersButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.searchBar.frame), self.view.frame.size.width / 2, 35.0)];
    [self.usersButton setTitle:@"Users" forState:UIControlStateNormal];
    [self addAttributesToButton:self.usersButton];
    [self.view addSubview:self.usersButton];
    
    self.occasionButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2, CGRectGetMaxY(self.searchBar.frame), self.view.frame.size.width / 2, 35.0)];
    [self.occasionButton setTitle:@"Occasion" forState:UIControlStateNormal];
    [self addAttributesToButton:self.occasionButton];
    [self.view addSubview:self.occasionButton];
    
    UIView *separatorForButtonsView = [UIView new];
    separatorForButtonsView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    separatorForButtonsView.frame = CGRectMake(0, 0, 0.5, 15.0);
    separatorForButtonsView.center = CGPointMake(self.view.frame.size.width / 2.0, CGRectGetMidY(self.usersButton.frame));
    [self.view addSubview:separatorForButtonsView];
    
    self.searchResultUsersArray = [[NSMutableArray alloc] initWithArray:@[]];
    self.searchResultOccasionArray = [[NSMutableArray alloc] initWithArray:@[]];
    
    CGRect tablesFrame = CGRectMake(0, CGRectGetMaxY(self.occasionButton.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.occasionButton.frame));
    
    self.usersTableView = [[UITableView alloc] initWithFrame:tablesFrame style:UITableViewStyleGrouped];
    self.usersTableView.delegate = self;
    self.usersTableView.dataSource = self;
    [self.usersTableView registerClass:[SearchUsersTableViewCell class] forCellReuseIdentifier:usersTableViewCellID];
    self.usersTableView.separatorInset = UIEdgeInsetsZero;
    [self.view addSubview:self.usersTableView];
    
    self.occasionTableView = [[UITableView alloc] initWithFrame:tablesFrame style:UITableViewStyleGrouped];
    self.occasionTableView.delegate = self;
    self.occasionTableView.dataSource = self;
    [self.occasionTableView registerClass:[SearchOccasionTableViewCell class] forCellReuseIdentifier:occasionTableViewCellID];
    self.occasionTableView.separatorInset = UIEdgeInsetsZero;
    [self.view addSubview:self.occasionTableView];
    
    if (self.openType == SearchVCOpenTypeUsers) {
        [self selectedButton:self.usersButton];
    }else
        [self selectedButton:self.occasionButton];
    
    self.pickerView = [[UIPickerView alloc] initWithFrame:self.view.bounds];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    self.pickerView.hidden = YES;
    self.pickerView.showsSelectionIndicator = YES;
    self.pickerView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.pickerView];
    
    UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewTapGestureRecognized:)];
    gestureRecognizer.delegate = self;
    [self.pickerView addGestureRecognizer:gestureRecognizer];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    //[self.view addGestureRecognizer:tapRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self refreshUsersSearchTableWithOffset:0];
    [self refreshOccasionsSearchTableWithOccasionID:[self.selectedOccation.occasion_id integerValue] withOffset:0];
    
//    if (self.usersButton.selected)
//    {
//        [self.searchBar resignFirstResponder];
//    }
//    else
//    {
//        [self.searchBar becomeFirstResponder];
//    }
}

-(void)changeOccasion
{
    self.pickerView.hidden = NO;
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.view addGestureRecognizer:tapRecognizer];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return true;
}

-(void)addAttributesToButton:(UIButton*)button
{
    [button setTitleColor:colorPrimaryGreen forState:UIControlStateSelected];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)selectedButton:(UIButton*)button
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [self.usersButton setSelected:(button == self.usersButton)];
    [self.occasionButton setSelected:!(button == self.usersButton)];
    self.usersTableView.hidden = !(button == self.usersButton);
    self.occasionTableView.hidden = (button == self.usersButton);
    self.searchBar.hidden = !(button == self.usersButton);
    self.selectOccasionsButton.hidden = (button == self.usersButton);
}

-(void)dismissSearch
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

-(void)setOpenType:(SearchVCOpenType)openType
{
    _openType = openType;
    if (openType == SearchVCOpenTypeUsers) {
        [self selectedButton:self.usersButton];
    }else
        [self selectedButton:self.occasionButton];
}

#pragma mark - Table view
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.usersTableView) {
        SearchUsersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:usersTableViewCellID forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.cellDelegate = self;
        NSDictionary *userDictionary = self.searchResultUsersArray[indexPath.row];
        
        cell.userDict = userDictionary;
        cell.userNameLabel.text = [NSString stringWithFormat:@"""@%@", [userDictionary objectForKey:@"username"]];
        
        NSString *userImageURLString = [userDictionary objectForKey:@"user_image"];
        if (![userImageURLString isKindOfClass:[NSNull class]]) {
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:userImageURLString] placeholderImage:[UIImage imageNamed:@"user_profile_empty.png"]];
        }else
            cell.userImageView.image = [UIImage imageNamed:@"user_profile_empty.png"];
        
        return cell;
    }
    else
    {
        SearchOccasionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:occasionTableViewCellID forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        Post *post = self.searchResultOccasionArray[indexPath.row];
        
        cell.userNameString = [NSString stringWithFormat:@"""@%@", post.userName];
        
        [cell.itemAImageView sd_setImageWithURL:[NSURL URLWithString:post.photo_a_url]];
        [cell.itemBImageView sd_setImageWithURL:[NSURL URLWithString:post.photo_b_url]];
        
        CGFloat votesAPercent = [post.total_votes floatValue] > 0 ? [post.photo_a_votes floatValue] / [post.total_votes floatValue] : 0;
        CGFloat votesBPercent = [post.total_votes floatValue] > 0 ? [post.photo_b_votes floatValue] / [post.total_votes floatValue] : 0;
                
        cell.itemALabel.text = [NSString stringWithFormat:@"%.0f", votesAPercent * 100.0];
        cell.itemBLabel.text = [NSString stringWithFormat:@"%.0f", votesBPercent * 100.0];
        
        if (![post.userImageURL isKindOfClass:[NSNull class]])
        {
            if ([post.userImageURL containsString:@":/"])
            {
                [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:post.userImageURL]];
            }
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.usersTableView)
    {
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        NSDictionary *selectedUser = self.searchResultUsersArray[indexPath.row];
        NSInteger selectedUserIndex = [[selectedUser objectForKey:@"id"] integerValue];
        UserVC *userVC = [UserVC new];
        userVC.userID = selectedUserIndex;
        
        [[SlideNavigationController sharedInstance] pushViewController:userVC animated:YES];
    }
    else
    {
        Post *post = self.searchResultOccasionArray[indexPath.row];
        UserVC *userVC = [UserVC new];
        userVC.userID = post.user_id;
        
        [[SlideNavigationController sharedInstance] pushViewController:userVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.usersTableView) {
        return CGFLOAT_MIN;
    }else
    {
        return 32.0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.usersTableView) {
        return 75.0;
    }else
    {
        return 275.0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.usersTableView) {
        return self.searchResultUsersArray.count;
    }else
        return self.searchResultOccasionArray.count;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.usersTableView) {
        return @"";
    }else
    {
        return [NSString stringWithFormat:@"  %ld results", self.fidingOccasionsCount];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isDecelerating)
    {
        return;
    }
    
    if (tableView == self.usersTableView)
    {
        SearchUsersTableViewCell *usersCell = (SearchUsersTableViewCell*)cell;
        NSDictionary *userDictionary = self.searchResultUsersArray[indexPath.row];
        NSString *idString = [[userDictionary objectForKey:@"id"] stringValue];
        NSInteger followInteger = [[self.followersDictionary objectForKey:idString] integerValue];
        BOOL follow = !(followInteger == 0);
        
        usersCell.follow = follow;
        
        
        
        if (indexPath.row == self.searchResultUsersArray.count - 1 && !self.lastSearchUserResult) {
            self.usersOffset++;
            [self refreshUsersSearchTableWithOffset:self.usersOffset];
        }
    }
    else
    {
        if (indexPath.row == self.searchResultOccasionArray.count - 1 && self.fidingOccasionsCount > self.searchResultOccasionArray.count)
        {
            self.occasionsOffset++;
            [self refreshOccasionsSearchTableWithOccasionID:[self.selectedOccation.occasion_id integerValue] withOffset:self.occasionsOffset];
        }
    }
}

#pragma mark - Picker view
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return occasions.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return ((Occasion*)occasions[row]).name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedOccation = occasions[row];
    [self.selectOccasionsButton setTitle:self.selectedOccation.name forState:UIControlStateNormal];
    self.fidingOccasionsCount = 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 35;
}

-(void)didTapAnywhere:(UITapGestureRecognizer *)recognizer
{
    if (!self.pickerView.hidden)
    {
        self.pickerView.hidden = YES;
        [self.view removeGestureRecognizer:tapRecognizer];
        [self refreshOccasionsSearchTableWithOccasionID:[self.selectedOccation.occasion_id integerValue] withOffset:0];
    }
    else if ([self.searchBar isFirstResponder])
    {
        [self.searchBar resignFirstResponder];
    }
}

- (void)pickerViewTapGestureRecognized:(UITapGestureRecognizer*)gestureRecognizer
{
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view.superview];
    CGSize rowSize = [self.pickerView rowSizeForComponent:0];
    CGRect rowRect = CGRectMake(0, (self.pickerView.frame.size.height - rowSize.height) / 2, rowSize.width, rowSize.height);
    
    if (CGRectContainsPoint(rowRect, touchPoint))
    {
        self.pickerView.hidden = YES;
        [self.view removeGestureRecognizer:tapRecognizer];
        [self refreshOccasionsSearchTableWithOccasionID:[self.selectedOccation.occasion_id integerValue] withOffset:0];
    }
}

#pragma mark - Search bar
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    //NSLog(@"searchBar:textDidChange:searchText = %@", searchText);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.lastSearchUserResult = NO;
        
        self.searchString = searchText;
        
        [self refreshUsersSearchTableWithOffset:0];
    });
}

#pragma mark - API

-(void)refreshUsersSearchTableWithOffset:(NSInteger)offset
{
    if (offset == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.usersOffset = 0;
            [self.searchResultUsersArray removeAllObjects];
            [self.usersTableView reloadData];
        });
    }
    
    //NSLog(@"refreshUsersSearchTable:searchText = %@", self.searchString);
    
    if (self.searchString.length == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.searchResultUsersArray removeAllObjects];
            [self.usersTableView reloadData];
        });
    }
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUsersByName/", SERVICE_URL]];
        NSData *requestData = [[NSString stringWithFormat:@"user_id=%@&query=%@&limit=10&offset=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], self.searchString, offset*10] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        __block NSError *error;
        if (!error) {
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
            request.HTTPMethod = @"POST";
            request.HTTPBody = requestData;
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if(error)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        alert.tag = 1;
                        //[alert show];
                        
                    });
                }
                else if (!data)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"SearchVC:/user/getUsersByName/: response data is nil");
                    });
                }
                else
                {
                    NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    
                    if (error)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.usersTableView reloadData];
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //NSLog(@"dispatch_async:searchText = %@", self.searchString);
                            
                            if (self.searchString.length == 0)
                            {
                                [self.searchResultUsersArray removeAllObjects];
                            }
                            else
                            {
                                if (self.usersOffset == 0)
                                {
                                    [self.searchResultUsersArray removeAllObjects];
                                }
                                
                                self.lastSearchUserResult = (response_array.count < 10.0);
                                
                                for (NSDictionary *user in response_array)
                                {
                                    [self.followersDictionary setObject:@([[user objectForKey:@"is_follower"] boolValue]) forKey:[[user objectForKey:@"id"] stringValue]];
                                }
                                
                                [self.searchResultUsersArray addObjectsFromArray:response_array];
                            }
                            
                            [self.usersTableView reloadData];
                        });
                    }
                    
//                    if (response_array.count < 10.0)
//                    {
//                        
//                    }
//                    if (!error)
//                    {
//                        for (NSDictionary *user in response_array)
//                        {
//                            [self.followersDictionary setObject:@([[user objectForKey:@"is_follower"] boolValue]) forKey:[[user objectForKey:@"id"] stringValue]];
//                        }
//                        [self.searchResultUsersArray addObjectsFromArray:response_array];
//                        
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self.usersTableView reloadData];
//                        });
//                    }
                }
            }];
        }
    }
}

-(void)refreshOccasionsSearchTableWithOccasionID:(NSInteger)occasionID withOffset:(NSInteger)offset
{
    if (offset == 0)
    {
        self.occasionsOffset = 0;
        [self.searchResultOccasionArray removeAllObjects];
        [self.occasionTableView setContentOffset:CGPointZero];
        [self.occasionTableView reloadData];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getOutfitsByOccasion/", SERVICE_URL]];
    NSData *requestData = [[NSString stringWithFormat:@"user_id=%@&occasion_id=%ld&limit=10&offset=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], occasionID, offset*10] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
    request.HTTPMethod = @"POST";
    request.HTTPBody = requestData;
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SearchVC:/user/getOutfitsByOccasion/: response data is nil");
             });
         }
         else
         {
             NSDictionary * responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             dispatch_async(dispatch_get_main_queue(), ^{

                 if (!error)
                 {
                     self.fidingOccasionsCount = [[responseDict objectForKey:@"count"] integerValue];
                     NSArray *posts = [responseDict objectForKey:@"posts"];
                     
                     for (NSDictionary *postDict in posts)
                     {
                         Post *post = [Post new];
                         post.photo_a_url = [postDict objectForKey:@"outfit_a__image"];
                         post.photo_b_url = [postDict objectForKey:@"outfit_b__image"];
                         post.occasion = [postDict objectForKey:@"occasion__label"];
                         post.photo_a_votes = [postDict objectForKey:@"outfit_a_votes"];
                         post.photo_b_votes = [postDict objectForKey:@"outfit_b__votes"];
                         post.total_votes = [postDict objectForKey:@"total_votes"];
                         post.outfit_a_id = [postDict objectForKey:@"outfit_a__id"];
                         post.outfit_b_id = [postDict objectForKey:@"outfit_b__id"];
                         post.outfit_a_flagged = [postDict objectForKey:@"outfit_a__flagged"];
                         post.outfit_b_flagged = [postDict objectForKey:@"outfit_b__flagged"];
                         post.userName = [postDict objectForKey:@"username"];
                         post.userImageURL = [postDict objectForKey:@"user_image"];
                         post.user_id = [[postDict objectForKey:@"user_id"] integerValue];
                         
                         [self.searchResultOccasionArray addObject:post];
                     }
                     
                     [self.occasionTableView reloadData];
                }
             });
         }
     }];
}

#pragma mark - Scroll view
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

#pragma mark - Search User Cell Delegate
-(void)needFollowing:(BOOL)needFollowing toUserID:(NSInteger)userID
{
    NSString *URL = needFollowing ? @"/follower/createFollower/" : @"/follower/deleteFollower/";
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVICE_URL, URL]];
    NSString *reqestDataString = [NSString stringWithFormat:@"user_id=%@&following_user_id=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"], (long)userID];
    NSData *requestData = [reqestDataString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    __block NSError *error;
    if (!error) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        request.HTTPBody = requestData;
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(error)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag = 1;
                    //[alert show];
                    
                });
            }
            else if (!data)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"SearchVC:/follower/deleteFollower/: response data is nil");
                });
            }
            else
            {
                NSDictionary * response_Dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if (!error) {
                    if ([[response_Dict objectForKey:@"status"] isEqualToString:@"success"]) {
                        if (needFollowing) {
                            [self.followersDictionary setObject:@YES forKey:[NSString stringWithFormat:@"%ld", (long)userID]];
                        }else
                        {
                            [self.followersDictionary setObject:@NO forKey:[NSString stringWithFormat:@"%ld", (long)userID]];
                        }
                    }
                }
            }
        }];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([self.usersTableView isEqual:scrollView])
    {
        NSInteger lastVisibleCell = ((NSIndexPath *)[[self.usersTableView indexPathsForVisibleRows] lastObject]).row;
        
        if (lastVisibleCell == self.searchResultUsersArray.count - 1 && !self.lastSearchUserResult)
        {
            self.usersOffset++;
            [self refreshUsersSearchTableWithOffset:self.usersOffset];
        }
    }
    else
    {
        NSInteger lastVisibleCellIndex = ((NSIndexPath *)[[self.occasionTableView indexPathsForVisibleRows] lastObject]).row;
        
        if  (lastVisibleCellIndex == self.searchResultOccasionArray.count - 1 && self.fidingOccasionsCount > self.searchResultOccasionArray.count)
        {
            self.occasionsOffset++;
            [self refreshOccasionsSearchTableWithOccasionID:[self.selectedOccation.occasion_id integerValue] withOffset:self.occasionsOffset];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

@end






//
//  SearchUsersTableViewCell.m
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "SearchUsersTableViewCell.h"

@implementation SearchUsersTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    CGFloat cellHeight = 75.0;
    
    self.userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46.0, 46.0)];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width / 2.0;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.userImageView.center = CGPointMake(20.0 + self.userImageView.frame.size.width / 2.0, cellHeight / 2.0);
    [self.contentView addSubview:self.userImageView];
    self.userImageView.backgroundColor = [UIColor lightGrayColor];
    
    self.followButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 83.0, 29.0)];
    [self.followButton setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
    [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    self.followButton.titleLabel.font = [UIFont fontWithName:fontPrimaryName size:12];
    self.followButton.layer.borderColor = colorPrimaryGreen.CGColor;
    self.followButton.layer.borderWidth = 2.0;
    self.followButton.layer.cornerRadius = 3.0;
    self.followButton.layer.masksToBounds = YES;
    self.followButton.center = CGPointMake(self.frame.size.width - self.followButton.frame.size.width / 2.0 - 20.0, cellHeight / 2.0);
    [self.followButton addTarget:self action:@selector(followButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.followButton];
    
    self.userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userImageView.frame) + 15.0, 0, CGRectGetMinX(self.followButton.frame) -CGRectGetMaxX(self.userImageView.frame) - 15.0, cellHeight)];
    self.userNameLabel.numberOfLines = 0;
    self.userNameLabel.minimumScaleFactor = 0.5;
    [self.contentView addSubview:self.userNameLabel];
    self.userNameLabel.text = @"Nissa B";
    
    return self;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.userImageView.image = nil;
}

-(void)setFollow:(BOOL)follow
{
    _follow = follow;
    if (follow) {
        [self.followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.followButton.backgroundColor = colorPrimaryGreen;
        [self.followButton setTitle:@"Following" forState:UIControlStateNormal];
    }else
    {
        [self.followButton setTitleColor:colorPrimaryGreen forState:UIControlStateNormal];
        self.followButton.backgroundColor = [UIColor whiteColor];
        [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
    }
}

-(void)followButtonClick
{
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(needFollowing:toUserID:)])
    {
        [self.cellDelegate needFollowing:!self.follow toUserID:[[self.userDict objectForKey:@"id"] integerValue]];
        self.follow = !self.follow;
    }
}

@end

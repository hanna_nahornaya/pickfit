//
//  ForgotPasswordView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/15/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "ForgotPasswordView.h"

@interface ForgotPasswordView ()

@end

@implementation ForgotPasswordView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"9ad426"];
    
    background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    background.image = [UIImage imageNamed:@"icons.png"];
    background.alpha = .5;
    [self.view addSubview:background];
    
    back_button = [[UIButton alloc] initWithFrame:CGRectMake(9, 23, 53/2, 70/2)];
    [back_button addTarget:self action:@selector(back_pressed) forControlEvents:UIControlEventTouchUpInside];
    [back_button setImage:[UIImage imageNamed:@"button_back_arrow.png"] forState:UIControlStateNormal];
    [self.view addSubview:back_button];
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-200/2, 30, 200, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Forgot your password?";
    top_label.textColor = [UIColor whiteColor];
    [self.view addSubview:top_label];
    
    info_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-300/2, 100, 300, 20)];
    info_label.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    info_label.textAlignment = NSTextAlignmentCenter;
    info_label.text = @"Please enter your email:";
    info_label.textColor = [UIColor whiteColor];
    [self.view addSubview:info_label];
    
    UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)]; 
    
    username = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 75+60, 264, 45)];
    username.textAlignment = NSTextAlignmentLeft;
    username.backgroundColor = [UIColor whiteColor];
    username.placeholder = @" Email";
    UIColor *color = [UIColor lightGrayColor];
    username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:username.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    username.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    username.textColor = [UIColor blackColor];
    username.autocorrectionType = UITextAutocorrectionTypeNo;
    username.delegate = self;
    username.returnKeyType = UIReturnKeyNext;
    username.keyboardType = UIKeyboardTypeASCIICapable;
    username.autocapitalizationType = UITextAutocapitalizationTypeNone;
    username.layer.cornerRadius = 3;
    username.autocorrectionType = UITextAutocorrectionTypeNo;// this value vary as per your desire
    username.clipsToBounds = YES;
    [username setKeyboardAppearance:UIKeyboardAppearanceAlert];
    username.leftView = paddingView;
    username.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview: username];
    
    
    
    submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 508, 320, 60)];
    [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
    [submit setTitle:@"Submit" forState:UIControlStateNormal];
    [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
    [submit setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
    submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
    [self.view addSubview:submit];
    
    
    // Do any additional setup after loading the view.
}
-(void)submit_pressed
{
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/resetSimplePassword/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"identifier=%@", username.text] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"ForgotPassword:/user/resetSimplePassword/: response data is nil");
             });
         }
         else
         {
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             //NSLog(@"/user/resetSimplePassword/ = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                
                 
                 if ([status isEqualToString:@"success"])
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Your new password has been sent to your email." delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     [alert show];
                     
                     email.text = @"";
                     username.text = @"";
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                 }
                 
             });
         }
         
     }];
    
}
-(void)back_pressed
{
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (BOOL) textFieldShouldReturn: (UITextField *) txtField
{
    if (txtField == username)
    {
        [email becomeFirstResponder];
    }
    [txtField resignFirstResponder];
    return YES;
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer
{
    [email resignFirstResponder];
    [username resignFirstResponder];
}
@end

//
//  SearchVC.h
//  pickfit
//
//  Created by D.Gonchenko on 16.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {SearchVCOpenTypeUsers, SearchVCOpenTypeOccasions} SearchVCOpenType;

@interface SearchVC : UIViewController

@property (nonatomic, strong) UIViewController *backVC;
@property (nonatomic) SearchVCOpenType openType;

@end

//
//  OutfitDetailsView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/6/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "Post.h"
//#import "UIViewController+MaryPopin.h"

@interface OutfitDetailsView : UIViewController
{
    UIButton * x;
    UIButton * share;
    UILabel * top_label;
    UIImageView * top_background;
    
    UIButton * photo_stack_a;
    UIButton * photo_stack_b;
    
    UIImageView * occasion_background;
    UILabel * occasion_label;
    UIImageView * occasion_hanger;
    
    UIImageView * line;
    UIImageView * line_vert;
    
    UILabel * outfit_a_label;
    UILabel * outfit_b_label;
    
    NSMutableArray * a_tags;
    NSMutableArray * b_tags;
    
    BOOL user_pressed_a;
    BOOL user_pressed_b;
    
}
@property (strong) Post * current_post;
-(void)load;
@end

//
//  FeedVC.m
//  pickfit
//
//  Created by D.Gonchenko on 13.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "FeedVC.h"
#import "Post.h"
#import "FeedTableViewCell.h"
#import "UserVC.h"
#import "UIImageView+AFNetworking.h"
#import "SlideNavigationController.h"

@interface FeedVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *postsArray;
@property (nonatomic, strong) UIRefreshControl * refreshControl;
@property (nonatomic) NSInteger pageOffset;
@property (nonatomic) BOOL lastFeedResult;

@property (nonatomic, strong) UIButton *goToSearchButton;

@end

static NSString *cellID = @"cellID";

@implementation FeedVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageOffset = 0;
    
    self.postsArray = [NSMutableArray new];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [self.tableView registerClass:[FeedTableViewCell class] forCellReuseIdentifier:cellID];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithRed:227/255.0 green:227/255.0 blue:227/255.0 alpha:1.0];
    self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
    self.tableView.estimatedRowHeight = 554 / 2.0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.lastFeedResult = NO;
    
    self.goToSearchButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50.0)];
    [self.goToSearchButton setTitle:@"Follow other Pickfit’ers" forState:UIControlStateNormal];
    [self.goToSearchButton addTarget:self action:@selector(needSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.goToSearchButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.goToSearchButton.hidden = YES;
    [self.view addSubview:self.goToSearchButton];
//    
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    self.refreshControl.backgroundColor = [UIColor clearColor];
//    self.refreshControl.tintColor = [UIColor whiteColor];
//    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
//    [self.tableView addSubview:self.refreshControl];
    
}
-(void)refresh
{
    [self getFeedsForLimit:10 andOffset:self.pageOffset];
    [self.refreshControl endRefreshing];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}
-(void)viewWillAppear:(BOOL)animated
{
    //[self getFeedsForLimit:10 andOffset:self.pageOffset];
    self.pageOffset = 0;
    [self getFeedsForLimit:10 andOffset:self.pageOffset];
}

-(void)needSearch
{
    if (self.feedVCDelegate && [self.feedVCDelegate respondsToSelector:@selector(feedVCGoToSearch)]) {
        [self.feedVCDelegate feedVCGoToSearch];
    }
}

#pragma mark - Table view
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.postsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 554 / 2.0;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 554 / 2.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[FeedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];;
    }
    
    Post *post = self.postsArray[indexPath.row];
    
    cell.userNameString = [NSString stringWithFormat:@"""@%@", post.userName];
    
    [cell.itemAImageView sd_setImageWithURL:[NSURL URLWithString:post.photo_a_url]];
    [cell.itemBImageView sd_setImageWithURL:[NSURL URLWithString:post.photo_b_url]];
    
    CGFloat votesAPercent = [post.total_votes floatValue] > 0 ? [post.photo_a_votes floatValue] / [post.total_votes floatValue] : 0;
    CGFloat votesBPercent = [post.total_votes floatValue] > 0 ? [post.photo_b_votes floatValue] / [post.total_votes floatValue] : 0;
    
    cell.itemALabel.text = [NSString stringWithFormat:@"%.0f", votesAPercent * 100.0];
    cell.itemBLabel.text = [NSString stringWithFormat:@"%.0f", votesBPercent * 100.0];
    
    if (![post.userImageURL isKindOfClass:[NSNull class]])
    {
       // if ([post.userImageURL containsString:@":/"])
        {
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:post.userImageURL]];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    
//    NSDictionary *selectedUser = self.searchResultUsersArray[indexPath.row];
//    NSInteger selectedUserIndex = [[selectedUser objectForKey:@"id"] integerValue];
    UserVC *userVC = [UserVC new];
    userVC.userID = ((Post *)[self.postsArray objectAtIndex:indexPath.row]).user_id;
    
    [[SlideNavigationController sharedInstance] pushViewController:userVC animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.isDecelerating)
    {
        return;
    }
    
    if (indexPath.row == self.postsArray.count - 1  && !self.lastFeedResult)
    {
        self.pageOffset++;
        [self getFeedsForLimit:10 andOffset:self.pageOffset];
    }
    
}
#pragma mark - API

-(void)getFeedsForLimit:(NSInteger)limit andOffset:(NSInteger)offset
{
    //NSLog(@"getFeedsForLimit");
    
    if (offset == 0)
    {
        [self.postsArray removeAllObjects];
        [self.tableView reloadData];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *requestURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/getUserFollowingsRecentPosts/", SERVICE_URL]];
    NSData *requestData = [[NSString stringWithFormat:@"user_id=%@&limit=%ld&offset=%ld", [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"],limit, offset*10] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    __block NSError *error;
    if (!error) {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestURL];
        request.HTTPMethod = @"POST";
        request.HTTPBody = requestData;
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if(error)
            {
                //NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"FeedVC:/user/getUserFollowingsRecentPosts/:error:%@", [error localizedDescription]);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag = 1;
                    //[alert show];
                    
                });
            }
            else if (!data)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"FeedVC:/user/getUserFollowingsRecentPosts/: response data is nil");
                });
            }
            else
            {
                NSArray * response_array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if (response_array.count == 0 && offset == 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (self.feedVCDelegate && [self.feedVCDelegate respondsToSelector:@selector(feedVCNoFeeds)]) {
                            [self.feedVCDelegate feedVCNoFeeds];
                            self.goToSearchButton.hidden = NO;
                            self.tableView.hidden = YES;
                        }
                        
                    });
                    
                }else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        self.goToSearchButton.hidden = YES;
                        self.tableView.hidden = NO;
                        
                        if (response_array.count < 10.0)
                        {
                            self.lastFeedResult = YES;
                        }
                        
                        if (!error)
                        {
                            for (NSDictionary *postDict in response_array)
                            {
                                Post *post = [Post new];
                                post.photo_a_url = [postDict objectForKey:@"outfit_a__image"];
                                post.photo_b_url = [postDict objectForKey:@"outfit_b__image"];
                                post.occasion = [postDict objectForKey:@"occasion__label"];
                                post.photo_a_votes = [postDict objectForKey:@"outfit_a_votes"];
                                post.photo_b_votes = [postDict objectForKey:@"outfit_b__votes"];
                                post.total_votes = [postDict objectForKey:@"total_votes"];
                                post.outfit_a_id = [postDict objectForKey:@"outfit_a__id"];
                                post.outfit_b_id = [postDict objectForKey:@"outfit_b__id"];
                                post.outfit_a_flagged = [postDict objectForKey:@"outfit_a__flagged"];
                                post.outfit_b_flagged = [postDict objectForKey:@"outfit_b__flagged"];
                                post.userName = [postDict objectForKey:@"username"];
                                post.userImageURL = [postDict objectForKey:@"user_image"];
                                post.user_id = [[postDict objectForKey:@"user_id"] integerValue];
                                
                                [self.postsArray addObject:post];
                            }
                        }
                        
                        [self.tableView reloadData];
                    });
                }
                
            }
        }];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger lastVisibleCell = ((NSIndexPath *)[[self.tableView indexPathsForVisibleRows] lastObject]).row;
    
    if (lastVisibleCell == self.postsArray.count - 1  && !self.lastFeedResult)
    {
        self.pageOffset++;
        [self getFeedsForLimit:10 andOffset:self.pageOffset];
    }
}

@end

//
//  ChangePasswordView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 12/8/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "ChangePasswordView.h"

@interface ChangePasswordView ()

@end

@implementation ChangePasswordView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [self colorWithHexString:@"9ad426"];
    
    background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    background.image = [UIImage imageNamed:@"icons.png"];
    background.alpha = .5;
    [self.view addSubview:background];
    
    close_button = [[UIButton alloc] initWithFrame:CGRectMake(9, 23, 53/2, 70/2)];
    [close_button addTarget:self action:@selector(close_pressed) forControlEvents:UIControlEventTouchUpInside];
    [close_button setImage:[UIImage imageNamed:@"button_back_arrow.png"] forState:UIControlStateNormal];
    [self.view addSubview:close_button];
    
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-200/2, 30, 200, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Change your password";
    top_label.textColor = [UIColor whiteColor];
    [self.view addSubview:top_label];
    
    UIGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    
    old_password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 75+60, 264, 45)];
    old_password.textAlignment = NSTextAlignmentLeft;
    old_password.backgroundColor = [UIColor whiteColor];
    old_password.placeholder = @"  New Password";
    UIColor *color = [UIColor lightGrayColor];
    old_password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:old_password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    old_password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    old_password.textColor = [UIColor blackColor];
    old_password.autocorrectionType = UITextAutocorrectionTypeNo;
    old_password.delegate = self;
    old_password.secureTextEntry = YES;
    old_password.returnKeyType = UIReturnKeyNext;
    old_password.keyboardType = UIKeyboardTypeASCIICapable;
    old_password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    old_password.layer.cornerRadius = 3;
    old_password.autocorrectionType = UITextAutocorrectionTypeNo;// this value vary as per your desire
    old_password.clipsToBounds = YES;
    [old_password setKeyboardAppearance:UIKeyboardAppearanceAlert];
    old_password.leftView = paddingView;
    old_password.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview: old_password];
    
    new_password = [[UITextField alloc] initWithFrame: CGRectMake(self.view.frame.size.width*.5-264/2, 135+60, 264, 45)];
    new_password.textAlignment = NSTextAlignmentLeft;
    new_password.backgroundColor = [UIColor whiteColor];
    new_password.placeholder = @"  Confirm New Password";
    new_password.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    new_password.textColor = [UIColor blackColor];
    new_password.delegate = self;
    new_password.secureTextEntry = YES;
    new_password.returnKeyType = UIReturnKeyNext;
    new_password.keyboardType = UIKeyboardTypeEmailAddress;
    new_password.autocapitalizationType = UITextAutocapitalizationTypeNone;
    new_password.autocorrectionType = UITextAutocorrectionTypeNo;
    new_password.layer.cornerRadius = 3; // this value vary as per your desire
    new_password.clipsToBounds = YES;
    new_password.leftView = paddingView2;
    new_password.leftViewMode = UITextFieldViewModeAlways;
    new_password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:new_password.placeholder attributes:@{NSForegroundColorAttributeName: color}];
    [new_password setKeyboardAppearance:UIKeyboardAppearanceAlert];
    [self.view addSubview: new_password];
    
    info_label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(old_password.frame), 100, old_password.frame.size.width, 20)];
    info_label.font = [UIFont fontWithName:@"Neutraface Text" size:16];
    info_label.textAlignment = NSTextAlignmentCenter;
    info_label.text = @"Please enter your new password below";
    info_label.textColor = [UIColor whiteColor];
    [self.view addSubview:info_label];
    
    submit = [[UIButton alloc] initWithFrame:CGRectMake(0, 508, 320, 60)];
    [submit addTarget:self action:@selector(submit_pressed) forControlEvents:UIControlEventTouchUpInside];
    [submit setTitle:@"Submit" forState:UIControlStateNormal];
    [submit.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
    [submit setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
    submit.backgroundColor = [self colorWithHexString:@"76ac0a"];
    [self.view addSubview:submit];
    
}
-(void)change_password:(NSString *)password
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([old_password.text isEqualToString:new_password.text])
    {
        //make the activity indicator start spinning
        
        NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/user/editUser/", SERVICE_URL]];
        
        NSData *postData= [[NSString stringWithFormat:@"user_id=%@&username=%@&password=%@&email=%@&get_notified=%@&ios_token=%@&full_name=%@&facebook_token=%@&twitter_token=%@&gender=%@&gender_preference=%@", [defaults objectForKey:@"user_id"], @"", password, @"", @"", @"", @"", @"", @"", @"", @""] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if(error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     alert.tag = 1;
                     //[alert show];
                     
                 });
             }
             else if (!data)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     NSLog(@"ChangePasswordView:/user/editUser/: response data is nil");
                 });
             }
             else
             {
                 NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                 
                 //NSLog(@"update user = %@", response_dict);
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     
                     NSString * status = [response_dict objectForKey:@"status"];
                     
                     if ([status isEqualToString:@"success"])
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Your password has been changed." delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                         alert.delegate = self;
                         alert.tag = 1;
                         [alert show];
                         
                         old_password.text = @"";
                         new_password.text = @"";
                         
                         [defaults setObject:password forKey:@"password"];                   
                         
                         
                     }
                     else if([status isEqualToString:@"fail"])
                     {
                         
                         
                     }
                     
                 });
             }
             
         }];

    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Issue" message:@"Your passwords do not match!" delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
    
    
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        //this is the save alertview.
        [self close_pressed];
    }
    
}
-(void)submit_pressed
{
    [self change_password:new_password.text];
}
-(void)close_pressed
{
    [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (BOOL) textFieldShouldReturn: (UITextField *) txtField
{
    if (txtField == old_password)
    {
        [new_password becomeFirstResponder];
    }
    [txtField resignFirstResponder];
    return YES;
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer
{
    [new_password resignFirstResponder];
    [old_password resignFirstResponder];
}

@end

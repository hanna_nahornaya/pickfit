//
//  Outfit.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "Outfit.h"

#define kOnePixel  (1 / [[UIScreen mainScreen] scale])

@implementation Outfit

static NSString * const mainFont = @"Avenir";

static CGFloat const kCellBackgroudViewLeftOffset = 11;
static CGFloat const kCellBackgroudViewBottomOffset = 2;

static CGFloat const kImageViewTopOffset = 16;
static CGFloat const kImageViewLeftOffset = 22;
static CGFloat const kImageViewWidth = 70;
static CGFloat const kImageViewHeight = 209;

static CGFloat const kMiddleSeparatorViewTopOffset = 71;
static CGFloat const kMiddleSeparatorViewHeight = 90;//17 52

static CGFloat const kImageViewLabelLeftOffset = 7;
static CGFloat const kImageViewLabelWidth = 45;
static CGFloat const kImageViewLabelHeight = 24;
static CGFloat const kImageViewLabelFontSize = 16;

- (id)initWithData:(Post*)incoming
{
    //this is where you adjust the size of the cell.
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        // Initialization code
        self.this_post = incoming;
        self.loaded = NO;
        //now that we have the info we need to load it.
        
        self.cellBackgroudView = [[UIView alloc] initWithFrame:CGRectZero];
        self.cellBackgroudView.backgroundColor = [UIColor whiteColor];
        [self.cellScrollView addSubview:self.cellBackgroudView];
        
        self.frame_a = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.frame_a.backgroundColor = [UIColor whiteColor];
        self.frame_a.userInteractionEnabled = NO;
        self.frame_a.contentMode = UIViewContentModeScaleAspectFit;
        [self.cellBackgroudView addSubview:self.frame_a];
        
        self.frame_b = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.frame_b.backgroundColor = [UIColor whiteColor];
        self.frame_b.userInteractionEnabled = NO;
        self.frame_b.contentMode = UIViewContentModeScaleAspectFit;
        [self.cellBackgroudView addSubview:self.frame_b];
        
        self.outfit_a_label = [[UILabel alloc] initWithFrame:CGRectZero];
        self.outfit_a_label.font = [UIFont fontWithName:fontBoldPrimaryName size:kImageViewLabelFontSize];
        self.outfit_a_label.textAlignment = NSTextAlignmentLeft;
        self.outfit_a_label.textColor = colorPrimaryGreen;
        self.outfit_a_label.backgroundColor = [UIColor clearColor];
        [self.cellBackgroudView addSubview:self.outfit_a_label];
        
        self.outfit_b_label = [[UILabel alloc] initWithFrame:CGRectZero];
        self.outfit_b_label.font = [UIFont fontWithName:fontBoldPrimaryName size:kImageViewLabelFontSize];
        self.outfit_b_label.textAlignment = NSTextAlignmentRight;
        self.outfit_b_label.textColor = colorPrimaryGreen;
        self.outfit_b_label.backgroundColor = [UIColor clearColor];
        [self.cellBackgroudView addSubview:self.outfit_b_label];
        
        self.middle_sep = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.middle_sep.backgroundColor = [UIColor colorWithRed:209.0 / 255.0 green:209.0 / 255.0 blue:209.0 / 255.0 alpha:1.0];
        [self.cellBackgroudView addSubview:self.middle_sep];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat cellWidth = self.frame.size.width;
    CGFloat cellHeight = self.frame.size.height;
    
    CGFloat cellBackgroudViewWidth = self.cellBackgroudView.frame.size.width;
    
    self.cellBackgroudView.frame = CGRectMake(kCellBackgroudViewLeftOffset, 0, cellWidth - kCellBackgroudViewLeftOffset * 2, cellHeight - kCellBackgroudViewBottomOffset);
    
    self.frame_a.frame = CGRectMake(kImageViewLeftOffset, kImageViewTopOffset, kImageViewWidth, kImageViewHeight);
    self.frame_b.frame = CGRectMake(cellBackgroudViewWidth - kImageViewLeftOffset - kImageViewWidth, kImageViewTopOffset, kImageViewWidth, kImageViewHeight);
    
    self.outfit_a_label.frame = CGRectMake(CGRectGetMaxX(self.frame_a.frame) + kImageViewLabelLeftOffset, (self.frame_a.frame.size.height - kImageViewLabelHeight) / 2, kImageViewLabelWidth, kImageViewLabelHeight);
    
    self.outfit_b_label.frame = CGRectMake(CGRectGetMinX(self.frame_b.frame) - kImageViewLabelLeftOffset - kImageViewLabelWidth, self.outfit_a_label.frame.origin.y, kImageViewLabelWidth, kImageViewLabelHeight);
    
    self.middle_sep.frame = CGRectMake(cellBackgroudViewWidth / 2, kMiddleSeparatorViewTopOffset, kOnePixel, kMiddleSeparatorViewHeight);
}

-(void)load
{
    float outfit_a_percent;
    float outfit_b_percent;
    
    if (self.this_post.photo_a_votes.floatValue != 0)
        outfit_a_percent = self.this_post.photo_a_votes.floatValue/self.this_post.total_votes.floatValue;
    else
        outfit_a_percent = 0;
    
    
    if (self.this_post.photo_b_votes.floatValue != 0)
        outfit_b_percent = self.this_post.photo_b_votes.floatValue/self.this_post.total_votes.floatValue;
    else
        outfit_b_percent = 0;
    
    int the_percent_a = outfit_a_percent*100;
    int the_percent_b = outfit_b_percent*100;
    
    self.outfit_a_label.text = [NSString stringWithFormat:@"%d%%", the_percent_a];
    self.outfit_b_label.text = [NSString stringWithFormat:@"%d%%", the_percent_b];
    
    self.backgroundColor = [UIColor clearColor];
    
    //    self.frame_a = [[UIImageView alloc] initWithFrame:CGRectMake(1, 5, 77, 210)];
    //    self.frame_a.backgroundColor = [UIColor redColor];
    //    //self.frame_a.image = [UIImage imageNamed:@"dots_rectangle.png"];
    //    self.frame_a.userInteractionEnabled = NO;
    //    self.frame_a.contentMode = UIViewContentModeScaleAspectFit;
    //   [self.cellScrollView addSubview:self.frame_a];
    //
    //    self.frame_b = [[UIImageView alloc] initWithFrame:CGRectMake(163, 5, 77, 210)];
    //    self.frame_b.backgroundColor = [UIColor blueColor];
    //    //self.frame_b.image = [UIImage imageNamed:@"dots_rectangle.png"];
    //    self.frame_b.userInteractionEnabled = NO;
    //    self.frame_b.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.cellScrollView addSubview:self.frame_b];
    //
    //    self.outfit_a_label = [[UILabel alloc] initWithFrame:CGRectMake(75, 60, 60, 20)];
    //    self.outfit_a_label.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    //    self.outfit_a_label.textAlignment = NSTextAlignmentCenter;
    //    self.outfit_a_label.text = [NSString stringWithFormat:@"%d%%", the_percent_a];
    //    self.outfit_a_label.textColor = [self colorWithHexString:@"9ad426"];
    //   [self.cellScrollView addSubview:self.outfit_a_label];
    //
    //    self.outfit_b_label = [[UILabel alloc] initWithFrame:CGRectMake(117, 60, 60, 20)];
    //    self.outfit_b_label.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    //    self.outfit_b_label.textAlignment = NSTextAlignmentCenter;
    //    self.outfit_b_label.text = [NSString stringWithFormat:@"%d%%", the_percent_b];
    //    self.outfit_b_label.textColor = [self colorWithHexString:@"9ad426"];
    //    [self.cellScrollView addSubview:self.outfit_b_label];
    //
    //    self.middle_sep = [[UIImageView alloc] initWithFrame:CGRectMake(124, 33, 2.5, 155/2)];
    //    self.middle_sep.image = [UIImage imageNamed:@"vertical_dot_line.png"];
    //    [self.cellScrollView addSubview:self.middle_sep];
    
}
-(void)load_images
{
    if (!self.loaded)
    {
        NSRange range = [self.this_post.photo_a_url rangeOfString:@".jpg"];
        NSString *substring = [[self.this_post.photo_a_url substringToIndex:range.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString * thumbnail_url = [NSString stringWithFormat:@"%@_thumb.jpg", substring];
        
        //self.outfit_a = [[UIImageView alloc] initWithFrame:CGRectMake(7, 5, 70, 210)];
        self.outfit_a = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kImageViewWidth, kImageViewHeight)];
        [self.outfit_a sd_setImageWithURL:[NSURL URLWithString:thumbnail_url] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
         {
             self.this_post.outfit_a_image = image;
             
             self.outfit_a.alpha = 0.0;
             [UIView animateWithDuration:1.0
                              animations:^
              {
                  self.outfit_a.alpha = 1.0;
              }];
             
         }];
        self.outfit_a.tag = 100;
        self.outfit_a.contentMode = UIViewContentModeScaleAspectFit;
        [self.frame_a addSubview:self.outfit_a];
        
        NSRange range2 = [self.this_post.photo_b_url rangeOfString:@".jpg"];
        NSString *substring2 = [[self.this_post.photo_b_url substringToIndex:range2.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString * thumbnail_url2 = [NSString stringWithFormat:@"%@_thumb.jpg", substring2];
        
        //self.outfit_b = [[UIImageView alloc] initWithFrame:CGRectMake(7, 5, 70, 210)];
        self.outfit_b = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kImageViewWidth, kImageViewHeight)];
        [self.outfit_b sd_setImageWithURL:[NSURL URLWithString:thumbnail_url2] placeholderImage:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL * imageURL)
         {
             self.this_post.outfit_b_image = image;
             
             self.outfit_b.alpha = 0.0;
             [UIView animateWithDuration:1.0
                              animations:^
              {
                  self.outfit_b.alpha = 1.0;
              }];
             
         }];
        
        self.outfit_b.tag = 100;
        self.outfit_b.contentMode = UIViewContentModeScaleAspectFit;
        [self.frame_b addSubview:self.outfit_b];
        
        self.loaded = YES;
        
    }
    
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
@end

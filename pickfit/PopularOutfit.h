//
//  PopularOutfit.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/7/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridViewCell.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface PopularOutfit : AQGridViewCell

@property (atomic, strong) UIImageView * frame_outfit;
@property (atomic, strong) NSString * img_url;
@property (atomic, strong) UIImageView * photo;
@property (atomic, strong) NSNumber * outfit_id;
@property (atomic, strong) UIImage * photo_img;
@property (atomic, strong) NSNumber * is_flagged;

@property (atomic, strong) NSString * userName;

- (id) initWithFrame: (CGRect) frame reuseIdentifier : (NSString *) aReuseIdentifier : (NSNumber *) outfit_id_incoming : (NSString *) pic_url;
- (void)load;
@end

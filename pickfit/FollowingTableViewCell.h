//
//  FollowingTableViewCell.h
//  pickfit
//
//  Created by treasure on 3/20/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "SearchUsersTableViewCell.h"

@protocol FollowingTableViewCellDelegate;

@interface FollowingTableViewCell : SearchUsersTableViewCell

@property (nonatomic, assign) id <FollowingTableViewCellDelegate> followingCellDelegate;

@end

@protocol FollowingTableViewCellDelegate <NSObject>

- (void)didButtonUnfollowPress:(FollowingTableViewCell *)cell;

@end

//
//  OpeningView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/4/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "HomeView.h"
#import "RegisterView.h"
#import "ForgotPasswordView.h"
#import <FacebookSDK/FacebookSDK.h>

@interface OpeningView : UIViewController<UITextFieldDelegate>
{
    UIImageView * logo;
    UITextField * username;
    UITextField * password;
    UIButton * login;
    UIButton * facebook_login;
    UIButton * dont_have_account;    
    UIButton * forgot_password;
    UIButton * or_label;
    UIImageView * background;
        
    
}
@end

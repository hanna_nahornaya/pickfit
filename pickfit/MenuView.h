//
//  MenuView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/5/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeView.h"
#import "FavoritesView.h"
#import "PopularView.h"
#import "SettingsView.h"
#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "TrendingVC.h"

@interface MenuView : UIViewController
{
    UIButton * home_button;
    UIButton * trending_button;
    UIButton * settings_button;
    UIButton * invite_button;
}
//@property (strong, nonatomic) HomeView * home_view;
@property (strong, nonatomic) SettingsView * settings_view;
@property (strong, nonatomic) TrendingVC * trendingVC;

@end

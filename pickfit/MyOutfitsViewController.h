//
//  MyOutfitsViewController.h
//  pickfit
//
//  Created by ann on 3/17/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOutfitsViewController : UIViewController

- (void)addRefreshControl;

@end

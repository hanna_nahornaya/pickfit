//
//  TutorialView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 11/6/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "TutorialView.h"

@interface TutorialView ()

@end

float firstX2;
float firstY2;
float originalCenter_a2;
float originalCenter_b2;

@implementation TutorialView

- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        background.backgroundColor = [self colorWithHexString:@"9ad325"];
        background.alpha = .8;
        [self.view addSubview:background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(35, 30, 250, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:19];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.textColor = [UIColor whiteColor];
        top_label.backgroundColor = [UIColor clearColor];
        top_label.text = @"How to Pickfit:";
        [self.view addSubview:top_label];
        
        the_frame_for_photo_a = [[UIImageView alloc] initWithFrame:CGRectMake(-605, 4, 150, 460)];
        the_frame_for_photo_b = [[UIImageView alloc] initWithFrame:CGRectMake(605, 4, 150, 460)];
        
        photo_stack_a  = [[UIButton alloc] initWithFrame:CGRectMake(0, 65, 320/2, 933/2)];
        [photo_stack_a setImage:[UIImage imageNamed:@"box_left.png"] forState:UIControlStateNormal];
        [photo_stack_a addTarget:self action:@selector(view_a) forControlEvents:UIControlEventTouchUpInside];
        [photo_stack_a setImage:[photo_stack_a imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [self.view addSubview: photo_stack_a];
        
        photo_stack_b  = [[UIButton alloc] initWithFrame:CGRectMake(160, 65, 320/2, 933/2)];
        [photo_stack_b setImage:[UIImage imageNamed:@"box_right.png"] forState:UIControlStateNormal];
        [photo_stack_b setImage:[photo_stack_b imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [photo_stack_b addTarget:self action:@selector(view_b) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: photo_stack_b];    
        
        check_mark_layer_a = [[UIView alloc] initWithFrame:CGRectMake(5, 4, photo_stack_a.frame.size.width-12, photo_stack_a.frame.size.height-16)];
        check_mark_layer_a.backgroundColor = [self colorWithHexString:@"9ad325"];
        check_mark_layer_a.alpha = 0;
        [photo_stack_a addSubview:check_mark_layer_a];
        
        this_outfit_check_a = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_a.frame.size.width/2 - 35, check_mark_layer_a.frame.size.height*.7, 71, 70)];
        this_outfit_check_a.image = [UIImage imageNamed:@"check_mark.png"];
        [check_mark_layer_a addSubview:this_outfit_check_a];
        
        check_mark_layer_b = [[UIView alloc] initWithFrame:CGRectMake(7, 4, photo_stack_b.frame.size.width-12, photo_stack_b.frame.size.height-16)];
        check_mark_layer_b.backgroundColor = [self colorWithHexString:@"9ad325"];
        check_mark_layer_b.alpha = 0;
        [photo_stack_b addSubview:check_mark_layer_b];
        
        this_outfit_check_b = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_b.frame.size.width/2 - 35, check_mark_layer_b.frame.size.height*.7, 71, 70)];
        this_outfit_check_b.image = [UIImage imageNamed:@"check_mark.png"];
        [check_mark_layer_b addSubview:this_outfit_check_b];
        
        prompt = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-322/4, 130, 322/2, 322/2)];
        prompt.image = [UIImage imageNamed:@"swipe_up_vote.png"];
        [self.view addSubview:prompt];
        
        [self show_tutorial_post];
        
        dots_array = (NSMutableArray*)[self animationWith:@"Comp 1_" andimageCount:43];
        dots_moving = [[UIImageView alloc] initWithFrame:CGRectMake(65, 400, 51/2, 164/2)];
        dots_moving.animationImages = dots_array;
        dots_moving.animationDuration = 1.5;
        dots_moving.animationRepeatCount = 0;
        dots_moving.backgroundColor = [UIColor clearColor];
        [self.view addSubview:dots_moving];
        [dots_moving startAnimating];
        
        
        dots_moving2 = [[UIImageView alloc] initWithFrame:CGRectMake(230, 400, 51/2, 164/2)];
        dots_moving2.animationImages = dots_array;
        dots_moving2.animationDuration = 1.5;
        dots_moving2.animationRepeatCount = 0;
        dots_moving2.backgroundColor = [UIColor clearColor];
        [self.view addSubview:dots_moving2];
        [dots_moving2 startAnimating];
        
        arrows_array = (NSMutableArray*)[self animationWith:@"arrows_" andimageCount:32];
    }
    else
    {
        background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        background.backgroundColor = [self colorWithHexString:@"9ad325"];
        background.alpha = .8;
        [self.view addSubview:background];
        
        top_label = [[UILabel alloc] initWithFrame:CGRectMake(35, 20, 250, 20)];
        top_label.font = [UIFont fontWithName:@"Neutraface Text" size:19];
        top_label.textAlignment = NSTextAlignmentCenter;
        top_label.textColor = [UIColor whiteColor];
        top_label.backgroundColor = [UIColor clearColor];
        top_label.text = @"How to Pickfit:";
        [self.view addSubview:top_label];
        
        the_frame_for_photo_a = [[UIImageView alloc] initWithFrame:CGRectMake(-605, 4, 150, 460)];
        the_frame_for_photo_b = [[UIImageView alloc] initWithFrame:CGRectMake(605, 4, 150, 460)];
        
        photo_stack_a  = [[UIButton alloc] initWithFrame:CGRectMake(0, 40, 320/2, 933/2)];
        [photo_stack_a setImage:[UIImage imageNamed:@"box_left.png"] forState:UIControlStateNormal];
        [photo_stack_a addTarget:self action:@selector(view_a) forControlEvents:UIControlEventTouchUpInside];
        [photo_stack_a setImage:[photo_stack_a imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [self.view addSubview: photo_stack_a];
        
        photo_stack_b  = [[UIButton alloc] initWithFrame:CGRectMake(160, 40, 320/2, 933/2)];
        [photo_stack_b setImage:[UIImage imageNamed:@"box_right.png"] forState:UIControlStateNormal];
        [photo_stack_b setImage:[photo_stack_b imageForState:UIControlStateNormal] forState:UIControlStateHighlighted];
        [photo_stack_b addTarget:self action:@selector(view_b) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: photo_stack_b];
        
        check_mark_layer_a = [[UIView alloc] initWithFrame:CGRectMake(5, 4, photo_stack_a.frame.size.width-12, photo_stack_a.frame.size.height-16)];
        check_mark_layer_a.backgroundColor = [self colorWithHexString:@"9ad325"];
        check_mark_layer_a.alpha = 0;
        [photo_stack_a addSubview:check_mark_layer_a];
        
        this_outfit_check_a = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_a.frame.size.width/2 - 35, check_mark_layer_a.frame.size.height*.7, 71, 70)];
        this_outfit_check_a.image = [UIImage imageNamed:@"check_mark.png"];
        [check_mark_layer_a addSubview:this_outfit_check_a];
        
        check_mark_layer_b = [[UIView alloc] initWithFrame:CGRectMake(7, 4, photo_stack_b.frame.size.width-12, photo_stack_b.frame.size.height-16)];
        check_mark_layer_b.backgroundColor = [self colorWithHexString:@"9ad325"];
        check_mark_layer_b.alpha = 0;
        [photo_stack_b addSubview:check_mark_layer_b];
        
        this_outfit_check_b = [[UIImageView alloc] initWithFrame:CGRectMake(check_mark_layer_b.frame.size.width/2 - 35, check_mark_layer_b.frame.size.height*.7, 71, 70)];
        this_outfit_check_b.image = [UIImage imageNamed:@"check_mark.png"];
        [check_mark_layer_b addSubview:this_outfit_check_b];
        
        prompt = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-322/4, 130, 322/2, 322/2)];
        prompt.image = [UIImage imageNamed:@"swipe_up_vote.png"];
        [self.view addSubview:prompt];
        
        [self show_tutorial_post];
        
        dots_array = (NSMutableArray*)[self animationWith:@"Comp 1_" andimageCount:43];
        dots_moving = [[UIImageView alloc] initWithFrame:CGRectMake(65, 400, 51/2, 164/2)];
        dots_moving.animationImages = dots_array;
        dots_moving.animationDuration = 1.5;
        dots_moving.animationRepeatCount = 0;
        dots_moving.backgroundColor = [UIColor clearColor];
        [self.view addSubview:dots_moving];
        [dots_moving startAnimating];
        
        
        dots_moving2 = [[UIImageView alloc] initWithFrame:CGRectMake(230, 400, 51/2, 164/2)];
        dots_moving2.animationImages = dots_array;
        dots_moving2.animationDuration = 1.5;
        dots_moving2.animationRepeatCount = 0;
        dots_moving2.backgroundColor = [UIColor clearColor];
        [self.view addSubview:dots_moving2];
        [dots_moving2 startAnimating];
        
        arrows_array = (NSMutableArray*)[self animationWith:@"arrows_" andimageCount:32];
    }
    
}
-(void)show_tutorial_post
{
    the_frame_for_photo_a.frame = CGRectMake(5, 6, 150, 445);
    the_frame_for_photo_a.tag = 100;
    the_frame_for_photo_a.userInteractionEnabled = YES;
    check_mark_layer_a.alpha = 0.0;
    the_frame_for_photo_a.image = [UIImage imageNamed:@"Tutorial_1_Fancy_Edited.jpg"];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [the_frame_for_photo_a addGestureRecognizer:panRecognizer];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_a)];
    tapRecognizer.numberOfTapsRequired = 1;
    [the_frame_for_photo_a addGestureRecognizer:tapRecognizer];
    
    originalCenter_a2 = the_frame_for_photo_a.center.y;
    
    
    [photo_stack_a addSubview:the_frame_for_photo_a];
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    the_frame_for_photo_b.frame = CGRectMake(5, 6, 150, 445);
    the_frame_for_photo_b.tag = 100;
    the_frame_for_photo_b.userInteractionEnabled = YES;
    check_mark_layer_b.alpha = 0.0;
    the_frame_for_photo_b.image = [UIImage imageNamed:@"Tutorial_2_Fancy_Edited.png"];
    
    UIPanGestureRecognizer *panRecognizer23 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move2:)];
    [the_frame_for_photo_b addGestureRecognizer:panRecognizer23];
    
    UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_b)];
    tapRecognizer3.numberOfTapsRequired = 1;
    [the_frame_for_photo_b addGestureRecognizer:tapRecognizer3];
    
    originalCenter_b2 = the_frame_for_photo_b.center.y;
    [photo_stack_b addSubview:the_frame_for_photo_b];
    
}
-(void)move:(id)sender
{    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:photo_stack_a];
    check_mark_layer_a.alpha = 1.0;
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX2 = [[sender view] center].x;
        firstY2 = [[sender view] center].y;
    }
    else if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
    {
        if (translatedPoint.y >= 0)
        {
            
        }
        else
        {
            translatedPoint = CGPointMake(firstX2, firstY2+translatedPoint.y);
            [[sender view] setCenter:translatedPoint];
        }
        
    }
    else
    {
        
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded)
        {
            
            CGFloat velocityY = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            
            
            
            CGFloat finalY = translatedPoint.y + velocityY;
            if (finalY >= originalCenter_a2)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_a2);
                check_mark_layer_a.alpha = 0.0;
                [UIView commitAnimations];
                
            }
            else if (finalY >= -233)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_a2);
                check_mark_layer_a.alpha = 0.0;
                [UIView commitAnimations];
                
            }
            else
            {
                [self user_voted];
                [UIView animateWithDuration:0.5
                                 animations:^
                 {
                     [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_a2-600);
                     check_mark_layer_a.alpha = 0.0;
                     
                 }
                                 completion:^(BOOL finished)
                 {
                     
                 }];
            }
            
        }
    }
    
}
-(void)move2:(id)sender
{
    check_mark_layer_b.alpha = 1.0;
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:photo_stack_b];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX2 = [[sender view] center].x;
        firstY2 = [[sender view] center].y;
    }
    else if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged)
    {
        if (translatedPoint.y >= 0)
        {
            
        }
        else
        {
            translatedPoint = CGPointMake(firstX2, firstY2+translatedPoint.y);
            [[sender view] setCenter:translatedPoint];
        }
        
    }
    else
    {
        
        if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded)
        {
            
            CGFloat velocityY = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            
            
            
            CGFloat finalY = translatedPoint.y + velocityY;
            if (finalY >= originalCenter_b2)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_b2);
                check_mark_layer_b.alpha = 0.0;
                [UIView commitAnimations];
                
            }
            else if (finalY >= -233)
            {
                CGFloat animationDuration = (ABS(velocityY)*.0002)+.2;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:animationDuration];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                [UIView setAnimationDelegate:self];
                [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_b2);
                check_mark_layer_b.alpha = 0.0;
                [UIView commitAnimations];
            }
            else
            {
                [self user_voted];
                [UIView animateWithDuration:0.5
                                 animations:^
                 {
                     [(UIPanGestureRecognizer*)sender view].center = CGPointMake(firstX2, originalCenter_b2-600);
                     check_mark_layer_b.alpha = 0.0;
                     
                 }
                                 completion:^(BOOL finished)
                 {
                     
                 }];
            }
            
        }
    }    
    
}
-(void)user_voted
{
    UIImageView * overlay = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    overlay.image = [UIImage imageNamed:@"tut_3.png"];
    overlay.alpha = 0;
    [self.view addSubview:overlay];
    
    [UIView animateWithDuration:0.5
                     animations:^
     {
         overlay.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         prompt.hidden = YES;
         dots_moving.hidden = YES;
         dots_moving2.hidden = YES;
         
         arrows = [[UIImageView alloc] initWithFrame:CGRectMake(200, 20, 244/2, 326/2)];
         arrows.animationImages = arrows_array;
         arrows.animationDuration = 1.5;
         arrows.animationRepeatCount = 0;
         arrows.backgroundColor = [UIColor clearColor];
         [overlay addSubview:arrows];
         [arrows startAnimating];
         
         for (UIView * to_disable in self.view.subviews)
         {
             to_disable.userInteractionEnabled = NO;
         }
         
         UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(end_tutorial)];
         tapRecognizer.numberOfTapsRequired = 1;
         [self.view addGestureRecognizer:tapRecognizer];
     }];
    
}
-(void)end_tutorial
{
    [self.navigationController dismissCurrentPopinControllerAnimated:YES];
}
-(void)view_a
{
    prompt.image = [UIImage imageNamed:@"tap_tut.png"];
    [self performSelector:@selector(back_to_swipe) withObject:nil afterDelay:3.0];
}
-(void)view_b
{
    prompt.image = [UIImage imageNamed:@"tap_tut.png"];
    [self performSelector:@selector(back_to_swipe) withObject:nil afterDelay:3.0];
}
-(void)back_to_swipe
{
     prompt.image = [UIImage imageNamed:@"swipe_up_vote.png"];
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
- (NSArray *)animationWith:(NSString *) name andimageCount: (int) imageCount
{
    @autoreleasepool
    {
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:imageCount];
        
        for(int i=0; i <=imageCount; i++)
        {
            NSString *imgLocation = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%d",name,i] ofType:@"png"];
            UIImage *frameImage = [UIImage imageWithContentsOfFile:imgLocation];
            [tempArray addObject:frameImage];
        }
        return (tempArray);
        
    }
}
@end

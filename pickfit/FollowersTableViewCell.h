//
//  FollowersTableViewCell.h
//  pickfit
//
//  Created by treasure on 3/20/15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "SearchUsersTableViewCell.h"

@protocol FollowersTableViewCellDelegate;

@interface FollowersTableViewCell : SearchUsersTableViewCell

@property (nonatomic, assign) id <FollowersTableViewCellDelegate> followersCellDelegate;

@end

@protocol FollowersTableViewCellDelegate <NSObject>

- (void)didButtonFollowPress:(FollowersTableViewCell *)cell;

@end

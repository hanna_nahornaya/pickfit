//
//  TrendingVC.m
//  pickfit
//
//  Created by D.Gonchenko on 13.03.15.
//  Copyright (c) 2015 Koda Labs. All rights reserved.
//

#import "TrendingVC.h"
#import "SlideNavigationController.h"
#import "PopularView.h"
#import "FeedVC.h"
#import "SearchVC.h"

@interface TrendingVC () <UISearchBarDelegate, FeedVCDelegate>
{
    UIButton * closet_button;
    UIButton * menu_button;
    UILabel * top_label;
    UIImageView * top_background;
    NSMutableArray * outfits;
}

@property (nonatomic, strong) PopularView *popularVC;
@property (nonatomic, strong) UIView *popularView;

@property (nonatomic, strong) FeedVC *feedVC;
@property (nonatomic, strong) UIView *feedView;

@property (nonatomic, strong) UIButton *feedButton;
@property (nonatomic, strong) UIButton *popularButton;



@end

@implementation TrendingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.popularVC = [PopularView new];
    [self addChildViewController:self.popularVC];
    self.feedVC = [FeedVC new];
    self.feedVC.feedVCDelegate = self;
    [self addChildViewController:self.feedVC];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    outfits = [[NSMutableArray alloc] init];
    
    top_background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    top_background.image = [UIImage imageNamed:@"header_white_bar.png"];
    [self.view addSubview:top_background];
    
    top_label = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-120/2, 30, 120, 20)];
    top_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    top_label.textAlignment = NSTextAlignmentCenter;
    top_label.text = @"Trending";
    top_label.textColor = [UIColor blackColor];
    [self.view addSubview:top_label];
    
    closet_button  = [[UIButton alloc] initWithFrame:CGRectMake(280, 27, 66/2, 53/2)];
    [closet_button setImage:[UIImage imageNamed:@"button_coat_hanger.png"] forState:UIControlStateNormal];
    [closet_button addTarget:[SlideNavigationController sharedInstance] action:@selector(pushRightMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: closet_button];
    
    menu_button  = [[UIButton alloc] initWithFrame:CGRectMake(5, 27, 66/2, 53/2)];
    [menu_button setImage:[UIImage imageNamed:@"button_green_bars.png"] forState:UIControlStateNormal];
    [menu_button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: menu_button];
    
    UIButton *searchButton = [[UIButton alloc] initWithFrame:CGRectMake(11, CGRectGetMaxY(top_background.frame), self.view.frame.size.width - 11.0*2.0, 33.0)];
    [searchButton setImage:[UIImage imageNamed:@"searchIcon.png"] forState:UIControlStateNormal];
    [searchButton setTitle:@"Search" forState:UIControlStateNormal];
    [searchButton setTitleColor:[UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(beginSearch) forControlEvents:UIControlEventTouchUpInside];
    searchButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5.0);
    searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5.0, 0, 0);
    searchButton.layer.borderColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0].CGColor;
    searchButton.layer.borderWidth = 0.5;
    searchButton.layer.cornerRadius = 3;
    [self.view addSubview:searchButton];
    
    self.feedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(searchButton.frame), self.view.frame.size.width / 2, 35.0)];
    [self.feedButton setTitle:@"Feed" forState:UIControlStateNormal];
    [self addAttributesToButton:self.feedButton];
    [self.view addSubview:self.feedButton];
    
    self.popularButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2, CGRectGetMaxY(searchButton.frame), self.view.frame.size.width / 2, 35.0)];
    [self.popularButton setTitle:@"Popular" forState:UIControlStateNormal];
    [self addAttributesToButton:self.popularButton];
    [self.view addSubview:self.popularButton];
    
    [self selectedButton:self.feedButton];
    
    UIView *separatorForButtonsView = [UIView new];
    separatorForButtonsView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1.0];
    separatorForButtonsView.frame = CGRectMake(0, 0, 0.5, 15.0);
    separatorForButtonsView.center = CGPointMake(self.view.frame.size.width / 2.0, CGRectGetMidY(self.feedButton.frame));
    [self.view addSubview:separatorForButtonsView];
    
    self.popularView = self.popularVC.view;
    self.popularView.frame = CGRectMake(0, CGRectGetMaxY(self.popularButton.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.popularButton.frame));
    self.popularView.backgroundColor = [self colorWithHexString:@"f7faf6"];
    self.popularView.hidden = YES;
    [self.view addSubview:self.popularView];
    
    self.feedView = self.feedVC.view;
    self.feedView.frame = CGRectMake(0, CGRectGetMaxY(self.popularButton.frame), self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.popularButton.frame));
    self.feedView.backgroundColor = [self colorWithHexString:@"f7faf6"];
    self.feedView.hidden = NO;
    [self.view addSubview:self.feedView];
}

-(void)addAttributesToButton:(UIButton*)button
{
    [button setTitleColor:colorPrimaryGreen forState:UIControlStateSelected];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)selectedButton:(UIButton*)button
{
    [self.feedButton setSelected:(button == self.feedButton)];
    [self.popularButton setSelected:!(button == self.feedButton)];
    self.popularView.hidden = (button == self.feedButton);
    self.feedView.hidden = !(button == self.feedButton);
}

-(void)beginSearch
{
    [self beginSearchWithSearchType:SearchVCOpenTypeUsers];
}

-(void)beginSearchWithSearchType:(SearchVCOpenType)searchType
{
    SearchVC *searchVC = [SearchVC new];
    searchVC.openType = searchType;
    
    [[SlideNavigationController sharedInstance] pushViewController:searchVC animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    [[SDImageCache sharedImageCache] clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [Flurry logEvent:@"memory"];
}

- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

#pragma mark - FeedVCDelegate
-(void)feedVCNoFeeds
{
    [self selectedButton:self.popularButton];
}

-(void)feedVCGoToSearch
{
    [self beginSearchWithSearchType:SearchVCOpenTypeOccasions];
}


@end

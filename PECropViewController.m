//
//  PECropViewController.m
//  PhotoCropEditor
//
//  Created by kishikawa katsumi on 2013/05/19.
//  Copyright (c) 2013 kishikawa katsumi. All rights reserved.
//

#import "PECropViewController.h"
#import "PECropView.h"
#import "Flurry.h"

@interface PECropViewController () <UIActionSheetDelegate>

@property (nonatomic) PECropView *cropView;
@property (nonatomic) UIActionSheet *actionSheet;

- (void)commonInit;

@end

@implementation PECropViewController
@synthesize rotationEnabled = _rotationEnabled;
@synthesize tag_button;
@synthesize done_button;
@synthesize cropped_pic;
@synthesize tag_view;
@synthesize call_to_action;
@synthesize current_label;
@synthesize currently_tagging;
@synthesize search_controller;
@synthesize green_circle_button;
@synthesize tag_label;


+ (NSBundle *)bundle
{
    static NSBundle *bundle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        NSURL *bundleURL = [[NSBundle mainBundle] URLForResource:@"PEPhotoCropEditor" withExtension:@"bundle"];
        bundle = [[NSBundle alloc] initWithURL:bundleURL];
    });
    
    return bundle;
}

static inline NSString *PELocalizedString(NSString *key, NSString *comment)
{
    return [[PECropViewController bundle] localizedStringForKey:key value:nil table:@"Localizable"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}
- (void)commonInit
{
    self.rotationEnabled = YES;
}

#pragma mark -

- (void)loadView
{
    UIView *contentView = [[UIView alloc] init];
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentView.backgroundColor = [UIColor blackColor];
    self.view = contentView;
    
    self.cropView = [[PECropView alloc] initWithFrame:CGRectMake(contentView.frame.origin.x, contentView.frame.origin.y-15,  contentView.frame.size.width, contentView.frame.size.height)];
    [contentView addSubview:self.cropView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tag_view = NO;
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.toolbar.translucent = NO;
    
    UIImage *background = [UIImage imageNamed:@"white_x.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setBackgroundImage:background forState:UIControlStateNormal];  
    button.frame = CGRectMake(0 , 0 , 74/2, 62/2);
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button2 setTitle:@"Next" forState:UIControlStateNormal];
    button2.frame = CGRectMake(0 , 3 , 50, 20);
    button2.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Neutraface Text" size:18],
                                                                      NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
    [self.navigationController.navigationBar setBarTintColor:[self colorWithHexString:@"9ad325"]];
    
    [self.navigationItem setTitle:@"Crop Pic"];
    
    self.cropView.image = self.image;
    
    self.cropView.rotationGestureRecognizer.enabled = _rotationEnabled;
    
    self.view.backgroundColor = [self colorWithHexString:@"3b3b3b"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(create_tag:) name:@"CreateTag" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(create_edit:) name:@"CreateEdit" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(item_chosen:) name:@"TagChosen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(close_down_search) name:@"CloseSearch" object:nil];
  
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.cropAspectRatio != 0)
    {
        self.cropAspectRatio = self.cropAspectRatio;
    }
    if (!CGRectEqualToRect(self.cropRect, CGRectZero))
    {
        self.cropRect = self.cropRect;
    }
    if (!CGRectEqualToRect(self.imageCropRect, CGRectZero))
    {
        self.imageCropRect = self.imageCropRect;
    }
    self.keepingCropAspectRatio = self.keepingCropAspectRatio;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

#pragma mark -

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.cropView.image = image;
}
- (void)setKeepingCropAspectRatio:(BOOL)keepingCropAspectRatio
{
    _keepingCropAspectRatio = keepingCropAspectRatio;
    self.cropView.keepingCropAspectRatio = self.keepingCropAspectRatio;
}
- (void)setCropAspectRatio:(CGFloat)cropAspectRatio
{
    _cropAspectRatio = cropAspectRatio;
    self.cropView.cropAspectRatio = self.cropAspectRatio;
}
- (void)setCropRect:(CGRect)cropRect
{
    _cropRect = cropRect;
    _imageCropRect = CGRectZero;
    
    CGRect cropViewCropRect = self.cropView.cropRect;
    cropViewCropRect.origin.x += cropRect.origin.x;
    cropViewCropRect.origin.y += cropRect.origin.y;
    
    CGSize size = CGSizeMake(fminf(CGRectGetMaxX(cropViewCropRect) - CGRectGetMinX(cropViewCropRect), CGRectGetWidth(cropRect)),
                             fminf(CGRectGetMaxY(cropViewCropRect) - CGRectGetMinY(cropViewCropRect), CGRectGetHeight(cropRect)));
    
    cropViewCropRect.size = size;
    self.cropView.cropRect = cropViewCropRect;
}
- (void)setImageCropRect:(CGRect)imageCropRect
{
    _imageCropRect = imageCropRect;
    _cropRect = CGRectZero;
    
    self.cropView.imageCropRect = imageCropRect;
}
- (BOOL)isRotationEnabled
{
    return _rotationEnabled;
}
- (void)setRotationEnabled:(BOOL)rotationEnabled
{
    _rotationEnabled = rotationEnabled;
    self.cropView.rotationGestureRecognizer.enabled = _rotationEnabled;
}
- (CGAffineTransform)rotationTransform
{
    return self.cropView.rotation;
}
- (CGRect)zoomedCropRect
{
    return self.cropView.zoomedCropRect;
}
- (void)resetCropRect
{
    [self.cropView resetCropRect];
}
- (void)resetCropRectAnimated:(BOOL)animated
{
    [self.cropView resetCropRectAnimated:animated];
}
#pragma mark -
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)cancel:(id)sender
{
    if (tag_view)
    {
        [self done_tagging];
    }
    if ([self.delegate respondsToSelector:@selector(cropViewControllerDidCancel:)])
    {
        [self.delegate cropViewControllerDidCancel:self];
    }
}
- (void)done:(id)sender
{
    [self done_tagging];
    if ([self.delegate respondsToSelector:@selector(cropViewController:didFinishCroppingImage:)])
    {
        [self.delegate cropViewController:self didFinishCroppingImage:cropped_pic.image];
    }
}
-(void)next:(id)sender
{
    UIImage * cropped_image;
    if (self.imageCropRect.size.width == 0)
    {
        cropped_image = [self imageWithImage:self.cropView.croppedImage scaledToWidth:self.cropView.croppedImage.size.width];

    }
    else
    {
        cropped_image = [self imageWithImage:self.cropView.croppedImage scaledToWidth:self.imageCropRect.size.width];

    }
   
    
    UIImage * background  = [self imageWithColor:[UIColor whiteColor]: cropped_image];
    UIImage * new_image =[self imageByCombiningImage:background withImage:cropped_image];
 
    for (UIView * to_remove in self.view.subviews)
    {
        [to_remove removeFromSuperview];
    }
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        cropped_pic = [[TagImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 261/4 , 10, 261/2, 720/2)];
    }
    else
    {
        cropped_pic = [[TagImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 261/5 , 10, 261/2.5, 720/2.5)];
    }
    cropped_pic.contentMode = UIViewContentModeScaleAspectFit;
    cropped_pic.image = new_image;
    [self.view addSubview:cropped_pic];
    
    [self.navigationItem setTitle:@"Tag Photo"];  
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 , 0 , 55, 20);
    button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    UIButton * button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(clear_tags) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button2 setTitle:@"Clear" forState:UIControlStateNormal];
    button2.frame = CGRectMake(0 , 0 , 55, 20);
    button2.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:18];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        tag_button = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-384/4, 400, 384/2, 89/2)];
        [tag_button setImage:[UIImage imageNamed:@"tag_button_add_a_tag.png"] forState:UIControlStateNormal];
        [tag_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [tag_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tag_button addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tag_button];
        
        done_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 470, 320, 60)];
        [done_button setTitle:@"Done" forState:UIControlStateNormal];
        done_button.backgroundColor = [self colorWithHexString:@"9ad325"];
        [done_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:25]];
        [done_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [done_button addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: done_button];
        
    }
    else
    {
        tag_button = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-384/4, 400-84, 384/2, 89/2)];
        [tag_button setImage:[UIImage imageNamed:@"tag_button_add_a_tag.png"] forState:UIControlStateNormal];
        [tag_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [tag_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tag_button addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tag_button];
        
        done_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 470-84, 320, 60)];
        [done_button setTitle:@"Done" forState:UIControlStateNormal];
        done_button.backgroundColor = [self colorWithHexString:@"9ad325"];
        [done_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:25]];
        [done_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [done_button addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: done_button];
    }
    
    
}
-(void)tag_pressed
{
    [Flurry logEvent:@"add a tag pressed"];
    tag_view = YES;
    tag_button.hidden = YES;
    done_button.hidden = YES;
    green_circle_button.hidden = YES;
    tag_label.hidden = YES;
    
    [self.navigationItem setTitle:@"Tag Photo"];
    
    cropped_pic.userInteractionEnabled = YES;
    cropped_pic.is_tagging = YES;
    cropped_pic.is_editing = NO;
    
    self.navigationItem.leftBarButtonItem = nil;
    
    UIButton * button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(done_tagging) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button2 setTitle:@"OK" forState:UIControlStateNormal];
    button2.frame = CGRectMake(25 , 0 , 30, 20);
    button2.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        UIImageView * tap_tag = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-361/4, 400, 361/2, 191/2)];
        tap_tag.image = [UIImage imageNamed:@"tag_icon_tap.png"];
        [self.view addSubview:tap_tag];
    }
    else
    {
        UIImageView * tap_tag = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-361/4, 400-84, 361/2, 191/2)];
        tap_tag.image = [UIImage imageNamed:@"tag_icon_tap.png"];
        [self.view addSubview:tap_tag];
    }
}
-(void)current_tags
{
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/outfit/getTags/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"outfit_id=%@", self.outfit_id] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"PECropVC:/outfit/getTags/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             
             //NSLog(@"data back from /user/getTags/ = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 if ([status isEqualToString:@"success"])
                 {
                     NSString * tags_array_string = [response_dict objectForKey:@"tags"];
                     //if (![tags_array_string  isEqual:[NSNull null]])
                     if ([tags_array_string isKindOfClass:[NSString class]])
                     {
                         NSData* data_string = [tags_array_string dataUsingEncoding:NSUTF8StringEncoding];
                         NSMutableArray * tags_array = [NSJSONSerialization JSONObjectWithData:data_string options:kNilOptions error:nil];
                         if (tags_array)
                         {
                             //create tags
                             for (NSDictionary * a_tag in tags_array)
                             {
                                 NSNumber * x_p = [a_tag objectForKey:@"x"];
                                 NSNumber * y = [a_tag objectForKey:@"y"];
                                 NSNumber * p_id = [a_tag objectForKey:@"product_id"];
                                 CGPoint point = CGPointMake(x_p.floatValue, y.floatValue);
                                 
                                 
                                 [[PSSClient sharedClient] getProductByID:p_id success:^(PSSProduct *product)
                                  {
                                      PSSProduct * tagged_product = product;                                      
                                      TagLabel * created_label = [[TagLabel alloc] initWithFrame:CGRectMake(point.x, point.y, 90, 30)];
                                      created_label.font = [UIFont fontWithName:@"Neutraface Text" size:13];
                                      created_label.textAlignment = NSTextAlignmentCenter;
                                      created_label.textColor = [UIColor whiteColor];
                                      created_label.backgroundColor = [UIColor blackColor];
                                      created_label.text = tagged_product.name;
                                      created_label.product = tagged_product;
                                      created_label.hidden = NO;
                                      created_label.userInteractionEnabled = YES;
                                      cropped_pic.userInteractionEnabled = YES;
                                      [cropped_pic addSubview:created_label];
                                      [cropped_pic bringSubviewToFront:created_label];
                                  }
                                                                  failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                      
                                      
                                      
                                  }];
                                 
                                 
                                 
                             }
                             
                         }
                         
                     }
                     
                 }
                 
             });
         }
         
     }];
    
}
-(void)edit_pressed
{
    tag_view = YES;
    tag_button.hidden = YES;
    done_button.hidden = YES;
    green_circle_button.hidden = YES;
    
    [self.navigationItem setTitle:@"Tag Photo"];
    
    cropped_pic.userInteractionEnabled = YES;
    cropped_pic.is_tagging = NO;
    cropped_pic.is_editing = YES;
    
    UIButton * button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 addTarget:self action:@selector(done_editing) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button2 setTitle:@"DONE" forState:UIControlStateNormal];
    button2.frame = CGRectMake(0 , 0 , 55, 20);
    button2.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:17];
    [button2 setTitleColor:[self colorWithHexString:@"9ad325"] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton2 = [[UIBarButtonItem alloc] initWithCustomView:button2];
    self.navigationItem.rightBarButtonItem = barButton2;
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(cancel_edit) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 , 0 , 55, 20);
    button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:15];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    call_to_action = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-320/2, 400, 320, 40)];
    call_to_action.font = [UIFont fontWithName:@"Neutraface Text" size:20];
    call_to_action.textAlignment = NSTextAlignmentCenter;
    call_to_action.textColor = [UIColor whiteColor];
    call_to_action.text = @"Tap photo to get rid of your face";
    [self.view addSubview:call_to_action];
    
}
-(void)clear_tags
{
    for (TagLabel * to_use in cropped_pic.subviews)
    {
        if ([to_use isKindOfClass:[TagLabel class]])
        {
            [to_use removeFromSuperview];
        }
    }
}
-(void)done_tagging
{
    int tag_count = 0;
    NSMutableArray * to_send = [[NSMutableArray alloc] init];
    for (TagLabel * to_use in cropped_pic.subviews)
    {
        if ([to_use isKindOfClass:[TagLabel class]])
        {
            NSMutableDictionary * an_item = [[NSMutableDictionary alloc] init];
            [an_item setValue:to_use.product.productID forKey:@"product_id"];
            [an_item setValue:[NSNumber numberWithFloat:to_use.frame.origin.x] forKey:@"x"];
            [an_item setValue:[NSNumber numberWithFloat:to_use.frame.origin.y] forKey:@"y"];
            [to_send addObject:an_item];
            tag_count++;
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TagsForPhoto" object:to_send];
   
    
    for (UIView * to_remove in self.view.subviews)
    {
        if (![to_remove isKindOfClass:[TagImageView class]])
        {
            [to_remove removeFromSuperview];
        }
    }
    
    [self.navigationItem setTitle:@"Tag Photo"];
    
     self.navigationItem.rightBarButtonItem = nil;
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 , 0 , 55, 20);
    button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:15];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        tag_button = [[UIButton alloc] initWithFrame:CGRectMake(20, 400, 384/2, 89/2)];
        [tag_button setImage:[UIImage imageNamed:@"tag_button_add_a_tag.png"] forState:UIControlStateNormal];
        [tag_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [tag_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tag_button addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tag_button];
        
        tag_label = [[UILabel alloc] initWithFrame:CGRectMake(220, 401, 65, 40)];
        tag_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        tag_label.textAlignment = NSTextAlignmentCenter;
        tag_label.textColor = [self colorWithHexString:@"2ec8fc"];
        if (tag_count == 1)
        {
            tag_label.text = [NSString stringWithFormat:@"%d Tag", tag_count];
        }
        else
        {
            tag_label.text = [NSString stringWithFormat:@"%d Tags", tag_count];
        }
        [[tag_label layer] setBorderWidth:.5f];
        [[tag_label layer] setBorderColor:[self colorWithHexString:@"2ec8fc"].CGColor];
        [self.view addSubview:tag_label];
        
        done_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 470, 320, 60)];
        [done_button setTitle:@"Done" forState:UIControlStateNormal];
        done_button.backgroundColor = [self colorWithHexString:@"9ad325"];
        [done_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:25]];
        [done_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [done_button addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: done_button];

    }
    else
    {
        
        tag_button = [[UIButton alloc] initWithFrame:CGRectMake(20, 400-84, 384/2, 89/2)];
        [tag_button setImage:[UIImage imageNamed:@"tag_button_add_a_tag.png"] forState:UIControlStateNormal];
        [tag_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
        [tag_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tag_button addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: tag_button];
        
        tag_label = [[UILabel alloc] initWithFrame:CGRectMake(220, 401-84, 65, 40)];
        tag_label.font = [UIFont fontWithName:@"Neutraface Text" size:20];
        tag_label.textAlignment = NSTextAlignmentCenter;
        tag_label.textColor = [self colorWithHexString:@"2ec8fc"];
        if (tag_count == 1)
        {
            tag_label.text = [NSString stringWithFormat:@"%d Tag", tag_count];
        }
        else
        {
            tag_label.text = [NSString stringWithFormat:@"%d Tags", tag_count];
        }
        [[tag_label layer] setBorderWidth:.5f];
        [[tag_label layer] setBorderColor:[self colorWithHexString:@"2ec8fc"].CGColor];
        [self.view addSubview:tag_label];
        
        done_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 470-84, 320, 60)];
        [done_button setTitle:@"Done" forState:UIControlStateNormal];
        done_button.backgroundColor = [self colorWithHexString:@"9ad325"];
        [done_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:25]];
        [done_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [done_button addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview: done_button];

    }
   
}
-(void)done_editing
{
    UIImage * edited_image = [self imageWithView:(UIView*)cropped_pic];
    
    for (UIView * to_remove in self.view.subviews)
    {
        [to_remove removeFromSuperview];
    }
    
    cropped_pic = [[TagImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 200/2 , 0, 261/2, 720/2)];
    cropped_pic.contentMode = UIViewContentModeScaleAspectFit;
    cropped_pic.image = edited_image;
    [self.view addSubview:cropped_pic];
    
    [self.navigationItem setTitle:@"Tag/Edit Pic"];
    
     self.navigationItem.rightBarButtonItem = nil;
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside]; //adding action
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    button.frame = CGRectMake(0 , 0 , 55, 20);
    button.titleLabel.font = [UIFont fontWithName:@"Neutraface Text" size:15];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButton;
    
//    green_circle_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 350, 320, 80/2)];
//    [green_circle_button setTitle:@"Edit This Photo" forState:UIControlStateNormal];
//    green_circle_button.backgroundColor = [self colorWithHexString:@"9ad325"];
//    [green_circle_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
//    [green_circle_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [green_circle_button addTarget:self action:@selector(edit_pressed) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview: green_circle_button];
    
    tag_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 410, 320, 80/2)];
    [tag_button setTitle:@"Tag Your Clothing" forState:UIControlStateNormal];
    tag_button.backgroundColor = [self colorWithHexString:@"9ad325"];
    [tag_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
    [tag_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [tag_button addTarget:self action:@selector(tag_pressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: tag_button];
    
    done_button = [[UIButton alloc] initWithFrame:CGRectMake(0, 470, 320, 80/2)];
    [done_button setTitle:@"Done" forState:UIControlStateNormal];
    done_button.backgroundColor = [self colorWithHexString:@"9ad325"];
    [done_button.titleLabel setFont:[UIFont fontWithName:@"Neutraface Text" size:20]];
    [done_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [done_button addTarget:self action:@selector(done:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: done_button];

    
}
- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
-(void)create_tag:(NSNotification*)message
{
    if (!currently_tagging)
    {
        currently_tagging = YES;
        
        [Flurry logEvent:@"create a tag"];
        
        UITouch * touch = message.object;
        CGPoint point = [touch locationInView:cropped_pic];
        
        TagLabel * created_label = [[TagLabel alloc] initWithFrame:CGRectMake(point.x-90/2, point.y, 90, 30)];
        created_label.font = [UIFont fontWithName:@"Neutraface Text" size:13];
        created_label.textAlignment = NSTextAlignmentCenter;
        created_label.textColor = [UIColor whiteColor];
        created_label.backgroundColor = [UIColor blackColor];
        created_label.text = @"What is this?";
        [cropped_pic addSubview:created_label];
        
        current_label = created_label;
        
        [self search_for_item];
        
    }
    
}
-(void)create_edit:(NSNotification*)message
{
    UITouch * touch = message.object;
    CGPoint point = [touch locationInView:cropped_pic];
    
    UIImageView * created_block = [[UIImageView alloc] initWithFrame:CGRectMake(point.x-149/5, point.y-149/5, 149/2.5, 149/2.5)];
    created_block.image = [UIImage imageNamed:@"green_dot.png"];
    created_block.tag = 101;
    [cropped_pic addSubview:created_block];    
    
}
-(void)cancel_edit
{
    [self next:nil];
}
-(void)search_for_item
{
    self.navigationController.navigationBarHidden = YES;
    
    search_controller = [[SearchForTagViewController alloc] init];
    search_controller.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:search_controller.view];
}
-(void)item_chosen:(NSNotification*)message
{
     PSSProduct *thisProduct = message.object;
     current_label.product = thisProduct;
     current_label.text = thisProduct.name;
    
     currently_tagging = NO;
     [search_controller.view removeFromSuperview];
     self.navigationController.navigationBarHidden = NO;
}
-(void)close_down_search
{
    currently_tagging = NO;
    [search_controller.view removeFromSuperview];
    self.navigationController.navigationBarHidden = NO;
    [current_label removeFromSuperview];
}
- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage
{
    UIImage *image = nil;
    
    CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    if (UIGraphicsBeginImageContextWithOptions != NULL) {
        UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(newImageSize);
    }
    [firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
                                        roundf((newImageSize.height-firstImage.size.height)/2))];
    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
                                         roundf((newImageSize.height-secondImage.size.height)/2))];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
- (UIImage *)imageWithColor:(UIColor *)color :(UIImage*)old_image
{
    if (self.imageCropRect.size.width == 0)
    {
        return nil;
    }
    
    CGRect rect = CGRectMake(0.0f, 0.0f, self.imageCropRect.size.width-1, self.imageCropRect.size.height-1);
    UIGraphicsBeginImageContext(self.imageCropRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
- (void)constrain:(id)sender
{
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:PELocalizedString(@"Cancel", nil)
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:
                        PELocalizedString(@"Original", nil),
                        PELocalizedString(@"Square", nil),
                        PELocalizedString(@"3 x 2", nil),
                        PELocalizedString(@"3 x 5", nil),
                        PELocalizedString(@"4 x 3", nil),
                        PELocalizedString(@"4 x 6", nil),
                        PELocalizedString(@"5 x 7", nil),
                        PELocalizedString(@"8 x 10", nil),
                        PELocalizedString(@"16 x 9", nil), nil];
    [self.actionSheet showFromToolbar:self.navigationController.toolbar];
}
#pragma mark -
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        CGRect cropRect = self.cropView.cropRect;
        CGSize size = self.cropView.image.size;
        CGFloat width = size.width;
        CGFloat height = size.height;
        CGFloat ratio;
        if (width < height)
        {
            ratio = width / height;
            cropRect.size = CGSizeMake(CGRectGetHeight(cropRect) * ratio, CGRectGetHeight(cropRect));
        }
        else
        {
            ratio = height / width;
            cropRect.size = CGSizeMake(CGRectGetWidth(cropRect), CGRectGetWidth(cropRect) * ratio);
        }
        self.cropView.cropRect = cropRect;
    } else if (buttonIndex == 1) {
        self.cropView.cropAspectRatio = 1.0f;
    } else if (buttonIndex == 2) {
        self.cropView.cropAspectRatio = 2.0f / 3.0f;
    } else if (buttonIndex == 3) {
        self.cropView.cropAspectRatio = 3.0f / 5.0f;
    } else if (buttonIndex == 4) {
        CGFloat ratio = 3.0f / 4.0f;
        CGRect cropRect = self.cropView.cropRect;
        CGFloat width = CGRectGetWidth(cropRect);
        cropRect.size = CGSizeMake(width, width * ratio);
        self.cropView.cropRect = cropRect;
    } else if (buttonIndex == 5) {
        self.cropView.cropAspectRatio = 4.0f / 6.0f;
    } else if (buttonIndex == 6) {
        self.cropView.cropAspectRatio = 5.0f / 7.0f;
    } else if (buttonIndex == 7) {
        self.cropView.cropAspectRatio = 8.0f / 10.0f;
    } else if (buttonIndex == 8) {
        CGFloat ratio = 9.0f / 16.0f;
        CGRect cropRect = self.cropView.cropRect;
        CGFloat width = CGRectGetWidth(cropRect);
        cropRect.size = CGSizeMake(width, width * ratio);
        self.cropView.cropRect = cropRect;
    }
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
@end

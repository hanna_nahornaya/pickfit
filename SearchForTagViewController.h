//
//  SearchForTagViewController.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 9/29/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POPSUGARShopSense.h"

@interface SearchForTagViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
{
    
    UIButton * cancel;
    UITableView * results_table;
    UISearchBar * search_bar;
    UIViewController * owner;
    NSMutableArray * search_results;
}
@end

//
//  TagImageView.h
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/1/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagLabel.h"

@interface TagImageView : UIImageView

@property BOOL is_tagging;
@property BOOL is_editing;
@property NSMutableArray * tags;

@end

//
//  PECropViewController.h
//  PhotoCropEditor
//
//  Created by kishikawa katsumi on 2013/05/19.
//  Copyright (c) 2013 kishikawa katsumi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchForTagViewController.h"
#import "TagImageView.h"
#import "TagLabel.h"
#import "POPSUGARShopSense.h"

@protocol PECropViewControllerDelegate;

@interface PECropViewController : UIViewController

@property (nonatomic, weak) id<PECropViewControllerDelegate> delegate;
@property (nonatomic) UIImage *image;

@property (nonatomic) BOOL keepingCropAspectRatio;
@property (nonatomic) CGFloat cropAspectRatio;

@property (nonatomic) CGRect cropRect;
@property (nonatomic) CGRect imageCropRect;

@property (nonatomic) BOOL toolbarHidden;

@property (nonatomic, assign, getter = isRotationEnabled) BOOL rotationEnabled;

@property (nonatomic, readonly) CGAffineTransform rotationTransform;

@property (nonatomic, readonly) CGRect zoomedCropRect;

@property (nonatomic) UIView * bottom_bar;
@property (nonatomic) UIButton * tag_button;
@property (nonatomic) UILabel * tag_label;
@property (nonatomic) UIButton * green_circle_button;
@property (nonatomic) UIButton * done_button;
@property (nonatomic) TagImageView * cropped_pic;
@property (nonatomic) UILabel * call_to_action;
@property (nonatomic) TagLabel * current_label;
@property (nonatomic, strong) SearchForTagViewController * search_controller;
@property (nonatomic) NSNumber * outfit_id;


@property BOOL tag_view;
@property BOOL currently_tagging;


- (void)resetCropRect;
- (void)resetCropRectAnimated:(BOOL)animated;
- (void)next: (id)sender;
- (void)current_tags;

@end

@protocol PECropViewControllerDelegate <NSObject>

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage;
- (void)cropViewControllerDidCancel:(PECropViewController *)controller;

@end

//
//  TagImageView.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/1/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "TagImageView.h"
#import "PECropViewController.h"

@implementation TagImageView
@synthesize is_tagging;
@synthesize is_editing;

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
     
    UITouch *touch = [[UITouch alloc] init];
    touch = [touches anyObject];
    if (is_tagging)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateTag" object:touch];
    else if(is_editing)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateEdit" object:touch];
    
}
@end

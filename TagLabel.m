//
//  TagLabel.m
//  Pickfit
//
//  Created by Nicholas Krzemienski on 10/1/14.
//  Copyright (c) 2014 Koda Labs. All rights reserved.
//

#import "TagLabel.h"

@implementation TagLabel
@synthesize product;
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"open_product" object:product];
}
@end

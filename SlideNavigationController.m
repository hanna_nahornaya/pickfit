//
//  SlideNavigationController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//
// https://github.com/aryaxt/iOS-Slide-Menu
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "SlideNavigationController.h"
#import "SlideNavigationContorllerAnimator.h"
#import "AppDelegate.h"

typedef enum {
	PopTypeAll,
	PopTypeRoot
} PopType;

@interface SlideNavigationController() <UIGestureRecognizerDelegate>
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) CGPoint draggingPoint;
@property (nonatomic, assign) Menu lastRevealedMenu;
@end

@implementation SlideNavigationController

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define MENU_SLIDE_ANIMATION_DURATION .3
#define MENU_QUICK_SLIDE_ANIMATION_DURATION .18
#define MENU_IMAGE @"menu-button"
#define MENU_SHADOW_RADIUS 10
#define MENU_SHADOW_OPACITY 1
#define MENU_DEFAULT_SLIDE_OFFSET 60
#define MENU_FAST_VELOCITY_FOR_SWIPE_FOLLOW_DIRECTION 1200
#define STATUS_BAR_HEIGHT 20

static SlideNavigationController *singletonInstance;

#pragma mark - Initialization -

+ (SlideNavigationController *)sharedInstance
{
	if (!singletonInstance)
		NSLog(@"SlideNavigationController has not been initialized. Either place one in your storyboard or initialize one in code");
	
	return singletonInstance;
}

- (id)init
{
	if (self = [super init])
	{
		[self setup];
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
		[self setup];
	}
	
	return self;
}

- (id)initWithRootViewController:(UIViewController *)rootViewController
{
	if (self = [super initWithRootViewController:rootViewController])
	{
		[self setup];
	}
	
	return self;
}

- (void)setup
{
	if (singletonInstance)
		NSLog(@"Singleton instance already exists. You can only instantiate one instance of SlideNavigationController. This could cause major issues");
	
	singletonInstance = self;
	
	self.landscapeSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
	self.portraitSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
	self.panGestureSideOffset = 0;
	self.avoidSwitchingToSameClassViewController = YES;
	self.enableShadow = YES;
	self.enableSwipeGesture = YES;
	self.delegate = self;
}

- (void)viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];
	
	// Update shadow size of enabled
	if (self.enableShadow)
		self.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
	
	// When menu open we disable user interaction
	// When rotates we want to make sure that userInteraction is enabled again
	[self enableTapGestureToCloseMenu:NO];
	
	// Avoid an ugnly shadow in background while rotating
	self.view.layer.shadowOpacity = 0;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	
	// Update rotation animation
	[self updateMenuFrameAndTransformAccordingToOrientation];
	
	self.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
	
	// we set shadowOpacity to 0 in willRotateToInterfaceOrientation, after the rotation we want to add the shadow back
	self.view.layer.shadowOpacity = MENU_SHADOW_OPACITY;
}

#pragma mark - Public Methods -

- (void)bounceMenu:(Menu)menu withCompletion:(void (^)())completion
{
	[self prepareMenuForReveal:menu];
	NSInteger movementDirection = (menu == MenuLeft) ? 1 : -1;
	
	[UIView animateWithDuration:.16 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
		[self moveHorizontallyToLocation:30*movementDirection];
	} completion:^(BOOL finished){
		[UIView animateWithDuration:.1 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
			[self moveHorizontallyToLocation:0];
		} completion:^(BOOL finished){
			[UIView animateWithDuration:.12 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
				[self moveHorizontallyToLocation:16*movementDirection];
			} completion:^(BOOL finished){
				[UIView animateWithDuration:.08 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
					[self moveHorizontallyToLocation:0];
				} completion:^(BOOL finished){
					[UIView animateWithDuration:.08 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
						[self moveHorizontallyToLocation:6*movementDirection];
					} completion:^(BOOL finished){
						[UIView animateWithDuration:.06 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
							[self moveHorizontallyToLocation:0];
						} completion:^(BOOL finished){
							if (completion)
								completion();
						}];
					}];
				}];
			}];
		}];
	}];
}

- (void)switchToViewController:(UIViewController *)viewController
		 withSlideOutAnimation:(BOOL)slideOutAnimation
					   popType:(PopType)poptype
				 andCompletion:(void (^)())completion
{
	if (self.avoidSwitchingToSameClassViewController && [self.topViewController isKindOfClass:viewController.class])
	{
		[self closeMenuWithCompletion:completion];
		return;
	}
	
	void (^switchAndCallCompletion)(BOOL) = ^(BOOL closeMenuBeforeCallingCompletion) {
		if (poptype == PopTypeAll) {
			[self setViewControllers:@[viewController]];
		}
		else {
			[super popToRootViewControllerAnimated:NO];
			[super pushViewController:viewController animated:NO];
		}
		
		if (closeMenuBeforeCallingCompletion)
		{
			[self closeMenuWithCompletion:^{
				if (completion)
					completion();
			}];
		}
		else
		{
			if (completion)
				completion();
		}
	};
	
	if ([self isMenuOpen])
	{
		if (slideOutAnimation)
		{
			[UIView animateWithDuration:(slideOutAnimation) ? MENU_SLIDE_ANIMATION_DURATION : 0
								  delay:0
								options:UIViewAnimationOptionCurveEaseOut
							 animations:^{
								 CGFloat width = self.horizontalSize;
								 CGFloat moveLocation = (self.horizontalLocation> 0) ? width : -1*width;
								 [self moveHorizontallyToLocation:moveLocation];
							 } completion:^(BOOL finished) {
								 switchAndCallCompletion(YES);
							 }];
		}
		else
		{
			switchAndCallCompletion(YES);
		}
	}
	else
	{
		switchAndCallCompletion(NO);
	}
}

- (void)switchToViewController:(UIViewController *)viewController withCompletion:(void (^)())completion
{
	[self switchToViewController:viewController withSlideOutAnimation:YES popType:PopTypeRoot andCompletion:completion];
}

- (void)popToRootAndSwitchToViewController:(UIViewController *)viewController
				  withSlideOutAnimation:(BOOL)slideOutAnimation
						  andCompletion:(void (^)())completion
{
	[self switchToViewController:viewController withSlideOutAnimation:slideOutAnimation popType:PopTypeRoot andCompletion:completion];
}

- (void)popToRootAndSwitchToViewController:(UIViewController *)viewController
						 withCompletion:(void (^)())completion
{
	[self switchToViewController:viewController withSlideOutAnimation:YES popType:PopTypeRoot andCompletion:completion];
}

- (void)popAllAndSwitchToViewController:(UIViewController *)viewController
		 withSlideOutAnimation:(BOOL)slideOutAnimation
				 andCompletion:(void (^)())completion
{
	[self switchToViewController:viewController withSlideOutAnimation:slideOutAnimation popType:PopTypeAll andCompletion:completion];
}

- (void)popAllAndSwitchToViewController:(UIViewController *)viewController
						 withCompletion:(void (^)())completion
{
	[self switchToViewController:viewController withSlideOutAnimation:YES popType:PopTypeAll andCompletion:completion];
}

- (void)closeMenuWithCompletion:(void (^)())completion
{
	[self closeMenuWithDuration:MENU_SLIDE_ANIMATION_DURATION andCompletion:completion];
}

- (void)openMenu:(Menu)menu withCompletion:(void (^)())completion
{
	[self openMenu:menu withDuration:MENU_SLIDE_ANIMATION_DURATION andCompletion:completion];
}

- (void)toggleLeftMenu
{
	[self toggleMenu:MenuLeft withCompletion:nil];
}

- (void)pushRightMenu
{
    //[self pushViewController:[[ClosetView alloc] init] animated:YES];
    ((AppDelegate *)[UIApplication sharedApplication].delegate).home_view.isShowRandomPost = YES;
    [self pushViewController:((AppDelegate *)[UIApplication sharedApplication].delegate).right_view animated:YES];
}

- (void)pushHomeViewController
{
    [self pushViewController:((AppDelegate *)[UIApplication sharedApplication].delegate).home_view animated:YES];
}

- (BOOL)isMenuOpen
{
	return (self.horizontalLocation == 0) ? NO : YES;
}

- (void)setEnableShadow:(BOOL)enable
{
	_enableShadow = enable;
	
	if (enable)
	{
		self.view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
		self.view.layer.shadowRadius = MENU_SHADOW_RADIUS;
		self.view.layer.shadowOpacity = MENU_SHADOW_OPACITY;
		self.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
		self.view.layer.shouldRasterize = YES;
		self.view.layer.rasterizationScale = [UIScreen mainScreen].scale;
	}
	else
	{
		self.view.layer.shadowOpacity = 0;
		self.view.layer.shadowRadius = 0;
	}
}

#pragma mark - Override Methods -

- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated
{
	if ([self isMenuOpen])
	{
		[self closeMenuWithCompletion:^{
			[super popToRootViewControllerAnimated:animated];
		}];
	}
	else
	{
		return [super popToRootViewControllerAnimated:animated];
	}

	return nil;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	if ([self isMenuOpen])
	{
		[self closeMenuWithCompletion:^{
			[super pushViewController:viewController animated:animated];
		}];
	}
	else
	{
		[super pushViewController:viewController animated:animated];
	}
}

- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated
{
	if ([self isMenuOpen])
	{
		[self closeMenuWithCompletion:^{
			[super popToViewController:viewController animated:animated];
		}];
	}
	else
	{
		return [super popToViewController:viewController animated:animated];
	}
	
	return nil;
}

#pragma mark - Private Methods -

- (void)updateMenuFrameAndTransformAccordingToOrientation
{
	// Animate rotatation when menu is open and device rotates
	CGAffineTransform transform = self.view.transform;
	self.leftMenu.view.transform = transform;
	self.rightMenu.view.transform = transform;
	
	self.leftMenu.view.frame = [self initialRectForMenu];
	self.rightMenu.view.frame = [self initialRectForMenu];
}

- (void)enableTapGestureToCloseMenu:(BOOL)enable
{
	if (enable)
	{
		if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
			self.interactivePopGestureRecognizer.enabled = NO;
		
		self.topViewController.view.userInteractionEnabled = NO;
		[self.view addGestureRecognizer:self.tapRecognizer];
	}
	else
	{
		if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
			self.interactivePopGestureRecognizer.enabled = YES;
		
		self.topViewController.view.userInteractionEnabled = YES;
		[self.view removeGestureRecognizer:self.tapRecognizer];
	}
}

- (void)toggleMenu:(Menu)menu withCompletion:(void (^)())completion
{
	if ([self isMenuOpen])
		[self closeMenuWithCompletion:completion];
	else
		[self openMenu:menu withCompletion:completion];
}

- (UIBarButtonItem *)barButtonItemForMenu:(Menu)menu
{
	SEL selector = (menu == MenuLeft) ? @selector(leftMenuSelected:) : @selector(righttMenuSelected:);
	UIBarButtonItem *customButton = (menu == MenuLeft) ? self.leftBarButtonItem : self.rightBarButtonItem;
	
	if (customButton)
	{
		customButton.action = selector;
		customButton.target = self;
		return customButton;
	}
	else
	{
		UIImage *image = [UIImage imageNamed:MENU_IMAGE];
        return [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:self action:selector];
	}
}

- (BOOL)shouldDisplayMenu:(Menu)menu forViewController:(UIViewController *)vc
{
	if (menu == MenuRight)
	{
		if ([vc respondsToSelector:@selector(slideNavigationControllerShouldDisplayRightMenu)] &&
			[(UIViewController<SlideNavigationControllerDelegate> *)vc slideNavigationControllerShouldDisplayRightMenu])
		{
			return YES;
		}
	}
	if (menu == MenuLeft)
	{
		if ([vc respondsToSelector:@selector(slideNavigationControllerShouldDisplayLeftMenu)] &&
			[(UIViewController<SlideNavigationControllerDelegate> *)vc slideNavigationControllerShouldDisplayLeftMenu])
		{
			return YES;
		}
	}
	
	return NO;
}

- (void)openMenu:(Menu)menu withDuration:(float)duration andCompletion:(void (^)())completion
{
	[self enableTapGestureToCloseMenu:YES];

	[self prepareMenuForReveal:menu];
	
	[UIView animateWithDuration:duration
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 CGRect rect = self.view.frame;
						 CGFloat width = self.horizontalSize;
						 rect.origin.x = (menu == MenuLeft) ? (width - self.slideOffset) : ((width - self.slideOffset )* -1);
						 [self moveHorizontallyToLocation:rect.origin.x];
					 }
					 completion:^(BOOL finished) {
						 if (completion)
							 completion();
					 }];
}

- (void)closeMenuWithDuration:(float)duration andCompletion:(void (^)())completion
{
	[self enableTapGestureToCloseMenu:NO];
	
	[UIView animateWithDuration:duration
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^{
						 CGRect rect = self.view.frame;
						 rect.origin.x = 0;
						 [self moveHorizontallyToLocation:rect.origin.x];
					 }
					 completion:^(BOOL finished) {
						 if (completion)
							 completion();
					 }];
}

- (void)moveHorizontallyToLocation:(CGFloat)location
{
	CGRect rect = self.view.frame;
	UIInterfaceOrientation orientation = self.interfaceOrientation;
	Menu menu = (self.horizontalLocation >= 0 && location >= 0) ? MenuLeft : MenuRight;
	
	if (UIInterfaceOrientationIsLandscape(orientation))
	{
		rect.origin.x = 0;
		rect.origin.y = (orientation == UIInterfaceOrientationLandscapeRight) ? location : location*-1;
	}
	else
	{
		rect.origin.x = (orientation == UIInterfaceOrientationPortrait) ? location : location*-1;
		rect.origin.y = 0;
	}
	
	self.view.frame = rect;
	[self updateMenuAnimation:menu];
}

- (void)updateMenuAnimation:(Menu)menu
{
	CGFloat progress = (menu == MenuLeft)
		? (self.horizontalLocation / (self.horizontalSize - self.slideOffset))
		: (self.horizontalLocation / ((self.horizontalSize - self.slideOffset) * -1));
	
	[self.menuRevealAnimator animateMenu:menu withProgress:progress];
}

- (CGRect)initialRectForMenu
{
	CGRect rect = self.view.frame;
	rect.origin.x = 0;
	rect.origin.y = 0;
	
	BOOL isIos7 = SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0");
	
	if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
	{
		if (!isIos7)
		{
			// For some reasons in landscape belos the status bar is considered y=0, but in portrait it's considered y=20
			rect.origin.x = (self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) ? 0 : STATUS_BAR_HEIGHT;
			rect.size.width = self.view.frame.size.width-STATUS_BAR_HEIGHT;
		}
	}
	else
	{
		if (!isIos7)
		{
			// For some reasons in landscape belos the status bar is considered y=0, but in portrait it's considered y=20
			rect.origin.y = (self.interfaceOrientation == UIInterfaceOrientationPortrait) ? STATUS_BAR_HEIGHT : 0;
			rect.size.height = self.view.frame.size.height-STATUS_BAR_HEIGHT;
		}
	}
	
	return rect;
}

- (void)prepareMenuForReveal:(Menu)menu
{
	// Only prepare menu if it has changed (ex: from MenuLeft to MenuRight or vice versa)
    if (self.lastRevealedMenu && menu == self.lastRevealedMenu)
        return;
    
    UIViewController *menuViewController = (menu == MenuLeft) ? self.leftMenu : self.rightMenu;
	UIViewController *removingMenuViewController = (menu == MenuLeft) ? self.rightMenu : self.leftMenu;

    self.lastRevealedMenu = menu;
	
	[removingMenuViewController.view removeFromSuperview];
	[self.view.window insertSubview:menuViewController.view atIndex:0];

	[self updateMenuFrameAndTransformAccordingToOrientation];
	
	[self.menuRevealAnimator prepareMenuForAnimation:menu];
}

- (CGFloat)horizontalLocation
{
	CGRect rect = self.view.frame;
	UIInterfaceOrientation orientation = self.interfaceOrientation;
	
	if (UIInterfaceOrientationIsLandscape(orientation))
	{
		return (orientation == UIInterfaceOrientationLandscapeRight)
			? rect.origin.y
			: rect.origin.y*-1;
	}
	else
	{
		return (orientation == UIInterfaceOrientationPortrait)
			? rect.origin.x
			: rect.origin.x*-1;
	}
}

- (CGFloat)horizontalSize
{
	CGRect rect = self.view.frame;
	UIInterfaceOrientation orientation = self.interfaceOrientation;
	
	if (UIInterfaceOrientationIsLandscape(orientation))
	{
		return rect.size.height;
	}
	else
	{
		return rect.size.width;
	}
}

#pragma mark - UINavigationControllerDelegate Methods -

- (void)navigationController:(UINavigationController *)navigationController
	  willShowViewController:(UIViewController *)viewController
					animated:(BOOL)animated
{
	if ([self shouldDisplayMenu:MenuLeft forViewController:viewController])
		viewController.navigationItem.leftBarButtonItem = [self barButtonItemForMenu:MenuLeft];
	
	if ([self shouldDisplayMenu:MenuRight forViewController:viewController])
		viewController.navigationItem.rightBarButtonItem = [self barButtonItemForMenu:MenuRight];
}

- (CGFloat)slideOffset
{
	return (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
		? self.landscapeSlideOffset
		: self.portraitSlideOffset;
}

#pragma mark - IBActions -

- (void)leftMenuSelected:(id)sender
{
	if ([self isMenuOpen])
		[self closeMenuWithCompletion:nil];
	else
		[self openMenu:MenuLeft withCompletion:nil];
		
}

- (void)righttMenuSelected:(id)sender
{
	if ([self isMenuOpen])
		[self closeMenuWithCompletion:nil];
	else
		[self openMenu:MenuRight withCompletion:nil];
}

#pragma mark - Gesture Recognizing -

- (void)tapDetected:(UITapGestureRecognizer *)tapRecognizer
{
	[self closeMenuWithCompletion:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
	if (self.panGestureSideOffset == 0)
		return YES;
	
	CGPoint pointInView = [touch locationInView:self.view];
	CGFloat horizontalSize = [self horizontalSize];
	
	return (pointInView.x <= self.panGestureSideOffset || pointInView.x >= horizontalSize - self.panGestureSideOffset)
		? YES
		: NO;
}

- (void)panDetected:(UIPanGestureRecognizer *)aPanRecognizer
{
    return;
    
	CGPoint translation = [aPanRecognizer translationInView:aPanRecognizer.view];
    CGPoint velocity = [aPanRecognizer velocityInView:aPanRecognizer.view];
	NSInteger movement = translation.x - self.draggingPoint.x;
	
    Menu currentMenu;
    
    if (self.horizontalLocation > 0)
        currentMenu = MenuLeft;
    else if (self.horizontalLocation < 0)
        currentMenu = MenuRight;
    else
        currentMenu = (translation.x > 0) ? MenuLeft : MenuRight;
    
    [self prepareMenuForReveal:currentMenu];
    
    if (aPanRecognizer.state == UIGestureRecognizerStateBegan)
	{
		self.draggingPoint = translation;
    }
	else if (aPanRecognizer.state == UIGestureRecognizerStateChanged)
	{
		static CGFloat lastHorizontalLocation = 0;
		CGFloat newHorizontalLocation = [self horizontalLocation];
		lastHorizontalLocation = newHorizontalLocation;
		newHorizontalLocation += movement;
		
		if (newHorizontalLocation >= self.minXForDragging && newHorizontalLocation <= self.maxXForDragging)
			[self moveHorizontallyToLocation:newHorizontalLocation];
		
		self.draggingPoint = translation;
	}
	else if (aPanRecognizer.state == UIGestureRecognizerStateEnded)
	{
        NSInteger currentX = [self horizontalLocation];
		NSInteger currentXOffset = (currentX > 0) ? currentX : currentX * -1;
		NSInteger positiveVelocity = (velocity.x > 0) ? velocity.x : velocity.x * -1;
		
		// If the speed is high enough follow direction
		if (positiveVelocity >= MENU_FAST_VELOCITY_FOR_SWIPE_FOLLOW_DIRECTION)
		{
			Menu menu = (velocity.x > 0) ? MenuLeft : MenuRight;
			
			// Moving Right
			if (velocity.x > 0)
			{
				if (currentX > 0)
				{
					if ([self shouldDisplayMenu:menu forViewController:self.visibleViewController])
						[self openMenu:(velocity.x > 0) ? MenuLeft : MenuRight withDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
				}
				else
				{
					[self closeMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
				}
			}
			// Moving Left
			else
			{
				if (currentX > 0)
				{
					[self closeMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
				}
				else
				{
					if ([self shouldDisplayMenu:menu forViewController:self.visibleViewController])
						[self openMenu:(velocity.x > 0) ? MenuLeft : MenuRight withDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
				}
			}
		}
		else
		{
			if (currentXOffset < (self.horizontalSize - self.slideOffset)/2)
				[self closeMenuWithCompletion:nil];
			else
				[self openMenu:(currentX > 0) ? MenuLeft : MenuRight withCompletion:nil];
		}
    }
}

- (NSInteger)minXForDragging
{
	if ([self shouldDisplayMenu:MenuRight forViewController:self.topViewController])
	{
		return (self.horizontalSize - self.slideOffset)  * -1;
	}
	
	return 0;
}

- (NSInteger)maxXForDragging
{
	if ([self shouldDisplayMenu:MenuLeft forViewController:self.topViewController])
	{
		return self.horizontalSize - self.slideOffset;
	}
	
	return 0;
}

#pragma mark - Setter & Getter -

- (UITapGestureRecognizer *)tapRecognizer
{
	if (!_tapRecognizer)
	{
		_tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
	}
	
	return _tapRecognizer;
}

- (UIPanGestureRecognizer *)panRecognizer
{
	if (!_panRecognizer)
	{
		_panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
		_panRecognizer.delegate = self;
	}
	
	return _panRecognizer;
}

- (void)setEnableSwipeGesture:(BOOL)markEnableSwipeGesture
{
	_enableSwipeGesture = markEnableSwipeGesture;
	
	if (_enableSwipeGesture)
	{
		[self.view addGestureRecognizer:self.panRecognizer];
	}
	else
	{
		[self.view removeGestureRecognizer:self.panRecognizer];
	}
}

- (void)setMenuRevealAnimator:(id<SlideNavigationContorllerAnimator>)menuRevealAnimator
{
	[self.menuRevealAnimator clear];
	
	_menuRevealAnimator = menuRevealAnimator;
}
-(void)present_outfit_creator
{
    [self closeMenuWithCompletion:nil];
    
    CreateOutfitView * to_present = [[CreateOutfitView alloc] init];
    [self presentViewController:to_present animated:YES completion:nil];

}
-(void)present_photo_details:(PhotoDetailsView*)incoming
{
    [self closeMenuWithCompletion:nil];
    
    [self presentViewController:incoming animated:YES completion:nil];
}
-(void)remove_outfit_creator
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)post_submitted:(NSMutableDictionary*)message
{
    NSMutableDictionary * post_info = message;
    
    just_posted_photo_a = [post_info objectForKey:@"photo_a"];
    just_posted_photo_b = [post_info objectForKey:@"photo_b"];
    just_posted_post_id = [post_info objectForKey:@"post_id"];
    
    UIViewController * popin = [[UIViewController alloc] init];
    popin.view.backgroundColor = [self colorWithHexString:@"9ad325"];
    [popin setPreferedPopinContentSize:CGSizeMake(280.0, 240.0)];
    
    UILabel * post_submitted = [[UILabel alloc] initWithFrame:CGRectMake(-20, 0, popin.view.frame.size.width, popin.view.frame.size.height*.1)];
    post_submitted.text = @"You have created a post";
    post_submitted.font =[UIFont fontWithName:@"Neutraface Text" size:20];
    post_submitted.textColor = [UIColor whiteColor];
    post_submitted.textAlignment = NSTextAlignmentCenter;
    [popin.view addSubview:post_submitted];
    
    UIImageView * check_mark = [[UIImageView alloc] initWithFrame:CGRectMake(105, 45, 71, 70)];
    check_mark.image = [UIImage imageNamed:@"check_mark.png"];
    [popin.view addSubview:check_mark];
    
    UIImageView * line_mark = [[UIImageView alloc] initWithFrame:CGRectMake(65, 120, 293/2, 8/2)];
    line_mark.image = [UIImage imageNamed:@"post_dots.png"];
    [popin.view addSubview:line_mark];
    
    UILabel * share_prompt = [[UILabel alloc] initWithFrame:CGRectMake(-20, 130, popin.view.frame.size.width, 22)];
    share_prompt.text = @"Share it with your friends:";
    share_prompt.font =[UIFont fontWithName:@"Neutraface Text" size:20];
    share_prompt.textColor = [UIColor whiteColor];
    share_prompt.textAlignment = NSTextAlignmentCenter;
    [popin.view addSubview:share_prompt];
    
    UIButton * text_button  = [[UIButton alloc] initWithFrame:CGRectMake(20, 170, 134/2.1, 76/2.1)];
    [text_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [text_button setTitle:@"Text" forState:UIControlStateNormal];
    [[text_button layer] setBorderWidth:1.0f];
    [[text_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    text_button.layer.cornerRadius = 2;
    text_button.clipsToBounds = YES;
    [text_button addTarget:self action:@selector(open_text_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: text_button];
    
    UIButton * email_button  = [[UIButton alloc] initWithFrame:CGRectMake(88, 170, 143/2.1, 76/2.1)];
    [email_button setTitle:@"Email" forState:UIControlStateNormal];
    [[email_button layer] setBorderWidth:1.0f];
    [[email_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    email_button.layer.cornerRadius = 2;
    email_button.clipsToBounds = YES;
    [email_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [email_button addTarget:self action:@selector(open_email_message) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: email_button];
    
    UIButton * facebook_button  = [[UIButton alloc] initWithFrame:CGRectMake(160, 170, 200/2.0, 76/2.1)];
    [facebook_button setTitle:@"Facebook" forState:UIControlStateNormal];
    [[facebook_button layer] setBorderWidth:1.0f];
    [[facebook_button layer] setBorderColor:[UIColor whiteColor].CGColor];
    facebook_button.layer.cornerRadius = 2;
    facebook_button.clipsToBounds = YES;
    [facebook_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [facebook_button addTarget:self action:@selector(share_facebook) forControlEvents:UIControlEventTouchUpInside];
    [popin.view addSubview: facebook_button];
    
    [popin setPopinTransitionStyle:2];
    
    BKTBlurParameters *blurParameters = [BKTBlurParameters new];
    blurParameters.alpha = 1.0f;
    blurParameters.radius = 8.0f;
    blurParameters.saturationDeltaFactor = 1.8f;
    blurParameters.tintColor = [UIColor colorWithRed:0.966 green:0.851 blue:0.038 alpha:0.2];
    [popin setBlurParameters:blurParameters];
    
    //Add option for a blurry background
    [popin setPopinOptions:[popin popinOptions] |BKTPopinBlurryDimmingView];
    
    
    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    [self presentPopinController:popin animated:YES completion:^{
        //NSLog(@"Popin presented !");
    }];
    
    [Flurry logEvent:@"a post created"];
    
    
}
-(void)share_facebook
{
    [Flurry logEvent:@"share facebook home screen"];
    
    NSNumber *post_id = just_posted_post_id;
    
    UIImageView * photo_a_image = just_posted_photo_a;
    UIImageView * photo_b_image = just_posted_photo_b;
    
    /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
    photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
    
    UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
    [to_render addSubview:photo_a_image];
    [to_render addSubview:photo_b_image];
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
    [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
    UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData * image_data = UIImageJPEGRepresentation(image, .5);*/
    
    NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
    NSString * base64 = [image_data base64EncodedStringWithOptions:0];
    
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@/utility/uploadImage64/", SERVICE_URL]];
    
    NSData *postData= [[NSString stringWithFormat:@"image=%@", base64] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(error)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSLog(@"sendAsynchronousRequest:error:%@", [error localizedDescription]);
                 
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
                 alert.tag = 1;
                 //[alert show];
             });
         }
         else if (!data)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSLog(@"SlideNC:/utility/uploadImage64/: response data is nil");
             });
         }
         else
         {
             //NSString * test = [[NSString alloc] initWithData:data encoding:NSStringEncodingConversionAllowLossy];
             
             //NSLog(@"data back from pic upload = %@" ,test);
             
             NSDictionary * response_dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             
             //NSLog(@"upload = %@", response_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSString * status = [response_dict objectForKey:@"status"];
                 NSString * message = [response_dict objectForKey:@"msg"];
                 
                 if ([status isEqualToString:@"success"])
                 {
                     [self share_to_facebook:post_id :[response_dict objectForKey:@"link"]];
                 }
                 else if([status isEqualToString:@"fail"])
                 {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc]
                                           initWithTitle:@"Error"
                                           message:message
                                           delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
                     
                     alert.tag = 1;
                     [alert show];
                     
                 }
                 
             });
         }
         
     }];
    
}
-(void)share_to_facebook:(NSNumber*)post_id :(NSString*)link
{
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound)
    {
        // permission does not exist
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                  {
                      if (error)
                      {
                          NSLog(@"error:%@",error);
                      }
                      else
                      {
                          
                          NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                         @"Help me pick an outfit!", @"name",
                                                         @"Which outfit do you prefer?", @"description",
                                                         [NSString stringWithFormat:@"http://pickfitapp.com/postVote//%@/",post_id], @"link",
                                                         link, @"picture",
                                                         nil];
                          
                          // Make the request
                          [FBRequestConnection startWithGraphPath:@"/me/feed"
                                                       parameters:params
                                                       HTTPMethod:@"POST"
                                                completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                                    if (!error)
                                                    {
                                                        // Link posted successfully to Facebook
                                                        NSLog(@"result: %@", result);
                                                        
                                                    }
                                                    else
                                                    {
                                                        // An error occurred, we need to handle the error
                                                        // See: https://developers.facebook.com/docs/ios/errors
                                                        NSLog(@"%@", error.description);
                                                    }
                                                    
                                                    
                                                }];
                          
                      }
                      
                  }];
             }
             
         } ];
        
        
    }
    else
    {
        // permission exists
        
        
        // If the Facebook app is installed and we can present the share dialog
        
        // Present the share dialog
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",@"public_profile",@"publish_actions",
                                nil];
        
        [FBSession openActiveSessionWithReadPermissions:permissions
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error)
         
         {
             if (state == FBSessionStateOpen)
             {
                 
                 NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                @"Help me pick an outfit!", @"name",
                                                @"Which outfit do you prefer?", @"description",
                                                [NSString stringWithFormat:@"http://pickfitapp.com/postVote//%@/",post_id], @"link",
                                                link, @"picture",
                                                nil];
                 
                 // Make the request
                 [FBRequestConnection startWithGraphPath:@"/me/feed"
                                              parameters:params
                                              HTTPMethod:@"POST"
                                       completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          // Link posted successfully to Facebook
                          NSLog(@"result: %@", result);
                          
                      }
                      else
                      {
                          // An error occurred, we need to handle the error
                          // See: https://developers.facebook.com/docs/ios/errors
                          NSLog(@"%@", error.description);
                      }
                      
                      
                  }];
                 
             }
             
         }];
        
    }
}
-(void)open_text_message
{
    //[self.navigationController dismissCurrentPopinControllerAnimated:YES];
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.body =  [NSString stringWithFormat:@"Help me pick an outfit!  Which outfit do you prefer? %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", just_posted_post_id]];
        controller.messageComposeDelegate = self;
        
        UIImageView * photo_a_image = just_posted_photo_a;
        UIImageView * photo_b_image = just_posted_photo_b;
        
        /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
        photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
        
        UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        [to_render addSubview:photo_a_image];
        [to_render addSubview:photo_b_image];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData * image_data = UIImagePNGRepresentation(image);*/
        
        NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
        [controller addAttachmentData:image_data typeIdentifier:@"public.content" filename:@"image.png"];
        [self presentViewController:controller animated:YES completion:nil];
        [Flurry logEvent:@"share text home screen"];
    }
}
-(void)open_email_message
{
    //[self.navigationController dismissCurrentPopinControllerAnimated:YES];
    [self dismissCurrentPopinControllerAnimated:YES];
    
    if([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;
        
        [mailCont setSubject:@"Help me choose an outfit!"];
        [mailCont setMessageBody: [NSString stringWithFormat:@"Help me pick an outfit!  Which outfit do you prefer? %@", [NSString stringWithFormat:@"http://pickfitapp.com/postVote/%@/", just_posted_post_id]] isHTML:NO];
        
        UIImageView * photo_a_image = just_posted_photo_a;
        UIImageView * photo_b_image = just_posted_photo_b;
        
        /*photo_a_image.frame = CGRectMake(0, 0, 261/2, 720/2);
        photo_b_image.frame = CGRectMake(261/2, 0, 261/2, 720/2);
        
        UIView * to_render = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
        [to_render addSubview:photo_a_image];
        [to_render addSubview:photo_b_image];
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(261, 200), NO, [UIScreen mainScreen].scale);
        [to_render drawViewHierarchyInRect:CGRectMake(0, -50, to_render.bounds.size.width, to_render.bounds.size.height) afterScreenUpdates:YES];
        UIImage *  image = UIGraphicsGetImageFromCurrentImageContext();
        
        NSData * image_data = UIImagePNGRepresentation(image);*/
        
        NSData *image_data = UIImageJPEGRepresentation([[SlideNavigationController sharedInstance] createImageToShareWithLeftImage:photo_a_image.image andRightImage:photo_b_image.image], 0.5);
        [mailCont addAttachmentData:image_data mimeType:@"image/jpeg" fileName:[NSString stringWithFormat:@"a.jpg"]];
        UIGraphicsEndImageContext();
        
        [self presentViewController:mailCont animated:YES completion:nil];
        
        [Flurry logEvent:@"share email home screen"];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"We do not have access to your mail app. Please set up your email app in order to send a post via email." delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        alert.tag = 1;
        [alert show];
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch(result)
    {
        case MessageComposeResultCancelled:
            // user canceled sms
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultSent:
            // user sent sms
            //perhaps put an alert here and dismiss the view on one of the alerts buttons
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:
            // sms send failed
            //perhaps put an alert here and dismiss the view when the alert is canceled
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        default:
            break;
    }
    
}
- (UIColor *) colorWithHexString: (NSString *) hexString
{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length])
    {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
- (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (UIImage *)createImageToShareWithLeftImage:(UIImage *)leftImage andRightImage:(UIImage *)rightImage
{
    CGFloat imageWidth = 261.0 / 2;
    CGFloat imageHeight = 360.0;
    
    UIImage *image = nil;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imageWidth * 2, imageHeight), NO, [UIScreen mainScreen].scale);
    [leftImage drawInRect:CGRectMake(0, 0, imageWidth, imageHeight)];
    [rightImage drawInRect:CGRectMake(imageWidth, 0, imageWidth, imageHeight)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
